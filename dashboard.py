# --*-- coding: utf-8 --*--
"""

This file was generated with the customdashboard management command, it
contains the two classes for the main dashboard and app index dashboard.
You can customize these classes as you want.

To activate your index dashboard add the following to your settings.py::
    ADMIN_TOOLS_INDEX_DASHBOARD = 'gone.dashboard.CustomIndexDashboard'

And to activate the app index dashboard::
    ADMIN_TOOLS_APP_INDEX_DASHBOARD = 'gone.dashboard.CustomAppIndexDashboard'
"""

from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse

from admin_tools.dashboard import modules, Dashboard, AppIndexDashboard
from admin_tools.utils import get_admin_site_name


class CustomIndexDashboard(Dashboard):
    """
    Custom index dashboard for gone.
    """
    def __init__(self, **kwargs):
        Dashboard.__init__(self, **kwargs)
        self.children.append(
            modules.ModelList(
                title = u'Основной контент',
                models=(
                    'main.models.Article',
                    ''
                ),
                )
        )
        self.children.append(
            modules.ModelList(
                title = u'Упоминаемый обьек',
                models=(
                    'main.models.MentionsObject',
                    'main.models.Speaker',
                    'main.models.SideSpeaker',
                ),
                )
        )
        self.children.append(
            modules.ModelList(
                title = u'KM',
                models=(
                    'main.models.KM',
                    'main.models.KMMention',
                    'main.models.KMKeyValue',
                ),
                )
        )
        self.children.append(
            modules.ModelList(
                title = u'Продукты',
                models=(
                   'main.models.Product',
                   'main.models.MentionProduct',
                   'main.models.MentionProductValue',
                ),
                )
        )
        self.children.append(
            modules.ModelList(
                title = u'Тех-поддержка',
                models=(
                    'support.models.Support_Category',
                    'support.models.Support',
                    'qa.models.Question',
                ),
                )
        )
        self.children.append(
            modules.ModelList(
                title = u'Трекеры',
                models=(
                    'tracker.models.Tracker',
                    'tracker.models.BasicFunctions',
                ),
                )
        )

        self.children.append(
                modules.ModelList(
                    title = u'Заявки',
                    models=(
                        'feedback.models.FeedBack',
                        'feedback.models.Services',
                        'support_service.models.SupportServices'
                    ),
                    )
            )
        # self.children.append(
        #     modules.ModelList(
        #         title = u'Коментарии и Контакты',
        #         models=(
        #             'contacts.models.Contacts',
        #             'comments.models.Coment',
        #             'comments.models.Coment_news',
        #             'comments.models.Coment_sale',
        #             'comments.models.Coment_example',
        #             'comments.models.Coment_video',
        #         ),
        #         )
        # )
    def init_with_context(self, context):
        site_name = get_admin_site_name(context)
        # append a link list module for "quick links"
        self.children.append(modules.LinkList(
            _('Quick links'),
            layout='inline',
            draggable=False,
            deletable=False,
            collapsible=False,
            children=[
                [_('Return to site'), '/'],
                [_('Change password'),
                 reverse('%s:password_change' % site_name)],
                [_('Log out'), reverse('%s:logout' % site_name)],
                ]
        ))

        # append an app list module for "Applications"


        # append an app list module for "Administration"

        # append a recent actions module
        self.children.append(modules.RecentActions(_('Recent Actions'), 5))

        # append a feed module


        # append another link list module for "support".



