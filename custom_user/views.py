# --*-- coding: utf-8 --*--
import json
import urllib
import urllib2
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import PasswordChangeForm
from django.core.exceptions import PermissionDenied
from django.http import HttpResponseRedirect, HttpResponse

from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login
# Create your views here.
from django.template.response import TemplateResponse
from custom_user.form import CustomUserForm, ProfileFrom, ChangePhoto
from custom_user.models import CustomUser, Company, Social, ProductRating, UserAplicationSettings
from django.contrib.auth.models import Group, User
from django.core.mail import send_mail
from django.views.generic import View, DeleteView
from django.db.models import Count
from braces.views import JSONResponseMixin, JsonRequestResponseMixin, CsrfExemptMixin
from cStringIO import StringIO
from info.settings import MEDIA_ROOT
import xlwt
import datetime

from monitoring_product.models import MonitoringProduct

decorator_with_arguments = lambda decorator: lambda *args, **kwargs: lambda func: decorator(func, *args, **kwargs)
@decorator_with_arguments
def custom_permission_required(function, perm):
    def _function(request, *args, **kwargs):
        if request.user.has_perm(perm):
            return function(request, *args, **kwargs)
        else:
            return redirect ('/')
            # Return a response or redirect to referrer or some page of your choice
    return _function


def custom_login(request):
    redirect_to = request.REQUEST.get('next', '')
    print redirect_to, 'redirect_to'
    error = False
    if request.POST:
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                return redirect(redirect_to)
            else:
                error = u'Аккаунт с таким именем не существует'
        else:
            error = u'Неверный логин или пароль'
    return TemplateResponse(request, 'login.html', locals())



@login_required(login_url='/login/')
@custom_permission_required('custom_user.can_change_self_profile', )
def profile(request):
    user = CustomUser.objects.get(id = request.user.id)
    if request.method == "POST":
        data= json.loads(request.body)
        for key, val in data.items():
            if key == 'password1':
                user.set_password(data['password1'])
            if key == 'group':
                for group in user.groups.all():
                    user.groups.remove(group)
                user.groups.add(Group.objects.get(id=int(val)))
            else:
                print key, val, '------'
                setattr(user, key, val)
            user.save()
            return HttpResponse(json.dumps({
                'status': 'OK',
            }))
    social_list = user.social_set.all().values()
    social_list = json.dumps(list(social_list))
    company = Company.objects.get(id= user.company.id)
    group = user.groups.all().first()
    departament_list = company.department_set.all().values()
    departament_list = json.dumps(list(departament_list))
    groups_list = Group.objects.all().values('name', 'id')
    group_name = Group.objects.all().first().name
    groups_list = json.dumps(list(groups_list))
    return TemplateResponse(request, 'profile.html', locals())


@login_required(login_url='/login/')
@custom_permission_required('custom_user.can_change_self_profile', )
def update_profile(request, id=None):
    if id is None:
        user = request.user
    else:
        user = CustomUser.objects.get(id = id)
        if not request.user.has_perm('cusrom_user.can_change_all_profile'):
            raise PermissionDenied

    if request.method == "POST":
        data= json.loads(request.body)
        for key, val in data.items():
            if key == 'department':
                key = 'department_id'
            if key == 'password1':
                user.set_password(data['password1'])
            if key == 'group':
                for group in user.groups.all():
                    user.groups.remove(group)
                user.groups.add(Group.objects.get(id=int(val)))
            else:
                print key, val, '------'
                setattr(user, key, val)
            user.save()
            return HttpResponse(json.dumps({
                'status': 'OK',
            }))

@login_required(login_url='/login/')
@custom_permission_required('custom_user.can_change_self_profile', )
def profile_api(request):
    user = CustomUser.objects.get(id = request.user.id)
    if request.method == "POST":
        data= json.loads(request.body)
        for key, val in data.items():
            if key == 'password1':
                user.set_password(data['password1'])
            if key == 'group':
                for group in user.groups.all():
                    user.groups.remove(group)
                user.groups.add(Group.objects.get(id=int(val)))
            else:
                print key, val, '------'
                setattr(user, key, val)
            user.save()
            return HttpResponse(json.dumps({
                'status': 'OK',
            }))
    social_list = list(user.social_set.all().values())
    company = Company.objects.prefetch_related('department_set', 'customuser_set', 'customuser_set__groups',
                                               'customuser_set__department').get(id=user.company.id)
    groups_list = list(Group.objects.all().values('name', 'id'))
    departments = list(
        company.department_set.annotate(num_users=Count('customuser')).all().values('id', 'title', 'num_users'))
    return HttpResponse(json.dumps({
        "social_list": social_list,
        "user": user.to_dict(),
        "company": company.to_dict(),
        "groups_list": groups_list,
        "departments_list": departments,
    }))

@login_required(login_url='/login/')
@custom_permission_required('custom_user.can_change_all_profile', )
def delete_user(request, id):
    if request.method == "POST":
        user = CustomUser.objects.get(id = id)
        user.delete()
        return HttpResponse(json.dumps({
            'status': 'OK',
        }))
    else:
        raise PermissionDenied


def add_social(request):
    if request.method == "POST":
        data= json.loads(request.body)
        social = Social.objects.create(custom_user = request.user, title = data['title_social'], link = data['name_social'])
        return HttpResponse(json.dumps({
            'status': 'OK',
            'id': social.id
        }))

def delete_social(request, id):
    social = Social.objects.get(id=id)
    social.delete()
    return HttpResponse(json.dumps({
        'status': 'OK',
    }))


def password_change(request, id):
    user = CustomUser.objects.get(id = id)
    if request.method == 'POST':
        data= json.loads(request.body)
        user.set_password(data['password1'])
    return HttpResponse(json.dumps({
        'status': 'OK',
    }))
import time

def change_photo(request, id):
    user = CustomUser.objects.get(id = id)

    if request.method == "POST":
        user.photo = request.FILES['file']
        save_file(request.FILES['file'])
        user.save()

        return HttpResponse(json.dumps({
            'status': 'OK',
            'src': user.photo.url,
        }))


def save_file(file, path=''):
    ''' Little helper to save a file
    '''
    filename = file._get_name()
    fd = open('%s/%s' % (MEDIA_ROOT, str(path) + str(filename.encode('utf-8'))), 'wb')
    for chunk in file.chunks():
        fd.write(chunk)
    fd.close()

def vote(request, pk):
    if request.method == "POST":
        data = json.loads(request.body)
        try:
            rating = request.user.product_ratings.get(product_id=pk)
            rating.quality = data['rating']
            rating.term = data['term']
            rating.save()
        except ProductRating.DoesNotExist:
            rating = ProductRating.objects.create(user_id=request.user.pk,
                                                  product_id=pk,
                                                  quality=data['rating'],
                                                  term=data['term']
                                                  )
        name = request.user.get_full_name()
        role = request.user.groups.first().name
        msg = u"Пользователь {0}({1}) оценил Мониторинговый продукт {2}\r\n" \
              u"Качество - {3}\r\n" \
              u"Срок - {4}\r\n" \
              u"Время - {5}".format(name, role, rating.product.title, rating.quality, rating.term, rating.time)
        send_mail(u"Exllibris. Проект получил оценку", msg, "aem@exlibris.ru", ('aem@exlibris.ru', ))
        return HttpResponse(json.dumps({
                'status': 'OK',
                }))


class UserSettingsView(JSONResponseMixin, View):

    def get(self, request, *args, **kwargs):
        settings = UserAplicationSettings.objects.get_or_create(user=request.user)[0].settings
        return self.render_json_response(settings)

get_user_settings = UserSettingsView.as_view()


class SetSettingsView(JsonRequestResponseMixin, View):
    require_json = True

    def post(self, request, *args, **kwargs):
        settings, created = UserAplicationSettings.objects.get_or_create(user=request.user)
        settings.settings.update(self.request_json.get('settings', {}))
        settings.save()
        return self.render_json_response(settings.settings)

set_user_settings = SetSettingsView.as_view()


@login_required(login_url='/login/')
def make_votes_report(request, pk):
    if request.method == "POST":
        votes_from = datetime.datetime.strptime(request.POST.get('votes_from', '1900-01-01'), "%Y-%m-%d")
        votes_to = datetime.datetime.strptime(request.POST.get('votes_to', '2050-01-01'), "%Y-%m-%d")+datetime.timedelta(0, 60*(60*23+59), 0)
        votes = ProductRating.objects.filter(user_id=pk,
                                             time__gte=votes_from,
                                             time__lte=votes_to).select_related('user').prefetch_related('user__groups')

        response = HttpResponse(content_type='application/vnd.ms-excel')
        response['Content-Disposition'] = 'attachment; filename="Votes_Report.xls"'
        buf = StringIO()

        document = xlwt.Workbook()
        ws = document.add_sheet(u'Лист1')

        ws.write(0, 0, u'Пользователь')
        ws.write(0, 1, u'Роль')
        ws.write(0, 2, u'Категория')
        ws.write(0, 3, u'Оценка')
        ws.write(0, 4, u'Дата')
        ws.write(0, 5, u'Время')

        for y, vote in enumerate(votes, 1):
            ws.write(y*2-1, 0, vote.user.get_full_name())
            ws.write(y*2, 0, vote.user.get_full_name())
            ws.write(y*2-1, 1, vote.user.groups.first().name)
            ws.write(y*2, 1, vote.user.groups.first().name)
            ws.write(y*2-1, 2, u'Качество')
            ws.write(y*2, 2, u'Срок')
            ws.write(y*2-1, 3, vote.quality)
            ws.write(y*2, 3, vote.term)
            ws.write(y*2-1, 4, str(vote.time.date()))
            ws.write(y*2, 4, str(vote.time.date()))
            ws.write(y*2-1, 5, str(vote.time.strftime("%H:%M")))
            ws.write(y*2, 5, str(vote.time.strftime("%H:%M")))

        document.save(buf)
        response.write(buf.getvalue())
        buf.close()
        return response


class GetUsersAccessView(JSONResponseMixin, View):
    require_json = True

    def get(self, request, *args, **kwargs):
        context = {}
        context['departments'] = list(request.user.company.get_department_list().values('id', 'title'))
        context['users'] = map(lambda x: {'name': x.get_full_name(), 'id': x.pk, 'department': x.department_id},
                               request.user.company.get_user_list().exclude(pk=request.user.pk))
        users_with_permissions = map(lambda x: {'name': x.get_full_name(), 'id': x.pk, 'department': x.department_id},
                               MonitoringProduct.objects.get(pk=self.kwargs.get('pk')).users_with_perm.all().exclude(pk=request.user.pk))

        for u in context['users']:
            u['selected'] = True if u in users_with_permissions else False

        context['available_only_for_me'] = not len(users_with_permissions)

        return self.render_json_response(context)

get_users_access = GetUsersAccessView.as_view()


class SetUsersAccessView(JsonRequestResponseMixin, View):
    require_json = True

    def post(self, request, *args, **kwargs):

        if not request.user.has_perm('cusrom_user.can_change_product'):
            raise PermissionDenied

        product = MonitoringProduct.objects.get(pk=self.kwargs.get('pk'))
        users = self.request_json.get('users', []) # get array of users id
        if not product.users_with_perm.filter(pk=request.user.pk) and not request.user.groups.filter(name__in=["SuperAdministrator", "Administrator"]):
            raise PermissionDenied

        product.users_with_perm.clear()
        product.users_with_perm.add(request.user.pk)
        for u in users:
            product.users_with_perm.add(u)

        return self.render_json_response({'status': 'Ok'})

set_users_access = SetUsersAccessView.as_view()