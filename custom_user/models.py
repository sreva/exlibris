# --*-- coding: utf-8 --*--

from django.db import models
from django.contrib.auth.models import User, UserManager
from jsonfield import JSONField
import json

# Create your models here.



class Department(models.Model):
    title = models.CharField(max_length=200, verbose_name=u'Название', blank=True, null=True)
    company = models.ForeignKey('Company', verbose_name=u'Компания')

    class Meta:
        verbose_name = u'подразделение'
        verbose_name_plural = u'Подразделения'

    def __unicode__(self):
        return self.title


    def get_user_count(self):
        return self.customuser_set.all().count()

class Company(models.Model):
    title = models.CharField(max_length=200, verbose_name=u'Название', blank=True, null=True)
    city = models.CharField(max_length=100, verbose_name=u'Город')
    site = models.CharField(max_length=100, verbose_name=u'Сайт')
    social = models.CharField(max_length=100, verbose_name=u'Социальная сеть')
    icon = models.ImageField(upload_to='upload_to/', verbose_name=u'Логотип')
    class Meta:
        verbose_name = u'Компанию'
        verbose_name_plural = u'Компании'

    def to_dict(self):
        try:
            icon = self.icon.url
        except:
            icon = None
        return {
            'title': self.title,
            'city': self.city,
            'site': self.site,
            'social': self.social,
            'icon': icon,
            'users': map(lambda x: x.to_dict(), self.customuser_set.all()),
        }

    def __unicode__(self):
        return self.title

    def get_department_list(self):
        return self.department_set.all()


    def get_user_list(self):
        return self.customuser_set.all()


class CustomUser(User):
    skype = models.CharField(max_length=100, verbose_name=u'Skype', blank=True, null=True)
    position = models.CharField(max_length=200, verbose_name=u'Должность', blank=True, null=True)
    phone = models.CharField(max_length=200, verbose_name=u'Телефон', blank=True, null=True)
    department = models.ForeignKey(Department, verbose_name=u'Подразделение', blank=True, null=True)
    photo = models.ImageField(upload_to='upload_to/', verbose_name=u'Фото', blank=True, null=True)
    company = models.ForeignKey(Company, verbose_name=u'компания',  blank=True, null=True)
    project = models.ManyToManyField('main.Project', verbose_name=u'Проекты',  blank=True, null=True)
    ratings = models.ManyToManyField('monitoring_product.MonitoringProduct', verbose_name="Оценки", through='ProductRating')
    access_to_products = models.ManyToManyField('monitoring_product.MonitoringProduct', blank=True, null=True, verbose_name=u"Доступ к продуктам", related_name='users_with_perm')
    objects = UserManager()

    def get_full_name(self):
        full_name = ' '.join((self.first_name, self.last_name)).strip() or self.username.strip()
        return full_name

    def to_dict(self):
        try:
            photo = self.photo.url
        except:
            photo = None
        return {
            'photo': photo,
            'first_name': self.first_name,
            'last_name': self.last_name,
            'position': self.position,
            'department': {"title": self.department.title, "id": self.department.id},
            'group': {"name": getattr(self.groups.first(), 'name', None), "id": getattr(self.groups.first(), 'id', None)},
            'username': self.username,
            'email': self.email,
            'phone': self.phone,
            'skype': self.skype,
            'id': self.id
        }

    class Meta:
        verbose_name = u'Пользователя'
        verbose_name_plural = u'Пользователи'
        permissions = (
            ("can_create_product", u"Создание продукта"),
            ("can_change_product", u"Изменить продукт"),
            ("can_change_view_product", u"Изменение отображение продукта"),
            ("can_delete_product", u"Удаление продукта"),
            ("can_recovery_product", u"Восстановление продукта"),
            ("can_read_logs", u"Чтение логов (действия пользователей)"),
            ("can_analityc_constructor", u"Конструктор аналитики"),
            ('transformer', u"Трансформер"),
            ('can_import_file', u'Импорт данных'),
            ('can_constructor_w0', u'Конструктро В0'),
            ('can_see_product', u'Видимость продуктов'),
            ('can_rate_the_product', u'Оценить продукт'),
            ('can_change_self_profile', u'Изменение профиля(свой)'),
            ('can_change_all_profile', u'Изменение профиля(все)'),
            ('can_add_permission', u'Назначения прав')
        )

    def social_list(self):
        return self.social_set.all()


class Social(models.Model):
    title = models.CharField(max_length=200, verbose_name=u'Название', blank=True, null=True)
    link = models.CharField(max_length=200, verbose_name=u'Ссылка', blank=True, null=True)
    custom_user = models.ForeignKey(CustomUser, verbose_name=u'User')
    class Meta:
        verbose_name = u'Социальную сеть'
        verbose_name_plural = u'Социальные сети'

    def __unicode__(self):
        return self.title


class ProductRating(models.Model):
    user = models.ForeignKey('CustomUser', related_name="product_ratings")
    product = models.ForeignKey('monitoring_product.MonitoringProduct', related_name="user_ratings")
    quality = models.PositiveIntegerField(null=True, blank=True)
    term = models.PositiveIntegerField(null=True, blank=True)
    time = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = u'Оценка'
        verbose_name_plural = u'Оценки'


class UserAplicationSettings(models.Model):
    user = models.OneToOneField(CustomUser, related_name="frontend_settings")
    settings = JSONField()
