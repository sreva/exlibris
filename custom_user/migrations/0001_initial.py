# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import __builtin__
from django.conf import settings
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0001_initial'),
        ('monitoring_product', '0006_auto_20150316_1216'),
    ]

    operations = [
        migrations.CreateModel(
            name='Company',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=200, null=True, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435', blank=True)),
                ('city', models.CharField(max_length=100, verbose_name='\u0413\u043e\u0440\u043e\u0434')),
                ('site', models.CharField(max_length=100, verbose_name='\u0421\u0430\u0439\u0442')),
                ('social', models.CharField(max_length=100, verbose_name='\u0421\u043e\u0446\u0438\u0430\u043b\u044c\u043d\u0430\u044f \u0441\u0435\u0442\u044c')),
                ('icon', models.ImageField(upload_to=b'upload_to/', verbose_name='\u041b\u043e\u0433\u043e\u0442\u0438\u043f')),
            ],
            options={
                'verbose_name': '\u041a\u043e\u043c\u043f\u0430\u043d\u0438\u044e',
                'verbose_name_plural': '\u041a\u043e\u043c\u043f\u0430\u043d\u0438\u0438',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='CustomUser',
            fields=[
                ('user_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to=settings.AUTH_USER_MODEL)),
                ('skype', models.CharField(max_length=100, null=True, verbose_name='Skype', blank=True)),
                ('position', models.CharField(max_length=200, null=True, verbose_name='\u0414\u043e\u043b\u0436\u043d\u043e\u0441\u0442\u044c', blank=True)),
                ('phone', models.CharField(max_length=200, null=True, verbose_name='\u0422\u0435\u043b\u0435\u0444\u043e\u043d', blank=True)),
                ('photo', models.ImageField(upload_to=b'upload_to/', null=True, verbose_name='\u0424\u043e\u0442\u043e', blank=True)),
                ('access_to_products', models.ManyToManyField(related_name=b'users_with_perm', null=True, verbose_name='\u0414\u043e\u0441\u0442\u0443\u043f \u043a \u043f\u0440\u043e\u0434\u0443\u043a\u0442\u0430\u043c', to='monitoring_product.MonitoringProduct', blank=True)),
                ('company', models.ForeignKey(verbose_name='\u043a\u043e\u043c\u043f\u0430\u043d\u0438\u044f', blank=True, to='custom_user.Company', null=True)),
            ],
            options={
                'verbose_name': '\u041f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u0435\u043b\u044f',
                'verbose_name_plural': '\u041f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u0435\u043b\u0438',
            },
            bases=('auth.user',),
        ),
        migrations.CreateModel(
            name='Department',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=200, null=True, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435', blank=True)),
                ('company', models.ForeignKey(verbose_name='\u041a\u043e\u043c\u043f\u0430\u043d\u0438\u044f', to='custom_user.Company')),
            ],
            options={
                'verbose_name': '\u043f\u043e\u0434\u0440\u0430\u0437\u0434\u0435\u043b\u0435\u043d\u0438\u0435',
                'verbose_name_plural': '\u041f\u043e\u0434\u0440\u0430\u0437\u0434\u0435\u043b\u0435\u043d\u0438\u044f',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ProductRating',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('quality', models.PositiveIntegerField(null=True, blank=True)),
                ('term', models.PositiveIntegerField(null=True, blank=True)),
                ('time', models.DateTimeField(auto_now=True)),
                ('product', models.ForeignKey(related_name=b'user_ratings', to='monitoring_product.MonitoringProduct')),
                ('user', models.ForeignKey(related_name=b'product_ratings', to='custom_user.CustomUser')),
            ],
            options={
                'verbose_name': '\u041e\u0446\u0435\u043d\u043a\u0430',
                'verbose_name_plural': '\u041e\u0446\u0435\u043d\u043a\u0438',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Social',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=200, null=True, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435', blank=True)),
                ('link', models.CharField(max_length=200, null=True, verbose_name='\u0421\u0441\u044b\u043b\u043a\u0430', blank=True)),
                ('custom_user', models.ForeignKey(verbose_name='User', to='custom_user.CustomUser')),
            ],
            options={
                'verbose_name': '\u0421\u043e\u0446\u0438\u0430\u043b\u044c\u043d\u0443\u044e \u0441\u0435\u0442\u044c',
                'verbose_name_plural': '\u0421\u043e\u0446\u0438\u0430\u043b\u044c\u043d\u044b\u0435 \u0441\u0435\u0442\u0438',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='UserAplicationSettings',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('settings', jsonfield.fields.JSONField(default=__builtin__.dict)),
                ('user', models.OneToOneField(related_name=b'frontend_settings', to='custom_user.CustomUser')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='customuser',
            name='department',
            field=models.ForeignKey(verbose_name='\u041f\u043e\u0434\u0440\u0430\u0437\u0434\u0435\u043b\u0435\u043d\u0438\u0435', blank=True, to='custom_user.Department', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='customuser',
            name='ratings',
            field=models.ManyToManyField(to='monitoring_product.MonitoringProduct', verbose_name=b'\xd0\x9e\xd1\x86\xd0\xb5\xd0\xbd\xd0\xba\xd0\xb8', through='custom_user.ProductRating'),
            preserve_default=True,
        ),
    ]
