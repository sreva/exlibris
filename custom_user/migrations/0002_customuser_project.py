# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '__first__'),
        ('custom_user', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='customuser',
            name='project',
            field=models.ManyToManyField(to='main.Project', null=True, verbose_name='\u041f\u0440\u043e\u0435\u043a\u0442\u044b', blank=True),
            preserve_default=True,
        ),
    ]
