# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('custom_user', '0003_merge'),
    ]

    operations = [
        migrations.AlterField(
            model_name='productrating',
            name='time',
            field=models.DateTimeField(auto_now=True),
        ),
    ]
