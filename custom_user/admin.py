from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User
from django.contrib.auth.forms import ReadOnlyPasswordHashField
from django.contrib.auth.forms import UserChangeForm
from django.utils.translation import ugettext_lazy as _
from custom_user.models import CustomUser, Department, Social, Company, ProductRating
from django.contrib import admin

class CustomUserChangeForm(UserChangeForm):
    password = ReadOnlyPasswordHashField(
        label=_("Password"),
        help_text=_("Raw passwords are not stored, so there is no way to see "
                    "this user's password, but you can change the password "
                    "using <a href=\"password/\">this form</a>."))

    def clean_password(self):
        return self.initial["password"]

    class Meta:
        model = CustomUser
class DepartamentAdminInline(admin.TabularInline):
    model = Department
class CompanyAdmin(admin.ModelAdmin):
    inlines = [DepartamentAdminInline]
admin.site.register(Company, CompanyAdmin)

class SocialAdminInline(admin.TabularInline):
    model = Social

class RatingInlite(admin.TabularInline):
    model = ProductRating
    extra = 0


class CustomUserAdmin(UserAdmin):
    inlines = [SocialAdminInline, RatingInlite]
    form = CustomUserChangeForm

    list_display = ('username', 'last_name', 'first_name',
                    'is_staff', 'is_active')
    filter_horizontal = ('groups', 'user_permissions', 'project')
    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        (_('Personal info'), {'fields': (
                'first_name', 'last_name', 'email', 'skype', 'position', 'phone', 'department', 'project', 'photo', 'company',
            )}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
        (_('Groups'), {'fields': ('groups',)}),
    )

# admin.site.unregister(User)
admin.site.register(CustomUser, CustomUserAdmin)