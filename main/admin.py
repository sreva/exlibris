# --*-- coding: utf-8 --*--

from django.contrib import admin

from django.contrib.admin import forms
from main.form import ImportFileForm
from main.views import make_published
from models import ImportFile, MediaLevel, Region, FederalDistrict, Article, Country, MediaView, MediaList, \
    MediaCategory, MaterialGenre, MentionsObject, ReferredObject, EmotionalToneObject, InteractionKind, AdvertisementPromo, \
    BrandOrProduct, EmotionalToneMaterial, EmotionalToneTitle, GlobalLocalEvent, HeaderTonality, Initiation, KM, KMKeyValue, \
    KMSpecial, Macroregion, MainTonality, MediaCategorySpecial, MediaListSpecial, MediaTypeSpecial, ObjectRole, Penetration, \
    Product, Project, SideSpeaker, SideSpeakerStatus, SideSpeakerTonality, Speaker, SpeakerCategory, SpeakerLevel, SpeakerQuote, \
    SpeakerQuoteUniqueness, SpeakerSpeechHate, Technologies, ThirdPartySpeakers, Visibility, VisibilitySpecial, Category, MentionProductValue, \
    EdgeRegion, EmotionalToneTitleObject, Image, KMMention, MediaType, MentionProduct, ClientProduct, StatementSpeaker, Author, InfoReason, Branch, \
    ThematicCategory, Tag, IdPressRelease, MaterialReason, BissinesDirection, Day, Month, Week, Duration, ArticleTitle, DirectoryCategory, DirectoryCategoryForm

from daterange_filter.filter import DateRangeFilter
# class HashTagsAdmin(admin.ModelAdmin):
#     pass
# admin.site.register(HashTags, HashTagsAdmin)
from django import forms

class DirectoryCategoryAdmin(admin.ModelAdmin):
    form = DirectoryCategoryForm


admin.site.register(DirectoryCategory, DirectoryCategoryAdmin)

class ArticleTitleAdmin(admin.ModelAdmin):
    pass


admin.site.register(ArticleTitle, ArticleTitleAdmin)

class DurationAdmin(admin.ModelAdmin):
    pass


admin.site.register(Duration, DurationAdmin)


class WeekAdmin(admin.ModelAdmin):
    pass


admin.site.register(Week, WeekAdmin)



class MonthAdmin(admin.ModelAdmin):
    pass


admin.site.register(Month, MonthAdmin)


class DayAdmin(admin.ModelAdmin):
    pass


admin.site.register(Day, DayAdmin)


class MaterialReasonAdmin(admin.ModelAdmin):
    pass


admin.site.register(MaterialReason, MaterialReasonAdmin)


class BissinesDirectionAdmin(admin.ModelAdmin):
    pass


admin.site.register(BissinesDirection, BissinesDirectionAdmin)



class TagAdmin(admin.ModelAdmin):
    pass


admin.site.register(Tag, TagAdmin)


class IdPressReleaseAdmin(admin.ModelAdmin):
    pass


admin.site.register(IdPressRelease, IdPressReleaseAdmin)


class ThematicCategoryAdmin(admin.ModelAdmin):
    pass


admin.site.register(ThematicCategory, ThematicCategoryAdmin)


class InfoReasonAdmin(admin.ModelAdmin):
    pass


admin.site.register(InfoReason, InfoReasonAdmin)


class BranchAdmin(admin.ModelAdmin):

    pass

admin.site.register(Branch, BranchAdmin)

class AuthorAdmin(admin.ModelAdmin):

    pass

admin.site.register(Author, AuthorAdmin)



class ClientProductAdmin(admin.ModelAdmin):
    class Media:
        js = (
            'ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js',
            'js/custom_admin.js'

        )
admin.site.register(ClientProduct, ClientProductAdmin)

class ImportFileAdmin(admin.ModelAdmin):
    list_display = ('file_import', 'date', 'file_type', 'proccesing',)
    list_filter = ('date',  'file_type',)
    filter_horizontal = ('product',)
    form = ImportFileForm
    class Media:
        js = (
            '/media/js/lib/jquery.min.js',
            '/media/js/admin/import-files.js',

        )

admin.site.register(ImportFile, ImportFileAdmin)


class MentionsObjectAdminInline(admin.TabularInline):
    model = MentionsObject


class ArticleAdmin(admin.ModelAdmin):
    actions = [make_published]
    list_display = ('id', 'date', 'title','country', 'date_upload',)
    inlines = [MentionsObjectAdminInline, ]
    filter_horizontal = ('monitoring_product',)
    list_filter = ('project',
                   ('date', DateRangeFilter),
                   ('date_upload',DateRangeFilter),
                   'import_file'
                   )



admin.site.register(Article, ArticleAdmin)



class MentionProductAdmin(admin.ModelAdmin):
    list_display = ('mention_product_value', 'product', )


admin.site.register(MentionProduct, MentionProductAdmin)


class MediaTypeAdmin(admin.ModelAdmin):
    list_display = ('title', )


admin.site.register(MediaType, MediaTypeAdmin)


class KMMentionAdmin(admin.ModelAdmin):
    list_display = ('km', 'key', )


admin.site.register(KMMention, KMMentionAdmin)


class ImageAdmin(admin.ModelAdmin):
    list_display = ('title', )


admin.site.register(Image, ImageAdmin)


class EmotionalToneTitleObjectAdmin(admin.ModelAdmin):
    list_display = ('title', )


admin.site.register(EmotionalToneTitleObject, EmotionalToneTitleObjectAdmin)


class EdgeRegionAdmin(admin.ModelAdmin):
    list_display = ('title', )


admin.site.register(EdgeRegion, EdgeRegionAdmin)


class MentionProductValueAdmin(admin.ModelAdmin):
    list_display = ('title', )


admin.site.register(MentionProductValue, MentionProductValueAdmin)


class MediaLevelAdmin(admin.ModelAdmin):
    list_display = ('title', )


admin.site.register(MediaLevel, MediaLevelAdmin)


class RegionAdmin(admin.ModelAdmin):
    list_display = ('title', )


admin.site.register(Region, RegionAdmin)


class FederalDistrictAdmin(admin.ModelAdmin):
    list_display = ('title', )


admin.site.register(FederalDistrict, FederalDistrictAdmin)


class CountryAdmin(admin.ModelAdmin):
    list_display = ('title', )


admin.site.register(Country, CountryAdmin)


class MediaViewAdmin(admin.ModelAdmin):
    list_display = ('title', )


admin.site.register(MediaView, MediaViewAdmin)


class MediaListAdmin(admin.ModelAdmin):
    list_display = ('title', )


admin.site.register(MediaList, MediaListAdmin)


class MediaCategoryAdmin(admin.ModelAdmin):
    list_display = ('title', )


admin.site.register(MediaCategory, MediaCategoryAdmin)


class MaterialGenreAdmin(admin.ModelAdmin):
    list_display = ('title', )


admin.site.register(MaterialGenre, MaterialGenreAdmin)


class SpeakerAdminInline(admin.TabularInline):
    model = Speaker

class StatementSpeakerAdmin (admin.ModelAdmin):
    pass
admin.site.register(StatementSpeaker, StatementSpeakerAdmin)


class StatementSpeakerAdminInline(admin.TabularInline):
    model = StatementSpeaker


class MentionsObjectAdmin(admin.ModelAdmin):
    list_display = ['article', 'referred_object', 'emotional_tone_object']
    list_filter = ['referred_object']
    inlines = [StatementSpeakerAdminInline,]
    filter_horizontal = ('technologies', )


admin.site.register(MentionsObject, MentionsObjectAdmin)


class ReferredObjectAdmin(admin.ModelAdmin):
    list_display = ('title', )


admin.site.register(ReferredObject, ReferredObjectAdmin)


class InteractionKindAdmin(admin.ModelAdmin):
    list_display = ('title', )


admin.site.register(InteractionKind, InteractionKindAdmin)


class EmotionalToneObjectAdmin(admin.ModelAdmin):
    list_display = ('title', )


admin.site.register(EmotionalToneObject, EmotionalToneObjectAdmin)


class AdvertisementPromoAdmin(admin.ModelAdmin):
    list_display = ('title', )


admin.site.register(AdvertisementPromo, AdvertisementPromoAdmin)


class BrandOrProductAdmin(admin.ModelAdmin):
    list_display = ('title', )


admin.site.register(BrandOrProduct, BrandOrProductAdmin)


class EmotionalToneMaterialAdmin(admin.ModelAdmin):
    list_display = ('title', )


admin.site.register(EmotionalToneMaterial, EmotionalToneMaterialAdmin)


class EmotionalToneTitleAdmin(admin.ModelAdmin):
    list_display = ('title', )


admin.site.register(EmotionalToneTitle, EmotionalToneTitleAdmin)


class GlobalLocalEventAdmin(admin.ModelAdmin):
    list_display = ('title', )


admin.site.register(GlobalLocalEvent, GlobalLocalEventAdmin)


class HeaderTonalityAdmin(admin.ModelAdmin):
    list_display = ('title', )


admin.site.register(HeaderTonality, HeaderTonalityAdmin)


class InitiationAdmin(admin.ModelAdmin):
    list_display = ('title', )


admin.site.register(Initiation, InitiationAdmin)


class KMAdmin(admin.ModelAdmin):
    pass


admin.site.register(KM, KMAdmin)


class KMKeyValueAdmin(admin.ModelAdmin):
    pass


admin.site.register(KMKeyValue, KMKeyValueAdmin)


class KMSpecialAdmin(admin.ModelAdmin):
    list_display = ('title', )


admin.site.register(KMSpecial, KMSpecialAdmin)


class MacroregionAdmin(admin.ModelAdmin):
    list_display = ('title', )


admin.site.register(Macroregion, MacroregionAdmin)


class MainTonalityAdmin(admin.ModelAdmin):
    list_display = ('title', )


admin.site.register(MainTonality, MainTonalityAdmin)


class MediaCategorySpecialAdmin(admin.ModelAdmin):
    list_display = ('title', )


admin.site.register(MediaCategorySpecial, MediaCategorySpecialAdmin)


class MediaListSpecialAdmin(admin.ModelAdmin):
    list_display = ('title', )


admin.site.register(MediaListSpecial, MediaListSpecialAdmin)


class ObjectRoleAdmin(admin.ModelAdmin):
    list_display = ('title', )


admin.site.register(ObjectRole, ObjectRoleAdmin)


class PenetrationAdmin(admin.ModelAdmin):
    list_display = ('title', )


admin.site.register(Penetration, PenetrationAdmin)


class CategoryAdmin(admin.ModelAdmin):
    list_display = ('title', )
    list_filter = ('project',)

admin.site.register(Category, CategoryAdmin)


class ProductAdmin(admin.ModelAdmin):
    pass


admin.site.register(Product, ProductAdmin)


class KMAdminInline(admin.TabularInline):
    model = KM


class ProjectAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug":('title',)}
    list_display = ('title','slug', 'login', 'password')
    inlines = [KMAdminInline, ]


admin.site.register(Project, ProjectAdmin)


class SideSpeakerAdmin(admin.ModelAdmin):
    list_display = ('fio', )


admin.site.register(SideSpeaker, SideSpeakerAdmin)


class SideSpeakerStatusAdmin(admin.ModelAdmin):
    list_display = ('title', )


admin.site.register(SideSpeakerStatus, SideSpeakerStatusAdmin)


class SideSpeakerTonalityAdmin(admin.ModelAdmin):
    list_display = ('title', )


admin.site.register(SideSpeakerTonality, SideSpeakerTonalityAdmin)


class SpeakerAdmin(admin.ModelAdmin):
    list_display = ('fio', )


admin.site.register(Speaker, SpeakerAdmin)


class SpeakerCategoryAdmin(admin.ModelAdmin):
    list_display = ('title', )


admin.site.register(SpeakerCategory, SpeakerCategoryAdmin)


class SpeakerLevelAdmin(admin.ModelAdmin):
    list_display = ('title', )


admin.site.register(SpeakerLevel, SpeakerLevelAdmin)


class SpeakerQuoteAdmin(admin.ModelAdmin):
    list_display = ('title', )


admin.site.register(SpeakerQuote, SpeakerQuoteAdmin)


class SpeakerQuoteUniquenessAdmin(admin.ModelAdmin):
    list_display = ('title', )


admin.site.register(SpeakerQuoteUniqueness, SpeakerQuoteUniquenessAdmin)


class SpeakerSpeechHateAdmin(admin.ModelAdmin):
    list_display = ('title', )


admin.site.register(SpeakerSpeechHate, SpeakerSpeechHateAdmin)


class TechnologiesAdmin(admin.ModelAdmin):
    list_display = ('title', )


admin.site.register(Technologies, TechnologiesAdmin)


class ThirdPartySpeakersAdmin(admin.ModelAdmin):
    pass


admin.site.register(ThirdPartySpeakers, ThirdPartySpeakersAdmin)


class VisibilityAdmin(admin.ModelAdmin):
    list_display = ('title', )


admin.site.register(Visibility, VisibilityAdmin)


class VisibilitySpecialAdmin(admin.ModelAdmin):
    list_display = ('title', )


admin.site.register(VisibilitySpecial, VisibilitySpecialAdmin)


class MediaTypeSpecialAdmin(admin.ModelAdmin):
    list_display = ('title', )


admin.site.register(MediaTypeSpecial, MediaTypeSpecialAdmin)