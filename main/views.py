# --*-- coding: utf-8 --*--
import os
from django.contrib.auth.decorators import login_required, user_passes_test, permission_required
from django.contrib.staticfiles import storage
from django.core.exceptions import ObjectDoesNotExist
from django.core.files.storage import default_storage
from django.core.mail import send_mail, EmailMultiAlternatives, EmailMessage
from django.shortcuts import redirect, get_object_or_404, render_to_response, render
from django.views.generic import TemplateView
import datetime
from django.template.loader import get_template
from django.template.response import TemplateResponse
from django.utils.encoding import smart_str
from info.settings import STATIC_URL, STATIC_ROOT, REPORT_ROOT, BASE_DIR
from main.form import LoginForm
from main.models import Project, FederalDistrict, MediaView, Category, Article, ReferredObject, MentionsObject, ImportFile
from xml.dom.minidom import parseString
from docx import Document
from django.http import HttpResponse, HttpResponseRedirect, Http404
from main.models import Article
from monitoring_product.models import MonitoringProduct, MonitoringProductSetting
from custom_user.models import UserAplicationSettings
import xlrd
# import xlsxwriter
import xlwt
from django.template import Context
from datetime import datetime, timedelta
import json
from django.core.files.move import file_move_safe


def make_published(modeladmin, request, queryset):
    make_published.short_description = u"Импортировать выделиные статьи"
    date = datetime.datetime.now().strftime('%d_%m_%Y_%H_%M')
    workbook = xlsxwriter.Workbook('report' + date + '.xlsx', )
    ws = workbook.add_worksheet('info_sheet')
    fo = open(STATIC_ROOT + "all_title_list.txt", "r")
    line = fo.readlines()
    line_count = 0
    for elem in line:
        ws.write(0, line_count, elem.decode('utf-8'))
        line_count += 1
    fo.close()
    article_list = queryset

    def get_fk_value(item):
        try:
            result = item.title
        except:
            result = ''
        return result

    row = 1
    project = article_list[0].project
    km_list = project.km_set.all()
    km_count = 0
    for elem in km_list:
        ws.write(0, 265 + km_count, elem.title)
        km_count += 1
    for elem in article_list:

        ws.write(row, 0, row)
        date = elem.date.strftime('%m/%d/%Y')
        ws.write(row, 1, date)
        ws.write(row, 2, elem.day)
        ws.write(row, 3, elem.month)
        ws.write(row, 4, elem.week)
        ws.write(row, 5, get_fk_value(elem.media_level))
        ws.write(row, 6, elem.city.title)
        ws.write(row, 7, get_fk_value(elem.federal_district))
        ws.write(row, 8, get_fk_value(elem.region))
        ws.write(row, 9, elem.branch)
        ws.write(row, 10, get_fk_value(elem.country))
        ws.write(row, 11, get_fk_value(elem.author))
        ws.write(row, 12, get_fk_value(elem.media_type))
        ws.write(row, 13, get_fk_value(elem.media_view))
        ws.write(row, 14, get_fk_value(elem.media_list))
        ws.write(row, 15, get_fk_value(elem.media_category))
        ws.write(row, 16, get_fk_value(elem.source))
        ws.write(row, 17, elem.title)
        ws.write(row, 18, elem.annotation)
        ws.write(row, 19, elem.description)
        ws.write(row, 20, elem.url)
        ws.write(row, 21, elem.bissines_direction)
        ws.write(row, 22, elem.thematic_category)
        ws.write(row, 23, get_fk_value(elem.material_genre))
        ws.write(row, 24, elem.material_reason)
        ws.write(row, 25, get_fk_value(elem.emotional_tone_title))
        ws.write(row, 26, get_fk_value(elem.emotional_tone_material))
        ws.write(row, 196, elem.mo)
        ws.write(row, 209, get_fk_value(elem.interaction_kind))
        ws.write(row, 222, get_fk_value(elem.penetration))
        ws.write(row, 223, elem.id_press_release)
        if elem.km_result:
            km_result = u'Да'
        else:
            km_result = ''
        ws.write(row, 285, km_result)
        ws.write(row, 286, elem.tag)
        ws.write(row, 287, elem.duration)
        ws.write(row, 288, elem.release_time)
        ws.write(row, 289, elem.cover)
        ws.write(row, 290, get_fk_value(elem.visibility))
        ws.write(row, 291, elem.infographics)
        ws.write(row, 292, get_fk_value(elem.macroregion))
        ws.write(row, 293, get_fk_value(elem.media_list_special))
        ws.write(row, 294, get_fk_value(elem.media_type_special))
        ws.write(row, 295, get_fk_value(elem.media_category_special))
        ws.write(row, 296, get_fk_value(elem.visibility_special))
        ws.write(row, 297, get_fk_value(elem.image))
        ws.write(row, 298, get_fk_value(elem.km_special))
        ws.write(row, 299, get_fk_value(elem.brand_or_product))
        ws.write(row, 300, get_fk_value(elem.speaker_quote))
        ws.write(row, 301, get_fk_value(elem.third_patry_speakers))
        ws.write(row, 302, get_fk_value(elem.headder_tonality))
        ws.write(row, 303, get_fk_value(elem.main_tonality))
        ws.write(row, 304, get_fk_value(elem.global_local_event))
        ws.write(row, 309, get_fk_value(elem.edge_region))
        ws.write(row, 310, elem.id)
        #KM
        km_mention_list = elem.kmmention_set.all()
        for el in km_mention_list:
            try:
                key = el.key.kay

            except:
                key = ''
            ws.write(row, 264 + el.km.index_sort, key)
            #Product
        product_mention_list = elem.mentionproduct_set.all()
        for elem in product_mention_list:
            ws.write(row, 224 + elem.product.index_sort, get_fk_value(elem.mention_product_value))
            #Cattegory MPPT
        if elem.category != None:
            category_count = 0
            categorys_list = elem.category.get_ancestors()
            for it in categorys_list:
                ws.write(row, 305 + category_count, it.title)
                # print row, 305 + category_count, it.title
                category_count += 1
        mention_objects = elem.mention_objects.all()
        mention_objects_title = ";".join(get_fk_value(x.referred_object) for x in mention_objects)
        ws.write(row, 27, mention_objects_title)
        mention_object_count = 0
        speaker_count = 0
        side_speaker_count = 0
        #MentionObject
        for el in mention_objects:
            ws.write(row, 28 + el.referred_object.index_sort, get_fk_value(el.emotional_tone_object))
            ws.write(row, 40 + el.referred_object.index_sort, get_fk_value(el.emotional_tone_title))
            ws.write(row, 52 + el.referred_object.index_sort, get_fk_value(el.object_role))
            ws.write(row, 64 + el.referred_object.index_sort, get_fk_value(el.info_reason))
            ws.write(row, 172 + el.referred_object.index_sort, el.prt)
            ws.write(row, 184 + el.referred_object.index_sort, el.prv)
            ws.write(row, 197 + el.referred_object.index_sort, el.mq)
            ws.write(row, 210 + el.referred_object.index_sort, get_fk_value(el.initiation))
            ws.write(row, 241 + el.referred_object.index_sort, get_fk_value(el.advertisement_promo))
            technologies_list = el.technologies.all()
            technologies = ";".join(x.title for x in technologies_list)
            ws.write(row, 253 + el.referred_object.index_sort, technologies)

            #Speaker
            speaker_list = elem.statementspeaker_set.all()
            speaker_fio = ";".join(x.speaker.fio for x in speaker_list)
            ws.write(row, 76 + speaker_count, speaker_fio)
            speaker_category = ";".join(get_fk_value(x.speaker.speaker_category) for x in speaker_list)
            ws.write(row, 77 + speaker_count, speaker_category)
            speaker_level = ";".join(get_fk_value(x.speaker.speaker_level) for x in speaker_list)
            ws.write(row, 78 + speaker_count, speaker_level)
            speaker_speech_hate = ";".join(get_fk_value(x.speaker_speech_hate) for x in speaker_list)
            ws.write(row, 79 + speaker_count, speaker_speech_hate)
            speaker_quote_uniqueness = ";".join(get_fk_value(x.speaker.speaker_quote_uniqueness) for x in speaker_list)
            ws.write(row, 80 + speaker_count, speaker_quote_uniqueness)
            speaker_count += 4
            #SideSpeaker
            side_speaker_list = elem.sidespeaker_set.all()
            side_speaker_fio_position = ";".join('%s, %s' % (x.fio, x.position) for x in side_speaker_list)
            ws.write(row, 136 + side_speaker_count, side_speaker_fio_position)
            side_speaker_tonality = ";".join(get_fk_value(x.side_speaker_tonality) for x in side_speaker_list)
            ws.write(row, 137 + side_speaker_count, side_speaker_tonality)
            side_speaker_status = ";".join(get_fk_value(x.side_speaker_status) for x in side_speaker_list)
            ws.write(row, 138 + side_speaker_count, side_speaker_status)
            side_speaker_count += 3

            mention_object_count += 1
        row += 1

    workbook.close()
    file = workbook.filename
    file_move_safe(file, REPORT_ROOT + file)
    return redirect('/static/report/' + file)


decorator_with_arguments = lambda decorator: lambda *args, **kwargs: lambda func: decorator(func, *args, **kwargs)
@decorator_with_arguments
def custom_permission_required(function, perm):
    def _function(request, *args, **kwargs):
        if request.user.has_perm(perm):
            return function(request, *args, **kwargs)
        else:
            return redirect ('/')
            # Return a response or redirect to referrer or some page of your choice
    return _function

# @user_passes_test(lambda u: u.has_perm('custom_user.can_change_view_product'))
@login_required(login_url='/login/')
# @custom_permission_required('custom_user.can_see_product', )
def index(request, slug):
    print type(request.GET), '---------sadasdasd'
    print str(json.dumps(dict(request.GET)))

    try:
        project = get_object_or_404(Project, slug=slug)
    except Project.DoesNotExist:
        raise Http404
    # if request.user
    auth = False
    if request.user.groups.filter(name__in=["SuperAdministrator", "Administrator"]):
        monitoring_products_list = project.monitoring_products.all()
    else:
        monitoring_products_list = project.monitoring_products.filter(users_with_perm=request.user)
    monitoring_products_list = project.monitoring_products.all()
    # monitoring_products = project.monitoring_products.all()
    # for elem in monitoring_products_list:
    #     if elem.type == "day":
    #         now = datetime.now() + timedelta(hours = 6, minutes = 30)
    #         time_now = now.time()
    #         time_start = elem.time_start
    #         if time_start < time_now:
    #             hour = time_now.hour - time_start.hour
    #             minutes = time_now.minute - time_start.minute
    #             date_start = now - timedelta(days=1, hours = hour, minutes = minutes)
    #             date_end = date_start + timedelta(days = 1)
    #         else:
    #             hour = time_start.hour -  time_now.hour
    #             minutes = time_start.minute -  time_now.minute
    #             date_start = now - timedelta(days=2) + timedelta(hours = hour) + timedelta(minutes = minutes)
    #             date_end = date_start + timedelta(days = 1)
    #     elif elem.type == "week":
    #         print 'week'
    #         now = datetime.now() + timedelta(hours = 6, minutes = 30)
    #         time_now = now.time()
    #         time_start = elem.time_start
    #         weekday_now = now.weekday()
    #         weekday_start = int(elem.day_start)
    #         print weekday_now, weekday_start, '-=-=-=-=--'
    #         if time_start < time_now:
    #
    #             hour = time_now.hour - time_start.hour
    #             minutes = time_now.minute - time_start.minute
    #             days = weekday_now - weekday_start
    #             print days
    #             date_start = now - timedelta(weeks = 1, days=days, hours = hour, minutes = minutes)
    #             date_end = date_start + timedelta(weeks = 1)
    #         else:
    #             hour = time_start.hour -  time_now.hour
    #             minutes = time_start.minute -  time_now.minute
    #             days = weekday_start - weekday_now
    #             date_start = now - timedelta(weeks=2) + timedelta(hours = hour) + timedelta(minutes = minutes) + timedelta(days = days)
    #             date_end = date_start + timedelta(weeks = 1)
    #     elif elem.type == "month":
    #         print 'month'
    #         now = datetime.now() + timedelta(hours = 6, minutes = 30)
    #         time_now = now.time()
    #         time_start = elem.time_start
    #         # weekday_now = now.month()
    #         # weekday_start = int(monitoring_product_item.day_start)
    #         # print weekday_now, weekday_start, '-=-=-=-=--'
    #         if time_start < time_now:
    #
    #             hour = time_now.hour - time_start.hour
    #             minutes = time_now.minute - time_start.minute
    #
    #
    #             date_start = now - timedelta(days = 30, hours = hour, minutes = minutes)
    #             date_end = date_start + timedelta(days = 30)
    #         else:
    #             hour = time_start.hour -  time_now.hour
    #             minutes = time_start.minute -  time_now.minute
    #
    #             date_start = now - timedelta(days=60) + timedelta(hours = hour) + timedelta(minutes = minutes)
    #             date_end = date_start + timedelta(days = 30)
    #
    #
    #     date_start_to_template = date_start.strftime("%d.%m.%Y")
    #     date_end_to_template = date_end.strftime("%d.%m.%Y")
    #     link = '/%s/monitoring_product/%d/'%(slug, elem.id)
    #     monitoring_products.append({'title':elem.title, 'date_start': date_start_to_template, 'date_end': date_end_to_template, 'link' : link  })
    # print monitoring_products
    monitoring_products = []
    for elem in monitoring_products_list:
        try:
            article_last = elem.get_article_set().latest('date_upload')
            monitoring_product_type = elem.type
            if article_last:
                if monitoring_product_type == "day":
                    days = 1
                    date_start = article_last.date_upload  - timedelta(days = 1)
                    date_end = article_last.date_upload
                elif monitoring_product_type == "week":
                    days = 7
                    date_start = article_last.date_upload  - timedelta(weeks = 1)
                    date_end = article_last.date_upload
                elif monitoring_product_type == "month":
                    days = 30
                    date_start = article_last.date_upload  - timedelta(days = 30)
                    date_end = article_last.date_upload
                elif monitoring_product_type == "date_release" or monitoring_product_type == "date_upload":
                    date_start = elem.time_from
                    date_end = elem.time_to

            link = '/%s/monitoring_product/%d/'%(slug, elem.id)
            try:
                rating = elem.user_ratings.get(user=request.user)
                quality = rating.quality
                term = rating.term
            except ObjectDoesNotExist:
                quality = None
                term = None
            monitoring_products.append({'pk': elem.pk, 'title':elem.title, 'link' : link, 'type': elem.get_type_display(),
                                        'parent_id': elem.parent_id, 'users_count': elem.users_with_perm.count(),
                                        'date_start': date_start, 'date_end': date_end, 'quality': quality, 'term': term})
        except Exception as e:
            print e
            pass
        pk = monitoring_products_list.latest('pk').pk
        template_settings = json.dumps({
            "id": pk,
            "settings": json.loads(MonitoringProductSetting.objects.get_or_create(user = request.user, monitoring_product_id = pk)[0].setting),
        })

    return render(request, 'index.html', locals())


# def login(request, slug):
#     client = Project.objects.get(slug=slug)
#
#     auth = request.session.get(slug, False)
#     if auth or client.access == '2':
#         return HttpResponseRedirect('/'+slug+'/')
#     else:
#         form = LoginForm()
#         if request.POST:
#             form = LoginForm(request.POST)
#             if form.is_valid():
#                 request.session[slug] = 'yes'
#                 return HttpResponseRedirect('/%s/' % slug)
#             else:
#                 return TemplateResponse(request, 'login.html', locals())
#
#     return TemplateResponse(request, 'login.html', locals())




def add_monitoring_product(request):
    return TemplateResponse(request, 'monitoring_product/add_monitorint_product.html', locals())


def feedback(request):
    if request.method == "POST":
        data= json.loads(request.body)


        send_mail('feedback meccano', data['feedback_text'], request.user.email,
            ['aem@exlibris.ru'], fail_silently=False)
    return HttpResponse(json.dumps({
        'status': 'OK',
        }))

from docx import Document
path = os.path.join(BASE_DIR, 'templates/template.docx')

def generate_report(document, title, annotation, text):
    document.add_paragraph(title, style='Header')
    document.add_paragraph(annotation, style='Annotation')
    document.add_paragraph(text, style='TextContent')


def generate_doc(request):
    data= json.loads(request.body)
    article_list = Article.objects.filter(id__in= data['articles_id_list'])
    document = Document(path)
    for elem in article_list:
        title = u"%s, %s, %s"%(elem.source,elem.date_upload.strftime('%d-%m-%Y'),elem.title)
        generate_report(document,title,elem.annotation, elem.description)
    file_name = 'report'+datetime.strftime(datetime.now(),'%d_%m_%Y_%H_%M_%s' )+'.doc'
    file = document.save(os.path.join(BASE_DIR, 'media/%s'%file_name))
    file = os.path.join(BASE_DIR, 'media/%s'%file_name)

    return HttpResponse(json.dumps({
        'status': 'OK',
        'file_name': file_name,
        'from': request.user.email
        }))


def send_articles(request):
    if request.method == "POST":
        data= json.loads(request.body)
        email_list = data['send_obj']['to'].split(',')
        file_name = data['send_obj']['file_name']
        message = data['send_obj']['message']
        topic = data['send_obj']['topic']
        file = os.path.join(BASE_DIR, 'media/%s'%file_name)
        file = open(file, 'r')
        msg = EmailMultiAlternatives(topic,message, 'test@test.ru',email_list)
        msg.attach("offer.doc", file.read(), "application/ms-word")
        msg.send()
        default_storage.delete(os.path.join(BASE_DIR, 'media/%s'%file_name))
        # else:
        #     item = Article.objects.get(id = data['articles_id_list'][0])
        #     item = Context({ 'title': item.title.title,
        #                      'source': item.source,
        #                      'date_upload': item.date_upload,
        #                      'annotation': item.annotation,
        #                      'description': item.description
        #     })
        #     message = get_template('email/message.html').render(Context(item))
        #     msg = EmailMultiAlternatives('sdsdsdsd', message, 'test@test.test', [email,])
        #     htmly= get_template('email/message.html')
        #     html_content = htmly.render(item)
        #     msg.attach_alternative(html_content, "text/html")
        #     msg.send()

        return HttpResponse(json.dumps({
            'status': 'OK',
            }))

def print_articles(request):
    if request.method == "POST":
        data= json.loads(request.body)
        article_list = Article.objects.filter(id__in= data['articles_id_list'])
        document = Document(path)
        for elem in article_list:
            title = u"%s, %s, %s"%(elem.source,elem.date_upload.strftime('%d-%m-%Y'),elem.title)
            generate_report(document,title,elem.annotation, elem.description)
        file_name = 'report'+datetime.strftime(datetime.now(),'%d_%m_%Y_%H_%M_%s' )+'.doc'
        file = document.save(os.path.join(BASE_DIR, 'media/%s'%file_name))
        file_path = 'media/%s'%file_name
        return HttpResponse(json.dumps({
                'status': 'OK',
                'file_path': file_path
                }))

# search ------------------__!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! <<<<<<<<<<<<<<
# import shlex, re
#
# def search(request):
#     query = ""
#     article_list = Article.objects.all().filter(id__in = [267, 269])
#     def foo(query):
#         pieces = shlex.split(query)
#         include, exclude = [], []
#         for piece in pieces:
#             if piece.startswith('-'):
#                 exclude.append(re.compile(piece[1:]))
#             else:
#                 include.append(re.compile(piece))
#         def validator(s):
#             return (all(r.search(s) for r in include) and
#                     not any(r.search(s) for r in exclude))
#         return validator
#     for elem in article_list:
#         test = foo('без|пруда')
#         ss = test("труда не выловишь и рибку из пруда")
#


@login_required(login_url='/login/')
@custom_permission_required('custom_user.can_change_product', )
def rename_product(request, pk):
    if request.method == "POST":
        data = json.loads(request.body)
        import pprint; pprint.pprint(data)
        if not len(data.get('name', "").strip()):
            return HttpResponse(json.dumps({
                    'error': 'Введите название',
                    }), 400)
        MonitoringProduct.objects.filter(pk=pk).update(title=data.get('name'))
        return HttpResponse(json.dumps({
                'status': 'OK',
                }))


@login_required(login_url='/login/')
@custom_permission_required('custom_user.can_delete_product', )
def delete_product(request, pk):
    if request.method == "POST":
        product = get_object_or_404(MonitoringProduct, pk=pk)
        product.delete()
        return HttpResponse(json.dumps({
                'status': 'OK',
                }))


class AngularTemplateView(TemplateView):
    path = "angular/%s"

    def get_template_names(self):
        return [self.path % self.kwargs['slug']]

get_angular_template = AngularTemplateView.as_view()