# --*-- coding: utf-8 --*--
import xml.etree.ElementTree as ET
import datetime
from django.core.cache import cache

from django.db import models
from xml.dom.minidom import parseString
# from monitoring_product.models import MonitoringProduct
# Create your models here.
from django.dispatch import receiver
from mptt.models import MPTTModel
from django.db.models.signals import post_save, m2m_changed
#from views import import_file
# from monitoring_product.models import MonitoringProduct
from ckeditor.widgets import CKEditorWidget
from django import forms

import xlrd
from info.settings import MEDIA_URL, MEDIA_ROOT
import managers

class DirectoryCategory(MPTTModel):
    parent = models.ForeignKey('self', null=True, blank=True, verbose_name=u'Подкатегория')
    title = models.CharField(max_length=200, verbose_name=u'Название категории', null=True, blank=True, )
    text = models.TextField(blank=True, null= True)
    index_sort = models.IntegerField(verbose_name=u'Индекс сортировки', default=1)


    class Meta:
        verbose_name = u'Справка'
        verbose_name_plural = u'Справка'

    def __unicode__(self):
        return self.title



class DirectoryCategoryForm(forms.ModelForm):
    class Meta:
        model = DirectoryCategory
        widgets = {
            'text': CKEditorWidget(config_name='default'),
            }


FILE_TYPE = (
    ('1', 'xls'),
    ('2', 'xml type 1')
)

PROCESSING = (
    ('1', u'Файл в обработке'),
    ('2', u'Файл обработан')
)

ACCESS = (
    ('1', u'Приватный'),
    ('2', u'Публичный'),
)



class Custom_Abstract(models.Model):
    title = models.CharField(max_length=1000, verbose_name=u'Название', blank=True, null=True)
    project = models.ForeignKey('Project', verbose_name=u'Проект', blank=True, null=True)
    monitoring_product = models.ManyToManyField('monitoring_product.MonitoringProduct', verbose_name = u'мониторинговый продукт', blank = True, null = True)
    class Meta:
        abstract = True

class Project(models.Model):
    title = models.CharField(max_length=200, blank=True, null=True)
    slug = models.CharField(max_length=200, verbose_name=u'Слаг', unique=True, db_index=True)
    login = models.CharField(max_length=200, verbose_name=u'Логин', null=True, blank=True)
    access = models.CharField(max_length=30, verbose_name=u'Доступ к порталу', choices=ACCESS)
    password = models.CharField(max_length=200, verbose_name=u'Пароль', null=True, blank=True)
    # logo = models.ImageField(upload_to='upload_to/', verbose_name=u'Logo проекта', blank=True, null=True)



    class Meta:
        verbose_name = u'Проект'
        verbose_name_plural = u'Проекты'

    def __unicode__(self):
        return self.title
    def sdfdsf(self):
        return self.referredobject_set

class ClientProduct(models.Model):
    project = models.ForeignKey(Project, verbose_name= u'Проект')
    class Meta:
        verbose_name = u'Готовый продукт'
        verbose_name_plural = u'Готовие продукти'

    #def __unicode__(self):
    #    return self.date


class ImportFile(models.Model):
    file_import = models.FileField(upload_to='upload_to/', verbose_name=u'Файл для импорта')
    date = models.DateTimeField(verbose_name=u'Время импорта')
    file_type = models.CharField(max_length=200, verbose_name=u'Тип импортируемого файла', choices=FILE_TYPE)
    proccesing = models.CharField(max_length=200, verbose_name=u'Обработка файла', choices=PROCESSING, null=True, default='1')
    project = models.ForeignKey(Project, verbose_name=u'Проект',null=True)
    product = models.ManyToManyField('monitoring_product.MonitoringProduct', verbose_name=u'Продукт', blank=True, null=True)
    def __unicode__(self):
       return u'{0}'.format(self.file_import.name)

    def save(self, *args, **kw):
        if not self.id:
            self.date = datetime.date.today()
        if self.pk is not None:
            orig = ImportFile.objects.get(pk=self.pk)
            if orig.date != self.date:
                for article in self.article_set.all():
                    article.date_upload = self.date
                    article.save()
        super(ImportFile, self).save(*args, **kw)

    #     item = ImportFile.objects.get(id = self.id)
    #     print item.product.all(), 'save_self.product'
    #     for elem in item.product.all():
    #         print elem.title
    #     print args, 'save_args',
    #     print kwargs, 'save_kwargs'
    #     print self, 'save_self'
    #     import_file(self)
    # def save(self, *args, **kwargs):
        # s = ImportFile()
        # print s.product.all(), 'save_self.product'
    #
    #
    # def clean(self):
    #     for product in self.cleaned_data['product']:
    #         print product.title, 'title'
    #     print self.product ,'clean file_type'
    #     for elem in self.product:

    #
    #     import_file(self)
        # print self.product,'clean self.product'


class MediaLevel(Custom_Abstract):


    class Meta:
        verbose_name = u'Уровень СМИ'
        verbose_name_plural = u'Уровени СМИ'

    def __unicode__(self):
        return self.title

    def __str__(self):
        return self.title


class FederalDistrict(Custom_Abstract):


    class Meta:
        verbose_name = u'Федеральный округ'
        verbose_name_plural = u'Федеральные округа'

    def __unicode__(self):
        return self.title


class Region(Custom_Abstract):


    class Meta:
        verbose_name = u'Регион'
        verbose_name_plural = u'Регионы'

    def __unicode__(self):
        return self.title


class Country(Custom_Abstract):


    class Meta:
        verbose_name = u'Страна'
        verbose_name_plural = u'Страны'

    def __unicode__(self):
        return self.title


class MediaView(Custom_Abstract):


    class Meta:
        verbose_name = u'Вид'
        verbose_name_plural = u'Виды СМИ'

    def __unicode__(self):
        return self.title

    def __str__(self):
        return self.title


class MediaList(Custom_Abstract):


    class Meta:
        verbose_name = u'Список'
        verbose_name_plural = u'Списки СМИ'

    def __unicode__(self):
        return self.title


class MediaCategory(Custom_Abstract):


    class Meta:
        verbose_name = u'Категорию'
        verbose_name_plural = u'Категории СМИ'

    def __unicode__(self):
        return self.title


class MaterialGenre(Custom_Abstract):


    class Meta:
        verbose_name = u'Жанр'
        verbose_name_plural = u'Жанры материала'

    def __unicode__(self):
        return self.title


class InteractionKind(Custom_Abstract):


    class Meta:
        verbose_name = u'Вид взаемодействия'
        verbose_name_plural = u'Вид взаемодействия'

    def __unicode__(self):
        return self.title


class Penetration(Custom_Abstract):


    class Meta:
        verbose_name = u'Степень проникновения ПР/Новости'
        verbose_name_plural = u'Степень проникновения ПР/Новости'

    def __unicode__(self):
        return self.title


class Product(Custom_Abstract):
    index_sort = models.IntegerField(verbose_name=u'Индекс сортировки', default=1)

    class Meta:
        verbose_name = u'Продукт'
        verbose_name_plural = u'Продукты'

    def __unicode__(self):
        return self.title


class MentionProductValue(Custom_Abstract):
    article = models.ForeignKey('Article', verbose_name=u'Статья', blank=True, null=True)

    class Meta:
        verbose_name = u'Значение(как упоминаеться продукт)'
        verbose_name_plural = u'Значение(как упоминаеться продукт)'

    def __unicode__(self):
        return self.title


class MentionProduct(models.Model):
    product = models.ForeignKey(Product, verbose_name=u'Продукт', blank=True, null=True)
    mention_product_value = models.ForeignKey(MentionProductValue, verbose_name=u'как упоминаеться продукт', blank=True,
                                              null=True)
    article = models.ForeignKey('Article', verbose_name=u'Статья', blank=True, null=True)
    monitoring_product = models.ManyToManyField('monitoring_product.MonitoringProduct', verbose_name = u'мониторинговый продукт', blank = True, null = True)

    class Meta:
        verbose_name = u'Упоминание продукта'
        verbose_name_plural = u'Упоминания продукта'

    def __unicode__(self):
        return self.mention_product_value.title


class KMKeyValue(models.Model):
    kay = models.IntegerField(verbose_name=u'Ключ', blank=True, null=True)
    value = models.TextField(verbose_name=u'Полное описание значения', blank=True, null=True)
    project = models.ForeignKey(Project, verbose_name=u'Проект', blank=True, null=True)
    monitoring_product = models.ManyToManyField('monitoring_product.MonitoringProduct', verbose_name = u'мониторинговый продукт', blank = True, null = True)

    class Meta:
        verbose_name = u'KM Значение'
        verbose_name_plural = u'KM Значения'

        #def __unicode__(self):
        #    return self.kay


class KM(Custom_Abstract):
    index_sort = models.IntegerField(verbose_name=u'Индекс сортировки', default=1)


    class Meta:
        verbose_name = u'KM'
        verbose_name_plural = u'KM'

    def __unicode__(self):
        return self.project.title


class KMMention(models.Model):
    km = models.ForeignKey(KM, verbose_name=u'KM', blank=True, null=True)
    key = models.ForeignKey(KMKeyValue, verbose_name=u'Ключ (полное описание)', blank=True, null=True)
    article = models.ForeignKey('Article', verbose_name=u'Статья', blank=True, null=True)
    project = models.ForeignKey(Project, verbose_name=u'Проект', blank=True, null=True)
    monitoring_product = models.ManyToManyField('monitoring_product.MonitoringProduct', verbose_name = u'мониторинговый продукт', blank = True, null = True)

    class Meta:
        verbose_name = u'Упоминание KM'
        verbose_name_plural = u'Упоминание KM'


class Visibility(Custom_Abstract):


    class Meta:
        verbose_name = u'Видимость, проверить'
        verbose_name_plural = u'Видимость, проверить'

    def __unicode__(self):
        return self.title


class Macroregion(Custom_Abstract):


    class Meta:
        verbose_name = u'Заметность'
        verbose_name_plural = u'Заметность'

    def __unicode__(self):
        return self.title


class MediaListSpecial(Custom_Abstract):


    class Meta:
        verbose_name = u'Media list(spec)'
        verbose_name_plural = u'Media list(spec)'

    def __unicode__(self):
        return self.title


class MediaTypeSpecial(Custom_Abstract):


    class Meta:
        verbose_name = u'Media type(spec)'
        verbose_name_plural = u'Media type(spec)'

    def __unicode__(self):
        return self.title


class MediaCategorySpecial(Custom_Abstract):


    class Meta:
        verbose_name = u'Media category(spec)'
        verbose_name_plural = u'Media category(spec)'

    def __unicode__(self):
        return self.title


class VisibilitySpecial(Custom_Abstract):


    class Meta:
        verbose_name = u'Заметность(spec)'
        verbose_name_plural = u'Заметность(spec)'

    def __unicode__(self):
        return self.title


class KMSpecial(Custom_Abstract):


    class Meta:
        verbose_name = u'KM(spec)'
        verbose_name_plural = u'KM(spec)'

    def __unicode__(self):
        return self.title


class BrandOrProduct(Custom_Abstract):


    class Meta:
        verbose_name = u'Brand or Product Mention'
        verbose_name_plural = u'Brand or Product Mention'

    def __unicode__(self):
        return self.title


class SpeakerQuote(Custom_Abstract):


    class Meta:
        verbose_name = u'Speaker’s Quote'
        verbose_name_plural = u'Speaker’s Quote'

    def __unicode__(self):
        return self.title


class ThirdPartySpeakers(Custom_Abstract):


    class Meta:
        verbose_name = u'Third Party Speakers’ Supporting Quotes'
        verbose_name_plural = u'Third Party Speakers’ Supporting Quotes'

    def __unicode__(self):
        return self.title


class HeaderTonality(Custom_Abstract):


    class Meta:
        verbose_name = u'Header Tonality'
        verbose_name_plural = u'Header Tonality'

    def __unicode__(self):
        return self.title


class MainTonality(Custom_Abstract):


    class Meta:
        verbose_name = u'Main Tonality'
        verbose_name_plural = u'Main Tonality'

    def __unicode__(self):
        return self.title


class GlobalLocalEvent(Custom_Abstract):


    class Meta:
        verbose_name = u'Global/Local Event'
        verbose_name_plural = u'Global/Local Event'

    def __unicode__(self):
        return self.title


class EmotionalToneMaterial(Custom_Abstract):


    class Meta:
        verbose_name = u'Эмтон материала'
        verbose_name_plural = u'Эмтон материала'

    def __unicode__(self):
        return self.title


class EmotionalToneTitle(Custom_Abstract):


    class Meta:
        verbose_name = u'Эмтон Заголовка'
        verbose_name_plural = u'Эмтон Заголовка'

    def __unicode__(self):
        return self.title


class Image(Custom_Abstract):


    class Meta:
        verbose_name = u'Image'
        verbose_name_plural = u'Image'

    def __unicode__(self):
        return self.title


class MediaType(Custom_Abstract):


    class Meta:
        verbose_name = u'Вид сми'
        verbose_name_plural = u'Вид сми'

    def __unicode__(self):
        return self.title


class Category(MPTTModel):
    parent = models.ForeignKey('self', null=True, blank=True, verbose_name=u'Подкатегория')
    title = models.CharField(max_length=200, verbose_name=u'Название категории', null=True, blank=True, )
    project = models.ForeignKey(Project, verbose_name=u'проект', null=True, blank=True, related_name="category")
    monitoring_product = models.ManyToManyField('monitoring_product.MonitoringProduct', verbose_name = u'мониторинговый продукт', blank = True, null = True)

    class Meta:
        verbose_name = u'Категорию'
        verbose_name_plural = u'Категории'

    def __unicode__(self):
        return self.title


class EdgeRegion(Custom_Abstract):


    class Meta:
        verbose_name = u'Область край'
        verbose_name_plural = u'Область край'

    def __unicode__(self):
        return self.title


class City(Custom_Abstract):


    class Meta:
        verbose_name = u'Город'
        verbose_name_plural = u'Города'

    def __unicode__(self):
        return self.title


class Source(Custom_Abstract):


    class Meta:
        verbose_name = u'Источник'
        verbose_name_plural = u'Источники'

    def __unicode__(self):
        return self.title

class Branch(Custom_Abstract):


    class Meta:
        verbose_name = u'Филиал'
        verbose_name_plural = u'Филиалы'

    def __unicode__(self):
        return self.title


class Url(Custom_Abstract):


    class Meta:
        verbose_name = u'Url'
        verbose_name_plural = u'Url'

    def __unicode__(self):
        return self.title


class Author(Custom_Abstract):


    class Meta:
        verbose_name = u'Автор'
        verbose_name_plural = u'Автора'

    def __unicode__(self):
        return self.title


class ThematicCategory(Custom_Abstract):


    class Meta:
        verbose_name = u'Тематическую категорию'
        verbose_name_plural = u'Тематические категории'

    def __unicode__(self):
        return self.title

class MaterialReason(Custom_Abstract):


    class Meta:
        verbose_name = u'MaterialReason'
        verbose_name_plural = u'MaterialReason'

    def __unicode__(self):
        return self.title


class BissinesDirection(Custom_Abstract):


    class Meta:
        verbose_name = u'Направление бизнеса'
        verbose_name_plural = u'Направление бизнеса'

    def __unicode__(self):
        return self.title


class IdPressRelease(Custom_Abstract):


    class Meta:
        verbose_name = u'ID-пресс-релиза'
        verbose_name_plural = u'ID-пресс-релиза'

    def __unicode__(self):
        return self.title


class Tag(Custom_Abstract):


    class Meta:
        verbose_name = u'Tag'
        verbose_name_plural = u'Tag'

    def __unicode__(self):
        return self.title


class Week(Custom_Abstract):


    class Meta:
        verbose_name = u'Неделя'
        verbose_name_plural = u'Неделя'

    def __unicode__(self):
        return self.title


class Day(Custom_Abstract):


    class Meta:
        verbose_name = u'День'
        verbose_name_plural = u'День'

    def __unicode__(self):
        return self.title


class Month(Custom_Abstract):


    class Meta:
        verbose_name = u'Месяц'
        verbose_name_plural = u'Месяц'

    def __unicode__(self):
        return self.title


class ArticleTitle(Custom_Abstract):


    class Meta:
        verbose_name = u'Заголовок'
        verbose_name_plural = u'Заголовоки'

    def __unicode__(self):
        return self.title


class Duration(Custom_Abstract):


    class Meta:
        verbose_name = u'Длительность'
        verbose_name_plural = u'Длительность'

    def __unicode__(self):
        return self.title

#
# class ExelFile(models.Model):
#     title = models.CharField(max_length=1000, verbose_name=u'Название', blank=True, null=True)
#     project = models.ForeignKey('Project', verbose_name=u'Проект', blank=True, null=True)
#
#     class Meta:
#         verbose_name = u'Длительность'
#         verbose_name_plural = u'Длительность'
#
#     def __unicode__(self):
#         return self.title



class Article(models.Model):
    import_file = models.ForeignKey(ImportFile, verbose_name=u'Проект', blank=True, null=True)
    monitoring_product = models.ManyToManyField('monitoring_product.MonitoringProduct', verbose_name=u'файл импорта', blank= True, null= True)
    project = models.ForeignKey(Project, verbose_name=u'Проект', null=True, blank=True, related_name='articles')
    date = models.DateTimeField(verbose_name=u'Дата', blank=True, null=True)
    date_upload = models.DateTimeField(verbose_name=u'Дата загрузки', blank=True, null=True)
    day = models.ForeignKey(Day,verbose_name=u'День', blank=True, null=True )
    week = models.ForeignKey(Week,verbose_name=u'Неделя', blank=True, null=True)
    month = models.ForeignKey(Month, verbose_name=u'Месяц', blank=True, null=True)
    # day = models.IntegerField(verbose_name=u'День', blank=True, null=True)
    # week = models.IntegerField(verbose_name=u'Неделя', blank=True, null=True)
    # month = models.IntegerField(verbose_name=u'Месяц', blank=True, null=True)
    media_level = models.ForeignKey(MediaLevel, verbose_name=u'Уровень СМИ', blank=True, null=True)
    federal_district = models.ForeignKey(FederalDistrict, verbose_name=u'Федеральный округ', blank=True, null=True)
    region = models.ForeignKey(Region, verbose_name=u'Регион', blank=True, null=True)
    country = models.ForeignKey(Country, verbose_name=u'Страна', blank=True, null=True)
    media_view = models.ForeignKey(MediaView, verbose_name=u'Вид СМИ', blank=True, null=True)
    media_list = models.ForeignKey(MediaList, verbose_name=u'Спосиок СМИ', blank=True, null=True)
    media_category = models.ForeignKey(MediaCategory, verbose_name=u'Категория СМИ', blank=True, null=True)
    material_genre = models.ForeignKey(MaterialGenre, verbose_name=u'Жанр материала', blank=True, null=True)
    image = models.ForeignKey(Image, verbose_name=u'Image', blank=True, null=True)
    media_type = models.ForeignKey(MediaType, verbose_name=u'Media type', blank=True, null=True)
    #mentions_object = models.ForeignKey(MentionsObject, verbose_name=u'Упоминаемость объекта')
    interaction_kind = models.ForeignKey(InteractionKind, verbose_name=u'Вид взаемодействия', blank=True, null=True)
    penetration = models.ForeignKey(Penetration, verbose_name=u'Степень проникновения ПР/Новости', blank=True,
                                    null=True)
    visibility = models.ForeignKey(Visibility, verbose_name=u'Заметность', blank=True, null=True)
    macroregion = models.ForeignKey(Macroregion, verbose_name=u'Macroregion', blank=True, null=True)
    media_list_special = models.ForeignKey(MediaListSpecial, verbose_name=u'Media list(spec)', blank=True, null=True)
    media_type_special = models.ForeignKey(MediaTypeSpecial, verbose_name=u'Media type(spec)', blank=True, null=True)
    media_category_special = models.ForeignKey(MediaCategorySpecial, verbose_name=u'Media category(spec)', blank=True,
                                               null=True)
    visibility_special = models.ForeignKey(VisibilitySpecial, verbose_name=u'Заметность(spec)', blank=True, null=True)
    km_special = models.ForeignKey(KMSpecial, verbose_name=u'KM(spec)', blank=True, null=True)
    brand_or_product = models.ForeignKey(BrandOrProduct, verbose_name=u'Brand or Product Mention', blank=True,
                                         null=True)
    speaker_quote = models.ForeignKey(SpeakerQuote, verbose_name=u'Speaker’s Quote', blank=True, null=True)
    third_patry_speakers = models.ForeignKey(ThirdPartySpeakers,
                                             verbose_name=u'Third Party Speakers’ Supporting Quotes', blank=True,
                                             null=True)
    headder_tonality = models.ForeignKey(HeaderTonality, verbose_name=u'Header Tonality', blank=True, null=True)
    main_tonality = models.ForeignKey(MainTonality, verbose_name=u'Main Tonality', blank=True, null=True)
    global_local_event = models.ForeignKey(GlobalLocalEvent, verbose_name=u'Global/Local Event', blank=True, null=True)
    emotional_tone_material = models.ForeignKey(EmotionalToneMaterial, verbose_name=u'Эмтон материала', blank=True,
                                                null=True)
    emotional_tone_title = models.ForeignKey(EmotionalToneTitle, verbose_name=u'Эмтон Заголовка', blank=True, null=True)
    city = models.ForeignKey(City, verbose_name=u'Город', blank=True, null=True)
    branch = models.ForeignKey(Branch, verbose_name=u'Филиал', blank=True, null=True)
    url = models.ForeignKey(Url, verbose_name=u'Url', blank= True, null= True)
    author = models.ForeignKey(Author, verbose_name=u'Автор', blank=True, null=True)
    source = models.ForeignKey(Source, verbose_name=u'Источник', blank=True, null=True)
    # title = models.CharField(max_length=400, verbose_name=u'Загаловок', blank=True, null=True)
    title = models.ForeignKey(ArticleTitle, verbose_name=u'Загаловок', blank=True, null=True)
    annotation = models.TextField(verbose_name=u'Анотация', blank=True, null=True)
    description = models.TextField(verbose_name=u'Описание', blank=True, null=True)
    bissines_direction = models.ForeignKey(BissinesDirection, verbose_name=u'Направление бизнеса', blank=True, null=True)
    thematic_category = models.ForeignKey(ThematicCategory, verbose_name=u'Тематическая категория', blank=True, null=True)
    material_reason = models.ForeignKey(MaterialReason, blank=True, null=True)
    id_press_release = models.ForeignKey(IdPressRelease, verbose_name=u'ID-пресс-релиза', blank=True, null=True)
    km_result = models.BooleanField(verbose_name=u'KM_Итог', default=False)
    tag = models.ForeignKey(Tag, verbose_name=u'Таг', blank=True, null=True)
    duration = models.ForeignKey(Duration, verbose_name=u'Длительность', blank=True, null=True)
    release_time = models.TimeField(verbose_name=u'Время выхода', blank=True, null=True)
    cover = models.FloatField(verbose_name=u'Обложка', blank=True, null=True)
    infographics = models.FloatField(verbose_name=u'Инфографика', blank=True, null=True)
    mo = models.IntegerField(verbose_name=u'MO', blank=True, null=True)
    category = models.ForeignKey(Category, blank=True, null=True, verbose_name=u'Категория')
    edge_region = models.ForeignKey(EdgeRegion, verbose_name=u'Область край', blank=True, null=True)

    objects = models.Manager()
    filtered_objects = managers.FilteredManager()

    class Meta:
        verbose_name = u'Статью'
        verbose_name_plural = u'Статьи'

    def __unicode__(self):
        return unicode(self.title.title)

    def save(self, *args, **kwargs):
        project = self.project
        monitoring_products = project.monitoring_products.all()
        for elem in monitoring_products:
            cache.delete('mo-%s'%elem.id)

        super(Article, self).save(*args, **kwargs)


class ReferredObject(models.Model):
    title = models.CharField(max_length=200, verbose_name=u'Название', )
    project = models.ForeignKey(Project, verbose_name=u'Проект', related_name='referred_objects')
    index_sort = models.IntegerField(verbose_name=u'Индекс сортировки', default=1)
    # monitoring_product = models.ManyToManyField('monitoring_product.MonitoringProduct', verbose_name = u'мониторинговый продукт', blank = True, null = True)


    class Meta:
        verbose_name = u'Упоминаемые объект'
        verbose_name_plural = u'Упоминаемые объекы'

    def __unicode__(self):
        return self.title



class HashTags(models.Model):
    title = models.CharField(max_length=200, verbose_name=u'Название', )
    monitoring_product = models.ManyToManyField('monitoring_product.MonitoringProduct', verbose_name = u'мониторинговый продукт', blank = True, null = True)
    project = models.ForeignKey(Project, verbose_name=u'Проект')
    article = models.ForeignKey(Article, verbose_name=u'Статья', related_name='hash_tags')

    class Meta:
        verbose_name = u'хэштег'
        verbose_name_plural = u'хэштеги'

    def __unicode__(self):
        return self.title



class EmotionalToneObject(Custom_Abstract):


    class Meta:
        verbose_name = u'Эмтон объекта'
        verbose_name_plural = u'Эмтон объекта'

    def __unicode__(self):
        return self.title


class EmotionalToneTitleObject(Custom_Abstract):


    class Meta:
        verbose_name = u'Эмтон заголовка упоминаемого обьекта'
        verbose_name_plural = u'Эмтон заголовка упоминаемого обьекта'

    def __unicode__(self):
        return self.title


class ObjectRole(Custom_Abstract):


    class Meta:
        verbose_name = u'Роль Объекта'
        verbose_name_plural = u'Роль Объекта'

    def __unicode__(self):
        return self.title


class Technologies(Custom_Abstract):


    class Meta:
        verbose_name = u'Технологию'
        verbose_name_plural = u'Технологии'

    def __unicode__(self):
        return self.title


class AdvertisementPromo(Custom_Abstract):


    class Meta:
        verbose_name = u'реклама/промо'
        verbose_name_plural = u'реклама/промо'

    def __unicode__(self):
        return self.title


class Initiation(Custom_Abstract):


    class Meta:
        verbose_name = u'Инициирование'
        verbose_name_plural = u'Инициирование'

    def __unicode__(self):
        return self.title


class InfoReason(Custom_Abstract):


    class Meta:
        verbose_name = u'Инфоповод'
        verbose_name_plural = u'Инфоповод'

    def __unicode__(self):
        return self.title


class MentionsObject(models.Model):
    monitoring_product = models.ManyToManyField('monitoring_product.MonitoringProduct', verbose_name = u'мониторинговый продукт', blank = True, null = True)
    article = models.ForeignKey(Article, verbose_name=u'Статья', blank=True, null=True, related_name='mention_objects')
    project = models.ForeignKey(Project, verbose_name=u'Проект', blank=True, null=True)
    referred_object = models.ForeignKey(ReferredObject, blank=True, null=True)
    emotional_tone_object = models.ForeignKey(EmotionalToneObject, verbose_name=u'Эмтон объекта', blank=True, null=True)
    emotional_tone_title = models.ForeignKey(EmotionalToneTitleObject, verbose_name=u'Эмтон заголовка', blank=True,
                                             null=True)
    object_role = models.ForeignKey(ObjectRole, verbose_name=u'Роль Объекта', blank=True, null=True)
    technologies = models.ManyToManyField(Technologies, verbose_name=u'Технологии', blank=True, null=True)
    advertisement_promo = models.ForeignKey(AdvertisementPromo, verbose_name=u'реклама/промо', blank=True, null=True)

    initiation = models.ForeignKey(Initiation, verbose_name=u'Инициирование', blank=True, null=True)
    info_reason = models.ForeignKey(InfoReason, blank=True, null=True, verbose_name=u"Инфоповод")
    prt = models.FloatField(verbose_name=u'PRT', blank=True, null=True)
    prv = models.FloatField(verbose_name=u'PRV', blank=True, null=True)
    mo = models.IntegerField(verbose_name=u'MO', blank=True, null=True)
    mq = models.FloatField(verbose_name=u'MQ', blank=True, null=True)

    class Meta:
        ordering = ["referred_object__index_sort"]
        verbose_name = u'Упоминаемость обьектов'
        verbose_name_plural = u'Упоминаемость обьектов'

    # def save(self, *args, **kwargs):
    #     project = self.project
    #     monitoring_products = project.monitoring_products.all()
    #     for elem in monitoring_products:
    #         cache.delete('prt-%s'%elem.id)
    #         cache.delete('prv-%s'%elem.id)


class SpeakerCategory(Custom_Abstract):


    class Meta:
        verbose_name = u'Категорию спикера'
        verbose_name_plural = u'Категории спикера'

    def __unicode__(self):
        return self.title


class SpeakerLevel(Custom_Abstract):


    class Meta:
        verbose_name = u'Уровень спикера'
        verbose_name_plural = u'Уровни спикера'

    def __unicode__(self):
        return self.title


class SpeakerSpeechHate(Custom_Abstract):


    class Meta:
        verbose_name = u'Характер высказывания спикера'
        verbose_name_plural = u'Характер высказывания спикера'

    def __unicode__(self):
        return self.title


class SpeakerQuoteUniqueness(Custom_Abstract):


    class Meta:
        verbose_name = u'Уникальность цитат спикера'
        verbose_name_plural = u'Уникальность цитат спикера'

    def __unicode__(self):
        return self.title


class Speaker(models.Model):
    monitoring_product = models.ManyToManyField('monitoring_product.MonitoringProduct', verbose_name = u'мониторинговый продукт', blank = True, null = True)
    project = models.ForeignKey(Project, verbose_name=u'Проект', blank=True, null=True)
    fio = models.CharField(max_length=300, verbose_name=u'ФИО', blank=True, null=True)
    speaker_category = models.ForeignKey(SpeakerCategory, verbose_name=u'Категория спикера', blank=True, null=True)
    speaker_level = models.ForeignKey(SpeakerLevel, verbose_name=u'Уровень спикера', blank=True, null=True)
    speaker_quote_uniqueness = models.ForeignKey(SpeakerQuoteUniqueness, verbose_name=u'Уникальность цитат спикера',
                                                 blank=True, null=True)

    class Meta:
        verbose_name = u'Спикера'
        verbose_name_plural = u'Спикеры'

    def __unicode__(self):
        return self.fio


class StatementSpeaker(models.Model):
    project = models.ForeignKey(Project, verbose_name=u'Проект', blank=True, null=True)
    article = models.ForeignKey(Article, verbose_name=u'Статья', blank=True, null=True)
    speaker = models.ForeignKey(Speaker, verbose_name=u'Спикер', blank=True, null= True)
    mentions_object = models.ForeignKey(MentionsObject, verbose_name=u'Упоминаемость объекта', blank=True, null=True, related_name= 'statementspeakers')
    speaker_speech_hate = models.ForeignKey(SpeakerSpeechHate, verbose_name=u'Характер высказывания спикера',
                                            blank=True, null=True)
    monitoring_product = models.ManyToManyField('monitoring_product.MonitoringProduct', verbose_name = u'мониторинговый продукт', blank = True, null = True)


    class Meta:
        verbose_name = u'Висказивание спикера'
        verbose_name_plural = u'Висказивание спикера'

    # def __unicode__(self):
    #     return self.speaker_speech_hate
    #


class SideSpeakerTonality(Custom_Abstract):


    class Meta:
        verbose_name = u'Тональность'
        verbose_name_plural = u'Тональность'

    def __unicode__(self):
        return self.title


class SideSpeakerStatus(Custom_Abstract):


    class Meta:
        verbose_name = u'Статус'
        verbose_name_plural = u'Статуси сторонего Спикера'

    def __unicode__(self):
        return self.title


class SideSpeaker(models.Model):
    monitoring_product = models.ManyToManyField('monitoring_product.MonitoringProduct', verbose_name = u'мониторинговый продукт', blank = True, null = True)
    project = models.ForeignKey(Project, verbose_name=u'Проект', blank=True, null=True)
    article = models.ForeignKey(Article, verbose_name=u'Статья', blank=True, null=True)
    mentions_object = models.ForeignKey(MentionsObject, verbose_name=u'Упоминаемость объекта', blank=True, null=True)
    fio = models.CharField(max_length=300, verbose_name=u'ФИО', blank=True, null=True)
    position = models.CharField(max_length=300, verbose_name=u'Должность', blank=True, null=True)
    side_speaker_tonality = models.ForeignKey(SideSpeakerTonality, verbose_name=u'Тональность', blank=True, null=True)
    side_speaker_status = models.ForeignKey(SideSpeakerStatus, verbose_name=u'Статус', blank=True, null=True)


    class Meta:
        def __init__(self):
            pass

        verbose_name = u'Стороний Спикер'
        verbose_name_plural = u'Стороний спикер'

    def __unicode__(self):
        return self.fio

from celery.decorators import task
from django.core import serializers
import time
import json
from django.core import serializers
@receiver(post_save, sender=ImportFile, dispatch_uid='guide_saved')
def import_file_pre(instance, **kwargs):


    if instance.proccesing == '2':
        return 'done'
    else:
        # import_file(instance.id)
        import_file.delay(instance.id)
    return 'done'




import ast
@task(name='import_file',ignore_result=True)

def import_file(instance):
    import time
    time.sleep(2)
    instance = ImportFile.objects.get(id = instance)
    # # instance = json.loads(instance)
    # instance = ast.literal_eval(instance)
    # print instance, type(instance)
    # instance = instance[0]['fields']
    # print instance['file_type']
    # instance = ImportFile.objects.get(id = instance_id)
        # instance.product.all().all()
        # for elem in instance.product.all().all():
        #     print elem.id, elem.title
        # for val in pk_set:
        #     print val
    # print sender, 'sender, **kwargs'
    # # print instance.product.all().all()
    # print instance.product.all().all()
    # for elem in instance.product.all().all:
    #     print elem.title, 'title'
    # print sender.product.objects.all(), 'sender'
    # print created , 'created'

    if instance.file_type == '1':

        #self.proccesing = '2'
        #self.save()
        item_id = 0
        workbook = xlrd.open_workbook(instance.file_import.path)
        # worksheet = workbook.sheet_by_name('base')
        worksheet = workbook.sheet_by_index(0)

        num_rows = worksheet.nrows - 1
        curr_row = 0

        z = 0.1
        z = float(z)

        def get_float_value(list, step):
            if list:
                value = list[step]
                if value != '':
                    return_object = value
                else:
                    return_object  = None
            else :
                return_object = None
            return return_object

        def create_object_model_list_step(Model, list, step):
            if list:
                try:
                    value = list[step]

                    if value != '':
                        return_object = Model.objects.get_or_create(title=value, project=project)[0]
                        for elem in instance.product.all():
                            return_object.monitoring_product.add(elem)
                            return_object.save()
                        return return_object
                except:
                    return_object = None

                else:
                    return_object = None
            else:
                return_object = None
            return return_object

        def get_or_create_object_model_value(Model, value):
            if value != '':
                return_object = Model.objects.get_or_create(title=value[:500], project=project)[0]
                for elem in instance.product.all():
                    return_object.monitoring_product.add(elem)
                    return_object.save()

            else:
                return_object = None
            return return_object


        project = Project.objects.get(id=instance.project.id)
        now = datetime.datetime.now()
        # print project.id, 'project_id'
        row_number = 0
        while curr_row < num_rows:
            curr_row += 1
            print curr_row
            row = worksheet.row(curr_row)
            # print row[310].value, '-===========---=-'
            # if row[310].value != '':
            #     try:
            #         item = Article.objects.get(id=int(row[310].value))
            #         item_id = item.id
            #         item.delete()
            #     except Article.DoesNotExist:
            #         pass
            quantity_row = 20
            category_number = 4
            category_count = 0
            cat = None
            from monitoring_product.models import MonitoringProductCategory
            for elem in range(category_number):
                row_number = 305 + category_count
                if row[row_number].value != '':
                    try:
                        cat = Category.objects.get_or_create(parent_id=cat.id, title=row[row_number].value, project=project)[0]
                    except:
                        cat = Category.objects.get_or_create(parent_id=None, title=row[row_number].value, project=project)[0]
                    for elem in instance.product.all():
                        if cat.parent == None:
                            MonitoringProductCategory.objects.get_or_create(category = cat, monitoring_product = elem)
                        cat.monitoring_product.add(elem)
                        cat.save()
                category_count += 1
            km_count = 0
            for elem in range(quantity_row):
                try:
                    row_number = 265 + km_count
                    value = KMKeyValue.objects.get(kay=row[row_number].value, project=project)
                    km = KM.objects.create(project=project, value=value)
                except:
                    value = None
                km_count += 1
            if len(row[20].value)>500:
                url = row[20].value[500]
            else:
                url = row[20].value

            edge_region = get_or_create_object_model_value(EdgeRegion, row[309].value)
            federal_district = get_or_create_object_model_value(FederalDistrict, row[7].value)
            region = get_or_create_object_model_value(Region, row[8].value)
            country = get_or_create_object_model_value(Country, row[10].value)
            media_view = get_or_create_object_model_value(MediaView, row[13].value)
            media_list = get_or_create_object_model_value(MediaList, str(row[14].value))
            media_category = get_or_create_object_model_value(MediaCategory, str(row[15].value))
            material_genre = get_or_create_object_model_value(MaterialGenre, row[23].value)
            interaction_kind = get_or_create_object_model_value(InteractionKind, row[209].value)
            penetration = get_or_create_object_model_value(Penetration, row[222].value)
            visibility = get_or_create_object_model_value(Visibility, row[290].value)
            macroregion = get_or_create_object_model_value(Macroregion, row[292].value)
            media_list_special = get_or_create_object_model_value(MediaListSpecial, row[293].value)
            media_type_special = get_or_create_object_model_value(MediaTypeSpecial, row[294].value)
            media_category_special = get_or_create_object_model_value(MediaCategorySpecial, row[295].value)
            visibility_special = get_or_create_object_model_value(VisibilitySpecial, row[296].value)
            image = get_or_create_object_model_value(Image, row[297].value)
            km_special = get_or_create_object_model_value(KMSpecial, row[298].value)
            brand_or_product = get_or_create_object_model_value(BrandOrProduct, row[299].value)
            speakers_quote = get_or_create_object_model_value(SpeakerQuote, row[300].value)
            third_patry_speakers = get_or_create_object_model_value(ThirdPartySpeakers, row[301].value)
            headder_tonality = get_or_create_object_model_value(HeaderTonality, row[302].value)
            main_tonality = get_or_create_object_model_value(MainTonality, row[303].value)
            global_local_event = get_or_create_object_model_value(GlobalLocalEvent, row[304].value)
            emotional_tone_title = get_or_create_object_model_value(EmotionalToneTitle, row[25].value)
            emotional_tone_material = get_or_create_object_model_value(EmotionalToneMaterial, row[26].value)
            id_press_release = get_or_create_object_model_value(IdPressRelease, row[223].value)
            bissines_direction=get_or_create_object_model_value(BissinesDirection, row[21].value)
            thematic_category=get_or_create_object_model_value(ThematicCategory, row[22].value)
            material_reason=get_or_create_object_model_value(MaterialReason, row[24].value)
            city = get_or_create_object_model_value(City, row[6].value)
            branch=get_or_create_object_model_value(Branch, row[9].value)
            tag=get_or_create_object_model_value(Tag, row[286].value)
            url=get_or_create_object_model_value(Url, url)
            author = get_or_create_object_model_value(Author, row[11].value)
            source = get_or_create_object_model_value(Source, row[16].value)
            # print row[17].value, len(row[17].value), '!!!!!!!!!!!!!'
            title = get_or_create_object_model_value(ArticleTitle, row[17].value)
            duration=get_or_create_object_model_value(Duration, row[287].value)
            cover = row[289].value
            infographics = row[291].value
            dates = row[1].value
            dates = xlrd.xldate_as_tuple(int(dates),0)
            dates = '%s/%s/%s' % (dates[2], dates[1], dates[0])
            date = datetime.datetime.strptime(dates , "%d/%m/%Y")
            release_time = row[288].value
            # print release_time , 'release_time_table'
            try:
                release_time = int(release_time * 24 * 3600) # convert to number of seconds
                release_time = datetime.time(release_time//3600, (release_time%3600)//60,)
                release_time = release_time.strftime('%H:%M')

            except:
                release_time = None

            #
            # print release_time,'release_time'

            if cover == '':
                cover = None
            else:
                cover = float(cover)

            if infographics == '':
                infographics = None
            else:
                infographics = float(infographics)
            media_type = get_or_create_object_model_value(MediaType, row[12].value)
            media_level = get_or_create_object_model_value(MediaLevel, row[5].value)
            day = get_or_create_object_model_value(Day, row[2].value)
            week = get_or_create_object_model_value(Week, row[3].value)
            month = get_or_create_object_model_value(Month, row[4].value)
            description = row[19]
            # if row[311].value != '':
            #     description += row[311].value
            # if row[312].value != '':
            #     description += row[312].value
            # if row[2].value != '':
            #     day = row[2].value
            # else:
            #     day = None
            # if row[3].value != '':
            #     week = row[3].value
            # else:
            #     week = None
            # if row[4].value != '':
            #     month = row[4].value
            # else:
            #     month = None
            # print category, 'category!!!!!!'
            # print row[196].value, 'MO!!!!'
            print title
            try:
                mo = int(row[196].value)
            except ValueError:
                mo = None
            if item_id != 0:

                article = Article.objects.create(date = date, import_file = instance, date_upload = now, id=item_id, title=title, project=project, media_level=media_level,
                                                 media_type=media_type, day=day, \
                                                 month=month, week=week, city=city,
                                                 federal_district=federal_district, region=region, country=country,
                                                 media_view=media_view, \
                                                 media_list=media_list, media_category=media_category,
                                                 material_genre=material_genre, interaction_kind=interaction_kind,
                                                 penetration=penetration, \
                                                 visibility=visibility, macroregion=macroregion,
                                                 media_list_special=media_list_special,
                                                 media_type_special=media_type_special,
                                                 media_category_special=media_category_special, \
                                                 visibility_special=visibility_special, image=image,
                                                 km_special=km_special, brand_or_product=brand_or_product,
                                                 speaker_quote=speakers_quote, \
                                                 third_patry_speakers=third_patry_speakers,
                                                 headder_tonality=headder_tonality, main_tonality=main_tonality,
                                                 global_local_event=global_local_event, \
                                                 emotional_tone_title=emotional_tone_title,
                                                 emotional_tone_material=emotional_tone_material, branch=branch,
                                                 author=author, source=source, \
                                                 annotation=row[18].value, description=description.value, url=url,
                                                 bissines_direction=bissines_direction, thematic_category=thematic_category, \
                                                 material_reason=material_reason, id_press_release=id_press_release,
                                                 km_result=row[285].value, tag=tag, duration=duration, \
                                                 release_time=release_time, cover=cover, infographics=infographics,
                                                 mo=mo, category=cat, edge_region=edge_region
                )
            else:
                article = Article.objects.create(date = date, import_file = instance, date_upload = now, title=title, project=project, media_level=media_level,
                                                 media_type=media_type, day=day, \
                                                 month=month, week=week, city=city,
                                                 federal_district=federal_district, region=region, country=country,
                                                 media_view=media_view, \
                                                 media_list=media_list, media_category=media_category,
                                                 material_genre=material_genre, interaction_kind=interaction_kind,
                                                 penetration=penetration, \
                                                 visibility=visibility, macroregion=macroregion,
                                                 media_list_special=media_list_special,
                                                 media_type_special=media_type_special,
                                                 media_category_special=media_category_special, \
                                                 visibility_special=visibility_special, image=image,
                                                 km_special=km_special, brand_or_product=brand_or_product,
                                                 speaker_quote=speakers_quote, \
                                                 third_patry_speakers=third_patry_speakers,
                                                 headder_tonality=headder_tonality, main_tonality=main_tonality,
                                                 global_local_event=global_local_event, \
                                                 emotional_tone_title=emotional_tone_title,
                                                 emotional_tone_material=emotional_tone_material, branch=branch,
                                                 author=author, source=source, \
                                                 annotation=row[18].value, description=description.value, url=url,
                                                 bissines_direction=bissines_direction, thematic_category=thematic_category, \
                                                 material_reason=material_reason, id_press_release=id_press_release,
                                                 km_result=row[285].value, tag=tag, duration=duration, \
                                                 release_time=release_time, cover=cover, infographics=infographics,
                                                 mo=mo, category=cat, edge_region=edge_region
                )
            for elem in instance.product.all():
                # print elem.id,'product-id'
                article.monitoring_product.add(elem)
                article.save()


            # Если мониторинговый продукт социальных сми, создаем хештеги!
            for elem in instance.product.all():
                if elem.type == '2':
                    hash_tags = row['22'].value.split[';']
                    for it in hash_tags:
                        hash_tag = HashTags.objects.get_or_create(project=project, article=article, title = it.strip(' '))

                        for elem in instance.product.all():
                            hash_tag[0].monitoring_product.add(elem)
                            hash_tag[0].save()


            #Product
            product_count = 1
            for el in range(17):
                if row[223 + product_count].value != '':
                    try:
                        product = Product.objects.get(index_sort=product_count, project=project)
                        mention_product_value, status = MentionProductValue.objects.get_or_create(title=row[223 + product_count].value)
                        mention_product = MentionProduct.objects.create(article=article, product=product,
                                                                        mention_product_value=mention_product_value)
                    except Exception, e:
                        print "Exception, %s" % e
                product_count += 1
                #KM
            km_count = 1
            for el in range(20):
                if row[264 + km_count].value != '':
                    try:
                        key = KMKeyValue.objects.get(kay=row[264 + km_count].value, project=project)

                    except KMKeyValue.DoesNotExist:
                        key = None

                    if key != None:
                        try:
                            km = KM.objects.get(index_sort=km_count)
                        except KM.DoesNotExist:
                            km = None
                        km_mention = KMMention.objects.create(article=article, project=project, key=key, km=km)
                        for elem in instance.product.all():
                            km_mention.monitoring_product.add(elem)
                            km_mention.save()
                km_count += 1
                #упоминаемий обьект ------------------------------------------------------------

            mantion_objects = row[27].value.split(';')
            quantity_row = 12
            technologies_list = []
            i = 0
            for elem in range(quantity_row):
                row_number = 253 + i
                if row[row_number].value != '':
                    technologies_all = row[row_number].value.split(';')
                    for elem in technologies_all:
                        technologies_list.append(elem)
                i += 1
            emotional_tone_object_list = []
            emotional_tone_title_list = []
            info_reason_list = []
            initiation_list = []
            advertisement_promo_list = []
            prt_list = []
            prv_list = []
            mq_list = []
            object_role_list = []
            i = 0
            for elem in range(quantity_row):

                row_number = 28 + i
                if row[row_number].value != '':
                    emotional_tone_object_list.append(row[row_number].value)

                row_number = 40 + i
                if row[row_number].value != '':
                    emotional_tone_title_list.append(row[row_number].value)

                row_number = 52 + i
                if row[row_number].value != '':
                    object_role_list.append(row[row_number].value)

                row_number = 62 + i
                if row[row_number].value != '':
                    info_reason_list.append(row[row_number].value)

                row_number = 210 + i
                if row[row_number].value != '':
                    initiation_list.append(row[row_number].value)

                row_number = 241 + i
                if row[row_number].value != '':
                    advertisement_promo_list.append(row[row_number].value)

                row_number = 172 + i
                if row[row_number].value != '':
                    prt_list.append(row[row_number].value)

                row_number = 184 + i
                if row[row_number].value != '':
                    prv_list.append(row[row_number].value)

                row_number = 197 + i
                mq_list.append(row[row_number].value)
                i += 1


            #запись упоминаемого обьекта-------------------------------------------------------
            z = 0
            k = 0
            k_side = 0

            for elem in mantion_objects:

                emotional_tone_object = create_object_model_list_step(EmotionalToneObject, emotional_tone_object_list, z)
                # print emotional_tone_object_list, 'emotional_tone_object_list'
                # print z
                # print emotional_tone_object_list[z]
                emotional_tone_title = create_object_model_list_step(EmotionalToneTitleObject, emotional_tone_title_list, z)
                mention_object_title = elem.strip(' ')
                initiation = create_object_model_list_step(Initiation, initiation_list, z)
                advertisement_promo = create_object_model_list_step(AdvertisementPromo, advertisement_promo_list, z)
                prt = get_float_value(prt_list , z)
                prv = get_float_value(prv_list, z)
                mq = get_float_value(mq_list, z)
                try:
                    referred_object = project.referred_objects.get(title = mention_object_title)
                except ReferredObject.DoesNotExist:
                    referred_object = False
                if referred_object:
                    print emotional_tone_object, '!!!!!!'

                    info_reason = create_object_model_list_step(InfoReason, info_reason_list, z)
                    object_role = create_object_model_list_step(ObjectRole, object_role_list, z)
                    mention_object = MentionsObject.objects.create(project=project, article=article,
                                                                   referred_object=referred_object, \
                                                                   emotional_tone_object=emotional_tone_object,
                                                                   emotional_tone_title=emotional_tone_title,
                                                                   object_role=object_role, info_reason= info_reason, \
                                                                   initiation=initiation,
                                                                   advertisement_promo=advertisement_promo, prt=prt,
                                                                   prv=prv, mq=mq,
                    )
                    for elem in instance.product.all():
                        mention_object.monitoring_product.add(elem)
                        mention_object.save()
                    technologies_number = 0
                    for elem in technologies_list:
                        try:
                            technologies = get_or_create_object_model_value(Technologies, technologies_list[technologies_number])
                            mention_object.technologies.add(technologies)

                        except:
                            technologies = None
                        technologies_number += 1
                        mention_object.save()
                    z += 1

                    #упоминаемий обьект (СПИКЕР)------------------------------------------------------------

                    row_number = 76 + k
                    speakers_list = row[row_number].value.split(';')
                    speaker_categorys = row[row_number + 1].value.split(';')
                    speaker_levels = row[row_number + 2].value.split(';')
                    speaker_speech_hate = row[row_number + 3].value.split(';')
                    speaker_quote_uniqueness = row[row_number + 4].value.split(';')

                    speaker_quote_uniqueness_list = []
                    speaker_speech_hate_list = []
                    for elem in speaker_quote_uniqueness:
                        speaker_quote_uniqueness_list.append(elem)
                    for elem in speaker_speech_hate:
                        speaker_speech_hate_list.append(elem)

                    speaker_category_list = []
                    for elem in speaker_categorys:
                        speaker_category_list.append(elem)
                    speaker_level_list = []
                    for elem in speaker_levels:
                        speaker_level_list.append(elem)

                    speaker_num = 0
                    for el in speakers_list:
                        speaker_category = create_object_model_list_step(SpeakerCategory, speaker_category_list, speaker_num)
                        speaker_level = create_object_model_list_step(SpeakerLevel, speaker_level_list, speaker_num)
                        speaker_speech_hate = create_object_model_list_step(SpeakerSpeechHate, speaker_speech_hate_list, speaker_num)
                        speaker_quote_uniqueness = create_object_model_list_step(SpeakerQuoteUniqueness, speaker_quote_uniqueness_list, speaker_num)
                        if el != '':
                            speaker = Speaker.objects.get_or_create(fio = el.strip( ' ' ), project=project,)[0]
                            for elem in instance.product.all():
                                speaker.monitoring_product.add(elem)
                                speaker.save()
                            speaker.speaker_category = speaker_category
                            speaker.speaker_level = speaker_level
                            speaker.speaker_quote_uniqueness = speaker_quote_uniqueness
                            speaker.save()
                            # speaker = Speaker.objects.create(fio=el.strip( ' ' ), speaker_category=speaker_category,
                            #                                  mentions_object=mention_object, article=article,
                            #                                  project=project, \
                            #                                  speaker_level=speaker_level,
                            #                                  speaker_quote_uniqueness=speaker_quote_uniqueness)
                            speaker_num = speaker_num + 1
                            statement_speaker = StatementSpeaker.objects.create(speaker = speaker, mentions_object = mention_object, article = article, speaker_speech_hate = speaker_speech_hate, project = project)
                            for elem in instance.product.all():
                                statement_speaker.monitoring_product.add(elem)
                                statement_speaker.save()
                    k = k + 5
                    #упоминаемий обьект (СТОРОНИЙ СПИКЕР)------------------------------------------------------------
                    row_number_side = 136 + k_side
                    side_speaker_count = 0
                    side_speaker_status = row[row_number_side + 2].value.split(';')
                    side_speaker_tonality = row[row_number_side + 1].value.split(';')
                    side_speaker_tonality_list = []
                    for elem in side_speaker_tonality:
                        side_speaker_tonality_list.append(elem)
                    side_speaker_status_list = []
                    for elem in side_speaker_status:
                        side_speaker_status_list.append(elem)
                    if row[row_number_side].value != '':

                        side_speakers_list = row[row_number_side].value.split(';')

                        for elem in side_speakers_list:

                            side_speaker_tonality = create_object_model_list_step(SideSpeakerTonality,
                                                                         side_speaker_tonality_list, side_speaker_count)
                            side_speaker_status = get_or_create_object_model_value(SideSpeakerStatus,
                                                                       side_speaker_status_list[side_speaker_count])
                            side_speaker = elem.split(',')
                            side_speaker_fio = side_speaker[0]
                            try:
                                side_speaker_position = side_speaker[1]
                            except:
                                side_speaker_position = ''
                            if side_speaker_fio != '':
                                side_speaker = SideSpeaker.objects.get_or_create(fio=side_speaker_fio,
                                                                          position=side_speaker_position, article=article,
                                                                          mentions_object=mention_object, \
                                                                          project=project,
                                                                          side_speaker_tonality=side_speaker_tonality,
                                                                          side_speaker_status=side_speaker_status)
                                for elem in instance.product.all():
                                    side_speaker[0].monitoring_product.add(elem)
                                    side_speaker[0].save()
                    k_side = k_side + 3
                    #self.proccesing = '3'
    #                 #self.save()

    elif instance.file_type == '2':
    #     file = open(self.file_import.path, 'r')
        data = file.read()
        file.close()
        project = Project.objects.get(id=instance.project.id)
        dom = parseString(data)
        articles = dom.getElementsByTagName('ARTICLE')

        def get_item_value(item, tag_name):
            try:
                item = elem.getElementsByTagName(tag_name)[0].firstChild.data
            except:
                item = None
            return item

        def get_or_create_object_model_value(Model, value):
            if value != '':
                return_object = Model.objects.get_or_create(title=value, project=project)[0]
            else:
                return_object = None
            return return_object

        def get_or_create_object_model_value_create(Model, value):
            if value != '':
                return_object = Model.objects.create(title=value, project=project)
            else:
                return_object = None
            return return_object
        for elem in articles:
            url = get_item_value('url', 'URL')
            date = get_item_value('date', 'DATE')
            time = get_item_value('time', 'TIME')
            author = get_item_value('author', 'AUTHOR')
            title = get_item_value('title', 'TITLE')
            text = get_item_value('text', 'TEXT')
            town = get_item_value('town', 'TOWN')
            district = get_item_value('district', 'DISTRICT')
            media_view = get_item_value('media_view', 'MASSMEDIA')
            mention = get_item_value('mention', 'MENTION')
            annotation = get_item_value('annotation', 'HEADLINE')
            section1 = get_item_value('section1', 'SECTION1')
            section2 = get_item_value('section2', 'SECTION2')
            section3 = get_item_value('section3', 'SECTION3')
            section4 = get_item_value('section4', 'SECTION4')
            date = datetime.datetime.strptime(date, '%d.%m.%Y').strftime('%Y-%m-%d')
            federal_district = get_or_create_object_model_value(FederalDistrict, district)
            media_view = get_or_create_object_model_value(MediaView, media_view)
            section_list = [section1, section2, section3, section4]
            category_number = 4
            category_count = 0
            cat = None
            for elem in range(category_number):
                if section_list[elem] != None:

                    try:

                        cat = Category.objects.get_or_create(parent_id=cat[0].id, title=section_list[category_count], project=project)
                    except:
                        cat = Category.objects.get_or_create(parent_id=None, title=section_list[category_count], project=project)
                    category_count += 1
            try:
                category = cat[0]
            except:
                category = None
            mention_list = mention.split(';')

            article = Article.objects.create(title=title, project=project, date=date, release_time=time, author=author,
                                             description=text, \
                                             city=town, federal_district=federal_district, media_view=media_view,
                                             annotation=annotation, \
                                             category=category)
            for elem in mention_list:
                try:
                    referred_object = ReferredObject.objects.get(title = elem)
                except ReferredObject.DoesNotExist:
                    referred_object = False

                if referred_object:
                    mention_object = MentionsObject.objects.create(project=project, article=article,
                                                               referred_object=referred_object)
    instance.proccesing = '2'
    instance.save()
    return 'task done'

# post_save.connect(import_file_pre, sender=ImportFile)


    # m2m_changed.connect(import_file, sender=ImportFile.product)
    # m2m_changed.connect(ImportFile, sender=ImportFile.product.through)
    # post_save.connect(import_file, sender=ImportFile)