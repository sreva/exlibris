# --*-- coding: utf-8 --*--
from django.db import models
from django.db.models.query import QuerySet


class FilteredMixin(object):

    def filtered_by(self, settings):
        mention_object_filters = ['mq__range', 'prv__range', 'prt__range', 'referred_object__id__in',
                                  'info_reason__id__in', 'emotional_tone_object__id__in', 'advertisement_promo__id__in',
                                  ]
        filters = {}
        for key in settings.keys():

            if settings[key]['settings']['a9'] == u'шкала':
                param = settings[key]['list']
                if None in param:
                    param = False
            else:
                param = [x['id']
                         for x in settings[key]['list'] if x.get('selected', True)]
                if not param or len(param) == len(settings[key]['list']):
                    continue
            if key in mention_object_filters:
                key = "mention_objects__%s" % key
            elif key.startswith('speaker'):
                continue
            if param:
                filters[key] = param
            # хот фикс ---------------------

        # filters = {}


        return self.filter(**filters)


class FilteredQuerySet(QuerySet, FilteredMixin):
    pass


class FilteredManager(models.Manager, FilteredMixin):
    use_for_related_fields = True
    def get_queryset(self):
        return FilteredQuerySet(self.model)
