from custom_user.models import CustomUser
from django.contrib.auth.models import Group, Permission
from django.contrib.contenttypes.models import ContentType
content_type = ContentType.objects.get_for_model(CustomUser)
# permission = Permission.objects.create(codename='can_create_product',
#                                    name=u"Создание продукта",
#                                    content_type=content_type)
custom_permission_list =(
        ("can_change_product", u"Изменить продукт"),
        ("can_change_view_product", u"Изменение отображение продукта"),
        ("can_delete_product", u"Удаление продукта"),
        ("can_recovery_product", u"Восстановление продукта"),
        ("can_read_logs", u"Чтение логов (действия пользователей)"),
        ("can_analityc_constructor", u"Конструктор аналитики"),
        ('transformer', u"Трансформер"),
        ('can_import_file',u'Импорт данных'),
        ('can_constructor_w0', u'Конструктро В0'),
        ('can_see_product', u'Видимость продуктов'),
        ('can_rate_the_product', u'Оценить продукт'),
        ('can_change_self_profile',u'Изменение профиля(свой)'),
        ('can_change_all_profile',u'Изменение профиля(все)'),
        ('can_add_permission',u'Назначения прав'),
)
for elem in custom_permission_list:
    try:
        permission = Permission.objects.get_or_create(codename=elem[0],
                                       name=elem[1],
                                       content_type=content_type)
    except:
        pass