#!/bin/bash

case "$1" in
"start")
/root/.virtualenvs/exlibris/bin/python manage.py runfcgi method=threaded host=127.0.0.1 port=443 pidfile=/tmp/info_server.pid;;
"stop") 
kill -9 `cat /tmp/info_server.pid`
;;
"restart")
$0 stop
sleep 1
$0 start
;;
*) echo "Usage: ./server.sh {start|stop|restart}";;
esac
