# --*-- coding: utf-8 --*--
import xml.etree.ElementTree as ET
import datetime
from django.db import models
from xml.dom.minidom import parseString

# Create your models here.
from mptt.models import MPTTModel
from django.db.models.signals import post_save
#from views import import_file
import xlrd
from info.settings import MEDIA_URL, MEDIA_ROOT

FILE_TYPE = (
    ('1', 'xls'),
    ('2', 'xml type 1')
)

PROCESSING = (
    ('1', u'Фай в стадии загрузки'),
    ('2', u'Файл в обработке'),
    ('3', u'Файл обработан')
)

ACCESS = (
    ('1', u'Приватный'),
    ('2', u'Публичный'),
)


class Project(models.Model):
    title = models.CharField(max_length= 200, blank = True, null = True)
    slug = models.CharField(max_length=200,verbose_name=u'Слаг',unique=True,db_index=True)
    login = models.CharField(max_length=200, verbose_name=u'Логин', null=True, blank=True)
    access = models.CharField(max_length=30, verbose_name=u'Доступ к порталу', choices=ACCESS)
    password = models.CharField(max_length=200, verbose_name=u'Пароль', null=True, blank=True)
    class Meta:
        verbose_name = u'Проект'
        verbose_name_plural = u'Проекты'

    def __unicode__(self):
        return self.title

class ImportFile(models.Model):
    file_import = models.FileField(upload_to='upload_to/', verbose_name=u'Файл для импорта')
    date = models.DateTimeField(auto_now_add=True, verbose_name=u'Время импорта')
    file_type = models.CharField(max_length=200, verbose_name=u'Тип импортируемого файла', choices= FILE_TYPE)
    proccesing = models.CharField(max_length=200, verbose_name=u'Обработка файла',  choices= PROCESSING, blank= True, null= True)
    project = models.ForeignKey(Project, verbose_name=u'Проект', blank= True, null= True)
    class Meta:
        verbose_name = u'Импорт файла'
        verbose_name_plural = u'Импорт файла'

    #def __unicode__(self):
    #    return self.date


class MediaLevel(models.Model):
    title = models.CharField(max_length=200, verbose_name=u'Название', blank= True, null= True )
    project = models.ForeignKey(Project, verbose_name=u'Проект', blank= True, null= True)

    class Meta:
        verbose_name = u'Уровень СМИ'
        verbose_name_plural = u'Уровени СМИ'

    def __unicode__(self):
        return self.title


class FederalDistrict(models.Model):
    title = models.CharField(max_length=200, verbose_name=u'Название', )
    project = models.ForeignKey(Project, verbose_name=u'Проект')

    class Meta:
        verbose_name = u'Федеральный округ'
        verbose_name_plural = u'Федеральные округа'

    def __unicode__(self):
        return self.title


class Region(models.Model):
    title = models.CharField(max_length=200, verbose_name=u'Название', )
    project = models.ForeignKey(Project, verbose_name=u'Проект')

    class Meta:
        verbose_name = u'Регион'
        verbose_name_plural = u'Регионы'

    def __unicode__(self):
        return self.title


class Country(models.Model):
    title = models.CharField(max_length=200, verbose_name=u'Название', )
    project = models.ForeignKey(Project, verbose_name=u'Проект')

    class Meta:
        verbose_name = u'Страна'
        verbose_name_plural = u'Страны'

    def __unicode__(self):
        return self.title


class MediaView(models.Model):
    title = models.CharField(max_length=200, verbose_name=u'Название', )
    project = models.ForeignKey(Project, verbose_name=u'Проект')

    class Meta:
        verbose_name = u'Вид'
        verbose_name_plural = u'Виды СМИ'

    def __unicode__(self):
        return self.title


class MediaList(models.Model):
    title = models.CharField(max_length=200, verbose_name=u'Название', )
    project = models.ForeignKey(Project, verbose_name=u'Проект')

    class Meta:
        verbose_name = u'Список'
        verbose_name_plural = u'Списки СМИ'

    def __unicode__(self):
        return self.title


class MediaCategory(models.Model):
    title = models.CharField(max_length=200, verbose_name=u'Название', )
    project = models.ForeignKey(Project, verbose_name=u'Проект')

    class Meta:
        verbose_name = u'Категорию'
        verbose_name_plural = u'Категории СМИ'

    def __unicode__(self):
        return self.title


class MaterialGenre(models.Model):
    title = models.CharField(max_length=200, verbose_name=u'Название', )
    project = models.ForeignKey(Project, verbose_name=u'Проект')

    class Meta:
        verbose_name = u'Жанр'
        verbose_name_plural = u'Жанры материала'

    def __unicode__(self):
        return self.title


class InteractionKind(models.Model):
    title = models.CharField(max_length=200, verbose_name=u'Название', )
    project = models.ForeignKey(Project, verbose_name=u'Проект')

    class Meta:
        verbose_name = u'Вид взаемодействия'
        verbose_name_plural = u'Вид взаемодействия'

    def __unicode__(self):
        return self.title

class Penetration(models.Model):
    title = models.CharField(max_length=200, verbose_name=u'Название', )
    project = models.ForeignKey(Project, verbose_name=u'Проект')

    class Meta:
        verbose_name = u'Степень проникновения ПР/Новости'
        verbose_name_plural = u'Степень проникновения ПР/Новости'

    def __unicode__(self):
        return self.title


class Product(models.Model):
    title = models.CharField(max_length=200, verbose_name=u'Название', blank= True, null= True )
    project = models.ForeignKey(Project, verbose_name=u'Проект', blank= True, null= True)
    index_sort = models.IntegerField(verbose_name=u'Индекс сортировки', default= 1)

    class Meta:
        verbose_name = u'Продукт'
        verbose_name_plural = u'Продукты'

    def __unicode__(self):
        return self.title
class MentionProductValue(models.Model):
    title = models.CharField(max_length=200, verbose_name=u'Значение(как упоминаеться продукт)', blank= True, null= True )
    project = models.ForeignKey(Project, verbose_name=u'Проект', blank= True, null= True)
    article = models.ForeignKey('Article', verbose_name= u'Статья', blank= True, null= True)
    class Meta:
        verbose_name = u'Значение(как упоминаеться продукт)'
        verbose_name_plural = u'Значение(как упоминаеться продукт)'

    def __unicode__(self):
        return self.title

class MentionProduct(models.Model):
    product = models.ForeignKey(Product, verbose_name=u'Продукт', blank= True, null= True)
    mention_product_value = models.ForeignKey(MentionProductValue, verbose_name= u'как упоминаеться продукт', blank= True, null= True)
    article = models.ForeignKey('Article', verbose_name= u'Статья', blank= True, null= True)

    class Meta:
        verbose_name = u'Упоминание продукта'
        verbose_name_plural = u'Упоминания продукта'

    def __unicode__(self):
        return self.title


class KMKeyValue(models.Model):
    kay = models.IntegerField( verbose_name=u'Ключ', blank= True, null= True)
    value = models.TextField(verbose_name= u'Полное описание значения', blank= True, null= True)
    project = models.ForeignKey(Project, verbose_name=u'Проект', blank= True, null= True)

    class Meta:
        verbose_name = u'KM Значение'
        verbose_name_plural = u'KM Значения'

    #def __unicode__(self):
    #    return self.kay


class KM(models.Model):
    title = models.CharField(max_length=400, verbose_name=u'Значение', blank= True, null= True)
    project = models.ForeignKey(Project, verbose_name=u'Проект', blank= True, null= True)
    index_sort = models.IntegerField(verbose_name=u'Индекс сортировки', default= 1)
    class Meta:
        verbose_name = u'KM'
        verbose_name_plural = u'KM'

    def __unicode__(self):
        return self.project.title


class KMMention(models.Model):
    km = models.ForeignKey(KM, verbose_name=u'KM', blank= True, null= True)
    key = models.ForeignKey(KMKeyValue, verbose_name=u'Ключ (полное описание)', blank= True, null= True)
    article = models.ForeignKey('Article', verbose_name= u'Статья', blank= True, null= True)
    project = models.ForeignKey(Project, verbose_name= u'Проект', blank= True, null= True)
    class Meta:
        verbose_name = u'Упоминание KM'
        verbose_name_plural = u'Упоминание KM'

class Visibility(models.Model):
    title = models.CharField(max_length=200, verbose_name=u'Название', )
    project = models.ForeignKey(Project, verbose_name=u'Проект')
    class Meta:
        verbose_name = u'Видимость, проверить'
        verbose_name_plural = u'Видимость, проверить'

    def __unicode__(self):
        return self.title

class Macroregion(models.Model):
    title = models.CharField(max_length=200, verbose_name=u'Название', )
    project = models.ForeignKey(Project, verbose_name=u'Проект')
    class Meta:
        verbose_name = u'Заметность'
        verbose_name_plural = u'Заметность'

    def __unicode__(self):
        return self.title


class MediaListSpecial(models.Model):
    title = models.CharField(max_length=200, verbose_name=u'Название', )
    project = models.ForeignKey(Project, verbose_name=u'Проект')
    class Meta:
        verbose_name = u'Media list(spec)'
        verbose_name_plural = u'Media list(spec)'

    def __unicode__(self):
        return self.title


class MediaTypeSpecial(models.Model):
    title = models.CharField(max_length=200, verbose_name=u'Название', )
    project = models.ForeignKey(Project, verbose_name=u'Проект')
    class Meta:
        verbose_name = u'Media list(spec)'
        verbose_name_plural = u'Media list(spec)'

    def __unicode__(self):
        return self.title


class MediaCategorySpecial(models.Model):
    title = models.CharField(max_length=200, verbose_name=u'Название', )
    project = models.ForeignKey(Project, verbose_name=u'Проект')
    class Meta:
        verbose_name = u'Media category(spec)'
        verbose_name_plural = u'Media category(spec)'

    def __unicode__(self):
        return self.title


class VisibilitySpecial(models.Model):
    title = models.CharField(max_length=200, verbose_name=u'Название', )
    project = models.ForeignKey(Project, verbose_name=u'Проект')
    class Meta:
        verbose_name = u'Заметность(spec)'
        verbose_name_plural = u'Заметность(spec)'

    def __unicode__(self):
        return self.title


class KMSpecial(models.Model):
    title = models.CharField(max_length=200, verbose_name=u'Название', )
    project = models.ForeignKey(Project, verbose_name=u'Проект')
    class Meta:
        verbose_name = u'KM(spec)'
        verbose_name_plural = u'KM(spec)'

    def __unicode__(self):
        return self.title


class BrandOrProduct(models.Model):
    title = models.CharField(max_length=200, verbose_name=u'Название', )
    project = models.ForeignKey(Project, verbose_name=u'Проект')
    class Meta:
        verbose_name = u'Brand or Product Mention'
        verbose_name_plural = u'Brand or Product Mention'

    def __unicode__(self):
        return self.title


class SpeakerQuote(models.Model):
    title = models.CharField(max_length=200, verbose_name=u'Название', )
    project = models.ForeignKey(Project, verbose_name=u'Проект')
    class Meta:
        verbose_name = u'Speaker’s Quote'
        verbose_name_plural = u'Speaker’s Quote'

    def __unicode__(self):
        return self.title


class ThirdPartySpeakers(models.Model):
    title = models.CharField(max_length=200, verbose_name=u'Название', )
    project = models.ForeignKey(Project, verbose_name=u'Проект')
    class Meta:
        verbose_name = u'Third Party Speakers’ Supporting Quotes'
        verbose_name_plural = u'Third Party Speakers’ Supporting Quotes'

    def __unicode__(self):
        return self.title


class HeaderTonality(models.Model):
    title = models.CharField(max_length=200, verbose_name=u'Название', )
    project = models.ForeignKey(Project, verbose_name=u'Проект')
    class Meta:
        verbose_name = u'Header Tonality'
        verbose_name_plural = u'Header Tonality'

    def __unicode__(self):
        return self.title


class MainTonality(models.Model):
    title = models.CharField(max_length=200, verbose_name=u'Название', )
    project = models.ForeignKey(Project, verbose_name=u'Проект')
    class Meta:
        verbose_name = u'Main Tonality'
        verbose_name_plural = u'Main Tonality'

    def __unicode__(self):
        return self.title

class GlobalLocalEvent(models.Model):
    title = models.CharField(max_length=200, verbose_name=u'Название', )
    project = models.ForeignKey(Project, verbose_name=u'Проект')
    class Meta:
        verbose_name = u'Global/Local Event'
        verbose_name_plural = u'Global/Local Event'

    def __unicode__(self):
        return self.title

class EmotionalToneMaterial(models.Model):
    title = models.CharField(max_length=200, verbose_name=u'Название', )
    project = models.ForeignKey(Project, verbose_name=u'Проект')
    class Meta:
        verbose_name = u'Эмтон материала'
        verbose_name_plural = u'Эмтон материала'

    def __unicode__(self):
        return self.title

class EmotionalToneTitle(models.Model):
    title = models.CharField(max_length=200, verbose_name=u'Название', )
    project = models.ForeignKey(Project, verbose_name=u'Проект')
    class Meta:
        verbose_name = u'Эмтон Заголовка'
        verbose_name_plural = u'Эмтон Заголовка'

    def __unicode__(self):
        return self.title

class Image(models.Model):
    title = models.CharField(max_length=200, verbose_name=u'Название', )
    project = models.ForeignKey(Project, verbose_name=u'Проект')
    class Meta:
        verbose_name = u'Image'
        verbose_name_plural = u'Image'

    def __unicode__(self):
        return self.title

class MediaType(models.Model):
    title = models.CharField(max_length=200, verbose_name=u'Название', )
    project = models.ForeignKey(Project, verbose_name=u'Проект')
    class Meta:
        verbose_name = u'Вид сми'
        verbose_name_plural = u'Вид сми'

    def __unicode__(self):
        return self.title

class Category(MPTTModel):
    parent = models.ForeignKey('self', null=True, blank=True, verbose_name=u'Подкатегория')
    title = models.CharField(max_length=200, verbose_name=u'Название категории', null=True, blank=True,)

    class Meta:
        verbose_name = u'Категорию'
        verbose_name_plural = u'Категории'

    def __unicode__(self):
        return self.title

class EdgeRegion(models.Model):
    title = models.CharField(max_length=200, verbose_name=u'Название', null=True, blank=True,  )
    project = models.ForeignKey(Project, verbose_name=u'Проект', null=True, blank=True, )
    class Meta:
        verbose_name = u'Область край'
        verbose_name_plural = u'Область край'

    def __unicode__(self):
        return self.title

class Article(models.Model):
    monitoring_product = models.ManyToManyField('monitoring_product.MonitoringProduct', verbose_name=u'файл импорта', blank= True, null= True)
    project = models.ForeignKey(Project, verbose_name=u'Проект', null=True, blank=True, )
    date = models.DateField(verbose_name=u'Дата', blank= True, null= True)
    day = models.IntegerField(verbose_name=u'День', blank= True, null= True)
    week = models.IntegerField(verbose_name=u'Неделя', blank= True, null= True)
    month = models.IntegerField(verbose_name=u'Месяц', blank= True, null= True)
    media_level = models.ForeignKey(MediaLevel, verbose_name=u'Уровень СМИ', blank= True, null= True)
    federal_district = models.ForeignKey(FederalDistrict, verbose_name=u'Федеральный округ', blank= True, null= True)
    region = models.ForeignKey(Region, verbose_name=u'Регион', blank= True, null= True)
    country = models.ForeignKey(Country, verbose_name=u'Страна', blank= True, null= True)
    media_view = models.ForeignKey(MediaView, verbose_name=u'Вид СМИ', blank= True, null= True)
    media_list = models.ForeignKey(MediaList, verbose_name=u'Спосиок СМИ', blank= True, null= True)
    media_category = models.ForeignKey(MediaCategory, verbose_name=u'Категория СМИ', blank= True, null= True)
    material_genre = models.ForeignKey(MaterialGenre, verbose_name=u'Жанр материала', blank= True, null= True)
    image = models.ForeignKey(Image, verbose_name= u'Image', blank= True, null= True)
    media_type = models.ForeignKey(MediaType, verbose_name= u'Media type', blank= True, null= True)
    #mentions_object = models.ForeignKey(MentionsObject, verbose_name=u'Упоминаемость объекта')
    interaction_kind = models.ForeignKey(InteractionKind, verbose_name=u'Вид взаемодействия', blank= True, null= True)
    penetration = models.ForeignKey(Penetration, verbose_name= u'Степень проникновения ПР/Новости', blank= True, null= True)
    visibility = models.ForeignKey(Visibility, verbose_name= u'Заметность', blank= True, null= True)
    macroregion = models.ForeignKey(Macroregion, verbose_name= u'Macroregion', blank= True, null= True)
    media_list_special = models.ForeignKey(MediaListSpecial, verbose_name= u'Media list(spec)', blank= True, null= True)
    media_type_special = models.ForeignKey(MediaTypeSpecial, verbose_name= u'Media type(spec)', blank= True, null= True)
    media_category_special = models.ForeignKey(MediaCategorySpecial, verbose_name= u'Media category(spec)', blank= True, null= True)
    visibility_special = models.ForeignKey(VisibilitySpecial, verbose_name= u'Заметность(spec)', blank= True, null= True)
    km_special = models.ForeignKey(KMSpecial, verbose_name= u'KM(spec)', blank= True, null= True)
    brand_or_product = models.ForeignKey(BrandOrProduct, verbose_name= u'Brand or Product Mention', blank= True, null= True)
    speaker_quote = models.ForeignKey(SpeakerQuote, verbose_name= u'Speaker’s Quote', blank= True, null= True)
    third_patry_speakers = models.ForeignKey(ThirdPartySpeakers, verbose_name= u'Third Party Speakers’ Supporting Quotes', blank= True, null= True)
    headder_tonality = models.ForeignKey(HeaderTonality, verbose_name= u'Header Tonality', blank= True, null= True)
    main_tonality = models.ForeignKey(MainTonality, verbose_name=u'Main Tonality', blank= True, null= True)
    global_local_event = models.ForeignKey(GlobalLocalEvent, verbose_name= u'Global/Local Event', blank= True, null= True)
    emotional_tone_material = models.ForeignKey(EmotionalToneMaterial, verbose_name= u'Эмтон материала', blank= True, null= True)
    emotional_tone_title = models.ForeignKey(EmotionalToneTitle, verbose_name= u'Эмтон Заголовка', blank= True, null= True)
    city = models.CharField(max_length=200, verbose_name=u'Город', blank= True, null= True)
    branch = models.CharField(max_length=200, verbose_name=u'Филиал', blank= True, null= True)
    author = models.CharField(max_length=200, verbose_name=u'Автор', blank= True, null= True)
    source = models.CharField(max_length=200, verbose_name=u'Источник', blank= True, null= True)
    title = models.CharField(max_length=400, verbose_name=u'Загаловок', blank= True, null= True)
    annotation = models.TextField(verbose_name=u'Анотация', blank= True, null= True)
    description = models.TextField( verbose_name=u'Описание', blank= True, null= True)
    url = models.CharField(max_length=200 ,verbose_name=u'url', blank= True, null= True)
    bissines_direction = models.CharField(max_length=200 ,verbose_name=u'Направление бизнеса', blank= True, null= True)
    thematic_category = models.CharField(max_length=200, verbose_name=u'Тематическая категория', blank= True, null= True)
    material_reason = models.CharField(max_length=200, blank= True, null= True)
    id_press_release = models.CharField(max_length= 400, verbose_name=u'ID-пресс-релиза', blank= True, null= True)
    km_result = models.BooleanField(verbose_name=u'KM_Итог', default=False)
    tag = models.CharField(max_length=200, verbose_name= u'Таг', blank= True, null= True)
    duration = models.CharField(max_length=200, verbose_name= u'Длительность', blank= True, null= True)
    release_time = models.CharField(max_length=200, verbose_name=u'Время выхода', blank= True, null= True)
    cover = models.FloatField(verbose_name=u'Обложка', blank= True, null= True)
    infographics = models.FloatField(verbose_name=u'Инфографика', blank= True, null= True)
    mo = models.CharField(max_length=200, verbose_name=u'MO', blank= True, null= True)
    category = models.ForeignKey(Category, blank= True, null= True, verbose_name=u'Категория')
    edge_region = models.ForeignKey(EdgeRegion, verbose_name= u'Область край', blank= True, null= True )

    class Meta:
        verbose_name = u'Статью'
        verbose_name_plural = u'Статьи'

    #def __unicode__(self):
    #    return self.title




class ReferredObject(models.Model):
    title = models.CharField(max_length=200, verbose_name=u'Название', )
    project = models.ForeignKey(Project, verbose_name=u'Проект')

    class Meta:
        verbose_name = u'Упоминаемость объекта'
        verbose_name_plural = u'Упоминаемость объекта'

    def __unicode__(self):
        return self.title


class EmotionalToneObject(models.Model):
    title = models.CharField(max_length=200, verbose_name=u'Название', )
    project = models.ForeignKey(Project, verbose_name=u'Проект')


    class Meta:
        verbose_name = u'Эмтон объекта'
        verbose_name_plural = u'Эмтон объекта'

    def __unicode__(self):
        return self.title


class EmotionalToneTitleObject(models.Model):
    title = models.CharField(max_length=200, verbose_name=u'Название', )
    project = models.ForeignKey(Project, verbose_name=u'Проект')


    class Meta:
        verbose_name = u'Эмтон заголовка упоминаемого обьекта'
        verbose_name_plural = u'Эмтон заголовка упоминаемого обьекта'

    def __unicode__(self):
        return self.title


class ObjectRole(models.Model):
    title = models.CharField(max_length=200, verbose_name=u'Название', )
    project = models.ForeignKey(Project, verbose_name=u'Проект')


    class Meta:
        verbose_name = u'Роль Объекта'
        verbose_name_plural = u'Роль Объекта'

    def __unicode__(self):
        return self.title

class Technologies(models.Model):
    title = models.CharField(max_length=200, verbose_name=u'Название', )
    project = models.ForeignKey(Project, verbose_name=u'Проект')


    class Meta:
        verbose_name = u'Технологию'
        verbose_name_plural = u'Технологии'

    def __unicode__(self):
        return self.title


class AdvertisementPromo(models.Model):
    title = models.CharField(max_length=200, verbose_name=u'Название', )
    project = models.ForeignKey(Project, verbose_name=u'Проект')


    class Meta:
        verbose_name = u'реклама/промо'
        verbose_name_plural = u'реклама/промо'

    def __unicode__(self):
        return self.title


class Initiation(models.Model):
    title = models.CharField(max_length=200, verbose_name=u'Название', )
    project = models.ForeignKey(Project, verbose_name=u'Проект')


    class Meta:
        verbose_name = u'Инициирование'
        verbose_name_plural = u'Инициирование'

    def __unicode__(self):
        return self.title


class MentionsObject(models.Model):
    article = models.ForeignKey(Article, verbose_name=u'Статья', blank= True, null= True)
    project = models.ForeignKey(Project, verbose_name=u'Проект', blank= True, null= True)
    referred_object = models.ForeignKey(ReferredObject, blank= True, null= True)
    emotional_tone_object = models.ForeignKey(EmotionalToneObject, verbose_name=u'Эмтон объекта', blank= True, null= True)
    emotional_tone_title = models.ForeignKey(EmotionalToneTitleObject, verbose_name=u'Эмтон заголовка', blank= True, null= True)
    object_role = models.ForeignKey(ObjectRole, verbose_name=u'Роль Объекта', blank= True, null= True)
    technologies = models.ManyToManyField(Technologies, verbose_name= u'Технологии', blank= True, null= True)
    advertisement_promo = models.ForeignKey(AdvertisementPromo, verbose_name= u'реклама/промо', blank= True, null= True)

    initiation = models.ForeignKey(Initiation, verbose_name= u'Инициирование', blank= True, null= True)
    info_reason = models.CharField(max_length= 400, verbose_name= u'', blank= True, null= True)
    prt = models.FloatField(verbose_name= u'PRT', blank= True, null= True)
    prv = models.FloatField(verbose_name= u'PRV', blank= True, null= True)
    mo = models.IntegerField(verbose_name= u'MO', blank= True, null= True)
    mq = models.FloatField(verbose_name= u'MQ', blank= True, null= True)

    class Meta:
        verbose_name = u'Упоминаемый обьект'
        verbose_name_plural = u'Упоминаемые обьекты'

    #def __unicode__(self):
    #    return self.title



class SpeakerCategory(models.Model):
    title = models.CharField(max_length=200, verbose_name=u'Название', )
    project = models.ForeignKey(Project, verbose_name=u'Проект')


    class Meta:
        verbose_name = u'Категорию спикера'
        verbose_name_plural = u'Категории спикера'

    def __unicode__(self):
        return self.title


class SpeakerLevel(models.Model):
    title = models.CharField(max_length=200, verbose_name=u'Название', )
    project = models.ForeignKey(Project, verbose_name=u'Проект')


    class Meta:
        verbose_name = u'Уровень спикера'
        verbose_name_plural = u'Уровни спикера'

    def __unicode__(self):
        return self.title


class SpeakerSpeechHate(models.Model):
    title = models.CharField(max_length=200, verbose_name=u'Название', )
    project = models.ForeignKey(Project, verbose_name=u'Проект')


    class Meta:
        verbose_name = u'Характер высказывания спикера'
        verbose_name_plural = u'Характер высказывания спикера'

    def __unicode__(self):
        return self.title


class SpeakerQuoteUniqueness(models.Model):
    title = models.CharField(max_length=200, verbose_name=u'Название', )
    project = models.ForeignKey(Project, verbose_name=u'Проект')


    class Meta:
        verbose_name = u'Уникальность цитат спикера'
        verbose_name_plural = u'Уникальность цитат спикера'

    def __unicode__(self):
        return self.title


class Speaker(models.Model):
    project = models.ForeignKey(Project, verbose_name=u'Проект', blank= True, null= True)
    article = models.ForeignKey(Article, verbose_name=u'Статья', blank= True, null= True)
    mentions_object = models.ForeignKey(MentionsObject, verbose_name=u'Упоминаемость объекта', blank= True, null= True)
    fio = models.CharField(max_length=300, verbose_name=u'ФИО', blank= True, null= True)
    speaker_category = models.ForeignKey(SpeakerCategory, verbose_name=u'Категория спикера', blank= True, null= True)
    speaker_level = models.ForeignKey(SpeakerLevel, verbose_name=u'Уровень спикера', blank= True, null= True)
    speaker_speech_hate = models.ForeignKey(SpeakerSpeechHate, verbose_name=u'Характер высказывания спикера', blank= True, null= True)
    speaker_quote_uniqueness = models.ForeignKey(SpeakerQuoteUniqueness, verbose_name=u'Уникальность цитат спикера', blank= True, null= True)

    class Meta:
        verbose_name = u'Спикера'
        verbose_name_plural = u'Спикеры'

    def __unicode__(self):
        return self.fio


class SideSpeakerTonality(models.Model):
    title = models.CharField(max_length=200, verbose_name=u'Название', )
    project = models.ForeignKey(Project, verbose_name=u'Проект')


    class Meta:
        verbose_name = u'Тональность'
        verbose_name_plural = u'Тональность'

    def __unicode__(self):
        return self.title


class SideSpeakerStatus(models.Model):
    title = models.CharField(max_length=200, verbose_name=u'Название', )
    project = models.ForeignKey(Project, verbose_name=u'Проект')


    class Meta:
        verbose_name = u'Статус'
        verbose_name_plural = u'Статуси сторонего Спикера'

    def __unicode__(self):
        return self.title


class SideSpeaker(models.Model):
    project = models.ForeignKey(Project, verbose_name=u'Проект', blank= True, null= True)
    article = models.ForeignKey(Article, verbose_name=u'Статья', blank= True, null= True)
    mentions_object = models.ForeignKey(MentionsObject, verbose_name=u'Упоминаемость объекта', blank= True, null= True)
    fio = models.CharField(max_length=300, verbose_name=u'ФИО', blank= True, null= True)
    position = models.CharField(max_length=300, verbose_name=u'Должность', blank= True, null= True)
    side_speaker_tonality = models.ForeignKey(SideSpeakerTonality, verbose_name= u'Тональность', blank= True, null= True)
    side_speaker_status = models.ForeignKey(SideSpeakerStatus, verbose_name= u'Статус', blank= True, null= True)


    class Meta:
        def __init__(self):
            pass

        verbose_name = u'Стороний Спикер'
        verbose_name_plural = u'Стороний спикер'

    def __unicode__(self):
        return self.fio


def import_file(sender, instance, created, **kwargs):
    if instance.file_type == '1':
        instance.processing = '2'
        instance.save()
        item_id = 0
        print  'type        1'
        workbook = xlrd.open_workbook(instance.file_import.path)
        worksheet = workbook.sheet_by_name('info_sheet')
        num_rows = worksheet.nrows - 1
        print num_rows
        curr_row = 0

        z = 0.1
        z = float(z)
        def get_object_value(Model, value):
            if value != '':
                return_object = Model.objects.get_or_create(title = value, project = project)[0]

            else:
                return_object = None
            return return_object
        def get_object_value_create(Model, value):
            if value != '':
                return_object = Model.objects.create(title = value, project = project)

            else:
                return_object = None
            return return_object
        project = Project.objects.get(id = instance.project.id)
        row_number = 0
        print '4444'
        print curr_row, num_rows
        while curr_row < num_rows:
            print num_rows
            curr_row += 1
            row = worksheet.row(curr_row)
            print row[310].value, '-===========---=-'
            if row[310].value != '':
                print row[11].value, '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
                try:
                    item = Article.objects.get(id = int(row[310].value))
                    item_id = item.id
                    item.delete()
                except Article.DoesNotExist:
                    pass
            quantity_row = 20
            category_number = 4
            category_count = 0
            cat = None
            print row[305].value
            for elem in range(category_number):
                row_number = 305 + category_count
                print row_number, row[row_number].value
                if row[row_number].value != '':
                    print 'create'
                    try:
                        cat = Category.objects.get_or_create(parent_id = cat[0].id, title = row[row_number].value )
                    except:
                        cat = Category.objects.get_or_create(parent_id = None, title = row[row_number].value )

                    print cat

                category_count += 1

            km_count = 0
            for elem in range(quantity_row):
                try:
                    row_number = 265 + km_count
                    value = KMKeyValue.objects.get(kay = row[row_number].value, project = project)
                    km = KM.objects.create(project = project, value = value)

                except:
                    value = None
                km_count += 1
            edge_region = get_object_value(EdgeRegion, row[309].value)
            federal_district = get_object_value(FederalDistrict, row[7].value)
            region = get_object_value(Region, row[8].value)
            country = get_object_value(Country, row[10].value)
            media_view = get_object_value(MediaView, row[13].value)
            media_list = get_object_value(MediaList, row[14].value)
            media_category = get_object_value(MediaCategory, row[15].value)
            material_genre = get_object_value(MaterialGenre, row[23].value)
            interaction_kind = get_object_value(InteractionKind, row[209].value)
            penetration = get_object_value(Penetration, row[222].value)
            visibility = get_object_value(Visibility, row[290].value)
            macroregion = get_object_value(Macroregion, row[292].value)
            media_list_special = get_object_value(MediaListSpecial, row[293].value)
            media_type_special = get_object_value(MediaTypeSpecial, row[294].value)
            media_category_special = get_object_value(MediaCategorySpecial, row[295].value)
            visibility_special = get_object_value(VisibilitySpecial, row[296].value)
            image = get_object_value(Image, row[297].value)
            km_special = get_object_value(KMSpecial, row[298].value)
            brand_or_product = get_object_value(BrandOrProduct, row[299].value)
            speakers_quote = get_object_value(SpeakerQuote, row[300].value)
            third_patry_speakers = get_object_value(ThirdPartySpeakers, row[301].value)
            headder_tonality = get_object_value(HeaderTonality, row[302].value)
            main_tonality = get_object_value(MainTonality, row[303].value)
            global_local_event = get_object_value(GlobalLocalEvent, row[304].value)
            emotional_tone_title = get_object_value(EmotionalToneTitle, row[25].value)
            emotional_tone_material = get_object_value(EmotionalToneMaterial, row[26].value)
            title = row[17].value
            cover = row[289].value
            infographics = row[291].value
            id_press_release = row[223].value
            if  cover == '':
                cover = None
            else:
                cover = float(cover)

            if  infographics == '':
                infographics = None
            else:
                infographics = float(infographics)

            try:
                category = cat[0]
            except :
                category = None

            media_type = get_object_value(MediaType, row[12].value)
            media_level = get_object_value(MediaLevel, row[5].value)
            #dt =datetime.datetime(* (xlrd.xldate_as_tuple(worksheet.cell_value(rowx=curr_row, colx=1), \
            #                  workbook.datemode)))
            if row[2].value != '':
                day =  row[2].value
            else:
                day = None
            if row[2].value != '':
                week =  row[2].value
            else:
                week = None
            if row[2].value != '':
                month =  row[2].value
            else:
                month = None


            if item_id != 0:
                article = Article.objects.create(id = item_id,title = title,project = project, media_level = media_level, media_type = media_type,  day = day, \
                month = month,week = week , city = row[6].value, federal_district = federal_district, region = region, country = country, media_view = media_view, \
                media_list = media_list, media_category = media_category, material_genre = material_genre, interaction_kind = interaction_kind, penetration = penetration, \
                visibility = visibility, macroregion = macroregion, media_list_special = media_list_special, media_type_special = media_type_special, media_category_special = media_category_special, \
                visibility_special = visibility_special, image = image,km_special = km_special, brand_or_product = brand_or_product, speaker_quote = speakers_quote, \
                third_patry_speakers = third_patry_speakers,headder_tonality = headder_tonality, main_tonality = main_tonality, global_local_event = global_local_event, \
                emotional_tone_title = emotional_tone_title, emotional_tone_material = emotional_tone_material, branch = row[9].value, author = row[11].value, source = row[16].value, \
                annotation = row[18].value, description = row[19].value  ,url = row[20].value, bissines_direction = row[21].value, thematic_category = row[22].value, \
                material_reason = row[23].value, id_press_release = id_press_release, km_result = row[285].value, tag = row[286].value, duration = row[287].value, \
                release_time = row[288].value, cover = cover, infographics = infographics, mo = row[196].value, category = category, edge_region = edge_region
                )
            else:
                article = Article.objects.create(title = title,project = project, media_level = media_level, media_type = media_type,  day = day, \
                month = month,week = week , city = row[6].value, federal_district = federal_district, region = region, country = country, media_view = media_view, \
                media_list = media_list, media_category = media_category, material_genre = material_genre, interaction_kind = interaction_kind, penetration = penetration, \
                visibility = visibility, macroregion = macroregion, media_list_special = media_list_special, media_type_special = media_type_special, media_category_special = media_category_special, \
                visibility_special = visibility_special, image = image,km_special = km_special, brand_or_product = brand_or_product, speaker_quote = speakers_quote, \
                third_patry_speakers = third_patry_speakers,headder_tonality = headder_tonality, main_tonality = main_tonality, global_local_event = global_local_event, \
                emotional_tone_title = emotional_tone_title, emotional_tone_material = emotional_tone_material, branch = row[9].value, author = row[11].value, source = row[16].value, \
                annotation = row[18].value, description = row[19].value  ,url = row[20].value, bissines_direction = row[21].value, thematic_category = row[22].value, \
                material_reason = row[23].value, id_press_release = id_press_release, km_result = row[285].value, tag = row[286].value, duration = row[287].value, \
                release_time = row[288].value, cover = cover, infographics = infographics, mo = row[196].value, category = category, edge_region = edge_region
                )
            #Product
            product_count = 1
            for el in range(17):
                if row[223 + product_count].value != '':
                    try:
                        product = Product.objects.get(index_sort = product_count)
                        mention_product_value = MentionProductValue.objects.get(title = row[223 + product_count].value)
                        mention_product = MentionProduct.objects.create(article = article, product = product, mention_product_value = mention_product_value)
                    except:
                        pass
            #KM
            km_count = 1
            for el in range(20):
                if row[264 + km_count].value != '':
                    try:
                        key = KMKeyValue.objects.get(kay = row[264 + km_count].value, project = project)

                    except:
                        key = None


                    if key != None:
                        try:
                            km = KM.objects.get(index_sort = km_count)
                        except KM.DoesNotExist:
                            km = None
                        km_mention = KMMention.objects.create(article = article, project = project, key = key, km = km)
                km_count +=1
            #упоминаемий обьект ------------------------------------------------------------

            mantion_objects = row[27].value.split(';')

            quantity_row = 12
            technologies_list = []
            i = 0
            for elem in range(quantity_row):
                row_number = 253 + i
                if row[row_number].value !='' :
                    technologies_all = row[row_number].value.split(';')
                    for elem in technologies_all:
                        technologies_list.append(elem)
                i+=1
            emotional_tone_object_list = []
            emotional_tone_title_list = []
            info_reason_list = []
            initiation_list = []
            advertisement_promo_list = []
            prt_list = []
            prv_list = []
            mq_list = []
            object_role_list = []

            i = 0
            for elem in range(quantity_row):

                row_number = 28 + i
                if row[row_number].value !='' :
                    emotional_tone_object_list.append(row[row_number].value)

                row_number = 40 + i
                if row[row_number].value !='' :
                    emotional_tone_title_list.append(row[row_number].value)

                row_number = 52 + i
                if row[row_number].value !='' :
                    object_role_list.append(row[row_number].value)

                row_number = 62 + i
                if row[row_number].value !='' :
                    info_reason_list.append(row[row_number].value)

                row_number = 210 + i
                if row[row_number].value !='' :
                    initiation_list.append(row[row_number].value)

                row_number = 241 + i
                if row[row_number].value !='' :
                    advertisement_promo_list.append(row[row_number].value)

                row_number = 172 + i
                prt_list.append(row[row_number].value)

                row_number = 184 + i
                prv_list.append(row[row_number].value)

                row_number = 197 + i
                mq_list.append(row[row_number].value)
                i+=1



            #запись упоминаемого обьекта-------------------------------------------------------
            z = 0
            k = 0
            k_side = 0
            for elem in mantion_objects:

                emotional_tone_object = get_object_value(EmotionalToneObject, emotional_tone_object_list[z])
                emotional_tone_title = get_object_value(EmotionalToneTitleObject, emotional_tone_title_list[z])
                try:
                    initiation = get_object_value(Initiation, initiation_list[z])
                except:
                    initiation = None

                try:
                    advertisement_promo = get_object_value(AdvertisementPromo, advertisement_promo_list[z])
                except:
                    advertisement_promo = None

                if  prt_list[z] == '':
                    prt = None
                else:
                    prt = float(prt_list[z])


                if  prv_list[z] == '':
                    prv = None
                else:
                    prv = float(prv_list[z])


                if  mq_list[z] == '':
                    mq = None
                else:
                    mq = float(mq_list[z])



                referred_object = get_object_value_create(ReferredObject, elem)
                object_role = get_object_value_create(ObjectRole, object_role_list[z])
                mention_object = MentionsObject.objects.create(project = project, article = article, referred_object = referred_object, \
                emotional_tone_object =  emotional_tone_object, emotional_tone_title = emotional_tone_title, object_role = object_role, info_reason = info_reason_list[z], \
                initiation = initiation, advertisement_promo = advertisement_promo, prt = prt, prv = prv, mq = mq,
                )
                technologies_number = 0
                for elem in technologies_list:
                    try:
                        technologies = get_object_value(Technologies, technologies_list[technologies_number])
                        mention_object.technologies.add(technologies)

                    except:
                        technologies = None
                    technologies_number +=1
                    mention_object.save()
                z+=1
                 #упоминаемий обьект (СПИКЕР)------------------------------------------------------------

                row_number = 76 +k

                speakers_list = row[row_number].value.split(';')
                speaker_categorys = row[row_number+1].value.split(';')
                speaker_levels = row[row_number+2].value.split(';')
                speaker_speech_hate = row[row_number+3].value.split(';')
                speaker_quote_uniqueness = row[row_number+4].value.split(';')

                speaker_quote_uniqueness_list = []
                speaker_speech_hate_list = []
                for elem in speaker_quote_uniqueness:
                    speaker_quote_uniqueness_list.append(elem)
                for elem in speaker_speech_hate:
                    speaker_speech_hate_list.append(elem)

                speaker_category_list = []
                for elem in speaker_categorys:
                    speaker_category_list.append(elem)
                speaker_level_list = []
                for elem in speaker_levels:
                    speaker_level_list.append(elem)

                speaker_num = 0
                for elem in speakers_list:


                    try:
                        speaker_category = get_object_value_create(SpeakerCategory, speaker_category_list[speaker_num])
                    except:
                        speaker_category = None
                    try:
                        speaker_level = get_object_value_create(SpeakerLevel, speaker_level_list[speaker_num])
                    except:
                        speaker_level = None
                    try:
                        speaker_speech_hate = get_object_value_create(SpeakerSpeechHate, speaker_speech_hate_list[speaker_num])
                    except:
                        speaker_speech_hate = None

                    try:
                        speaker_quote_uniqueness = get_object_value_create(SpeakerQuoteUniqueness, speaker_quote_uniqueness_list[speaker_num])
                    except:
                        speaker_quote_uniqueness = None

                    if elem != '':
                        speaker = Speaker.objects.create(fio = elem, speaker_category = speaker_category, mentions_object = mention_object, article = article, project = project, \
                        speaker_level = speaker_level,speaker_speech_hate = speaker_speech_hate, speaker_quote_uniqueness = speaker_quote_uniqueness)
                        speaker_num = speaker_num + 1
                k = k + 5
                #упоминаемий обьект (СТОРОНИЙ СПИКЕР)------------------------------------------------------------
                row_number_side = 136 + k_side
                side_speaker_count = 0
                side_speaker_status = row[row_number_side+2].value.split(';')
                side_speaker_tonality = row[row_number_side+1].value.split(';')
                side_speaker_tonality_list = []
                for elem in side_speaker_tonality:
                    side_speaker_tonality_list.append(elem)
                side_speaker_status_list = []
                for elem in side_speaker_status:
                    side_speaker_status_list.append(elem)
                if row[row_number_side].value != '':

                    side_speakers_list = row[row_number_side].value.split(';')

                    for elem in side_speakers_list:
                        try:
                            side_speaker_tonality = get_object_value(SideSpeakerTonality, side_speaker_tonality_list[side_speaker_count])

                        except:
                            side_speaker_tonality = None

                        try:
                            side_speaker_status = get_object_value(SideSpeakerStatus, side_speaker_status_list[side_speaker_count])

                        except:
                            side_speaker_status = None
                        side_speaker = elem.split(',')
                        side_speaker_fio = side_speaker[0]
                        try:
                            side_speaker_position = side_speaker[1]
                        except:
                            side_speaker_position = ''
                        if side_speaker_fio != '':
                            side_speaker = SideSpeaker.objects.create(fio = side_speaker_fio, position = side_speaker_position, article = article, mentions_object = mention_object, \
                             project = project, side_speaker_tonality = side_speaker_tonality,side_speaker_status = side_speaker_status )
                k_side = k_side + 3
        instance.processing = '3'
        instance.save()
    elif instance.file_type == '2':
        file = open(instance.file_import.path,'r')
        data = file.read()
        file.close()
        project = Project.objects.get(id = instance.project.id)
        dom = parseString(data)
        articles = dom.getElementsByTagName('ARTICLE')
        def get_item_value(item, tag_name):
            try:
                item = elem.getElementsByTagName(tag_name)[0].firstChild.data
            except:
                item = None
            return item
        def get_object_value(Model, value):
            if value != '':
                return_object = Model.objects.get_or_create(title = value, project = project)[0]

            else:
                return_object = None
            return return_object
        def get_object_value_create(Model, value):
            if value != '':
                return_object = Model.objects.create(title = value, project = project)

            else:
                return_object = None
            return return_object
        for elem in articles:
            url = get_item_value('url','URL' )
            date = get_item_value('date','DATE' )
            time = get_item_value('time','TIME' )
            author = get_item_value('author','AUTHOR' )
            title = get_item_value('title','TITLE' )
            text = get_item_value('text','TEXT' )
            town = get_item_value('town','TOWN' )
            district = get_item_value('district','DISTRICT' )
            media_view = get_item_value('media_view','MASSMEDIA' )
            mention = get_item_value('mention','MENTION' )
            annotation = get_item_value('annotation','HEADLINE' )
            section1 = get_item_value('section1','SECTION1' )
            section2 = get_item_value('section2','SECTION2' )
            section3 = get_item_value('section3','SECTION3' )
            section4 = get_item_value('section4','SECTION4' )
            date = datetime.datetime.strptime(date, '%d.%m.%Y').strftime('%Y-%m-%d')
            federal_district = get_object_value(FederalDistrict, district)
            media_view = get_object_value(MediaView, media_view)

            section_list =[section1, section2, section3, section4]

            category_number = 4
            category_count = 0
            cat = None
            for elem in range(category_number):
                if section_list[elem] != None:
                    print 'no none'
                    print category_count
                    try:

                        cat = Category.objects.get_or_create(parent_id = cat[0].id, title = section_list[category_count] )
                        print section_list[category_count]

                    except:

                        cat = Category.objects.get_or_create(parent_id = None, title = section_list[category_count] )

                category_count += 1
            try:
                category = cat[0]
            except :
                category = None
            mention_list = mention.split(';')

            article = Article.objects.create(title = title,project = project, date = date, release_time = time, author = author, description = text, \
                                             city = town, federal_district = federal_district, media_view = media_view, annotation = annotation, \
                                             category = category)
            #print title
            for elem in mention_list:
                referred_object = get_object_value_create(ReferredObject, elem)

                mention_object = MentionsObject.objects.create(project = project,article = article, referred_object = referred_object)
            print date

post_save.connect(import_file, sender=ImportFile)