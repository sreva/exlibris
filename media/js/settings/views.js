exlibris.factory('profileView', ['$modal', '$http', '$rootScope', '$cookies', function($modal, $http, $rootScope, $cookies) {
    return function($scope) {
        var templateUrl = "/angular/templates/profileModals.html";
        var modalInstance = $modal.open({
            templateUrl: templateUrl,
            windowClass: 'profileView',
            controller: function ($scope, $http, $cookies, $rootScope, $modalInstance) {
                ////
                $('<style>.profileView .modal-dialog{left:' + $('.open-profile-modal').offset().left + 'px;}</style>').appendTo('head');
                ////
                $scope.init = function(){
                    this.newSocial = false;
                    this.nameSocial = "";
                    this.titleSocial = "";
                    $http.get('/profile-api/').
                        success(function(data, status, headers, config) {
                            _.each(data, function(el, key){$scope[key]=el});
                            $rootScope.user = $scope.user;
                        });
                    $scope.delSocial = function(social_id){
                        $scope['show'+social_id] = true;
                        $http.defaults.headers.post['X-CSRFToken'] = $cookies['csrftoken'];
                        $http.post("/delete/social/" + social_id + '/', {});
                    };
                    $scope.doAddSocial = function(){
                        if(this.titleSocial && this.nameSocial) {
                            $scope.dataObject = {
                                title_social : this.titleSocial,
                                name_social : this.nameSocial
                            };
                            $scope.add_social = {
                                title : this.titleSocial,
                                link : this.nameSocial
                            };
                            this.submitTheSocial();
                        }

                    };
                    $scope.submitTheSocial = function() {
                        if(!_.findWhere($scope.social_list, $scope.add_social)){
                            $scope.social_list.push($scope.add_social);
                            this.newSocial = false;
                            this.nameSocial = "";
                            this.titleSocial = "";
                            $http.defaults.headers.post['X-CSRFToken'] = $cookies['csrftoken'];
                            $http.post("/add/social/", $scope.dataObject, {}).success(function(status, headers, config) {
                                _.findWhere($scope.social_list, $scope.add_social)['id'] = status.id;
                            });
                        }
                    };

                    // employee data
                    $scope.changeAttr = function(user, key, new_value){
                        $http.defaults.headers.post['X-CSRFToken'] = $cookies['csrftoken'];
                        user[key] = new_value;
                        var obj = {};
                        obj[key] = new_value.id;
                        $http.post("/profile/" + user.id + "/", obj);
                    };
                };
            }
        });
    }
}]);