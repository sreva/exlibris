exlibris.
    controller('dropsDown', function($scope, $http, $rootScope, dataService, dataFilter){
        $scope.source = false;
        $scope.periodicity = false;
        $scope.product = dataService.data.product[0];
        $scope.clearTime = function (){
            $scope.product.date_release_start = "";
            $scope.product.date_release_end = "";
            $scope.product.date_upload_start = "";
            $scope.product.date_upload_end = "";
            $scope.product.time_upload_start = "";
            $scope.product.time_upload_end = "";
        };
        $scope.initPeriodicity = function(){
            if($scope.product.type=='date_release'){
                $scope.product.date_release_start = $scope.product.date_start;
                $scope.product.date_release_end = $scope.product.date_end;
            }
            if($scope.product.type=='date_upload'){
                $scope.product.date_upload_start = $scope.product.date_start;
                $scope.product.date_upload_end = $scope.product.date_end;
                $scope.product.time_upload_start = "00:00:00";
                $scope.product.time_upload_end = "00:00:00";
            }
        };
        $scope.openSource = function (){
            $scope.source = !$scope.source;
            $scope.periodicity = false;
        };
        $scope.loadSources = function (){
            if (!$scope.product.parent_id){
                $scope.sources = [];
                $scope.current_source = {id: parseInt($rootScope.productId), title: $scope.product.title};
                return;
            }
            $http({
                method: 'GET',
                url: '/' + project_slug + '/monitoring_product/load_sources/'
            }).success(function(data, status, headers, config){
                $scope.sources = data;
                $scope.current_source = _.findWhere($scope.sources, {
                    id: parseInt($scope.product.parent_id)
                });
            });
        };
        $scope.changeSources = function (id){
            $scope.current_source = _.findWhere($scope.sources, {id: id});
            $rootScope.productId = id;
            $scope.source = !$scope.source;
            var parent_scope = angular.element($('[ng-controller=mainsWidget]')[0]).scope();
            $http.get('http://'+ window.location.host +'/monitoring-product/' + id + '/').success(function(res){
                dataService.data = res;
                dataService.lastId = res.last_id;
                dataService.count_end = res.count_end;
                parent_scope.product = dataService.data.product[0];
                parent_scope.dataService = dataService;

                parent_scope.generalFilters = dataService.data.product[0].referred_objects;
                $rootScope.dateStart = angular.copy(dataService.data.product[0].date_start.replace(/\-+/g, '.'));
                $rootScope.dateEnd = angular.copy(dataService.data.product[0].date_end.replace(/\-+/g, '.'));

                $rootScope.timeStart = dataService.data.product[0].time_start;
                $rootScope.timeEnd = dataService.data.product[0].time_end;

                parent_scope.dateType = dataService.data.product[0].type;
                parent_scope.title = dataService.data.product[0].title;
                if (parent_scope.dateType == "day") {
                    parent_scope.daysCount = 1;
                } else if (parent_scope.dateType == "week") {
                    parent_scope.daysCount = 7;
                } else if (parent_scope.dateType == "month") {
                    parent_scope.daysCount = 30;
                } else if (parent_scope.dateType == "date_release") {
                    parent_scope.product.date_release_start = $rootScope.dateStart;
                    parent_scope.product.date_release_end = $rootScope.dateEnd;
                } else if (parent_scope.dateType == "date_upload") {
                    parent_scope.product.date_upload_start = $rootScope.dateStart;
                    parent_scope.product.date_upload_end = $rootScope.dateEnd;
                }
                var menu = dataService.data.monitoring_product_template;
            });
            dataFilter.filter($rootScope.productId, $rootScope.productSlug).then(function(res){
                //_.each(res.filters.referred_object__id__in.list, function(el){
                //    el.mention_object = _.findWhere(res.mention_objects_list, {id: el.id});
                //})
                dataFilter.data = res;

                // other filters
                parent_scope.newFilters = dataFilter.data.filters;
                // sort by order
                parent_scope.sortableNewFilters = [];
                for (var key in parent_scope.newFilters) {
                    if (!parent_scope.newFilters[key].settings.a3) parent_scope.newFilters[key].settings.a3 = Infinity;
                    parent_scope.sortableNewFilters.push([key, parent_scope.newFilters[key]]);
                }
                parent_scope.sortableNewFilters.sort(function(a, b) {
                    return a[1].settings.a3 - b[1].settings.a3
                });
                // скрыть ФО
                parent_scope.hideFederalDistrict = false;

                (function (s){
                    s.show_slider = false;
                    setTimeout(function(){
                        s.show_slider = true;
                        s.$apply();
                    }, 500);
                })(angular.element($('[ng-controller=PersonalTabs]')[0]).scope());

            });
            console.log(id);
        };
        $scope.openPeriodicity = function (){
            $scope.periodicity = !$scope.periodicity;
            $scope.source = false;
        };
        $scope.setProductType = function (productType){
            $scope.periodicity = !$scope.periodicity;
            $scope.source = false;
            if (productType == 'period'){
                productType = 'date_release';
            }
            $scope.product.type = productType;
        };
    }).
     controller('accessCtrl', function($rootScope, $scope){
        $scope.addOtdel = function(){
            var list = document.getElementById('list-people');
            var newDiv = document.createElement('div');
            newDiv.className = 'block-access';
            newDiv.innerHTML = ' <p class="otdel-access noselect">Отдел PR<span ng-click="delOtdel()">X</span></p>';
            list.appendChild(newDiv);
        };
        $scope.openOtdel = function(){
            $scope.otdel = !$scope.otdel;
            $scope.people = false;
        };
        $scope.openPeople = function(){
            $scope.people = !$scope.people;
            $scope.otdel = false;
        };
        $scope.addPeople = function(){
            var list = document.getElementById('list-people');
            var newDiv = document.createElement('div');
            newDiv.className = 'block-access';
            newDiv.innerHTML = ' <p class="people-access noselect">Люди PR<span ng-click="delPeople()">X</span></p>';
            list.appendChild(newDiv);
        };
        $scope.delOtdel = function(){
            console.log('asdasd');
        };

        $scope.userAdd = function(id){
            $scope.usersAccess = _.filter($scope.company.users, function(e){return e.selected});
            $scope.selected_departments = _.each($scope.company.departments, function(el){
                el.users = _.where($scope.usersAccess, {department:el.id});
            });
            $scope.selected_departments = _.filter($scope.selected_departments, function(el){return el.users.length});
        };

        $scope.departmentAdd = function(id, title){
            var display_all = _.reduce($scope.company.departments, function(memo, el){ return memo || el.selected; }, false);
            if (!display_all){
                $scope.users = $scope.company.users.slice();
            }
            else {
                var selected = _.map(_.filter($scope.company.departments, function(el){return el.selected}), function(el){return el.id});
                $scope.users = _.reject($scope.company.users, function(el){return !(selected.indexOf(el.department)>-1)}).slice();
            }
        };
    }).
    //controller('PersonalTabs', function ($scope, $rootScope, $http, $cookies, modalConfirm, product_settings, dataAccessUser) {
    //    $scope.currentProfileTab = 'profile.html';
    //    $scope.currentSettingTab = 'amendment.html';
    //    $scope.$on("update_parent_tab", function(event, message){
    //        $scope.currentProfileTab = message;
    //    });
    //    $scope.onClickTab = function (tab) {
    //        $scope.currentProfileTab = tab;
    //    };
    //    $scope.isActiveTab = function(tabUrl) {
    //        return tabUrl == $scope.currentProfileTab;
    //    };
    //    $scope.main_list = [
    //        {name: 'source', text: 'Источник',id:1 },
    //        {name: 'date_up', text: 'Дата',id:2},
    //        {name: 'release_time', text: 'Время',id:3},
    //        {name: 'title', text: 'Заголовок',id:4},
    //        {name: 'author', text: 'Автор',id:5},
    //        {name: 'description', text: 'Полнотекст',id:6},
    //        {name: 'annotation', text: 'Аннотация',id:7},
    //        {name: '?????', text: 'Перепечатки',id:8},
    //        {name: 'url', text: 'Url',id:9},
    //        {name: 'mo', text: 'Media outreach',id:10}
    //      ];
    //    $scope.media_list = [
    //        {name: 'media_level', text: 'Уровень',id:21 },
    //        {name: 'media_type', text: 'Тип',id:22},
    //        {name: 'media_category', text: 'Категория',id:25},
    //        {name: 'media_view', text: 'Вид',id:23}
    //      ];
    //    $scope.geography_list = [
    //        {name: 'country', text: 'Страна',id:31 },
    //        {name: 'federal_district', text: 'Федеральный округ',id:32},
    //        {name: 'city', text: 'Город',id:35}
    //      ];
    //    $scope.indicators_list = [
    //        {name: 'prv', text: 'PRV',id:41 },
    //        {name: 'prt', text: 'PRt',id:42},
    //        {name: 'mo', text: 'Media outreach',id:43},
    //        {name: 'emotional_tone_object', text: 'Тональность',id:44}
    //      ];
    //    $scope.mention_list = [
    //        {name: 'title', text: 'Объекты',id:51 },
    //        {name: 'speakers', text: 'ФИО спикера',id:52}
    //
    //      ];
    //    $scope.date_checked = {
    //        main_list: [$scope.main_list[1]],
    //        media_list :[$scope.media_list[1]],
    //        geography_list :[$scope.geography_list[1]],
    //        indicators_list :[$scope.indicators_list[1]],
    //        mention_list :[$scope.mention_list[1]]
    //      };
    //
    //    if(Object.getOwnPropertyNames(product_settings.settings).length === 0){
    //        $scope.date_checked = {
    //        main_list: $scope.main_list,
    //        media_list :$scope.media_list,
    //        geography_list :$scope.geography_list,
    //        indicators_list :$scope.indicators_list,
    //        mention_list :$scope.mention_list
    //    };
    //    }else {
    //        $scope.date_checked = {
    //        main_list: product_settings.settings.main_list,
    //        media_list :product_settings.settings.media_list,
    //        geography_list :product_settings.settings.geography_list,
    //        indicators_list :product_settings.settings.indicators_list,
    //        mention_list :product_settings.settings.mention_list
    //    };
    //    }
    //    $scope.saveSettings = function(){
    //        $http.defaults.headers.post['X-CSRFToken'] = $cookies['csrftoken'];
    //        var responsePromise = $http.post("/save-product-settings/2/", {settings: $scope.date_checked});
    //    };
    //    $scope.onClickSetTab = function (tab) {
    //        $scope.currentSettingTab = tab;
    //    };
    //    $scope.isActiveSetTab = function(tabUrl) {
    //        return tabUrl == $scope.currentSettingTab;
    //    };
    //    $scope.changeAvailability = function() {
    //        var confirm = function(){
    //            $scope.available_only_for_me = !$scope.available_only_for_me;
    //        };
    //        if (!$scope.available_only_for_me){
    //            var modal_data = {title: "Точно хотите сделать доступным только для вас?", question: "Вы действительно хотите сделать продукт доступным только Вам? Ваши коллеги не будут иметь доступак даному продукту."};
    //            modalConfirm($scope, confirm, "dont_ask_if_make_product_availible_only_for_me", modal_data);
    //        }
    //        else {
    //            confirm();
    //        }
    //    };
    //    $scope.saveHavingAccessUsers = function(){
    //        var data = {users: _.map($scope.usersAccess, function(el){return el.id})};
    //        $http.defaults.headers.post['X-CSRFToken'] = $cookies['csrftoken'];
    //        $http.post('/monitoring_product/set_users_access/' + $rootScope.productId + '/', data).success(function(res) {
    //            console.log("Access to product was updated");
    //        });
    //    };
    //}).
    controller('filterUpdateNew', function($scope){
         $scope.toggleDatePicker = function($event, variable) {
             $event.preventDefault();
             $event.stopPropagation();
             this.openedStart = false;
             this.openedEnd = false;
             this.openedStartRelease = false;
             this.openedEndRelease = false;
             this[variable] = true;
        }
    }).
    controller('mainsWidget', function($scope, $rootScope, $http, $cookies, $location, dataService, dataFilter, profileView, modalConfirm){
      var pagination_obj = dataService.data.article_pagination_list;

      $rootScope.items_list = [];
      $rootScope.totalItems = dataService.data.articles_count;
      $rootScope.PerPage = pagination_obj.articles_on_page; // this should match however many results your API puts on one page


      $rootScope.pagination = {
          current: pagination_obj.number
      };

      $rootScope.pageChanged = function(newPage) {
          getResultsPage(newPage);
      };

      function getResultsPage(pageNumber) {
          // this is just an example, in reality this stuff should be in a service
            $scope.showFilteredArticles(true, pageNumber);

         }
          $scope.product = dataService.data.product[0];

          $scope.searchText = '';
          $scope.$watch("searchText", function(newValue, oldValue) {
              return $scope.searchText;
          });


        $scope.settings = false;
        $scope.openSetings = function (){
            $scope.settings = !$scope.settings;
        };
        $scope.closeSetting = function(){
            $scope.settings = false;
        };


        $scope.profile = false;
        $scope.openProfile = function (){
            $scope.profile = !$scope.profile;
        };

        $scope.$on("update_parent_controller", function(event, message){
            $scope.profile = message;
        });

         $scope.generateDoc = function(topic){
        $scope.topic =dataService.data.product[0].title;
        if(topic == 'translation'){
            $scope.topic ='На перевод';
        }
        var inputs = document.getElementsByName("checkbox");
        var checked = 0;
        var articles_id_list=[];
        for (var i = 0; i < inputs.length; i++) {
            if (inputs[i].checked){
                var id = inputs[i].id;
                articles_id_list.push(id.replace('select-', ''));
            }
        }

        $scope.shareMenu = false;
        $http.defaults.headers.post['X-CSRFToken'] = $cookies['csrftoken'];
        $http.post("/generate-doc/", {articles_id_list:articles_id_list}).success(function(res){
        $scope.sedModal = true;
        $scope.file_name = res['file_name'];
        $scope.message ='';

        $scope.from= res['from'];
        $scope.to='';
    });
    };
        $scope.printArticles = function(){
        var inputs = document.getElementsByName("checkbox");
        var checked = 0;
        var articles_id_list=[];
        for (var i = 0; i < inputs.length; i++) {
            if (inputs[i].checked){
                var id = inputs[i].id;
                articles_id_list.push(id.replace('select-', ''));
            }
        }
    $http.defaults.headers.post['X-CSRFToken'] = $cookies['csrftoken'];
    $http.post("/print/articles/", {articles_id_list:articles_id_list}).success(function(res) {
        $scope.shareMenu = false;
        window.location.href = '/'+res['file_path']
    });

    };
        $scope.copyLink = function(scope){
        var inputs = document.getElementsByName("checkbox");
        var checked = 0;
        var articles_link_list = [];
        for (var i = 0; i < inputs.length; i++) {
            if (inputs[i].checked){
                var title = inputs[i].title;
                articles_link_list.push(title);
            }
        }
        var result = '';
        for (var i = 0; i < articles_link_list.length; i++ ){
            if(articles_link_list.length ==1){
                result = result + articles_link_list[i];
            }
            else{
                if(articles_link_list.length == i){
                    result = result + articles_link_list[i]
                }
                else{
                    result = result + articles_link_list[i] + ', ';
                }
            }
        }

        return result;
    };
        // other filters
    $scope.newFilters = dataFilter.data.filters;
    // sort by order
    $scope.sortableNewFilters = [];
    for (var key in $scope.newFilters) {
        if (!$scope.newFilters[key].settings.a3) $scope.newFilters[key].settings.a3 = Infinity;
        $scope.sortableNewFilters.push([key, $scope.newFilters[key]]);
    }
    $scope.sortableNewFilters.sort(function(a, b) {
        return a[1].settings.a3 - b[1].settings.a3
    });
    // скрыть ФО
    $scope.hideFederalDistrict = false;
    $scope.is_right = function(ind) {
        return $scope.sortableNewFilters.length < 2*ind + 4;
    };
    $scope.alineFilters = function() {
        $('.filter-left').wrapAll('<div class="filter-block-left"></div>');
        $('.filter-right').wrapAll('<div class="filter-block-right"></div>');
        var heights = $('body').height();
        console.log(heights, 'heights');
        var block = $('.scroll-filter').css("height", heights - 270);
        console.log(block, 'block');
    };
    $scope.isRangeFilter = function(key) {return ~key.indexOf('range');}
    $scope.saveProduct = function(){
        //var filters = dataFilter.data.filters.slice();
        var product = dataService.data.product[0];
        if (product.type == "date_release"){
            product.date_from = product.date_release_start;
            product.date_to = product.date_release_end;
        }
        if (product.type == "date_upload"){
            var time_start = product.time_upload_start.split(":"), time_end = product.time_upload_end.split(":");

            product.date_from = product.date_upload_start;
            product.date_to = product.date_upload_end;

            product.date_from.setHours(parseInt(time_start[0])||0);
            product.date_from.setMinutes(parseInt(time_start[1])||0);
            product.date_from.setSeconds(parseInt(time_start[2])||0);

            product.date_to.setHours(parseInt(time_end[0])||0);
            product.date_to.setMinutes(parseInt(time_end[1])||0);
            product.date_to.setSeconds(parseInt(time_end[2])||0);
        }
        if(product.date_to && typeof(product.date_to)=='string'){
            product.date_to = new Date(product.date_to.split('-').reverse().join('-'))
        }
        if(product.date_from && typeof(product.date_from)=='string'){
            product.date_from = new Date(product.date_from.split('-').reverse().join('-'))
        }
        var data = angular.toJson({product: $scope.product,
            category_list: dataFilter.data.category_list,
            mention_objects_list: dataFilter.data.mention_objects_list,
            filters: dataFilter.data.filters,
            product: product
        });
        var confirm = function(){
            $http({
                method: 'POST',
                url: "/create-or-update-product/" +$rootScope.productId+ "/",
                data: data,
                headers: {"X-CSRFToken": $cookies.csrftoken,
                    "Content-Type": undefined}
            }).success(function(data){
                window.location = "/test/monitoring_product/" +  data.id + "/widget0/" + window.location.search;
            });
        };
        if (product.title==product.old_title){
            var question = "";
            var no_more_param_name = "";

            question = "Вы уверены, что хотите сохранить измененный продукт, сохранив его последнюю версию? Для подтверждения действия нажмите \"Да\"." +
            "Для сохранения нового продукта без удаления его исходной версии кликните на кнопку \"НАЗАД\", затем измените название продукта и нажмите" +
            "на \"Сохранить\"";

            no_more_param_name = "dont_ask_if_change_without_rename_product_availible_only_for_me";

            var modal_data = {title: "Точно хотите сохранить?", question: question};
            modalConfirm($scope, confirm, no_more_param_name, modal_data);
        }
        else{
            confirm();
        }
    };

    $scope.getMentionObjects = function(key) {return key=='referred_object__id__in'?$scope.mentionObjects:null;}
    // km_result
    // проверка на исключения фильтра release_time из общего списка
    $scope.isCustomFilter = function(key) {return ~key.indexOf('release_time');}
    $scope.haveReleaseTime = function() {return !angular.isUndefined($scope.newFilters.release_time);}

    if ($scope.haveReleaseTime()) {
        $scope.newFilters.km_result.list = [{
            id: 1, title: 'да'
        }, {
            id: 2, title: 'нет'
        }];
    }
    $scope.removeFilter = function(filter) {
        if (filter.filter_list) {
            for (var i = 0; i < filter.filter_list.length; i++) {
                filter.filter_list[i].selected = false;
                if (!angular.isUndefined(filter.isNull.emptyValue)) filter.isNull.emptyValue = false;
                filter.isNull.selectAll = false;
            }
        } else {
            filter.id.slider.min = filter.id.slider.floor;
            filter.id.slider.max = filter.id.slider.ceil;
        }
        $scope.showFilteredArticles(true);
    };
});