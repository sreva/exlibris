(function($) {
    var months = ['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'];
    var uDatepicker = $.datepicker._updateDatepicker;
    $.datepicker._updateDatepicker = function() {
        var ret = uDatepicker.apply(this, arguments);
        var $sel = this.dpDiv.find('select');
        $sel.find('option').each(function(i) {
            $(this).text(months[i]);
        });
        return ret;
    };
    $.datepicker._updateDatepicker_original = $.datepicker._updateDatepicker;
    $.datepicker._updateDatepicker = function(inst) {
        $.datepicker._updateDatepicker_original(inst);
        var afterShow = this._get(inst, 'afterShow');
        if (afterShow)
            afterShow.apply((inst.input ? inst.input[0] : null));
    }	
	var currentIndexMonth, currentIndexYear,
		methods = {
			goTop : function(options) { 
				if (currentIndexMonth < 0 || currentIndexMonth > 11) {
					currentIndexMonth = currentIndexMonth < 0 ? 0 : 11;
					return false;
				}
				if (currentIndexYear < 0 || currentIndexYear > 20){
					currentIndexYear = currentIndexYear < 0 ? 0 : 20;
					return false;
				}
				options.active.animate({top: -(options.steps - 1) * 32}, 300);

				var date = moment(options.handle.datepicker('getDate')).format('01.MM.YYYY').split('.'),
					$allList = options.active.children('li'),
					$lastSelected = $($allList.get(options.steps));
				$allList.removeClass('selected');
				$lastSelected.addClass('selected');

				if (options.active.is('.month')){
					date[1] = options.steps + 1;
				} else {
					date[2] = $lastSelected.children('a').html()
				}
				var setDate = date.join('.');
				options.handle.datepicker('setDate', setDate);
			}
		};
	$.fn.datepickerBaraban = function() {
		this.each(function() {
			var $datePickerHandler = $(this),
				widget = $datePickerHandler.datepicker("widget"),
				$datepickerMonth = widget.find('.ui-datepicker-month'),
				$datepickerYear = widget.find('.ui-datepicker-year'),
				$listDatepickerMonth = $('<ul class="datepicker-baraban-list month left"></ul>'),
				$listDatepickerYear = $('<ul class="datepicker-baraban-list left"></ul>'),
				$datepickerWrapper = $('<div class="datepicker-baraban-wrapper"></div>'),
				$datepicker = $('<div class="datepicker-baraban"></div>'),
				$arrowTopLeft = $('<span class="datepicker-baraban-arrow top left"></spn>'),
				$arrowBottomLeft = $('<span class="datepicker-baraban-arrow bottom left"></spn>'),
				$arrowTopRight = $('<span class="datepicker-baraban-arrow top right"></spn>'),
				$arrowBottomRight = $('<span class="datepicker-baraban-arrow bottom right"></spn>');
			currentIndexMonth = $datepickerMonth.children('option:selected').index(),
			currentIndexYear = $datepickerYear.children('option:selected').index();

			$datepickerMonth.children('option').each(function(){
				var $elemLi = $('<li class="datepicker-baraban-item">'+
									'<a href="'+ $(this).val() +'" class="elem-month">'+ $(this).html() +'</a>'+
								'</li>');
				$elemLi.appendTo($listDatepickerMonth);
			});
			methods.goTop({
				'active': $listDatepickerMonth,
				'steps': currentIndexMonth,
				'handle': $datePickerHandler
			});

			$datepickerYear.children('option').each(function(){
				var $elemLi = $('<li class="datepicker-baraban-item">'+
									'<a href="'+ $(this).val() +'">'+ $(this).html() +'</a>'+
								'</li>');
				$elemLi.appendTo($listDatepickerYear);
			});
			methods.goTop({
				'active': $listDatepickerYear,
				'steps': currentIndexYear,
				'handle': $datePickerHandler
			});

			$datepicker.append($listDatepickerMonth).append($listDatepickerYear);
			$datepickerWrapper
				.append('<div class="datepicker-baraban-pad"><div class="datepicker-baraban-pad-inner"></div></div>')
				.append($datepicker).append($arrowTopLeft)
				.append($arrowBottomLeft).append($arrowTopRight).append($arrowBottomRight);
			$datePickerHandler.datepicker('option', 'afterShow', function(input, inst){
				widget.find('.ui-datepicker-header').append($datepickerWrapper);
				$datepickerWrapper.on('click', '.datepicker-baraban-item a', function(){
					if ($(this).is('.elem-month')){
						currentIndexMonth = $(this).parent().index();
						methods.goTop({
							'active': $listDatepickerMonth,
							'steps': currentIndexMonth,
							'handle': $datePickerHandler
						});
					} else {
						currentIndexYear = $(this).parent().index();
						methods.goTop({
							'active': $listDatepickerYear,
							'steps': currentIndexYear,
							'handle': $datePickerHandler
						});
					}
					return false;
				}).on('click', '.datepicker-baraban-arrow.top.left', function(){
					methods.goTop({
						'active': $listDatepickerMonth,
						'steps': --currentIndexMonth,
						'handle': $datePickerHandler
					});
				}).on('click', '.datepicker-baraban-arrow.bottom.left', function(){
					methods.goTop({
						'active': $listDatepickerMonth,
						'steps': ++currentIndexMonth,
						'handle': $datePickerHandler
					});
				}).on('click', '.datepicker-baraban-arrow.top.right', function(){
					methods.goTop({
						'active': $listDatepickerYear,
						'steps': --currentIndexYear,
						'handle': $datePickerHandler
					});
				}).on('click', '.datepicker-baraban-arrow.bottom.right', function(){
					methods.goTop({
						'active': $listDatepickerYear,
						'steps': ++currentIndexYear,
						'handle': $datePickerHandler
					});
				});
				$('.ui-datepicker-calendar').wrap('<div class="ui-datepicker-calendar-wrapper"></div>');
			});			
		});
	};
})(jQuery);