/**
 * Filter for search text
 */
exlibris.filter('searchTextEntry', function() {
  return function(items, query) {
    var filtered = [];
    if (!query) query = '';
    angular.forEach(items, function(el) {
      if (el.title.toLowerCase().indexOf(query.toLowerCase()) === 0) filtered.push(el);
    });
    return filtered;
  }
});



/**
 * Filter for marked text in article description
 */
exlibris.filter("paintTheWordsFilter", [function() {
    return function(input, searchRegex, replaceRegex) {
        return input.replace(new RegExp(searchRegex, 'g'), replaceRegex);
    };
}]);

exlibris.filter("monitoringPeriodicity", [function() {
    return function(input) {
        var periodicity = {
            day: 'Ежедневно',
            week: 'Еженедельно',
            month: 'Ежемесячно',
            date_release: 'Заданный период',
            date_upload: 'Заданный период',
            period: 'Заданный период',
        }
        if (!input){
            return "Периодичность";
        }
        return periodicity[input];
    };
}]);