var exlibris = angular
    .module('exlibris', ['ui.bootstrap', 'ngSanitize', 'ngCookies', 'ngRoute'])
    .config(function($interpolateProvider, $routeProvider, $locationProvider) {
    	$interpolateProvider.startSymbol('{$');
    	$interpolateProvider.endSymbol('$}');
      $routeProvider.when('/:user/dashboard/', {
        templateUrl: "/template/analitic_product_vympelkom_inner.html/",
        controller: 'newController',
        resolve:{
          load_chart1: function ($rootScope, $route, dataService) {
            $rootScope.productId = $route.current.params.id;
              return dataService.load_chart_1($route.current.params.user, $route.current.params.id || 1).then(function(res){
                dataService.data = res;
              });
          },
          load_chart2: function ($rootScope, $route, dataService) {
            $rootScope.productId = $route.current.params.id;
              return dataService.load_chart_2($route.current.params.user, $route.current.params.id || 1).then(function(res){
                dataService.chart_2 = res;
              });
          },
          load_chart3: function ($rootScope, $route, dataService) {
            $rootScope.productId = $route.current.params.id;
              return dataService.load_chart_3($route.current.params.user, $route.current.params.id || 1).then(function(res){
                dataService.chart_3 = res;
              });
          },
          load_chart4: function ($rootScope, $route, dataService) {
            $rootScope.productId = $route.current.params.id;
              return dataService.load_chart_4($route.current.params.user, $route.current.params.id || 1).then(function(res){
                dataService.chart_4 = res;
              });
          },
          load_chart5: function ($rootScope, $route, dataService) {
            $rootScope.productId = $route.current.params.id;
              return dataService.load_chart_5($route.current.params.user, $route.current.params.id || 1).then(function(res){
                dataService.chart_5 = res;
              });
          },
          load_chart6: function ($rootScope, $route, dataService) {
            $rootScope.productId = $route.current.params.id;
              return dataService.load_chart_6($route.current.params.user, $route.current.params.id || 1).then(function(res){
                dataService.chart_6 = res;
              });
          },
          load_filters_data: function($rootScope, $route, dataService) {
            return dataService.load_filtersData().then(function(res){
              dataService.daysFilters = res.days;
              dataService.regionFilters = res.region;
              dataService.toneFilters = res.tone;
              dataService.objectFilters = res.objects;
            });
          },
          load_chart7: function ($rootScope, $route, dataService) {
            $rootScope.productId = $route.current.params.id;
              return dataService.load_chart_7($route.current.params.user, $route.current.params.id || 1).then(function(res){
                dataService.chart_7 = res;
              });
          }
        }
      });
    $locationProvider.html5Mode(true);
}).factory('dataService', function ($q, $http) {
    return {
      data: {},
      chart_1: {},
      chart_2: {},
      chart_3: {},
      chart_4: {},
      chart_5: {},
      chart_6: {},
      chart_7: {},
      daysFilters: null,
      regionFilters: null,
      toneFilters: null,
      objectFilters: null,
      load: function(user, id) {
        var defer = $q.defer();
        $http.get('http://'+ window.location.host + '/' + user + '/analytic_product/' + id + '/json1').success(function(res){
            defer.resolve(res);
        });
        return defer.promise;
      },
      load_chart_1: function(user, id) {
        var defer = $q.defer();
        $http.get('http://'+ window.location.host + '/' + user + '/analytic_product_vympelkom/' + id + '/json1').success(function(res){
            defer.resolve(res);
        });
        return defer.promise;
      },
      load_chart_2: function(user, id) {
        var defer = $q.defer();
        $http.get('http://'+ window.location.host + '/' + user + '/analytic_product_vympelkom/' + id + '/json2').success(function(res){
            defer.resolve(res);
        });
        return defer.promise;
      },
      load_chart_3: function(user, id) {
        var defer = $q.defer();
        $http.get('http://'+ window.location.host + '/' + user + '/analytic_product_vympelkom/' + id + '/json3').success(function(res){
            defer.resolve(res);
        });
        return defer.promise;
      },
      load_chart_4: function(user, id) {
        var defer = $q.defer();
        $http.get('http://'+ window.location.host + '/' + user + '/analytic_product_vympelkom/' + id + '/json4').success(function(res){
            defer.resolve(res);
        });
        return defer.promise;
      },
      load_chart_5: function(user, id) {
        var defer = $q.defer();
        $http.get('http://'+ window.location.host + '/' + user + '/analytic_product_vympelkom/' + id + '/json5').success(function(res){
            defer.resolve(res);
        });
        return defer.promise;
      },
      load_chart_6: function(user, id) {
        var defer = $q.defer();
        $http.get('http://'+ window.location.host + '/' + user + '/analytic_product_vympelkom/' + id + '/json6').success(function(res){
            defer.resolve(res);
        });
        return defer.promise;
      },
      load_chart_7: function(user, id) {
        var defer = $q.defer();
        $http.get('http://'+ window.location.host + '/' + user + '/analytic_product_vympelkom/' + id + '/json7').success(function(res){
            defer.resolve(res);
        });
        return defer.promise;
      },
      update_chart: function (user, id, chart, params){
        var defer = $q.defer();
        $http.get('http://'+ window.location.host + '/' + user + '/analytic_product_vympelkom/' + id + '/json' + chart, {"params": params}).success(function(res){
            defer.resolve(res);
        });
        return defer.promise;
      },
      load_filtersData: function(){
        var defer = $q.defer();
        $http.get('http://'+ window.location.host + '/get_filter_dictionary_vympelcom/').success(function(res){
            defer.resolve(res);
        });
        return defer.promise;
      },
      update_chart_1: function(user, id, params){
        this.update_chart(user, id, 1, params).then(function(res){
          this.chart_1 = res;
        })
      },
      titleOffset: [],
      activeTitle: '',
      headerObj: null,
      header: 0
    };
}).controller('newController', function ($scope, $route, dataService){
    FusionCharts.ready(function (){
        $scope.chart1 = new FusionCharts({
            type: 'msline',
            renderAt: 'chart',
            width: "420",
            height: "172",
            dataFormat: 'json',
            dataSource: dataService.data.chart
        });
        console.log(dataService.data.chart);
        $scope.chart1.render();

        $scope.chart2 = new FusionCharts({
            type: 'msline',
            renderAt: 'chart2',
            dataFormat: 'json',
            width: "420",
            height: "172",
            dataSource: dataService.chart_2.chart
        });
        $scope.chart2.render();

        $scope.chart3 = new FusionCharts({
            type: 'mscolumn2d',
            renderAt: 'chart3',
            width: "420",
            height: "172",
            dataFormat: 'json',
            dataSource: dataService.chart_3.chart
        });
        $scope.chart3.render();

        $scope.chart4 = new FusionCharts({
            type: 'mscolumn2d',
            renderAt: 'chart4',
            width: "420",
            height: "172",
            dataFormat: 'json',
            dataSource: dataService.chart_4.chart
        });
        $scope.chart4.render()

        $scope.chart5 = new FusionCharts({
            type: 'doughnut2d',
            renderAt: 'chart5',
            width: "420",
            height: "172",
            dataFormat: 'json',
            dataSource: dataService.chart_5.chart
        });
        $scope.chart5.render();

        $scope.chart6 = new FusionCharts({
            type: 'bar2d',
            renderAt: 'chart6',
            width: "420",
            height: "142",
            dataFormat: 'json',
            dataSource: dataService.chart_6.chart
        });
        $scope.chart6.render();

        $scope.chart7 = new FusionCharts({
            type: 'bar2d',
            renderAt: 'chart7',
            width: "420",
            height: "172",
            dataFormat: 'json',
            dataSource: dataService.chart_7.chart
        });
        $scope.chart7.render()
    });
    $scope.updateChart = function(params, id){
        dataService.update_chart($route.current.params.user, $route.current.params.id || 1, id, params).then(function (res){
            dataService['chart_' + id] = res.chart
            $scope['chart' + id].setJSONData(dataService['chart_' + id]);
            $scope['chart' + id].render();
        });
    }
    $scope.reRenderChart = function(data, id, radioModel) {
        console.log($scope[data + id]);
        var days = [], tone = [], region = [], objects = [], params = {};
        for (var i = 0; i < $scope['daysFilters' + id].length; i++){
            var elem = $scope['daysFilters' + id][i];
            if (elem.Selected) days.push(elem.id);
            if (days.length) params.days = days;
        }
        for (var i = 0; i < $scope['toneFilters' + id].length; i++){
            var elem = $scope['toneFilters' + id][i];
            if (elem.Selected) tone.push(elem.id);
            if (tone.length) params.tone = tone;
        }
        for (var i = 0; i < $scope['regionFilters' + id].length; i++){
            var elem = $scope['regionFilters' + id][i];
            if (elem.Selected) region.push(elem.id);
            if (region.length) params.region = region;
        }
        for (var i = 0; i < $scope['objectFilters' + id].length; i++){
            var elem = $scope['objectFilters' + id][i];
            if (elem.Selected) objects.push(elem.id);
            if (objects.length) params.objects = objects;
        }
        if (radioModel) {
            objects.push(radioModel);
            params.objects = objects;
        }
        $scope.updateChart(params, id);
    }
    $scope.selectAllFilters = function (currentAll, currentId, id) {
        if ($scope.filterItems[currentAll]) {
            $scope.filterItems[currentAll] = false;
        } else {
            $scope.filterItems[currentAll] = true;
        }
        angular.forEach($scope[currentId], function (item) {
            item.Selected = $scope.filterItems[currentAll];
        });
        $scope.reRenderChart(currentId, id);
    };
    $scope.filterItems = {
        globalMedia:            false,
        globalMediaAll1:        false,
        globalMediaAll2:        false,
        globalMediaAll3:        false,
        globalMediaAll4:        false,
        impactIndex:            false,
        impactIndexAll1:        false,
        impactIndexAll2:        false,
        impactIndexAll3:        false,
        outbreak:               false,
        outbreakAll1:           false,
        outbreakAll2:           false,
        outbreakAll3:           false,
        tonality:               false,
        tonalityAll1:           false,
        listsForMedia:          false,
        listsForMediaAll1:      false,
        listsForMediaAll2:      false,
        top5MediaPresence:      false,
        top5MediaPresenceAll1:  false,
        top5MediaPresenceAll2:  false,
        top5LabSpeakers:        false,
        top5LabSpeakersAll1:    false,
        top5LabSpeakersAll2:    false
    }
    $scope.filterItemsKeys = ['globalMedia', 'impactIndex', 'outbreak', 'tonality', 'listsForMedia', 'top5MediaPresence', 'top5LabSpeakers']
    $scope.daysFilters = dataService.daysFilters;
    $scope.regionFilters = dataService.regionFilters;
    $scope.toneFilters = dataService.toneFilters;
    $scope.objectFilters = dataService.objectFilters;

    $scope.daysFilters1 = angular.copy($scope.daysFilters);
    $scope.daysFilters2 = angular.copy($scope.daysFilters);
    $scope.daysFilters3 = angular.copy($scope.daysFilters);
    $scope.daysFilters4 = angular.copy($scope.daysFilters);
    $scope.daysFilters5 = angular.copy($scope.daysFilters);
    $scope.daysFilters6 = angular.copy($scope.daysFilters);
    $scope.daysFilters7 = angular.copy($scope.daysFilters);

    $scope.regionFilters1 = angular.copy($scope.regionFilters);
    $scope.regionFilters2 = angular.copy($scope.regionFilters);
    $scope.regionFilters3 = angular.copy($scope.regionFilters);
    $scope.regionFilters4 = angular.copy($scope.regionFilters);
    $scope.regionFilters5 = angular.copy($scope.regionFilters);
    $scope.regionFilters6 = angular.copy($scope.regionFilters);
    $scope.regionFilters7 = angular.copy($scope.regionFilters);

    $scope.toneFilters1 = angular.copy($scope.toneFilters);
    $scope.toneFilters2 = angular.copy($scope.toneFilters);
    $scope.toneFilters3 = angular.copy($scope.toneFilters);
    $scope.toneFilters4 = angular.copy($scope.toneFilters);
    $scope.toneFilters5 = angular.copy($scope.toneFilters);
    $scope.toneFilters6 = angular.copy($scope.toneFilters);
    $scope.toneFilters7 = angular.copy($scope.toneFilters);

    $scope.objectFilters1 = angular.copy($scope.objectFilters);
    $scope.objectFilters2 = angular.copy($scope.objectFilters);
    $scope.objectFilters3 = angular.copy($scope.objectFilters);
    $scope.objectFilters4 = angular.copy($scope.objectFilters);
    $scope.objectFilters5 = angular.copy($scope.objectFilters);
    $scope.objectFilters6 = angular.copy($scope.objectFilters);
    $scope.objectFilters7 = angular.copy($scope.objectFilters);
    
    $scope.objectFilters4Model = $scope.objectFilters[0].id;
    $scope.objectFilters5Model = $scope.objectFilters[0].id;
    $scope.objectFilters7Model = $scope.objectFilters[0].id;

    $scope.hideAllFilters = function(index) {
        for (var i = 0; i < $scope.filterItemsKeys.length; i++){
            if (index != i) { 
                $scope.filterItems[$scope.filterItemsKeys[i]] = false;
            }
        }
    }
});