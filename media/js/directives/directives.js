
/**
 * Directive for jqueryui selectmenu
 */
exlibris.directive('selectMenu', [function() {
	return {
		restrict: 'A',
		require : 'ngModel',
		scope: {
			list: '='
		},
		link: function(scope, element, attrs, ngModelCtrl) {
			$(function() {
				element.selectmenu({
					appendTo: attrs.appendTo,
					change: function(event, ui) {
						scope.$apply(function() {
							ngModelCtrl.$setViewValue(scope.list[ui.item.index].value);
						});
					}
				});
				if (!angular.isUndefined(attrs.iconButton)) {
					element.selectmenu('option', 'icons', {button: attrs.iconButton})
				}
			});
		}
	}
}]);



/**
 * Directive for focus
 */
exlibris.directive('ngFocus', ['$parse', function($parse) {
	return {
		restrict: 'A',
		link: function(scope, element, attr) {
			var fn = $parse(attr['ngFocus']);
			element.bind('focus', function(event) {
				scope.$apply(function() {
					fn(scope, {$event:event});
				});
			});
		}
	}
}]);



/**
 * Directive for blur
 */
exlibris.directive('ngBlur', ['$parse', function($parse) {
	return {
		restrict: 'A',
		link: function(scope, element, attr) {
			var fn = $parse(attr['ngBlur']);
			element.bind('blur', function(event) {
				scope.$apply(function() {
					fn(scope, {$event:event});
				});
			});
		}
	}
}]);

exlibris.directive('myBlur', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            element.bind('blur', function () {
                //apply scope (attributes)
                scope.$apply(attr.myBlur);
                //return scope value for focusing to false
                scope.$eval(attr.myFocus + '=false');
            });
        }
    };
});

exlibris.directive('myFocus', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            scope.$watch(attr.myFocus, function (n, o) {
                if (n != 0 && n) {
                    element[0].focus();
                }
            });
        }
    };
});

/**
 * Directive for inputMask
 */
exlibris.directive('inputMask', [function() {
    return {
        restrict: 'A',
        require : 'ngModel',
        scope: {},
        link : function (scope, element, attrs, ngModelCtrl) {
            $(function(){
                var maskPlaceholder = '_';
                maskPlaceholder = attrs.maskPlaceholder == 'empty' ? '' : maskPlaceholder;
                element.inputmask(attrs.alias, {
                    placeholder:  maskPlaceholder,
                    showMaskOnFocus: true,
					onBeforePaste: function (pastedValue) {
						ngModelCtrl.$setViewValue(pastedValue);
						return pastedValue;
					},
					onBeforeMask: function(MaskedValue) {
						return MaskedValue;
					}
                });
            });
        }
    }
}]);



/**
 * Directive for jquery datepicker
 */
exlibris.directive('ourdatepicker', [function() {
    return {
        restrict: 'A',
        require : 'ngModel',
        link : function (scope, element, attrs, ngModelCtrl) {
            $(function(){
                element.datepicker($.datepicker.regional[ "ru" ]);
                element.datepicker('option', {
                    dateFormat:'dd.mm.yy',
                    changeMonth: true,
                    changeYear: true,
                    onSelect: function (date) {
                        scope.$apply(function () {
                            ngModelCtrl.$setViewValue(date);
                        });
                    },
                    beforeShowDay: function(date){
                        var dateStart = new Date(scope.dateStart.split('.')[2], scope.dateStart.split('.')[1] - 1, scope.dateStart.split('.')[0]);
                        var dateEnd = new Date(scope.dateEnd.split('.')[2], scope.dateEnd.split('.')[1] - 1, scope.dateEnd.split('.')[0]);
                        if (date >= dateStart && date <= dateEnd) {
                            return [true, 'ui-state-selected', ''];
                        }
                        return [true, '', ''];
                    }
                });
                element.datepickerBaraban();
            });
        }
    }
}]);



/**
 * Directive for validate numbers only
 */
exlibris.directive('numbersOnly', [function(){
    return {
        require: 'ngModel',
        scope: {
            min: '=',
            max: '='
        },
        link: function(scope, element, attrs, modelCtrl) {
            modelCtrl.$parsers.push(function (inputValue) {
                console.log('2');
                if (inputValue == undefined) return '' ;
                var transformedInput = inputValue.replace(/[^0-9.]/g, ' ');
                if (transformedInput!=inputValue) {
                    modelCtrl.$setViewValue(transformedInput);
                    modelCtrl.$render();
                } else {
                    if (+inputValue < +scope.min) {
                        modelCtrl.$setViewValue(scope.min);
                        modelCtrl.$render();
                        return scope.min;
                    } else if (+inputValue > +scope.max) {
                        modelCtrl.$setViewValue(scope.max);
                        modelCtrl.$render();
                        return scope.max;
                    }
                }
                return transformedInput;
            });
        }
    }
}]);


/**
 * Directive for render prt, prv, rupor
 */
exlibris.directive('renderRightData', function(dataService) {
    return {
        scope: {
            value: '=',
            prt: '=',
            popupt: '@',
            show: '@'
        },
        template: '<a href ng-class="{active: actived}" ng-show="show" popover-trigger="mouseenter" popover="{$ popupt $}" class="icon-moon">' +
                  '</a>' +
                  '<span ng-show="show" ng-class="{positive: prtPositive}">{$ render(value, prt) $}</span>',
        link: function(scope) {
            scope.actived = true;
            scope.prtPositive = false;
            scope.show = true;
            scope.render = function(value, prt) {
                var newValue = parseFloat(value) || '';
                if(value==false){
                    scope.actived = false;
                }
                if (newValue && typeof value !== 'undefined') {
                    scope.show = true;
                    if (prt) {
                        if (newValue >= 0.75) scope.prtPositive = true; else scope.prtPositive = false;
                        newValue = newValue.toFixed(2);
                    } else {
                        if (newValue <= 1) {
                            newValue = newValue.toFixed(2);
                        } else if (newValue > 1 && newValue <= 1000) {
                            newValue = Math.round(newValue);
                        } else if (newValue > 1000 && newValue <= 1000000) {
                            newValue = (newValue / 1000).toFixed(2) + ' K';
                        } else if (newValue > 1000000 && newValue <= 1000000000) {
                            newValue = (newValue / 1000000).toFixed(2) + ' M';
                        }
                    }
                } if(typeof value === 'undefined') {
                    scope.show = false;
                }
                return newValue;
            }
        }
    }
});



/**
 * Directive for render dynamic breadcrumbs/page navigation
 */
exlibris.directive('dynamicBreadcrumbs', ['dataService', '$timeout',  function(dataService, $timeout) {
	return {
		template: '<span ng-repeat="elem in dataService.activeTitle"'+
                'ng-class="{collapsed: elem.collapsed}"'+
                'ng-style="{width: elem.width}"'+
                'class="dropdown dropdown-toggle" on-toggle="watcher(open, elem, $index)">'+
			'<span>{$ elem.title $}</span>'+
			'<ul class="dropdown-menu bread-crumbs-popup">'+
				'<li ng-repeat="brother in elem.brothers">'+
					'<a href ng-click="scrollToArticle(brother)">{$ brother.title $}</a>'+
				'</li>'+
			'</ul>'+
			'</span>',
        link: function(scope, element, attrs) {
            scope.maxWidth = element[0].offsetWidth;
        },
        controller: function($scope) {
			$scope.scrollToArticle = function(brother) {
			    if (brother.articles && brother.articles.length) {
			        dataService.scrollToElement(brother.id);
			        return false;
			    }
			    if (brother.children && brother.children.length) {
			        dataService.findFullCategory(brother.children, brother.id);
			        return false;
			    }
			    dataService.getAllArticles(brother.id);
			}
            var currentIndex = undefined;
            $scope.watcher = function(open, element, index) {
                if (open) {
                    var $currentPopup = $('.bread-crumbs-popup:visible');
                    if (angular.isUndefined(currentIndex)) {
                        currentIndex = index;
                    } else {
                        if (currentIndex > index) {
                            $currentPopup = $currentPopup.first();
                        } else if (currentIndex < index) {
                            $currentPopup = $currentPopup.last();
                        }
                        
                    }
                    for (var i = 0; i < dataService.activeTitle.length; i++) {
                        dataService.activeTitle[i].width = 'auto';
                        dataService.activeTitle[i].collapsed = false;
                    }
                    element.width = $currentPopup.width();
                    
                    $timeout(function() {
                        var totalWidth = 0,
                            $list = $('.dynamic-breadcrumbs').children('span');
                        
                        $list.each(function() {
                            totalWidth += (index != $(this).index() ? $(this).outerWidth() : 0);
                        });
                        totalWidth += ($list.length - 1) * 8;
                        totalWidth += element.width;

                        if (totalWidth > $scope.maxWidth) {
                            dataService.activeTitle[index != 0 ? 0 : dataService.activeTitle.length - 1].width = 40;
                            dataService.activeTitle[index != 0 ? 0 : dataService.activeTitle.length - 1].collapsed = true;
                        }
                        currentIndex = index;
                    });
                } else {
                    if (!$('.bread-crumbs-popup:visible').length) {
                        for (var i = 0; i < dataService.activeTitle.length; i++) {
                            dataService.activeTitle[i].width = 'auto';
                            dataService.activeTitle[i].collapsed = false;
                        }
                        var totalWidth = 0,
                            $list = $('.dynamic-breadcrumbs').children('span');
                        $timeout(function() {
                            $list.each(function() {
                                totalWidth += $(this).outerWidth();
                            });
                            totalWidth += ($list.length - 1) * 8;

                            if (totalWidth > $scope.maxWidth) {
                                dataService.activeTitle[0].width = 40;
                                dataService.activeTitle[0].collapsed = true;
                            }
                        });
                    }

                }
            }
            $scope.$watch('dataService.activeTitleChange', function() {
                $timeout(function() {
                    for (var i = 0; i < dataService.activeTitle.length; i++) {
                        dataService.activeTitle[i].width = 'auto';
                        dataService.activeTitle[i].collapsed = false;
                    }
                    var totalWidth = 0,
                        $list = $('.dynamic-breadcrumbs').children('span');
                    $timeout(function() {
                        $list.each(function() {
                            totalWidth += $(this).outerWidth();
                        });
                        totalWidth += ($list.length - 1) * 8;

                        if (totalWidth > $scope.maxWidth) {
                            dataService.activeTitle[0].width = 40;
                            dataService.activeTitle[0].collapsed = true;
                        }
                    });
                });
            }, true);
        }
	}
}]);

exlibris.directive('format', ['$filter', function ($filter) {
    return {
        require: '?ngModel',
        link: function (scope, elem, attrs, ctrl) {
            if (!ctrl) return;
            ctrl.$formatters.unshift(function (a) {
                return $filter(attrs.format)(ctrl.$modelValue)
            });
            ctrl.$parsers.unshift(function (viewValue) {
                var plainNumber = viewValue.replace(/[^\d|\-+|\.+]/g, '');
                elem.val($filter(attrs.format)(plainNumber));
                return plainNumber;
            });
        }
    };
}]);

exlibris.directive('rangeFilterContainer',  function() {
    return {
        restrict: 'E',
        scope: {
            filter: '=',
            key: '='
        },
        template:   '<div class="f-row__inner">'+
                        '<a href class="text-title" ng-click="toggledFilter()" ng-bind="filterTitle()"></a>'+
                        '<div class="slider-wrap" collapse="filter.toggled">'+
                            '<div class="left">'+
                                '<span class="s-label">от</span>'+
                                '<input type="text" numbers-only format="number" min="filter.slider.min" max="filter.slider.max" class="rzslider-value" ng-model="filter.slider.min">'+
                            '</div>'+
                            '<div class="right">'+
                                '<span class="s-label">до</span>'+
                                '<input type="text" numbers-only format="number" min="filter.slider.min" max="filter.slider.max" class="rzslider-value" ng-model="filter.slider.max">'+
                            '</div>'+
                            '<rzslider rz-slider-floor="filter.slider.floor" id="rzslider_{$ key $}"'+
                                'rz-slider-ceil="filter.slider.ceil"'+
                                'rz-slider-model="filter.slider.min"'+
                                'rz-slider-high="filter.slider.max"'+
                                'rz-slider-precision="{$ filter.slider.precision $}"'+
                                'rz-slider-step="{$ filter.slider.step $}">'+
                            '</rzslider>'+
                            '<div class="wrap-min-max clear">'+
                                '<span class="left s-label">min</span>'+
                                '<span class="right s-label">max</span>'+
                            '</div>'+
                        '</div>'+
                    '</div>',
        controller: function($scope, $filter) {
            $scope.filter.toggled = false;
            $scope.filter.slider = {
                min: angular.copy($scope.filter.list[0]),
                max: angular.copy($scope.filter.list[1]),
                floor: angular.copy($scope.filter.list[0]),
                ceil: angular.copy($scope.filter.list[1]),
                precision: ~$scope.key.indexOf('mo__range') ? 0 : 2,
                step: ~$scope.key.indexOf('mo__range') ? 1 : 0.01
            }
                $scope.filterTitle = function() {
                return $scope.filter.settings.a2 && angular.lowercase($scope.filter.settings.a2) != '[текст]' ? $scope.filter.settings.a2 : $scope.filter.settings.a1;
            }
            $scope.toggledFilter = function() {
                $scope.filter.toggled = !$scope.filter.toggled;
            }
        }
    }
});
exlibris.directive('rangeFilterNewContainer',  function() {
    return {
        restrict: 'E',
        scope: {
            filter: '=',
            key: '=',
            setings: '=',
            show_slide: '='
        },
        template:   '<div class="f-row__inner">'+
                        '<a href class="text-title" ng-click="toggledFilter()" ng-bind="filterTitle()"></a>'+
                        '<div class="slider-wrap" collapse="filter.toggled" ng-if="setings || show_slide">'+
                            '<div class="left">'+
                                '<span class="s-label">от</span>'+
                                '<input type="text" numbers-only min="filter.slider.min" format="number" max="filter.slider.max" class="rzslider-value" ng-change="filter.list[0]=filter.slider.min" ng-model="filter.slider.min">'+
                            '</div>'+
                            '<div class="right">'+
                                '<span class="s-label">до</span>'+
                                '<input type="text" numbers-only min="filter.slider.min" format="number" max="filter.slider.max" class="rzslider-value" ng-change="filter.list[1]=filter.slider.max" ng-model="filter.slider.max">'+
                            '</div>'+
                            '<rzslider rz-slider-floor="filter.slider.floor" id="rzslider_{$ key $}"'+
                                'rz-slider-ceil="filter.slider.ceil"'+
                                'rz-slider-model="filter.slider.min"'+
                                'rz-slider-high="filter.slider.max"'+
                                'rz-slider-precision="{$ filter.slider.precision $}"'+
                                'rz-slider-step="{$ filter.slider.step $}">'+
                            '</rzslider>'+
                            '<div class="wrap-min-max clear">'+
                                '<span class="left s-label">min</span>'+
                                '<span class="right s-label">max</span>'+
                            '</div>'+
                        '</div>'+
                    '</div>',
        controller: function($scope) {
            $scope.filter.toggled = false;
            $scope.filter.slider = {
                min: angular.copy($scope.filter.slider && $scope.filter.slider.min?$scope.filter.slider.min:$scope.filter.list[0]),
                max: angular.copy($scope.filter.slider && $scope.filter.slider.max?$scope.filter.slider.max:$scope.filter.list[1]),
                floor: angular.copy($scope.filter.slider && $scope.filter.slider.floor?$scope.filter.slider.floor:$scope.filter.list[0]),
                ceil: angular.copy($scope.filter.slider && $scope.filter.slider.ceil?$scope.filter.slider.ceil:$scope.filter.list[1]),
                precision: ~$scope.key.indexOf('mo__range') ? 0 : 2,
                step: ~$scope.key.indexOf('mo__range') ? 1 : 0.01
            }
            $scope.filterTitle = function() {
                return $scope.filter.settings.a2 && angular.lowercase($scope.filter.settings.a2) != '[текст]' ? $scope.filter.settings.a2 : $scope.filter.settings.a1;
            }
            console.log($scope.show_slide, '$scope.show_slide');
            $scope.showSlider = function(){
                if (typeof(angular.element($('.change_product.icon-moon')[0]).scope().show_slider) == 'undefined'){
                    return $scope.setings;
                }
                return $scope.setings && angular.element($('.change_product.icon-moon')[0]).scope().show_slider;
            }
            $scope.toggledFilter = function() {
                $scope.filter.toggled = !$scope.filter.toggled;
            }
        }
    }
});

exlibris.directive('filterContainer', ['dataService', function(dataService) {
    return {
        restrict: 'E',
        scope: {
            filter: '=',
            key: '=',
            dateTypeSend: '=',
            action: '&',
            hideFD: '='
        },
        template:   '<div ng-hide="filter.hideFederalDistrict" class="f-row__inner">'+
                        '<a href class="text-title" ng-if="!isLookUp()" ng-click="toggledFilter()" ng-bind="filterTitle()"></a>'+

                        '<div class="write-block" ng-if="isLookUp()">'+ // поле с LookUp
                            '<input type="text" placeholder="{$ filterTitle() $}" ng-model="filter.lookUp" ng-change="openFilter()">'+
                            '<a href class="toggle-arrow" ng-class="{open: !filter.toggled}" ng-click="toggledFilter()"></a>'+
                        '</div>'+

                        '<div class="group-check-radio" collapse="filter.toggled">'+

                            '<div class="select-all">'+
                                '<input type="checkbox" ng-init="initSelectAllValue()" ng-model="filter.selectAll" id="{$ filter.selectAllId $}" ng-change="selectAll()">'+
                                '<label for="{$ filter.selectAllId $}">&nbsp;</label>'+
                            '</div>'+

                            '<perfect-scrollbar class="scroller" wheel-propagation="false" wheel-speed="20" refresh-on-change="filter.list | searchTextEntry:filter.lookUp">'+

                                '<div class="wrap-row" ng-repeat="choice in filter.list  | searchTextEntry:filter.lookUp">'+
                                    '<input type="checkbox" id="{$ key + choice.id $}"'+
                                        'ng-init="initSelectValue(choice)" ng-model="choice.selected" ng-change="changeFilters(choice.id)">'+
                                    '<label for="{$ key + choice.id $}">{$ choice.title $}</label>'+
                                '</div>'+

                                '<div class="wrap-row" ng-if="emptyValue()">'+ // значение по умолчанию для пустых значений
                                    '<input type="checkbox" id="{$ key $}null"'+
                                        'ng-init="initSelectEmptyValue()" ng-model="filter.emptyValue"'+
                                        'ng-change="changeEmpty()">'+
                                    '<label for="{$ key $}null">{$ filter.settings.a8 $}</label>'+
                                '</div>'+

                            '</perfect-scrollbar>'+
                        '</div>'+
                    '</div>',
        controller: function($scope) {
            if ($scope.key == 'federal_district__id__in') {
                $scope.filter.hideFederalDistrict = $scope.hideFD;
            }
            $scope.filter.toggled = true;
            $scope.filter.lookUp = '';
            $scope.toggledFilter = function() {
                $scope.filter.toggled = !$scope.filter.toggled;
            }
            $scope.openFilter = function() {
                $scope.filter.toggled = false;
            }
            $scope.isLookUp = function() {
                return $scope.filter.settings.a6 == 'да';
            }
            $scope.filterTitle = function() {
                return $scope.filter.settings.a2 && angular.lowercase($scope.filter.settings.a2) != '[текст]' ? $scope.filter.settings.a2 : $scope.filter.settings.a1
            }
            $scope.initSelectValue = function(choice) {
                choice.selected = true;
            }
            $scope.initSelectEmptyValue = function() {
                $scope.filter.emptyValue = true;
            }
            $scope.initSelectAllValue = function() {
                $scope.filter.selectAll = true;
                $scope.filter.selectAllId = $scope.key + '_selectAll';
            }
            $scope.emptyValue = function() {
                return !/^$|\u002D/.test($scope.filter.settings.a8);
            }
            $scope.changeFilters = function(id) {
                console.log('change filter');
                dataService.badgeTopAt = angular.copy($scope.key + id);
                $scope.action();
            }
            $scope.changeEmpty = function() {
                dataService.badgeTopAt = angular.copy($scope.key + 'null');
                $scope.action();
            }
            $scope.selectAll = function() {
                $scope.filter.emptyValue = $scope.filter.selectAll;
                for (var i = 0; i < $scope.filter.list.length; i++) {
                    $scope.filter.list[i].selected = $scope.filter.selectAll;
                }
                dataService.badgeTopAt = angular.copy($scope.key + '_selectAll');
                $scope.action();
            }
            $scope.showHidefederalDistrict = function() {

            }
        }
    }
}]);

exlibris.directive('filterCustomContainer', ['$rootScope', '$http', '$modal', 'dataFilter', function($rootScope, $http, $modal, dataFilter) {
    return {
        restrict: 'E',
        scope: {
            mentionObjects: '=',
            filter: '=',
            key: '=',
            dateTypeSend: '=',
            action: '&',
            hideFD: '='
        },
        template:   '<div ng-hide="filter.hideFederalDistrict" class="f-row__inner">'+
                        '<a href class="text-title" ng-if="!isLookUp()" ng-click="toggledFilter()" ng-bind="filterTitle()"></a>'+

                        '<div class="write-block" ng-if="isLookUp()">'+ // поле с LookUp
                            '<input type="text" placeholder="{$ filterTitle() $}" ng-model="filter.lookUp" ng-change="openFilter()">'+
                            '<a href class="toggle-arrow" ng-class="{open: !filter.toggled}" ng-click="toggledFilter()"></a>'+
                        '</div>'+

                        '<div class="group-check-radio" collapse="filter.toggled">'+

                            '<div class="select-all">'+
                                '<input type="checkbox" ng-init="initSelectAllValue()" ng-model="filter.selectAll" id="setting-{$ filter.selectAllId $}" ng-change="selectAll()">'+
                                '<label for="setting-{$ filter.selectAllId $}">&nbsp;</label>'+
                            '</div>'+

                            '<perfect-scrollbar class="scroller" wheel-propagation="false" wheel-speed="20" refresh-on-change="filter.list | searchTextEntry:filter.lookUp">'+

                                '<div class="wrap-row" ng-repeat="choice in filter.list  | searchTextEntry:filter.lookUp">'+
                                    '<input type="checkbox" id="setting-{$ key + choice.id $}"'+
                                        'ng-init="initSelectValue(choice)" ng-model="choice.selected" ng-change="changeFilters(choice.id)">'+
                                    '<label for="setting-{$ key + choice.id $}">{$ choice.title $}</label>'+
                                 '<div ng-click="openAddModal(choice.id, choice)" ng-if="is_reffered_object(key)"></div>'+
                                '</div>'+

                                '<div class="wrap-row" ng-if="emptyValue()">'+ // значение по умолчанию для пустых значений
                                    '<input type="checkbox" id="{$ key $}null"'+
                                        'ng-init="initSelectEmptyValue()" ng-model="filter.emptyValue"'+
                                        'ng-change="changeEmpty()">'+
                                    '<label for="{$ key $}null">{$ filter.settings.a8 $}</label>'+
                                '</div>'+

                            '</perfect-scrollbar>'+
                        '</div>'+
                    '</div>',
        controller: function($scope) {
            if ($scope.key == 'federal_district__id__in') {
                $scope.filter.hideFederalDistrict = $scope.hideFD;
            }
            $scope.filter.toggled = true;
            $scope.is_reffered_object = function (key) {
                return key == "referred_object__id__in";
            };

            $scope.openAddModal= function(id, choice){
                var templateUrl = "/angular/templates/mentionObjectWords.html";
                var modalInstance = $modal.open({
                    templateUrl: templateUrl,
                    windowClass: 'mentionObjectWords',
                    controller: function ($scope, $modalInstance) {

//                        $scope.keywords = data;
                        $scope.listWord = "";
                        //$scope.new_keywords = choice.mention_object.keywords.slice();
                        $scope.new_keywords = _.findWhere(dataFilter.data.mention_objects_list, {id: id}).keywords.slice();
                        $scope.color = _.findWhere(dataFilter.data.mention_objects_list, {id: id}).color;
                        $scope.colors = [
                            'e4b7b7',
                            'c5c5c5',
                            'f2ab7b',
                            '99c5cf',
                            'dacc9b',
                            '9bddc3',
                            'b8e598',
                            'd7b1db',
                            'e8e774',
                            'bab2e9',
                            'acc6a2',
                            'b1ceee'
                        ];
                        $scope.isActiveColor = function (color) {
                            return $scope.color == color;
                        };
                        $scope.init = function () {
                            ///////////////////
                            var offset = $('#referred_object__id__in'+id).parent().offset();
                            $('.opensAdds').offset({top: offset.top-138});
                            //////////////////
                        };
                        $scope.setActiveColor = function (color) {
                            return $scope.color = color;
                        };
                        $scope.addList = function(){
                            $scope.addsList = true;
                        };
                        $scope.saveKeywords = function(){
                            _.findWhere(dataFilter.data.mention_objects_list, {id: id}).keywords = $scope.new_keywords;
                            _.findWhere(dataFilter.data.mention_objects_list, {id: id}).color = $scope.color;
                            //console.log(dataFilter.data.mention_objects_list);
                            $modalInstance.close();
                        };
                        $scope.deleteKeyword = function(index){
                            delete $scope.new_keywords[index];
                            $scope.new_keywords = _.compact($scope.new_keywords);
                        };

                        $scope.doAddsList = function(){
                            if(this.listWord.trim()){
                                $scope.add_word = {
                                    word : this.listWord
                                };
                                $scope.new_keywords.push($scope.add_word);
                            }
                            $scope.addsList = false;
                            this.listWord = '';
                        };
                    }
                });
                $scope.opensAdds = !$scope.opensAdds;

            };
            $scope.filter.lookUp = '';
            $scope.toggledFilter = function() {
                $scope.filter.toggled = !$scope.filter.toggled;
            }
            $scope.openFilter = function() {
                $scope.filter.toggled = false;
            }
            $scope.isLookUp = function() {
                return $scope.filter.settings.a6 == 'да';
            }
            $scope.filterTitle = function() {
                return $scope.filter.settings.a2 && angular.lowercase($scope.filter.settings.a2) != '[текст]' ? $scope.filter.settings.a2 : $scope.filter.settings.a1
            }
            $scope.initSelectValue = function(choice) {
                choice.selected = typeof(choice.selected) == "undefined"?true:choice.selected;
            }
            $scope.initSelectEmptyValue = function() {
                $scope.filter.emptyValue = true;
            }
            $scope.initSelectAllValue = function() {
                $scope.filter.selectAll = true;
                $scope.filter.selectAllId = $scope.key + '_selectAll';
            }
            $scope.emptyValue = function() {
                return !/^$|\u002D/.test($scope.filter.settings.a8);
            }
            $scope.changeFilters = function(id) {
                dataFilter.badgeTopAt = angular.copy($scope.key + id);
                $scope.action();
            }
            $scope.changeEmpty = function() {
                dataFilter.badgeTopAt = angular.copy($scope.key + 'null');
                $scope.action();
            }
            $scope.selectAll = function() {
                $scope.filter.emptyValue = $scope.filter.selectAll;
                for (var i = 0; i < $scope.filter.list.length; i++) {
                    $scope.filter.list[i].selected = $scope.filter.selectAll;
                }
                dataFilter.badgeTopAt = angular.copy($scope.key + '_selectAll');
                $scope.action();
            }
            $scope.showHidefederalDistrict = function() {

            }
        }
    }
}]).run(['$templateCache',function($templateCache){
            $templateCache.put('/angular/templates/mentionObjectWords.html','<div class="opensAdds" ng-init="init()"> <div class="body-modal-add"> <div class="title-add"> <p>Изменение объекта</p> </div> <div class="body-add"> <p>Цветовая метка:</p> <div class="colors-add"> <span ng-repeat="color in colors" ng-click="setActiveColor(color)" ng-class="{active: isActiveColor(color)}"><span style="background: #{$ color $}"></span></span> </div> <p>Ключевые слова:</p> <div class="list-add"> <perfect-scrollbar scrolls class="scroll-keywords" first-category="first_category_list" wheel-propagation="true" wheel-speed="10" min-scrollbar-length="20"> <ul ng-repeat="word in new_keywords"> <li data-id="word.id">{$ word.word $} <span ng-click="deleteKeyword($index)"></span></li> </ul> </perfect-scrollbar> <input type="text" ng-model="listWord" my-focus="focus" ng-show="addsList" my-blur="doAddsList()"/> <a href="" ng-click="addList()">добавить</a> </div> <button class="orange-btn" ng-click="saveKeywords()">сохранить</button> </div> </div> </div>');
        }]);
exlibris.directive('directoryBreadcrumbs', ['dataService', '$timeout',  function(dataService, $timeout) {
	return {
		template: '<span ng-repeat="elem in dataService.activeTitle"'+
                'ng-class="{collapsed: elem.collapsed}"'+
                'ng-style="{width: elem.width}"'+
                'class="dropdown dropdown-toggle" on-toggle="watcher(open, elem, $index)">'+
			'<span>{$ elem.title $}</span>'+
			'<ul class="dropdown-menu bread-crumbs-popup">'+
				'<li ng-repeat="brother in elem.brothers">'+
					'<a href ng-click="scrollToArticle(brother)">{$ brother.title $}</a>'+
				'</li>'+
			'</ul>'+
			'</span>',
        link: function(scope, element, attrs) {
            scope.maxWidth = element[0].offsetWidth;
        },
        controller: function($scope) {
            console.log(dataService.activeTitle);
			$scope.scrollToArticle = function(brother) {
			    if (brother.articles && brother.articles.length) {
			        dataService.scrollToElement(brother.id);
			        return false;
			    }
			    if (brother.children && brother.children.length) {
			        dataService.findFullCategory(brother.children, brother.id);
			        return false;
			    }
			    dataService.getAllArticles(brother.id);
			}
            var currentIndex = undefined;
            $scope.watcher = function(open, element, index) {
                if (open) {
                    var $currentPopup = $('.bread-crumbs-popup:visible');
                    if (angular.isUndefined(currentIndex)) {
                        currentIndex = index;
                    } else {
                        if (currentIndex > index) {
                            $currentPopup = $currentPopup.first();
                        } else if (currentIndex < index) {
                            $currentPopup = $currentPopup.last();
                        }

                    }
                    for (var i = 0; i < dataService.activeTitle.length; i++) {
                        dataService.activeTitle[i].width = 'auto';
                        dataService.activeTitle[i].collapsed = false;
                    }
                    element.width = $currentPopup.width();

                    $timeout(function() {
                        var totalWidth = 0,
                            $list = $('.dynamic-breadcrumbs').children('span');

                        $list.each(function() {
                            totalWidth += (index != $(this).index() ? $(this).outerWidth() : 0);
                        });
                        totalWidth += ($list.length - 1) * 8;
                        totalWidth += element.width;

                        if (totalWidth > $scope.maxWidth) {
                            dataService.activeTitle[index != 0 ? 0 : dataService.activeTitle.length - 1].width = вфеу;
                            dataService.activeTitle[index != 0 ? 0 : dataService.activeTitle.length - 1].collapsed = true;
                        }
                        currentIndex = index;
                    });
                } else {
                    if (!$('.bread-crumbs-popup:visible').length) {
                        for (var i = 0; i < dataService.activeTitle.length; i++) {
                            dataService.activeTitle[i].width = 'auto';
                            dataService.activeTitle[i].collapsed = false;
                        }
                        var totalWidth = 0,
                            $list = $('.dynamic-breadcrumbs').children('span');
                        $timeout(function() {
                            $list.each(function() {
                                totalWidth += $(this).outerWidth();
                            });
                            totalWidth += ($list.length - 1) * 8;

                            if (totalWidth > $scope.maxWidth) {
                                dataService.activeTitle[0].width = 40;
                                dataService.activeTitle[0].collapsed = true;
                            }
                        });
                    }

                }
            }
            //$scope.$watch('dataService.activeTitleChange', function() {
            //    $timeout(function() {
            //        for (var i = 0; i < dataService.activeTitle.length; i++) {
            //            dataService.activeTitle[i].width = 'auto';
            //            dataService.activeTitle[i].collapsed = false;
            //        }
            //        var totalWidth = 0,
            //            $list = $('.dynamic-breadcrumbs').children('span');
            //        $timeout(function() {
            //            $list.each(function() {
            //                totalWidth += $(this).outerWidth();
            //            });
            //            totalWidth += ($list.length - 1) * 8;
            //
            //            if (totalWidth > $scope.maxWidth) {
            //                dataService.activeTitle[0].width = 40;
            //                dataService.activeTitle[0].collapsed = true;
            //            }
            //        });
            //    });
            //}, true);
        }
	}
}]);