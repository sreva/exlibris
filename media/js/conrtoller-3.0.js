var exlibris = angular.module('exlibris', ['ui.bootstrap', 'ngSanitize', 'ngCookies', 'ngRoute', 'ngAnimate']);
exlibris.config(function($routeProvider, $locationProvider, $interpolateProvider) {
	$interpolateProvider.startSymbol('{$');
	$interpolateProvider.endSymbol('$}');
	$routeProvider.when('/:user/monitoring_product/:id/', {
		templateUrl: "/template/news.html/",
		controller: "NewsListCtrl",
        resolve:{
            load: function ($rootScope, $route, dataService) {
            	$rootScope.productId = $route.current.params.id
                $rootScope.$emit('loadStart');
                return dataService.load($route.current.params.id).then(function(res){
                    dataService.data = res;
                    dataService.lastId = res.last_id;
                    dataService.count_end = res.count_end;
                });
            }
        }
	})
	$locationProvider.html5Mode(true);
}).factory('dataService', function ($q, $http, $cookies, $rootScope) {
    return {
        data: {},
        load: function(id) {
            var defer = $q.defer();
           	$http.get('http://'+ window.location.host +'/monitoring-product/' + id + '/').success(function(res){
                defer.resolve(res);
            });
            return defer.promise;
        },
	    titleOffset: [],
	    activeTitle: [],
	    headerObj: null,
	    header: 0,
        lastId: null,
        count_end: null,
        stopLoadArticles: false,
        waitLoadAjax: true,
        scrollToElement: function(id) {
            var currentTitle = angular.element('#' + id);
            if (currentTitle.length) {
                var currentTitleHeight = currentTitle.height(),
                    currentTitleTop = currentTitleHeight == 0 ? 0 : currentTitle.offset().top - angular.element('.wrap-header').height() + currentTitleHeight + 2;
                angular.element("body, html").animate({scrollTop: currentTitleTop});
            }
        },
        findFullCategory: function(children) {
            for (var i=0; i<children.length; i++) {
                if (children[i].articles && children[i].articles.length) {
                    this.scrollToElement(children[i].id);
                    return false;
                }
                if (children[i].children && children[i].children.length) {
                    this.findFullCategory(children[i].children);
                    return false;
                }
                this.getAllArticles();
                return false;
            }
        },
        getAllArticles: function() {
            if (!this.stopLoadArticles) {
                $rootScope.loadingView= true;
                $http.defaults.headers.post['X-CSRFToken'] = $cookies['csrftoken'];
                var data = {
                    date_start: $rootScope.dateStart.replace(/\.+/g, '-'),
                    date_end: $rootScope.dateEnd.replace(/\.+/g, '-'),
                    all: true
                }
                $http.post('http://'+ window.location.host +'/monitoring-product/' + $rootScope.productId + '/', data).success($.proxy(function(res){
                    this.data.monitoring_product_template = res.monitoring_product_template;
                    $rootScope.hideBreadTitleFlag = true;
                    this.stopLoadArticles = true;
                    $rootScope.loadingView= false;
                }, this));
            }
        }
    };
}).factory('globalFlags', function () {
    var toneList = {
        'позитивное': 'green',
        'негативное': 'red',
        'нейтральное': 'grey'
    };
    var roleList = {
        'ведущая': 'full',
        'значимая': 'half',
        'контекстная': 'third'
    }
    return {
        flagType: function(tone, role) {
            return toneList[tone] + ' ' + roleList[role];
        }
    }
}).filter("paintTheWordsFilter", function() {
    return function(input, searchRegex, replaceRegex) {
        return input.replace(new RegExp(searchRegex, 'g'), replaceRegex);
    };
}).directive('ourdatepicker', function() {
    return {
        restrict: 'A',
        require : 'ngModel',
        link : function (scope, element, attrs, ngModelCtrl) {
            $(function(){
                element.datepicker($.datepicker.regional[ "ru" ]);
                element.datepicker('option', {
                    dateFormat:'dd.mm.yy',
                    changeMonth: true,
                    changeYear: true,
                    onSelect: function (date) {
                        scope.$apply(function () {
                            ngModelCtrl.$setViewValue(date);
                        });
                    },
                    beforeShowDay: function(date){
                        var dateStart = new Date(scope.dateStart.split('.')[2], scope.dateStart.split('.')[1] - 1, scope.dateStart.split('.')[0]);
                        var dateEnd = new Date(scope.dateEnd.split('.')[2], scope.dateEnd.split('.')[1] - 1, scope.dateEnd.split('.')[0]);
                        if (date >= dateStart && date <= dateEnd) {
                            return [true, 'ui-state-selected', ''];
                        }
                        return [true, '', ''];
                    }
                });
                element.datepickerBaraban();
            });
        }
    }
}).directive('categoryList', function($compile, $rootScope) {
    return {
        restrict: 'E',
        scope: {
            category: '=',
            pretitle: '='
        },
        templateUrl: 'categoryListTemplate',
        controller: function($scope) {
            $scope.createArray = function(pretitle, title, id, brothers){
                var arr = '';
                if (pretitle) arr = angular.copy(pretitle);
                arr += '{'+
                	'"title":"'+title+'",'+
                	'"id":'+id+','+
                	'"brothers":'+angular.toJson(brothers)+
            	'},';
                return arr;
            }
        },
        compile: function(tElement, tAttr, transclude) {
            var contents = tElement.contents().remove();
            var compiledContents;
            return function(scope, iElement, iAttr) {
                if(!compiledContents) {
                    compiledContents = $compile(contents, transclude);
                }
                compiledContents(scope, function(clone, scope) {
                    iElement.append(clone);
                });
            }
        }
    }
}).directive('singleArticle', function() {
    return {
        restrict: 'E',
        scope: {
            item: '='
        },
        templateUrl: 'singleArticleTemplate',
        controller: function($scope, $filter, $sce, globalFlags) {
            $scope.currentIndex = 0;
            $scope.activeWord = 0;
            $scope.flagType = globalFlags.flagType;
            $scope.status = {
                isCollapsed: true
            }
            $scope.activeWordChange = function(index) {
                $scope.activeWord = index;
                $scope.currentIndex = index;
            }
            $scope.previewWordChange = function(index) {
                $scope.currentIndex = $scope.activeWord;
                $scope.activeWord = index;
            }
            $scope.returnLastActive = function() {
                $scope.activeWord = $scope.currentIndex;
            }
            $scope.paintTheWords = function(html, mo) {
                text = html;
                for(var i = 0; i < mo.length; i++) {
                    for(var j = 0; j < mo[i].keywords.length; j++) {
                        text = $filter('paintTheWordsFilter')(text, mo[i].keywords[j], '<span class="colored-text" style="background-color:#'+ mo[i].color +'">' + mo[i].keywords[j] + '</span>');
                    }
                }
                return $sce.trustAsHtml(text);
            }
            $scope.renderSpeakers = function(speakers) {
                var text = '(';
                for (var i = 0; i < speakers.length; i++)
                    text += speakers[i].fio + (i != speakers.length - 1 ? ', ': '');
                text += ')';
                return text;
            }
            $scope.sourcePopup = true;
            $scope.copyRightPopup = true;
            $scope.urlPopup = true;
            $scope.showSourcePopup = function() {
                $scope.sourcePopup = false;
            }
            $scope.hideSourcePopup = function() {
                $scope.sourcePopup = true;
            }
            $scope.showCopyRightPopup = function() {
                $scope.copyRightPopup = false;
            }
            $scope.hideCopyRightPopup = function() {
                $scope.copyRightPopup = true;
            }
            $scope.showUrlPopup = function() {
                $scope.urlPopup = false;
            }
            $scope.hideUrlPopup = function() {
                $scope.urlPopup = true;
            }
        }
    }
}).directive('renderRightData', function() {
    return {
        scope: {
            value: '=',
            prt: '=',
            popupt: '@'
        },
        template: '<a href ng-class="{active: actived}" popover-trigger="mouseenter" popover="{$ popupt $}" class="icon-moon">' +
                  '</a>' +
                  '<span ng-class="{positive: prtPositive}">{$ render(value, prt) $}</span>',
        link: function(scope) {
            scope.actived = true;
            scope.prtPositive = false;
            scope.render = function(value, prt) {
                var newValue = parseFloat(value) || '';
                if (newValue) {
                	scope.actived = true;
                    if (prt) {
                        if (newValue >= 0.75) scope.prtPositive = true; else scope.prtPositive = false;
                        newValue = newValue.toFixed(2);
                    } else {
                        if (newValue <= 1) {
                            newValue = newValue.toFixed(2);
                        } else if (newValue > 1 && newValue <= 1000) {
                            newValue = Math.round(newValue);
                        } else if (newValue > 1000 && newValue <= 1000000) {
                            newValue = (newValue / 1000).toFixed(2) + ' K';
                        } else if (newValue > 1000000 && newValue <= 1000000000) {
                            newValue = (newValue / 1000000).toFixed(2) + ' M';
                        }
                    }
                } else {
                    scope.actived = false;
                }
                return newValue;
            }
        }
    }
}).directive('headerHeight', function(dataService) {
    return {
        link: function(scope, element, attrs) {
            // dataService.headerObj = element;
        }
    }
}).directive("scroll", function($rootScope, $window, $filter, $http, $cookies, dataService) {
    return function(scope, element, attrs) {
        var raw = element[0];
        angular.element($window).bind("scroll", function() {
            if (!dataService.stopLoadArticles && dataService.waitLoadAjax) {
                if (raw.scrollTop + raw.offsetHeight >= raw.scrollHeight) {
                    $rootScope.loadingLazyLoad = true;
                    dataService.waitLoadAjax = false;
                    $http.defaults.headers.post['X-CSRFToken'] = $cookies['csrftoken'];
                    var data = {
                        date_start: $rootScope.dateStart.replace(/\.+/g, '-'),
                        date_end: $rootScope.dateEnd.replace(/\.+/g, '-')
                    };
                    if (dataService.lastId){
                        angular.extend(data, {
                            last_id: dataService.lastId,
                            count_end: dataService.count_end
                        });
                    }
                    $http.post('http://'+ window.location.host +'/monitoring-product/' + $rootScope.productId + '/', data).success(function(res){
                    	dataService.waitLoadAjax = true;
                        dataService.lastId = res.last_id;
                        dataService.count_end = res.count_end;
                        if (!res.last_id) dataService.stopLoadArticles = true;
                        // ------------------------------------------
                        var currentData = dataService.data.monitoring_product_template,
                            nextData = res.monitoring_product_template,
                            mergeData = function(newArticles, path){
                                var currentlevel = currentData;
                                for (var index=0; index<path.length; index++){
                                    currentlevel = currentlevel[path[index]];
                                }
                                for(var counter=0; counter<newArticles.length; counter++){
                                    currentlevel.push(newArticles[counter]);
                                }
                            };
                        for (var i=0; i<nextData.length; i++){
                            var level_1 = nextData[i];
                            if (level_1.articles.length) mergeData(level_1.articles, [i, 'articles']);
                            if (level_1.children.length){
                                for(var j=0; j<level_1.children.length;j++){
                                    var level_2 = level_1.children[j];
                                    if (level_2.articles.length) mergeData(level_2.articles, [i, 'children', j, 'articles']);
                                    if (level_2.children.length){
                                        for(var k=0; k<level_2.children.length;k++){
                                            var level_3 = level_2.children[k];
                                            if (level_3.articles.length) mergeData(level_3.articles, [i, 'children', j, 'children', k, 'articles']);
                                            if (level_3.children.length){
                                                for(var e=0; e<level_3.children.length;e++){
                                                    var level_4 = level_3.children[e];
                                                    if (level_4.articles.length) mergeData(level_4.articles, [i, 'children', j, 'children', k, 'children', e, 'articles']);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        $rootScope.loadingLazyLoad = false;
                        // ------------------------------------------
                    });
                }
            }
            var res = [],
                headerHeight = 120;
            angular.forEach(dataService.titleOffset, function(item) {
                res.push({'top': item.elem.offset().top, 'list': item.list});
            });
            res = $filter('orderBy')(res, 'top');
            for (var i = 0; i < res.length; i++) {
                if (i < res.length - 1) {
                    if (res[i].top < pageYOffset + headerHeight && pageYOffset + headerHeight < res[i + 1].top) {
                        dataService.activeTitle = res[i].list;
                    }
                }
            }
/*            for (var i = 0; i < res.length; i++){
                if (i < res.length - 1) {
                    console.log('pageYOffset type 1 ', pageYOffset);
                    if (res[i].top < pageYOffset + headerHeight - 30 && pageYOffset + headerHeight - 30 < res[i + 1].top) {
                        dataService.activeTitle = res[i].list;
                    }
                }
                if (i == res.length - 1 && res[i].top < pageYOffset + headerHeight - 30) {
                    console.log('pageYOffset type 2 ', pageYOffset);
                    dataService.activeTitle = res[res.length - 1].list;
                }
            }*/
            scope.$apply();
        });
    };
}).directive('titleInfo', function($rootScope, dataService){
    return {
        scope: {
            title: '=',
            titleList: '=',
            catId: '=',
            brothers: '='
        },
        template: '<span ng-hide="hideBreadTitle" ng-repeat="elem in list">{$ elem.title $}</span>',
        link: function(scope, elem, attrs) {
        	var str = scope.titleList;
        	scope.list = angular.fromJson('[' + str.substring(0, str.length - 1) + ']');
        	scope.list.push({
        		id: scope.catId,
        		title: scope.title,
        		brothers: scope.brothers
        	});
            dataService.titleOffset.push({
            	'elem': elem,
            	'list': scope.list
            });
            console.log(dataService, '!!!!!');
			scope.hideBreadTitle = false;
			if ($rootScope.hideBreadTitleFlag) {
				scope.hideBreadTitle = true;
				$rootScope.hideBreadTitleFlag = false;
			}
            scope.$root.$emit('loadStart');
        }
    }
}).directive('dynamicBreadcrumbs', function(dataService) {
	return {
		template: '<span ng-repeat="elem in dataService.activeTitle" class="dropdown dropdown-toggle">'+
			'<span>{$ elem.title $}</span>'+
			'<ul class="dropdown-menu bread-crumbs-popup">'+
				'<li ng-repeat="brother in elem.brothers">'+
					'<a href ng-click="scrollToArticle(brother)">{$ brother.title $}</a>'+
				'</li>'+
			'</ul>'+
			'</span>',
		link: function(scope, elem, attrs) {
            scope.scrollToArticle = function(brother) {
                console.log('brother', brother);
                if (brother.articles && brother.articles.length) {
                    dataService.scrollToElement(brother.id);
                    return false;
                }
                if (brother.children && brother.children.length) {
                    dataService.findFullCategory(brother.children);
                    return false;
                }
                dataService.getAllArticles();
            }
        }
	}
}).controller('headerCtrl', function($rootScope, $scope, $http, $cookies, $filter, $location, dataService) {
    $rootScope.visibleMenu = [];
    $scope.dataService = dataService;
    $scope.showFiltersList = false;
    $scope.generalFilters = dataService.data.product[0].referred_objects;
    $scope.showHideFiltersList = function() {
        $scope.showFiltersList = !$scope.showFiltersList;
    }
    $rootScope.dateStart = angular.copy(dataService.data.product[0].date_start.replace(/\-+/g, '.'));

    $rootScope.dateEnd = angular.copy(dataService.data.product[0].date_end.replace(/\-+/g, '.'));

    $scope.dateType = dataService.data.product[0].type;
    $scope.title = dataService.data.product[0].title;
    if ($scope.dateType == "day") {
    	$scope.daysCount = 1;
    } else if ($scope.dateType == "week") {
    	$scope.daysCount = 7;
    } else if ($scope.dateType == "month") {
    	$scope.daysCount = 30;
    }
    var menu = dataService.data.monitoring_product_template;
    for(var i = 0; i < menu.length; i++) {
        $rootScope.visibleMenu.push(menu[i]);
    }
    $scope.openedStart = false;
    $scope.openedEnd = false;
    $scope.filterWidgetNull = function(mentionObjectId, tonalityId){
        var getDate = '?reffered_object_id='+mentionObjectId+'&date-start='+$scope.dateStart.replace(/\.+/g, '-')+'&date-end='+$scope.dateEnd.replace(/\.+/g, '-');
        if (tonalityId){
            getDate += '&emotional_tone_object__id='+tonalityId;
        }
        window.location.href = $location.path() + 'widget0/' + getDate;
    }
/*    $scope.rerenderMenu = function(children, id) {
        if (children && children.length) {
            $rootScope.visibleMenu = !id ? [] : [{
                title: "",
                children: $rootScope.visibleMenu,
                id: null,
                back: true
            }];
            $rootScope.visibleMenu = $rootScope.visibleMenu.concat(children);
            getFirstCategoryId(children);
        } else {
            for (var i = 0; i < $rootScope.visibleMenu.length; i++){
                if ($rootScope.visibleMenu[i].id == id){
                    $rootScope.visibleMenu[i].active = true;
                } else {
                    $rootScope.visibleMenu[i].active = false;
                }
            }
            scrollToElement(id);
        }
    }*/
/*    function scrollToElement(id) {
        var currentTitle = angular.element('#' + id);
        if (currentTitle.length) {
            angular.element("body, html").animate({scrollTop: currentTitle.offset().top - angular.element('.wrap-header').height() + currentTitle.height() + 2});
        }
    }*/
/*    function getFirstCategoryId(children) {
        if (children.length){
            if (children[0].articles && children[0].articles.length) {
                if (!dataService.stopLoadArticles) {
                    $http.defaults.headers.post['X-CSRFToken'] = $cookies['csrftoken'];
                    var data = {
                        date_start: $rootScope.dateStart.replace(/\.+/g, '-'),
                        date_end: $rootScope.dateEnd.replace(/\.+/g, '-'),
                        all: true
                    }
                    $http.post('http://'+ window.location.host +'/monitoring-product/' + $rootScope.productId + '/', data).success(function(res){
                        dataService.data.monitoring_product_template = res.monitoring_product_template;
                        dataService.stopLoadArticles = true;
                        scrollToElement(children[0].id);
                    });
                }
            }
        }
    }*/
    $scope.getNewsList = function(datePicker, val) {
        if (datePicker == 'start') {
            $scope.dateEnd = moment(new Date(val.split('.')[2], val.split('.')[1] - 1, val.split('.')[0])).add($scope.daysCount, 'days').format('DD.MM.YYYY');
        } else if (datePicker == 'end') {
            $scope.dateStart = moment(new Date(val.split('.')[2], val.split('.')[1] - 1, val.split('.')[0])).subtract($scope.daysCount, 'days').format('DD.MM.YYYY');
        }
        dateFilter();
    }
    $scope.nextPrev = function(type) {
        var operType = type == "prev" ? 'subtract' : 'add';
        $scope.dateStart = moment(new Date($scope.dateStart.split('.')[2], $scope.dateStart.split('.')[1] - 1, $scope.dateStart.split('.')[0]))[operType]($scope.daysCount, 'days').format('DD.MM.YYYY');
        $scope.dateEnd = moment(new Date($scope.dateEnd.split('.')[2], $scope.dateEnd.split('.')[1] - 1, $scope.dateEnd.split('.')[0]))[operType]($scope.daysCount, 'days').format('DD.MM.YYYY');
        dateFilter();
    }
    $scope.goToWidgetNull = function() {
        window.location.href = $location.path() + 'widget0/';
    }
    $scope.redirectToIndex = function(url) {
        window.location.href = '/' + url;
    }
    function dateFilter() {
        $rootScope.loadingView = true;
        $http.defaults.headers.post['X-CSRFToken'] = $cookies['csrftoken'];
        $http.post('http://'+ window.location.host +'/monitoring-product/'+ $rootScope.productId +'/', {
            date_start: $scope.dateStart.replace(/\.+/g, '-'),
            date_end: $scope.dateEnd.replace(/\.+/g, '-'),
            date_type: $scope.dateType
        }).success(function(res) {
        	$rootScope.hideBreadTitleFlag = true;
            dataService.activeTitle.length = 0;
            $rootScope.loadingView = false;
            dataService.data = res;
            dataService.titleOffset.length = 0;
            var menu = dataService.data.monitoring_product_template;
            $rootScope.visibleMenu.length = 0;
            for(var i = 0; i < menu.length; i++) {
                $rootScope.visibleMenu.push(menu[i]);
            }
            angular.element("body, html").animate({scrollTop: angular.element('html').offset().top});
            // $rootScope.$emit('loadStart');
        });
    }
}).controller('NewsListCtrl', function($scope, $filter, dataService) {
    $scope.category = dataService.data.monitoring_product_template;
    $scope.dataService = dataService;
    $scope.$root.dataService = dataService;
    root = $scope.$root;
    $scope.$root.$on('loadStart', function (){
        var res = [];
        angular.forEach(dataService.titleOffset, function(item) {
            res.push({'top': item.elem.offset().top, 'list': item.list});
        });
        res = $filter('orderBy')(res, 'top');
        if (res[0].list) dataService.activeTitle = res[0].list;
        if(!$scope.$root.$$phase) {$scope.$root.$apply();}
        dataService.header = 125;
        if(!$scope.$root.$$phase) {$scope.$root.$apply();}
    });
}).run(['$rootScope', function($root) {
    $root.$on('$routeChangeStart', function(e, curr, prev) {
        if (curr) {
            if (curr.$$route && curr.$$route.resolve) {
                $root.loadingView = true;
            }
        }
    });
    $root.$on('$routeChangeSuccess', function(e, curr, prev) {
        $root.loadingView = false;
    });
    $root.hideBreadTitleFlag = true;
}]);