var exlibris = angular.module('exlibris', ['ngSanitize', 'ngCookies', 'ngRoute', 'ngAnimate']);
exlibris.config(function($routeProvider, $locationProvider, $interpolateProvider) {
	$interpolateProvider.startSymbol('{$');
	$interpolateProvider.endSymbol('$}');
	$locationProvider.html5Mode(true);
}).controller('vidgetNullCtrl', function($scope, $http, $cookies, $filter) {
	$http.defaults.headers.post['X-CSRFToken'] = $cookies['csrftoken'];
	$scope.save = function() {
		var arr = angular.copy($scope.filters),
			sortedArr = arr.sort(),
			results = [];
		for (var i = 0; i < arr.length - 1; i++) {
			if (sortedArr[i + 1].a3 == sortedArr[i].a3 && sortedArr[i].a3 != '') {
				results.push(sortedArr[i].a3);
			}
		}
		if (!results.length) {
			$http.post(window.location.href, {
				data: JSON.stringify($scope.filters)
			}).success(function(res){
				
			});
		} else {
			alert('Поле "Порядок отображения в списке фильтров" заполнено неверно. Индексы не могут повторятся');
		}
	}
	$scope.upload = function() {
		$http.post(window.location.href, {
			 coppy_id: $scope.configuration
		}).success(function(res){
			$scope.filters = res.data;
		});
	}
	$scope.defaultValues = function(value) {
		var returnedValue = 1;
		if (typeof value.oneTimeBinding === 'undefined') {
			returnedValue = value.a8 == '' ? 0 : 1;
			value.oneTimeBinding = returnedValue;
			return returnedValue;
		} else {
			return value.oneTimeBinding;
		}		
	}
	$scope.configurations = configurations;

	$scope.configuration = $scope.configurations.length ? $scope.configurations[0].value : false;

	$scope.yesNoList = [
		{label: 'да', value: 'да'},
		{label: 'нет', value: 'нет'}
	];
	$scope.scaleOrList = [
		{label: 'list', value: 'list'},
		{label: 'шкала', value: 'шкала'}
	];
	$scope.filters = [];
	for (var i = 0; i < filters.length; i++) {
		filters[i].a2 = filters[i].a2 == '[текст]' ? '' : filters[i].a2;
		$scope.filters.push(filters[i]);
	}
	// console.table($scope.filters);
});