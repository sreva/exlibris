getCookie = function(name) {

    /*
    get cookie utility by name
    Good for AJAX calls
     */
    var cookie, cookies, currentName, currentParts, _i, _len;
    cookies = document.cookie.split(/;\s*/);
    for (_i = 0, _len = cookies.length; _i < _len; _i++) {
      cookie = cookies[_i];
      currentParts = cookie.split("=");
      currentName = decodeURIComponent(currentParts[0]);
      if (currentName === name) {
        return decodeURIComponent(currentParts[1]);
      }
    }
    return null;
  };

(function() {
    var hoverTimeOut = null,
	doRedirect = true;
	var $sotuWarp = $('.sotu-outer'),
        $back = $('.backs-all'),
		area = $(window).height() - $('.header').outerHeight(true),
		paddingTop = area / 2 - $sotuWarp.height() / 2,
        paddingBottom = area / 2 - $sotuWarp.height() / 2,
		$qualityTerm = $('.quality-term'),
		$dropdownMenu = $('.dropdown-menu');
	$sotuWarp.css('padding-top', paddingTop);
	$sotuWarp.css('padding-bottom', paddingBottom);


    // how many milliseconds is a long press?
    var longpress = 500;
    // holds the start time
    var start;

    jQuery( ".sasha" ).on( 'mousedown', function( e ) {
        start = new Date().getTime();
    } );

    jQuery( ".sasha" ).on( 'mouseleave', function( e ) {
        start = 0;
    } );

    jQuery( ".sasha" ).on( 'mouseup', function( e ) {
        if ( new Date().getTime() >= ( start + longpress )  ) {
                hoverTimeOut = setTimeout(function() {
                doRedirect = false;
                $sotuWarp.addClass('edited');
                $back.addClass('open');

            }, 100);
        } else {

        }
    } );

}());
(function() {
    var hoverTimeOut = null,
	doRedirect = true;
	var $sotuWarp = $('.sotu-outer'),
        $back = $('.backs-all'),
		area = $(window).height() - $('.header').outerHeight(true),
		paddingTop = area / 2 - $sotuWarp.height() / 2,
        paddingBottom = area / 2 - $sotuWarp.height() / 2,
		$qualityTerm = $('.quality-term'),
		$dropdownMenu = $('.dropdown-menu');
	$sotuWarp.css('padding-top', paddingTop);
	$sotuWarp.css('padding-bottom', paddingBottom);


    // how many milliseconds is a long press?
    var longpress = 500;
    // holds the start time
    var start;

    jQuery( ".go-inside" ).on( 'mousedown', function( e ) {
        start = new Date().getTime();
    } );

    jQuery( ".go-inside" ).on( 'mouseleave', function( e ) {
        start = 0;
    } );

    jQuery( ".go-inside" ).on( 'mouseup', function( e ) {
        if ( new Date().getTime() >= ( start + longpress )  ) {
                hoverTimeOut = setTimeout(function() {
                doRedirect = false;
                $sotuWarp.addClass('edited');
                $back.addClass('open');

            }, 100);
        } else {
           window.location.href = $(this).data('href');
        }
    } );

}());
$(function() {
    var hoverTimeOut = null,
	doRedirect = true;
	var $sotuWarp = $('.sotu-outer'),
        $back = $('.backs-all'),
		area = $(window).height() - $('.header').outerHeight(true),
		paddingTop = area / 2 - $sotuWarp.height() / 2,
        paddingBottom = area / 2 - $sotuWarp.height() / 2,
		$qualityTerm = $('.quality-term'),
		$dropdownMenu = $('.dropdown-menu');
	$sotuWarp.css('padding-top', paddingTop);
	$sotuWarp.css('padding-bottom', paddingBottom);

	$(document).
        on('touchstart mousedown', '.sasha', function(){

	}).
        on('touchend mouseup', '.sasha', function(){
        clearTimeout(hoverTimeOut);
            doRedirect = false
    }).
        on('click', '.go-inside', function(){
		if (doRedirect) window.location.href = $(this).data('href');
	});
    $('.backs-all').click(function(){
        var asd = $(this).closest('.hexagon').find('.quality-term');
        asd.hide();
    });
	$('.go-inside').parent().find('.favorite').click(function() {
		var $popup = $(this).parent().find('.quality-term');
		$popup.is(':visible') ? $popup.hide() : $popup.show();
		return false;
	}).
        end().parent().find('form').submit(function() {
		$(this).parent().hide();
		$sotuWarp.removeClass('edited');
		return false;
	}).
        end().parent().find('.shut').click(function() {
    	$qualityTerm.hide();
    	$sotuWarp.removeClass('edited');
		return false;
	});
    $('.backs-all').on('click', function(){
       var back = $(this);
        back.parent().find('.sasha').removeClass('edited');
        back.removeClass('open');
    });
	$('.settings-handle').click(function() {
		$dropdownMenu.is(':visible') ? $dropdownMenu.hide() : $dropdownMenu.show();
		return false;
	});

    $('.dialogs').on('click', 'button.no-no', function(e){
        $( ".dialogs" ).dialog( "close" );
    });

    $('body').on('click', '.js-rename', function(){
       var hText = $(this).parent().find('input');
        if(hText.attr('disabled', 'disabled')){
            hText.removeAttr('disabled');
            hText.focus();
        }
        //hText.change(function(){
        //    $( ".dialogs" ).dialog( "open" );
        //    var obj = $(this);
        //    var val = obj.val();
        //
        //    $('.dialogs .yes-yes').click(function(){
        //        $('.dialogs .yes-yes').off();
        //        if (!val.trim().length){
        //            return false;
        //        }
        //        $.ajax({
        //            url: "/product/rename/" + obj.attr('data-pk') + "/",
        //            type: "POST",
        //            data: {
        //                  name : val,
        //                  no_more_ask : $('#no-more').is(':checked')
        //                  }
        //            ,
        //            headers: {'X-CSRFToken': getCookie ('csrftoken')},
        //            success: function() {
        //                $(".dialogs").dialog("close");
        //            },
        //            error: function() {
        //                console.log("Error");
        //                $(".dialogs").dialog("close");
        //            }
        //        });
        //    });
        //    $(this).closest('.sasha').removeClass('edited');
        //
        //});
    });
	$(document).on('touchstart', function(e){
	    if ($(e.target).closest(".sotu-inner").length === 0) {
	    	$qualityTerm.hide();
        	$sotuWarp.removeClass('edited');
	    }
	    if ($(e.target).closest(".dropdown-menu").length === 0) {
        	$dropdownMenu.hide()
	    }
	});
	$(document).keyup(function(e) {
		if (e.keyCode == 27) {
			$qualityTerm.hide();
			$sotuWarp.removeClass('edited');
			$dropdownMenu.hide();
		}

	});
    $(".dialogs").dialog({
        autoOpen: false,
        show: {
            effect: "blind",
            duration: 1000
        },
        hide: {
            effect: "explode",
            duration: 1000
        }
    });
});
$(window).resize(function() {
	var $sotuWarp = $('.sotu-outer'),
		area = $(window).height() - $('.header').outerHeight(true),
		paddingTop = area / 2 - $sotuWarp.height() / 2;
		paddingBottom = area / 2 - $sotuWarp.height() / 2;
	$sotuWarp.css('padding-top', paddingTop);
	$sotuWarp.css('padding-bottom', paddingBottom);
});