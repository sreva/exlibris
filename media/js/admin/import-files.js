$(function() {
	var $importfileForm = $('#importfile_form'),
		clearSubmitForm = false;
	$importfileForm.submit(function() {
		if (!clearSubmitForm) {
			var valid = true,
				formData = {
					fileImport: $(this).find('input[name="file_import"]'),
					fileType: $(this).find('select[name="file_type"]'),
					fileProccesing: $(this).find('select[name="proccesing"]'),
					project: $(this).find('select[name="project"]'),
					product: $(this).find('select[name="product"]')
				};
			for (row in formData) {
				if (formData[row].prop('multiple')) {
					if (!formData[row].find('option').length) {
						valid = false;
						formData[row].closest('.selector-chosen').addClass('errors');
					} else {
						formData[row].closest('.selector-chosen').removeClass('errors');
					}
				} else if (formData[row].attr('type') == 'file') {
					if (formData[row].closest('.file-upload').length || formData[row].val()) {
						formData[row].closest('.form-row').removeClass('errors');
					} else {
						valid = false;
						formData[row].closest('.form-row').addClass('errors');
					}
				} else {
					if (!formData[row].val()) {
						valid = false;
						formData[row].closest('.form-row').addClass('errors');
					} else {
						formData[row].closest('.form-row').removeClass('errors');
					}
				}
			}
			if (valid) {
				$.ajax({
				    url: "/monitoring_product/check_valid_constructors_widget0/",
				    cache: false,
				    type: "POST",
				    dataType: "json",
				    data: {
				        'csrfmiddlewaretoken': getCookie('csrftoken'),
				        'project_id': formData.project.val(),
				        'list_id': function() {
				        	var list = [];
				        	formData[row].find('option').each(function() {
			        			list.push(+$(this).val());
				        	});
				        	return JSON.stringify(list);
				        }
				    },
				    success: function (data) {
				        if (data.status == 'OK') {
				        	clearSubmitForm = true;
				        	$importfileForm.submit();
				        }
				    }
				});
			}
			return false;
		}
	});
	var $id_project = $('#id_project'),
		$id_product_from = $('#id_product_from');
	$id_project.change(function() {
		renderProducts($(this).val());
	});
	if ($id_project.val().length) renderProducts($id_project.val())
	function renderProducts(projectId) {
		$.ajax({
		    url: "/api/filter_products_from_project/",
		    cache: false,
		    type: "POST",
		    dataType: "json",
		    data: {
		        'csrfmiddlewaretoken': getCookie('csrftoken'),
		        'project_id': projectId
		    },
		    success: function (data) {
		        if (data.status == 'OK') {
		        	SelectBox.move_all("id_product_to", "id_product_from");
		        	SelectFilter.refresh_icons("id_product");
		        	$id_product_from = $('#id_product_from');
		        	$id_product_from.find('option').hide();
					for (var i = 0; i < data.monitoring_products.length; i++) {
						$id_product_from.find('option[value="'+ data.monitoring_products[i] +'"]').show();
					}
		        } else {
		        	$('#id_product_to').closest('.selector-chosen').addClass('errors');
		        }
		    }
		});
	}
});
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}