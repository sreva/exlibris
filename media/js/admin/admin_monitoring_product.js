var form_valid = false,
	week_monitoring = false,
	dataInfo = null;
$(function() {
	var $widget_priorities = $('.widget-priorities'),
		$widget_monitoring = $('.widget-monitoring'),
		$monitoringproduct_form = $('#monitoringproduct_form'),
		$list_of_object = $('.list-of-object'),
		editing = $('#editing').length ? true : false,
		$c_priorities = $('.c-priorities'),
		$field_title = $('.field-title'),
		$field_type = $('.field-type'),
		$colorSwatchesBlock = $('.color-swatches-block');
	function showRows() {
		if ($('#id_type').val() == 'week') {
			week_monitoring = true;	
		}
		$field_title.show();
		$field_type.show();
		if (editing) {
			if (dataInfo.category_list && dataInfo.category_list.length) $widget_priorities.show();
			if (dataInfo.mention_objects_list && dataInfo.mention_objects_list.length) $widget_monitoring.show();
		}
	}
	function hideRows() {
		$field_title.hide();
		$field_type.hide();
		$widget_priorities.hide();
		$widget_monitoring.hide();
	}
	$monitoringproduct_form.find('input[name="_addanother"]').hide();
	$monitoringproduct_form.find('input[name="_continue"]').hide();

	if (editing) {
		$.ajax({
            url: "/api/get_mp_mention_obj_category/",
            cache: false,
            type: "POST",
            data: {
                'csrfmiddlewaretoken': getCookie('csrftoken'),
                'id': $('#editing').val()
            },
            dataType: "json",
            success: function (data) {
                if (data.status == 'OK') {
                	build_widgets(data);
                	dataInfo = data;
					showRows();
                }
            }
        });
	}
	$('#id_title, #id_type').change(function() {
		validate_form($monitoringproduct_form);
	});

	var $arrow = $('.vertical-scroll').find('.arrow'),
		$clicable_label, $sections;
	$widget_monitoring.on('click', 'ul label', function() {// select object
		if ($(this).parent().find('input[type="checkbox"]').prop('checked')) {
			var index = $(this).parents('li').index(),
				id = $(this).data().id;
			$sections.hide().filter(function() {
				return this.id == id
			}).show();
			$clicable_label.removeClass('active');
			$(this).addClass('active');
			$arrow.css('top', 5 * index + 1 + index * 22);
		}
	}).on('click', '.add-keyword', function() {// add keyword
		var $ul = $(this).prev(),
			$li = $ul.find('li').last().clone(true),
			$input = $li.find('input[type="text"]');
		$input.attr('name', 'keyword-' + $li.data().sendId + (+$input.attr('name').substr(-1) + 1)).val('');
		$ul.append($li);
		return false;
	});
	// select project
	$('#id_project').change(function() {
		var value = $(this).val();
		if (!value) {
			hideRows();
		} else {
/*			validate_form($monitoringproduct_form);
			$.ajax({
                url: "/api/get-mentions-list/",
                cache: false,
                type: "POST",
                data: {
                    'csrfmiddlewaretoken': getCookie('csrftoken'),
                    'id': value
                },
                dataType: "json",
                success: function (data) {
                    if (data.status == 'OK') {
                    	build_widgets(data);
						$field_title.show();
						$field_type.show();
						dataInfo = data;
						if ($('#id_type').val()) {
							$('#id_type').change();
						}
                    }
                }
            });*/
			$field_title.show();
			$field_type.show();
		}
	});
	// select type of monitoring
	$('#id_type').change(function() {
		var value = $(this).val();
		if (value) {
			if (value == 'week') {
				week_monitoring = true;
			} else {
				week_monitoring = false;
			}
			showRows();
		} else {
			$widget_priorities.hide();
			$widget_monitoring.hide();
		}
	});
	$monitoringproduct_form.on('submit', function(){
		validate_form($(this), true);
		if (form_valid) {
			var data = {};
			if (editing) {
				data.category_list = [];
				data.mention_objects_list = [];
				data.product = {};
				$widget_priorities.find('li').each(function() {
					data.category_list.push({
						id: $(this).data().sendId,
						index: $(this).find('input[type="text"]').val()
					});
				});
				var $keywords_block = $('.keywords-block');
				$list_of_object.children('.column').first().find('li').each(function() {
					if ($(this).find('input[type="checkbox"]').prop('checked')) {
						var added_keywords = [],
							obj_id = $(this).data().sendId;
						$keywords_block.find('li').filter(function() {
							return $(this).data().sendId == obj_id
						}).each(function() {
							var current_word = $(this).find('input[type="text"]').val();
							if (current_word) {
								added_keywords.push(current_word);
							}
						});
						data.mention_objects_list.push({
							id: $(this).data().sendId,
							color: $(this).find('.color-picker').data().color,
							keywords: added_keywords
						});
					}
				});
			}

			data.product.settings = $(this).find('#id_settings').val();
			data.product.article_count = $(this).find('select[name="article_count"]').val();
			data.product.project_name = $(this).find('select[name="project"]').val();
			data.product.article_count = $(this).find('#id_article_count').val();
			data.product.title = $(this).find('input[name="title"]').val();
			data.product.type = $(this).find('select[name="type"]').val();
			data.article_count = $(this).find('input[name="article_count"]').val();

			var saveToBdUrl = editing ? '/api/mp_mention_obj_category_update/' : '/api/mp_mention_obj_category/';
			if (editing) {
				data.id = $('#editing').val();
			}
			$.ajax({
                url: saveToBdUrl,
                cache: false,
                type: "POST",
                data: {
                    'csrfmiddlewaretoken': getCookie('csrftoken'),
                    data: JSON.stringify(data)
                },
                dataType: "json",
                success: function (data) {
                	if (data.status == 'OK') {
                		window.location.href = '/admin/monitoring_product/monitoringproduct/'
                	}
                }
            });
		}
		return false;
	})
	function build_widgets(data) {
    	var category_list = data.category_list,
    		mention_objects_list = data.mention_objects_list,
    		$ul = $widget_priorities.find('ul').empty();
		// Содержание
		if (category_list && category_list.length) {
			for (var i = 0; i < category_list.length; i++) {
				$ul.append('<li data-send-id="'+ category_list[i].id +'">' +
								'<label for="c-fild-'+ category_list[i].id +'">'+ category_list[i].title +'</label>' +
								'<span class="page">' +
									'<input type="text" name="paragraph-'+ category_list[i].id +'" id="c-fild-'+ category_list[i].id +'" value="'+ ('index' in category_list[i] ? category_list[i].index : 1) +'"/>' +
								'</span>' +
							'</li>');
			}
			$c_priorities.show();
		} else $c_priorities.hide()
    	// Объекты мониторинга
    	if (mention_objects_list && mention_objects_list.length) {
			var $monitoring_objects = $list_of_object.children('.column').first().children('ul').empty(),
				$keywords_block = $('.keywords-block').empty(),
				tab_elements = function(our_object) {
					var lis = '';
					if (!editing || !our_object.keywords.length) {
						for (var j = 0; j < 4; j++) {
							lis += '<li data-send-id="'+ our_object.id +'"><input type="text" name="keyword-'+ our_object.id + j +'" value=""/></li>';
						}
					} else {
						for (var j = 0; j < our_object.keywords.length; j++) {
							lis += '<li data-send-id="'+ our_object.id +'"><input type="text" name="keyword-'+ our_object.id + j +'" value="'+ our_object.keywords[j].word +'"/></li>'
						}
					}
					return lis;
				};
			for (var i = 0; i < mention_objects_list.length; i++) {
				var color = 'color' in mention_objects_list[i] ? mention_objects_list[i].color : '666666';
				if (!editing) {
					$monitoring_objects.append('<li data-send-id="'+ mention_objects_list[i].id +'">' +
													'<div class="left">' +
														'<label data-id="c-tab-'+ mention_objects_list[i].id +'" >'+ mention_objects_list[i].title +'</label>' +
														'<span class="color-picker">&nbsp;</span>' +
														'<span class="color-swatches">&nbsp;</span>' +
														'<input type="checkbox" name="check-'+ mention_objects_list[i].id +'" '+ (i == 0 ? 'checked' : '') +'/>' +
													'</div>' +
												'</li>');
				} else {
					$monitoring_objects.append('<li data-send-id="'+ mention_objects_list[i].id +'">' +
													'<div class="left">' +
														'<label data-id="c-tab-'+ mention_objects_list[i].id +'" >'+ mention_objects_list[i].title +'</label>' +
														'<span class="color-picker">&nbsp;</span>' +
														'<span class="color-swatches">&nbsp;</span>' +
														'<input type="checkbox" name="check-'+ mention_objects_list[i].id +'" '+ (mention_objects_list[i].checked ? 'checked' : '') +'/>' +
													'</div>' +
												'</li>');
				}
				$keywords_block.append('<section id="c-tab-'+ mention_objects_list[i].id +'">'+
											'<ul>' +
												tab_elements(mention_objects_list[i]) +
											'</ul>' +
											'<button class="add-keyword">Добавить слово</button>' +
										'</section>');
				$clicable_label = $widget_monitoring.find('ul').find('label');
				$sections = $('.keywords-block').children('section');
				$monitoring_objects.find('li').filter(function() {
					return $(this).data().sendId == mention_objects_list[i].id
				}).find('.color-picker').colpick({
					submit: 0,
					onChange: function(hsb, hex, rgb, el, bySetColor) {
						var $element = $(el);
						$element.css('background-color', '#' + hex).data({color: hex});
						$sections.filter(function() {
							return this.id == $element.prev().data().id
						}).find('input').css('color', '#' + hex);
					}
				}).colpickSetColor(color);
			}
			$clicable_label.first().click();
    	}
	}
	$widget_monitoring.on('click', '.color-swatches', function() {
		$(this).parents('li').append($colorSwatchesBlock.show());
		return false;
	});
	$colorSwatchesBlock.on('click', 'span', function() {
		var color = $(this).data().color;
		$(this).parents('li').find('.color-picker').css('background-color', '#' + color).colpickSetColor(color);
		$colorSwatchesBlock.hide();
		return false;
	});
	$(document).click(function(e) {
	    if ($(e.target).closest(".color-swatches-block").length === 0) {
	        $colorSwatchesBlock.hide();
	    }
	});
});
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
function validate_form($form, submit) {
	var $project = $form.find('select[name="project"]'),
		$title = $form.find('input[name="title"]'),
		$type_of_monitoring = $form.find('select[name="type"]'),
		valid = true;
	if (!$project.val()) {
		valid = false;
		 if (submit) $project.parents('.form-row').addClass('errors');
	} else {
		$project.parents('.form-row').removeClass('errors');
	}
	if (!$title.val()) {
		valid = false;
		if (submit) $title.parents('.form-row').addClass('errors');
	} else {
		$title.parents('.form-row').removeClass('errors')
	}
	if (!$type_of_monitoring.val()) {
		valid = false;
		if (submit) $type_of_monitoring.parents('.form-row').addClass('errors');
	} else {
		$type_of_monitoring.parents('.form-row').removeClass('errors')
	}
	if (valid) {
		form_valid = true;
	} else {
		form_valid = false;
		if (submit) $('html, body').animate({scrollTop: $('#content').offset().top}, 300);
	}
}