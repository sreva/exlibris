/**
 * Factory for flags on articles
 */
exlibris.factory('globalFlags', function () {
    var toneList = {
        'позитивное': 'green',
        'негативное': 'red',
        'нейтральное': 'grey'
    };
    var roleList = {
        'ведущая': 'full',
        'значимая': 'half',
        'контекстная': 'third'
    }
    return {
        flagType: function(tone, role) {
            return toneList[tone] + ' ' + roleList[role];
        }
    }
});