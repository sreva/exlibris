var exlibris = angular
    .module('exlibris', ['ui.bootstrap', 'ngSanitize', 'ngCookies','ngRoute', 'ngAnimate','perfect_scrollbar' ])
    .config(function($routeProvider, $locationProvider, $interpolateProvider) {
        $interpolateProvider.startSymbol('{$');
        $interpolateProvider.endSymbol('$}');

        $locationProvider.html5Mode(true);
    }).
    constant('ROOT_URL', '/static_root/').
    factory('user', function () {
        return user
    }).
    factory('departament_list', function () {
        return departament_list
    }).
    factory('social_list', function () {
        return social_list
    }).
    factory('groups_list', function () {
        return groups_list
    }).
    factory('category_list', function () {
        return category_list
    }).
    factory('directory_date', function(){
        return directory_date

    }).
    factory('category_structure', function(){
        return category_structure
    }).
    factory('first_category_list', function(){
        return first_category_list
    }).
    factory('modalConfirm', ['$modal', '$http', '$cookies', function($modal, $http, $cookies) {
        return function($scope, confirmCallback, no_more_param_name, modal_data) {

            var templateUrl = "/angular/templates/modalConfirm.html";
            var check_no_more = function(){
                var no_more = $('#no-more').is(':checked');
                    if (no_more){
                        var data = {"settings":{}};
                        data['settings'][no_more_param_name] = true;
                        $http.post('/profile/set-application-settings/', data, {"headers":{"X-CSRFToken": $cookies.csrftoken}}).
                            success(function(data, status, headers, config) {
                                console.log(data);
                                $scope.user_settings = data;
                            }).
                            error(function(data, status, headers, config) {
                                console.log(data);
                            });
                        }
            };

            var ask_for_confirm = function(){
                if(no_more_param_name in $scope.user_settings && $scope.user_settings[no_more_param_name]){
                    confirmCallback();
                }
                else {
                    var modalInstance = $modal.open({
                        templateUrl: templateUrl,
                        windowClass: 'modalConfirm',
                        controller: function ($scope, $modalInstance) {
                            $scope.confirmModalBox = modal_data;
                            $scope.confirmModal = function () {
                                confirmCallback();
                                $modalInstance.close();
                                check_no_more();
                                //console.log('ConfirmModal');
                            };
                            $scope.cancelModal = function () {
                                $modalInstance.dismiss('cancel');
                                check_no_more();
                                //console.log('CancelModal');
                                }
                            }
                    });
                }
            };

            if (!('user_settings' in $scope)){
                $http.get('/profile/application-settings/').
                    success(function(data, status, headers, config) {
                        $scope.user_settings = data;
                        ask_for_confirm();
                    }).
                    error(function(data, status, headers, config) {
                        console.log(data);
                    });
            }
            else{
                ask_for_confirm();
            }

        }
    }]).run(['$templateCache',function($templateCache){
            $templateCache.put('/angular/templates/modalConfirm.html','<div class="dialogs modal-dialog" title="Basic dialog"> <div class="modal-header"> <h3 class="modal-title"><p class="icon-moon icon-rename"></p> <span ng-bind-html="confirmModalBox.title"></span> </h3> </div> <div class="modal-body"><span ng-bind-html="confirmModalBox.question"></span> <form action="" class="yes-no"> <button type="button" class="no-no grey-btn" ng-click="cancelModal()">назад</button> <button type="button" class="yes-yes orange-btn" ng-click="confirmModal()">да</button> <div class="no-mores"><input type="checkbox" id="no-more"/> <label for="no-more">Больше не показывать</label></div> </form> </div> </div>');
        }]).
    directive('myFocus', function () {
        return {
            restrict: 'A',
            link: function (scope, element, attr) {
                scope.$watch(attr.myFocus, function (n, o) {
                    if (n != 0 && n) {
                        element[0].focus();
                    }
                });
            }
        };
    }).directive('checklistModel', ['$parse', '$compile', function($parse, $compile) {
  // contains
  function contains(arr, item) {
    if (angular.isArray(arr)) {
      for (var i = 0; i < arr.length; i++) {
        if (angular.equals(arr[i], item)) {
          return true;
        }
      }
    }
    return false;
  }

  // add
  function add(arr, item) {
    arr = angular.isArray(arr) ? arr : [];
    for (var i = 0; i < arr.length; i++) {
      if (angular.equals(arr[i], item)) {
        return arr;
      }
    }
    arr.push(item);
    return arr;
  }

  // remove
  function remove(arr, item) {
    if (angular.isArray(arr)) {
      for (var i = 0; i < arr.length; i++) {
        if (angular.equals(arr[i], item)) {
          arr.splice(i, 1);
          return;
        }
      }
    }
  }

  return {
    restrict: 'A',
    scope: true,
    link: function(scope, elem, attrs) {
      if (elem[0].tagName !== 'INPUT' || !elem.attr('type', 'checkbox')) {
        throw 'checklist-model should be applied to `input[type="checkbox"]`.';
      }

      if (!attrs.checklistValue) {
        throw 'You should provide `checklist-value`.';
      }

      // link to original model. Initially assigned in $watch
      var model;
      // need setter for case when original model not array
      var setter = $parse(attrs.checklistModel).assign;
      // value added to list
      var value = $parse(attrs.checklistValue)(scope.$parent);

      // local var storing individual checkbox model
      // scope.checked - will be set in $watch

      // exclude recursion
      elem.removeAttr('checklist-model');
      // compile with `ng-model` pointing to `checked`
      elem.attr('ng-model', 'checked');
      $compile(elem)(scope);

      // watch UI checked change
      scope.$watch('checked', function(newValue, oldValue) {
        if (newValue === oldValue) {
          return;
        } if (newValue === true) {
          // see https://github.com/vitalets/checklist-model/issues/11
          if (!angular.isArray(model)) {
            setter(scope.$parent, add([], value));
            // `model` will be updated in $watch
          } else {
            add(model, value);
          }
        } else if (newValue === false) {
          remove(model, value);
        }
      });

      // watch model change
      scope.$parent.$watch(attrs.checklistModel, function(newArr, oldArr) {
        // keep link with original model
        model = newArr;
        scope.checked = contains(newArr, value);
      }, true);
    }
  };
}])
    .
    directive('myBlur', function () {
        return {
            restrict: 'A',
            link: function (scope, element, attr) {
                element.bind('blur', function () {
                    //apply scope (attributes)
                    scope.$apply(attr.myBlur);
                    //return scope value for focusing to false
                    scope.$eval(attr.myFocus + '=false');
                });
            }
        };
    }).
    controller('fileCtrl', function($rootScope, $scope, $http, $cookies, user){
        console.log(user);
        if(!$rootScope.my_photo_cange){
            $rootScope.my_photo = user.photo;
        }
        $scope.uploadFile = function(files) {
            var fd = new FormData();
            //Take the first selected file
            fd.append("file", files[0]);
            $scope.my_photo = '/media/upload_to/'+files[0].name;
            $scope.$apply();
            $http.defaults.headers.post['X-CSRFToken'] = $cookies['csrftoken'];
            $http.post('/profile/change-photo/'+user.id+'/', fd, {
                withCredentials: true,
                headers: {'Content-Type': undefined },
                transformRequest: angular.identity
            }).success(function(status, headers, config) {
                console.log(files[0].name);
                $rootScope.my_photo = '/media/upload_to/'+files[0].name;
                $rootScope.my_photo_cange = true;
                $rootScope.ok();
                $scope.ok();
            })
        };
    }).
    controller('sentInput', function ($scope, $http, $cookies, $rootScope, user) {
        $scope.focus = false;
        $scope.doneEditing1 = function () {
            $scope.submitTheForm();
        };
        $scope.submitTheForm = function() {
            var dataObject = {
                first_name : $scope.first_name,
                last_name : $scope.last_name
            };
            $http.defaults.headers.post['X-CSRFToken'] = $cookies['csrftoken'];
            $http.post("/profile/", dataObject, {}).
                success(function(){
                    $rootScope.first_name = dataObject.first_name;
                    $rootScope.last_name = dataObject.last_name;
                });
        };
        $rootScope.first_name = user.first_name;
        $rootScope.last_name = user.last_name;
    }).
    controller('TabsCtrl', function ($scope, $window) {
        $scope.tabs = [
            { title:'Dynamic Title 1', content:'Dynamic content 1' },
            { title:'Dynamic Title 2', content:'Dynamic content 2', disabled: true }
        ];
        $scope.alertMe = function() {
            setTimeout(function() {
                $window.alert('You\'ve selected the alert tab!');
            });
        };
    }).
    controller('newSocial', function($scope, $http, $cookies, $rootScope, social_list){
        $scope.socialAddplus = false;
        $scope.show = true;
        $scope.socialAdd = { selected: undefined };
        $scope.social_list = social_list;
        $scope.delSocial = function(social_id){
            $scope['show'+social_id] = true;
            $http.defaults.headers.post['X-CSRFToken'] = $cookies['csrftoken'];
            $http.post("/delete/social/" + social_id + '/', {});
        };
        $scope.doAddSocial = function(){
            if( typeof $scope.titleSocial != "undefined" && typeof $scope.nameSocial != "undefined") {
                $scope.dataObject = {
                    title_social : $scope.titleSocial,
                    name_social : $scope.nameSocial
                };
                 $scope.add_social = {
                    title : $scope.titleSocial,
                    link : $scope.nameSocial
                };
                $scope.submitTheSocial();
            }
            $scope.submitTheSocial = function() {
                $http.defaults.headers.post['X-CSRFToken'] = $cookies['csrftoken'];
                $http.post("/add/social/", $scope.dataObject, {}).success(function(status, headers, config) {
                $scope.add_social['id']= status.id;
                social_list.push($scope.add_social);
                 $scope.social_list = social_list;
            });
                $scope.newSocial = false;
            };

        };
    }).

    controller('profileOpen', ['$rootScope', '$scope', "$http", "$cookies", "$modal", 'modalConfirm', function($rootScope, $scope, $http, $cookies, $modal, modalConfirm){
        $scope.profile = false;
        $scope.settings = false;
        $scope.ratings = [];
        $scope.openSetings = function (){
            $scope.settings = !$scope.settings;
            $scope.profile = false;
        };
        $scope.initRatings = function (id, rating, term) {
            $scope.ratings.push({id:id, rating:rating, term:term});
        };

        $scope.vote = function (item) {

            $http({
                method: 'POST',
                url: '/product/vote/' + $scope.ratings[item].id + '/',
                data:{
                        rating: $scope.ratings[item].rating,
                        term: $scope.ratings[item].term
                     },
                headers: {"X-CSRFToken": $cookies.csrftoken,
                          "Content-Type": "application/x-www-form-urlencoded"}
            })
            //$http.post("", $("#trace_form").serialize(), {"headers":{"X-CSRFToken": $cookies.csrftoken}})
            .success(function(data, status, headers, config){
            })
            .error(function(data, status, headers, config){
            });
        };

        //$scope.testModalConfirm = function () {
        //    var confirm = function(){console.log("confirm")};
        //    var modal_data = {title: "Точно хотите Удалить?", question: "Вы действительно хотите удалить даный продукт доступный только вам?"};
        //    modalConfirm($scope, confirm, "dont_ask_if_delete_product_availible_only_for_me", modal_data);
        //}

        $scope.deleteProduct = function (item) {
            var confirm = function(){
                $http({
                    method: 'POST',
                    url: '/product/delete/' + $scope.ratings[item].id + '/',
                    headers: {"X-CSRFToken": $cookies.csrftoken,
                              "Content-Type": "application/x-www-form-urlencoded"}
                })
                //$http.post("", $("#trace_form").serialize(), {"headers":{"X-CSRFToken": $cookies.csrftoken}})
                .success(function(data, status, headers, config){
                        $('div[data-number='+item+']').remove();
                })
                .error(function(data, status, headers, config){
                        console.log("error");
                        console.log(data);
                });
            };

            var modal_data = {title: "Точно хотите Удалить?", question: "Вы действительно хотите удалить даный продукт доступный только вам?"};
            modalConfirm($scope, confirm, "dont_ask_if_delete_product_availible_only_for_me", modal_data);
        };

        $scope.rename = function (item) {
            var confirm = function(){
                var name = $('input[data-pk=' + $scope.ratings[item].id + ']').val();
                $http({
                    method: 'POST',
                    url: '/product/rename/' + $scope.ratings[item].id + '/',
                    headers: {"X-CSRFToken": $cookies.csrftoken,
                              "Content-Type": "application/x-www-form-urlencoded"},
                    data: {name: name}
                })
                //$http.post("", $("#trace_form").serialize(), {"headers":{"X-CSRFToken": $cookies.csrftoken}})
                .success(function(data, status, headers, config){
                })
                .error(function(data, status, headers, config){
                        console.log("error");
                        console.log(data);
                });
            };

            var modal_data = {title: "Точно хотите изменить название?", question: "Вы действительно хотите изменить название продукта доступногог только вам?"};
            modalConfirm($scope, confirm, "dont_ask_if_rename_product_availible_only_for_me", modal_data);
        };

        $scope.isDisabled = function (item){
            return !($scope.ratings[item].rating && $scope.ratings[item].term);
        };
        $scope.openProfile = function (){
            $scope.profile = !$scope.profile;
            $scope.settings = false;
        };
        $scope.$on("update_parent_controller", function(event, message){
            $scope.profile = message;
        });
        $scope.openSeting = function (){
            $scope.settings = !$scope.settings;
            $scope.profile = false;
        };
        $scope.openProductWithSettings = function(product_id){
            window.location.href('/' + $rootScope.productSlug + '/monitoring_product/' + product_id + '/openProductWhithSettings')
        };
        $scope.closeProfile = function(){
            $scope.profile = false;
        };
        $scope.closeSetting = function(){
            $scope.settings = false;
        };
    }]).
    controller('sendFeedback', function($scope, $http, $cookies, $rootScope){
//        $scope.submit = function(){
//            $http.defaults.headers.post['X-CSRFToken'] = $cookies['csrftoken'];
//            $http.post("/send/feedback/", {feedback_text:$scope.feedback_text});
//        };
        $scope.closeProfile = function(){
            $http.defaults.headers.post['X-CSRFToken'] = $cookies['csrftoken'];
            $http.post("/send/feedback/", {feedback_text:$scope.feedback_text});
            $scope.profile = false;
            $scope.currentProfileTab = 'profile.html';
            $scope.$emit('update_parent_controller', $scope.profile);
            $scope.$emit('update_parent_tab', $scope.currentProfileTab);
        };

    }).
    controller('lists', function($scope, departament_list, groups_list, $http, $cookies, $modal, $rootScope){
        $scope.departament_list = departament_list;
        $scope.groups_list = groups_list;
        $scope.changeAttr = function(attr, id, name){
            $scope.showListBig = false;
            $scope.showList = false;
            $http.defaults.headers.post['X-CSRFToken'] = $cookies['csrftoken'];
            var key = attr;
            var obj = {};
            obj[key] = id;
            $http.post("/profile/", obj);
            $scope[attr] = name;
        };

    }).
    controller('oldNewPass', function($scope, $http, $cookies){
        $scope.focus = false;
        $scope.newPass = false;
        $scope.changePass = function (){
            $scope.newPass = !$scope.newPass;
            $scope.password2 = '';
        };
        $scope.delPass = function(elem){
            $scope.newPass = !$scope.newPass;
            $scope.password1 = '';
        };
        $scope.doneEditing1 = function () {
            var password1 = $scope.password1;
            var password2 = $scope.password2;
            if (password1!=password2)
            {
                $scope.class = "errorPas";
            }
            else{
                if( typeof password1 === "undefined" && typeof password2 === "undefined") {}
                else{
                    $scope.class = "";
                    $scope.submitTheForm();
                }
            }
        };
        $scope.doneEditing2 = function () {
            var password1 = $scope.password1, password2 = $scope.password2;
            if (password1!=password2){
                $scope.class = "errorPas";
            }
            else{
                if( typeof password1 === "undefined" && typeof password2 === "undefined") {
                    $scope.class = "";
                }
                else{
                    $scope.class = "";
                    $scope.submitTheForm();
                }
            }
        };
        $scope.submitTheForm = function() {
            var dataObject = {
                password1 : $scope.password1
            };
            $http.defaults.headers.post['X-CSRFToken'] = $cookies['csrftoken'];
            var responsePromise = $http.post("/profile/", dataObject, {});
        };
    }).
    controller('PersonalTabs', function ($scope, $rootScope, $http, $cookies, product_settings, modalConfirm, dataAccessUser) {
        $scope.currentProfileTab = 'profile.html';
        $scope.currentSettingTab = 'amendment.html';
        $scope.$on("update_parent_tab", function(event, message){
            $scope.currentProfileTab = message;
        });
        $scope.onClickTab = function (tab) {
            $scope.currentProfileTab = tab;
        };
        $scope.isActiveTab = function(tabUrl) {
            return tabUrl == $scope.currentProfileTab;
        };
        $scope.main_list = [
            {name: 'source', text: 'Источник',id:1 },
            {name: 'date_up', text: 'Дата',id:2},
            {name: 'release_time', text: 'Время',id:3},
            {name: 'title', text: 'Заголовок',id:4},
            {name: 'author', text: 'Автор',id:5},
            {name: 'description', text: 'Полнотекст',id:6},
            {name: 'annotation', text: 'Аннотация',id:7},
            {name: '?????', text: 'Перепечатки',id:8},
            {name: 'url', text: 'Url',id:9},
            {name: 'mo', text: 'Media outreach',id:10}
          ];
        $scope.media_list = [
            {name: 'media_level', text: 'Уровень',id:21 },
            {name: 'media_type', text: 'Тип',id:22},
            {name: 'media_category', text: 'Категория',id:25},
            {name: 'media_view', text: 'Вид',id:23}
          ];
        $scope.geography_list = [
            {name: 'country', text: 'Страна',id:31 },
            {name: 'federal_district', text: 'Федеральный округ',id:32},
            {name: 'city', text: 'Город',id:35}
          ];
        $scope.indicators_list = [
            {name: 'prv', text: 'PRV',id:41 },
            {name: 'prt', text: 'PRt',id:42},
            {name: 'emotional_tone_object', text: 'Тональность',id:44}
          ];
        $scope.mention_list = [
            {name: 'title', text: 'Объекты',id:51 },
            {name: 'speakers', text: 'ФИО спикера',id:52}

          ];
        if(Object.getOwnPropertyNames(product_settings.settings).length === 0){
            $scope.date_checked = {
            main_list: $scope.main_list,
            media_list :$scope.media_list,
            geography_list :$scope.geography_list,
            indicators_list :$scope.indicators_list,
            mention_list :$scope.mention_list
        };
        }else {
            $scope.date_checked = {
            main_list: product_settings.settings.main_list,
            media_list :product_settings.settings.media_list,
            geography_list :product_settings.settings.geography_list,
            indicators_list :product_settings.settings.indicators_list,
            mention_list :product_settings.settings.mention_list
        };
        }
        $scope.saveSettings = function(){
            $http.defaults.headers.post['X-CSRFToken'] = $cookies['csrftoken'];
            var responsePromise = $http.post("/save-product-settings/" + $rootScope.productId + "/" , {settings: $scope.date_checked}).success(function(){
                window.location.reload()
            });
        };
        $scope.onClickSetTab = function (tab) {
            $scope.currentSettingTab = tab;
            $http.get('/monitoring_product/get_users_access/' + $rootScope.productId + '/').success(function(userData) {
                $scope.company = userData;
                $scope.users = $scope.company.users;
                $scope.available_only_for_me = $scope.company.available_only_for_me;
                $scope.usersAccess = _.filter($scope.company.users, function(e){return e.selected});
                $scope.selected_departments = _.each($scope.company.departments, function(el){
                    el.selected = false;
                    el.users = _.where($scope.usersAccess, {department:el.id});
                });
                $scope.selected_departments = _.filter($scope.selected_departments, function(el){return el.users.length});
            });
        };

        $scope.isActiveSetTab = function(tabUrl) {
            return tabUrl == $scope.currentSettingTab;
        };
        $scope.changeAvailability = function() {
            var confirm = function(){
                $scope.available_only_for_me = !$scope.available_only_for_me;
            };
            if (!$scope.available_only_for_me){
                var modal_data = {title: "Точно хотите сделать доступным только для вас?", question: "Вы действительно хотите сделать продукт доступным только Вам? Ваши коллеги не будут иметь доступак даному продукту."};
                modalConfirm($scope, confirm, "dont_ask_if_make_product_availible_only_for_me", modal_data);
            }
            else {
                confirm();
            }
        };
        $scope.saveHavingAccessUsers = function(){
            var data = {users: _.map($scope.usersAccess, function(el){return el.id})};
            $http.defaults.headers.post['X-CSRFToken'] = $cookies['csrftoken'];
            $http.post('/monitoring_product/set_users_access/' + $rootScope.productId + '/', data).success(function(res) {
                console.log("Access to product was updated");
            });
        };
    }).
    controller('directoryCenter', function($scope, directory_date, category_list, first_category_list, $rootScope){
        $scope.first_category_list = first_category_list;
        $scope.directory_date = directory_date;
            for(var i = 0; i < $scope.directory_date.length; i++) {
        }
    }).
    controller('dropsDown', function($scope){
        $scope.source = false;
        $scope.periodicity = false;
        $scope.openSource = function (){
            $scope.source = !$scope.source;
            $scope.periodicity = false;
        };
        $scope.openPeriodicity = function (){
            $scope.periodicity = !$scope.periodicity;
            $scope.source = false;
        };
    }).
    controller('filterUpdateNew', function($scope){
         $scope.toggleDatePicker = function($event, variable) {
             $event.preventDefault();
             $event.stopPropagation();
             this.openedStart = false;
             this.openedEnd = false;
             this.openedStartRelease = false;
             this.openedEndRelease = false;
             this[variable] = true;
        }
    }).
    directive('categoryDirect',function(category_list, $timeout){
        return {
        restrict: "A",

        link: function(scope, elm, attrs){

          $timeout(function(){

            var top = elm.offset().top;
            var obj = {};
            obj['top'] = top;
            obj['text'] = elm.text();
            obj['elem'] = elm;
            obj['id'] = attrs.id;
            category_list.push(obj)
          }, 100)
        }
      };
    //return {
    //    link: $timeout(function(scope, elm, attrs, ctrl){
    //
    //        console.log(elm);
    //
    //        var top = elm.offset().top;
    //        var obj = {};
    //        obj['top'] = top;
    //        obj['text'] = elm.text();
    //        obj['elem'] = elm;
    //        console.log(obj);
    //        //obj['id'] = attrs[2].id;
    //
    //
    //        //console.log($timeout(hello()));
    //        //category_list.push(hello());
    //        //timer(hello, 0);
    //
    //
    //
    //    })
    //}
    }).
    directive('dynamicBreadcrumbsNew',  function( $anchorScroll, $rootScope ,$location, $timeout, first_category_list) {

	return {

		template: '<span ng-repeat="elem in first_category_list"'+
                'ng-class="{collapsed: elem.collapsed}"'+
                'ng-style="{width: elem.width}"'+
                'class="dropdown dropdown-toggle" on-toggle="watcher(open, elem, $index)">'+
			'<span>{$ elem.title $}</span>'+
			'<ul class="dropdown-menu bread-crumbs-popup">'+
				'<li ng-repeat="brother in elem.brothers">'+
					'<a href ng-click="scrollToArticle(brother.id)">{$ brother.title $}</a>'+
				'</li>'+
			'</ul>'+
			'</span>',
        link: function(scope, element, attrs) {
            scope.maxWidth = element[0].offsetWidth;
            console.log(scope.maxWidth, 'maxWidth');
          //  $timeout(function(){
          // console.log(element, attrs);
          //      element.width = 400
          //}, 100)
        },
        controller: function($scope, $anchorScroll, $location,$timeout, first_category_list, $filter) {
        $scope.scrollToArticle = function(brother_id) {
            $scope.first_category_list = first_category_list;

            var currentTitle = angular.element('#' + brother_id);
            if (currentTitle.length) {
                $location.hash(brother_id);
                $anchorScroll();
             }
			};
            var currentIndex = undefined;
            $scope.watcher = function(open, element, index) {
                if (open) {
                    console.log('open!');
                    var $currentPopup = $('.bread-crumbs-popup:visible');
                    if (angular.isUndefined(currentIndex)) {
                        currentIndex = index;
                    } else {
                        if (currentIndex > index) {
                            $currentPopup = $currentPopup.first();
                        } else if (currentIndex < index) {
                            $currentPopup = $currentPopup.last();
                        }
                    }
                    console.log($currentPopup.first(), '$currentPopup.first()');
                    for (var i = 0; i < $scope.first_category_list.length; i++) {
                        $scope.first_category_list[i].width = 'auto';
                        $scope.first_category_list[i].collapsed = false;
                    }
                    //-------------------------------------
                    $rootScope.first_category_list = first_category_list
                    console.log($rootScope.first_category_list, 'category_list');

                    var list = $filter('orderBy')($rootScope.first_category_list[index], 'max_width');
                    console.log(list ,'list', index,'index');
                    var cur = 0;
                        for (var j = 0; j < $rootScope.first_category_list[index]['brothers'].length; j++){
                            console.log($rootScope.first_category_list[index]['brothers'][j].title.length, $rootScope.first_category_list[index]['brothers'][j].title);
                            var title_width=$rootScope.first_category_list[index]['brothers'][j].title.length * 8;
                            if(title_width >cur){
                             cur = title_width
                            }
                        }
                    console.log(cur, 'cur !!!!!!');
                    element.width = cur + 'px';
                    $timeout(function() {
                        var totalWidth = 0,
                            $list = $('.dynamic-breadcrumbs').children('span');

                        $list.each(function() {
                            totalWidth += (index != $(this).index() ? $(this).outerWidth() : 0);
                        });
                        totalWidth += ($list.length - 1) * 8;
                        totalWidth += element.width;
                        if (totalWidth > $scope.maxWidth) {
                            $scope.first_category_list[index != 0 ? 0 : $scope.first_category_list.length - 1].width = 40;
                            $scope.first_category_list[index != 0 ? 0 : $scope.first_category_list.length - 1].collapsed = true;
                        }
                        currentIndex = index;
                    });
                }
                else {
                    console.log('open22222!');
                    if (!$('.bread-crumbs-popup:visible').length) {
                        for (var i = 0; i < $scope.first_category_list.length; i++) {
                            $scope.first_category_list[i].width = 'auto';
                            $scope.first_category_list[i].collapsed = false;
                        }
                        var totalWidth = 0,
                            $list = $('.dynamic-breadcrumbs').children('span');
                        $timeout(function() {
                            $list.each(function() {
                                totalWidth += $(this).outerWidth();
                            });
                            totalWidth += ($list.length - 1) * 8;

                            if (totalWidth > $scope.maxWidth) {
                                $scope.first_category_list[i].width = 'auto';
                                $scope.first_category_list[i].collapsed = true;
                            }
                        });
                    }

                }
            };
            //$scope.$watch('first_category_list', function() {
            //    $timeout(function() {
            //        for (var i = 0; i < first_category_list.length; i++) {
            //            first_category_list.width = 'auto';
            //            first_category_list.collapsed = false;
            //        }
            //        var totalWidth = 0,
            //            $list = $('.dynamic-breadcrumbs').children('span');
            //        $timeout(function() {
            //            $list.each(function() {
            //                totalWidth += $(this).outerWidth();
            //            });
            //            totalWidth += ($list.length - 1) * 8;
            //
            //            if (totalWidth > $scope.maxWidth) {
            //                first_category_list.width = 40;
            //               first_category_list.collapsed = true;
            //            }
            //        });
            //    });
            //}, true);_list
        }
	}
})
    .directive("scrolls", function($window, category_list, $rootScope, $filter, category_structure, first_category_list) {
   return {

        scope: {
            first_category: '=firstCategory'
        },
        link: function (scope, element, attrs) {
            var raw = element[0];
            var headerHeight = 170;
            element.bind('scroll', function () {
                category_list = $filter('orderBy')(category_list, 'top');
                for (var i = 0; i < category_list.length; i++){
            	if (category_list[i].top >= headerHeight) {
                    if (i < category_list.length - 1) {
                        if (category_list[i].top < raw.scrollTop + headerHeight && raw.scrollTop + headerHeight < category_list[i + 1].top) {
                            key = category_list[i].id;
                            var obj = category_structure[key];
                            scope.first_category = obj;
                            scope.$apply();
                        }
                    }
	                if (i == category_list.length - 1 && category_list[i].top < pageYOffset + headerHeight - 30 ) {
                            key = category_list[i].id;
                            var obj = category_structure[key];
                            scope.first_category = obj;
                            scope.$apply();
	                }
            	}
            }
            $rootScope.first_category_list = scope.first_category
            });
        }
    };
})
    .
    directive('categoryTitle', function($rootScope, directory_date){
    return {
        scope: {
            title: '=',
            titleList: '=',
            catId: '=',
            brothers: '='
        },
        template: '<a class="title-help" name="category-{$ category.id$}">{$ category.title$}</a>',
        link: function(scope, category, attrs) {
        	var str = scope.titleList;
        	scope.list = angular.fromJson('[' + str.substring(0, str.length - 1) + ']');
            //scope.list = [];
        	scope.list.push({
        		id: scope.catId,
        		title: scope.title,
        		brothers: scope.brothers
        	});
            dataService.titleOffset.push({
            	'elem': elem,
            	'list': scope.list
            });
			scope.hideBreadTitle = false;
			if ($rootScope.hideBreadTitleFlag) {
				scope.hideBreadTitle = true;
				$rootScope.hideBreadTitleFlag = false;
			}
            scope.$root.$emit('loadStart');
        }
    }
})
    .
    run(function($rootScope, $modal) {
        $rootScope.openModal = function(templateUrl) {
            var modalInstance;
            templateUrl = templateUrl || [];
            modalInstance = $modal.open({
                templateUrl: templateUrl,
                windowClass: templateUrl,
                controller: function($scope, $modalInstance) {
                    $scope.close = function() {
                        $modalInstance.dismiss('cancel');
                    };
                    $scope.goStep = function(templateUrl) {
                        $modalInstance.dismiss('cancel');
                        $scope.$root.openModal(templateUrl);
                    };
                    $scope.ok = function () {
                        $modalInstance.close();
                    };
                }
            });
        };
        $rootScope.openModaluserId = function(templateUrl,  userId, $http) {
            var modalInstance;
            templateUrl = templateUrl || [];
            modalInstance = $modal.open({
                templateUrl: templateUrl,
                windowClass: templateUrl,
                controller: function($scope, $modalInstance) {
                    $scope.close = function() {
                        $modalInstance.dismiss('cancel');
                    };
                    $scope.goStep = function(templateUrl) {
                        $modalInstance.dismiss('cancel');
                        $scope.$root.openModal(templateUrl);
                    };
                    $scope.ok = function () {
                        $modalInstance.close();
                    };
                }
            });
        };
    }).
    controller('accessCtr', function($scope){
        $scope.otdels=[
            {'name': "get milk",
             'id': '1'
             },
            {'name': "get milk2",
             'id': '2'
             }
        ];
    }).
    directive("addpeople", function($compile){
        return function(scope, element, attrs){
            element.bind("click", function(){
//                scope.count++;
                angular.element(document.getElementById('list-people')).append($compile("<div class='block-access'><p class='otdel-access noselect'>s<span>X</span></p></div>")(scope));
            });
        };
    }).
    directive('accessPeople', function(){
        return{
            restrict: 'AE',
            scope: '=',
            templateUrl: 'accessHtml',
            controller: function ($scope) {
               $scope.otdels = function(){
                   $scope.otdel = !$scope.otdel;
                   $scope.people = false;
               };
                $scope.peoples = function(){
                   $scope.people = !$scope.people;
                   $scope.otdel = false;
               };
                $scope.onClickAccess = function(done, id){
                  console.log(id, 'done');
                };
            }
        }
    });



