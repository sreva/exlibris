var exlibris = angular.module('exlibris', ['ui.bootstrap', 'ngSanitize', 'rzModule', 'ngCookies', 'ngRoute', 'angularUtils.directives.dirPagination','ngAnimate', 'ngClipboard', 'perfect_scrollbar']);
exlibris.config(function($routeProvider, $locationProvider, $interpolateProvider) {
	$interpolateProvider.startSymbol('{$');
	$interpolateProvider.endSymbol('$}');
	$routeProvider.when('/:product/monitoring_product/:id/:title/', {
		templateUrl: "/template/news2.html/",
		controller: "WidgetNullController",
        resolve:{
            load: function ($rootScope, $route, dataService) {
            	$rootScope.productId = $route.current.params.id;
                $rootScope.productSlug = $route.current.params.product;
                $rootScope.$emit('loadStart');
                return dataService.load($rootScope.productId, $rootScope.productSlug).then(function(res){
                    dataService.data = res;
                });
            },
            filter: function($rootScope, $route, dataFilter){
                $rootScope.productId = $route.current.params.id;
                $rootScope.productSlug = $route.current.params.product;
                $rootScope.$emit('loadStart');
                return dataFilter.filter($rootScope.productId, $rootScope.productSlug).then(function(res){
                    dataFilter.data = res;
                });
            }
        }
	})
	$locationProvider.html5Mode(true);
}).
    factory('product_settings', function(){
       return product_settings
    }).
    factory('dataFilter', function($q, $http){
        return{
            data:{},
            filter: function(id, slug){
                var defer = $q.defer();
                $http.get('http://'+ window.location.host +'/' + slug + '/get-monitoring-product-filters/' + id + '/').success(function(res){
                    defer.resolve(res);
                });
                return defer.promise;
            }
        }
    }).
    factory('dataService', function ($q, $http, $location) {
    return {
        data: {},
        load: function(id, slug) {
            var defer = $q.defer(),
                str = "",
                obj = $location.search();
            for (var key in obj) {
                if (str != "") {
                    str += "&";
                }
                str += key + "=" + obj[key];
            }
            if (str.length) str = '?' + str;
           	$http.get('http://'+ window.location.host +'/' + slug + '/monitoring_product/' + id + '/widget0-data/' + str).success(function(res){
                defer.resolve(res);
            });
            return defer.promise;
        },
	    titleOffset: [],
	    activeTitle: '',
	    headerObj: null,
	    header: 23,
        lessArticles: 100000000000,
        badgeTopAt: null
    };
}).
    directive('categoryList', function($compile) {
    return {
        restrict: 'E',
        scope: {
            category: '=',
            name: '=',
            searchText: '='
        },
        templateUrl: 'categoryListTemplate',

        controller: function($scope, $document) {
            $scope.createTitle = function(title, catTitle) {
                if (!title) return catTitle;
                return title + ' &rarr; ' + catTitle;
            }
        },
        compile: function(tElement, tAttr, transclude) {
            var contents = tElement.contents().remove();
            var compiledContents;
            return function(scope, iElement, iAttr) {
                if(!compiledContents) {
                    compiledContents = $compile(contents, transclude);
                }
                compiledContents(scope, function(clone, scope) {
                         iElement.append(clone);
                });
            }}
    }
}).
    directive('singleArticle', function() {
    return {
        restrict: 'E',
        scope: {
            item: '=',
            searchText: '='
        },
        templateUrl: 'singleArticleTemplate',
        controller: function($scope, $filter, $sce, globalFlags) {
            $scope.currentIndex = 0;
            $scope.activeWord = 0;
            $scope.flagType = globalFlags.flagType;
            $scope.status = {
                isCollapsed: true
            }
            $scope.activeWordChange = function(index) {
                $scope.activeWord = index;
                $scope.currentIndex = index;
            }
            $scope.previewWordChange = function(index) {
                $scope.currentIndex = $scope.activeWord;
                $scope.activeWord = index;
            }
            $scope.returnLastActive = function() {
                $scope.activeWord = $scope.currentIndex;
            }
            $scope.paintTheWords = function(html, mo) {
                text = html;
                for(var i = 0; i < mo.length; i++) {
                    for(var j = 0; j < mo[i].keywords.length; j++) {
                        text = $filter('paintTheWordsFilter')(text, '('+ mo[i].keywords[j] +')', '<span class="colored-text" style="background-color:#'+ mo[i].color +'">' + mo[i].keywords[j] + '</span>');
                    }
                }
                return $sce.trustAsHtml(text);
            }
            $scope.renderSpeakers = function(speakers) {
                var text = '(';
                for (var i = 0; i < speakers.length; i++)
                    text += speakers[i].fio + (i != speakers.length - 1 ? ', ': '');
                text += ')';
                return text;
            }
            $scope.sourcePopup = true;
            $scope.copyRightPopup = true;
            $scope.urlPopup = true;
            $scope.showSourcePopup = function() {
                $scope.sourcePopup = false;
            }
            $scope.hideSourcePopup = function() {
                $scope.sourcePopup = true;
            }
            $scope.showCopyRightPopup = function() {
                $scope.copyRightPopup = false;
            }
            $scope.hideCopyRightPopup = function() {
                $scope.copyRightPopup = true;
            }
            $scope.showUrlPopup = function() {
                $scope.urlPopup = false;
            }
            $scope.hideUrlPopup = function() {
                $scope.urlPopup = true;
            }
        }
    }
}).
    directive("scroll", function ($window, $filter, dataService) {
    return function(scope, element, attrs) {
        angular.element($window).bind("scroll", function() {
            var res = [],
                headerHeight = 23;
            angular.forEach(dataService.titleOffset, function(elem) {
                res.push({'top': elem.offset().top, 'elem': elem})
            })
            res = $filter('orderBy')(res, 'top');
            for (var i = 0; i < res.length; i++){
                if (i < res.length - 1) {
                    if (res[i].top < pageYOffset + headerHeight - 30 && pageYOffset + headerHeight - 30 < res[i + 1].top) {
                        dataService.activeTitle = res[i].elem.html();
                        break;
                    }
                }
                if (i == res.length - 1 && res[i].top < pageYOffset + headerHeight - 30) {
                    dataService.activeTitle = res[res.length - 1].elem.html();
                    break;
                }
            }
            scope.$apply();
        });
    };
}).
    directive('titleInfo', function(dataService){
    return {
        link: function(scope, elem, attrs) {
            dataService.titleOffset.push(elem);
            scope.$root.$emit('loadStart');
        }
    }
}).
    controller('headerCtrl', function($rootScope, $scope, $http, $cookies, $filter, dataService, $location) {
        $rootScope.visibleMenu = [];
        $scope.dataService = dataService;
        $scope.dateType = dataService.data.product[0].type;
        $scope.shareMenu = false;
        var menu = dataService.data.monitoring_product_template;
        for(var i = 0; i < menu.length; i++) {
            $rootScope.visibleMenu.push(menu[i]);
        };
        $scope.goToMP = function() {
            window.location.href = $location.path().substr(0, $location.path().length - 8);
        };
        $scope.rerenderMenu = function(children, id) {
            if (children) {
                if (children.length) {
                    $rootScope.visibleMenu = !id ? [] : [{
                        title: "",
                        children: $rootScope.visibleMenu,
                        id: null,
                        back: true
                    }];
                    $rootScope.visibleMenu = $rootScope.visibleMenu.concat(children);
                    getFirstCategoryId(children);
                } else {
                    scrollToElement(id);
                }
            } else {
                scrollToElement(id);
            }
            function scrollToElement(id) {
                var currentTitle = angular.element('#' + id);
                if (currentTitle.length) {
                    angular.element("body, html").animate({scrollTop: currentTitle.offset().top - angular.element('.wrap-header').height() + currentTitle.height() + 2});
                }
            }
            function getFirstCategoryId(children) {
                if (children[0].articles) {
                    if (children[0].articles.length) {
                        scrollToElement(children[0].id);
                    } else {
                        getFirstCategoryId(children[0].children);
                    }
                } else {
                    getFirstCategoryId(children[0].children);
                }
            }
        }
        $scope.openMenu = function(){
           var inputs = document.getElementsByName("checkbox");
        var checked = 0;
        for (var i = 0; i < inputs.length; i++) {
            'checkbox' == inputs[i].type && inputs[i].checked ? checked++ : false;
        }
        checked >= 1 ? $scope.shareMenu = !$scope.shareMenu : $scope.shareMenu = false
        };

}).
//    controller('mainsWidget', function($scope){
//        $scope.searchText = '';
//        $scope.$watch("searchText", function(newValue, oldValue) {
//            return $scope.searchText;
//        });
//
//
//        $scope.settings = false;
//        $scope.openSetings = function (){
//            $scope.settings = !$scope.settings;
//        };
//        $scope.closeSetting = function(){
//            $scope.settings = false;
//        };
//
//
//        $scope.profile = false;
//        $scope.openProfile = function (){
//            $scope.profile = !$scope.profile;
//        };
//
//        $scope.$on("update_parent_controller", function(event, message){
//            $scope.profile = message;
//        });
//
//         $scope.generateDoc = function(topic){
//        $scope.topic =dataService.data.product[0].title;
//        if(topic == 'translation'){
//            $scope.topic ='На перевод';
//        }
//        var inputs = document.getElementsByName("checkbox");
//        var checked = 0;
//        var articles_id_list=[];
//        for (var i = 0; i < inputs.length; i++) {
//            if (inputs[i].checked){
//                var id = inputs[i].id;
//                articles_id_list.push(id.replace('select-', ''));
//            }
//        }
//
//        $scope.shareMenu = false;
//        $http.defaults.headers.post['X-CSRFToken'] = $cookies['csrftoken'];
//        $http.post("/generate-doc/", {articles_id_list:articles_id_list}).success(function(res){
//        $scope.sedModal = true;
//        $scope.file_name = res['file_name'];
//        $scope.message ='';
//
//        $scope.from= res['from'];
//        $scope.to='';
//    });
//    };
//        $scope.printArticles = function(){
//        var inputs = document.getElementsByName("checkbox");
//        var checked = 0;
//        var articles_id_list=[];
//        for (var i = 0; i < inputs.length; i++) {
//            if (inputs[i].checked){
//                var id = inputs[i].id;
//                articles_id_list.push(id.replace('select-', ''));
//            }
//        }
//    $http.defaults.headers.post['X-CSRFToken'] = $cookies['csrftoken'];
//    $http.post("/print/articles/", {articles_id_list:articles_id_list}).success(function(res) {
//        $scope.shareMenu = false;
//        window.location.href = '/'+res['file_path']
//    });
//
//    };
//        $scope.copyLink = function(scope){
//        var inputs = document.getElementsByName("checkbox");
//        var checked = 0;
//        var articles_link_list = [];
//        for (var i = 0; i < inputs.length; i++) {
//            if (inputs[i].checked){
//                var title = inputs[i].title;
//                articles_link_list.push(title);
//            }
//        }
//        var result = '';
//        for (var i = 0; i < articles_link_list.length; i++ ){
//            if(articles_link_list.length ==1){
//                result = result + articles_link_list[i];
//            }
//            else{
//                if(articles_link_list.length == i){
//                    result = result + articles_link_list[i]
//                }
//                else{
//                    result = result + articles_link_list[i] + ', ';
//                }
//            }
//        }
//
//        return result;
//    };
//}).
    controller('WidgetNullController', function($rootScope, $scope, $filter, dataService, $cookies, $http, $window, $routeParams, $timeout) {

    var windowBadgeTimeout = null;
    $scope.activeFilters = []; // active filters in the header
    $scope.widget0NewsMarginTop = widget0NewsMarginTopFn();

    $scope.toggleDatePicker = function($event, variable) {
        $event.preventDefault();
        $event.stopPropagation();
        this.openedStart = false;
        this.openedEnd = false;
        this.openedStartRelease = false;
        this.openedEndRelease = false;
        this[variable] = true;
    }
    $scope.redirectToIndex = function(url) {window.location.href = '/' + url;}
    $scope.windowBadge = {countOfNews: null, show: false, isDisable: false}
    $scope.windowBadgeMuch = {show: false}
    $scope.dateTypeSend = dataService.data.product[0].date_type;

    // date filters
    $scope.dateStart = dataService.data.product[0].date_start;
    $scope.dateEnd = dataService.data.product[0].date_end;
    $scope.timeStart = dataService.data.product[0].time_start;
    $scope.timeEnd = dataService.data.product[0].time_end;
    $scope.dateStartRelease = '';
    $scope.dateEndRelease = '';
    $scope.timeStartRelease = '';
    $scope.timeEndRelease = '';

    // other filters
    $scope.filters = dataService.data.product[0].filters;
    // sort by order
    $scope.sortableFilters = [];
    for (var key in $scope.filters) {
        if (!$scope.filters[key].settings.a3) $scope.filters[key].settings.a3 = Infinity;
        $scope.sortableFilters.push([key, $scope.filters[key]]);
    }
    $scope.sortableFilters.sort(function(a, b) {
        return a[1].settings.a3 - b[1].settings.a3
    });

    // скрыть ФО
    $scope.hideFederalDistrict = false;

    $scope.isRangeFilter = function(key) {return ~key.indexOf('range');}
    // km_result

    // проверка на исключения фильтра release_time из общего списка
    $scope.isCustomFilter = function(key) {return ~key.indexOf('release_time');}

    $scope.haveReleaseTime = function() {return !angular.isUndefined($scope.filters.release_time);}

    if ($scope.haveReleaseTime()) {
        $scope.filters.km_result.list = [{
            id: 1, title: 'да'
        }, {
            id: 2, title: 'нет'
        }];
    }

    $scope.filterData = function(key) {
    	if (dateValidation($scope.dateTypeSend)) {
	        if (key == 'city__id__in') {showHidefederalDistrict();}
	        $http.defaults.headers.post['X-CSRFToken'] = $cookies['csrftoken'];
	        $http.post('http://'+ window.location.host +'/'+ $rootScope.productSlug +'/monitoring_product/'+ $rootScope.productId +'/widget0-data/', {
	            data: angular.toJson(getAllFilters()),
	            date_start: createDateValue('start', $scope.dateTypeSend),
	            date_end: createDateValue('end', $scope.dateTypeSend),
	            get_count: true,
	            date_type: $scope.dateTypeSend,
              page:1
	        }).success(function(response) {
	            $scope.windowBadge.countOfNews = response.articles_count;
	            $scope.widget0NewsMarginTop = widget0NewsMarginTopFn();
	            showBadeWindow(dataService.badgeTopAt);
	        });
    	} else {
    		alert('Не заполнено выбранное поле с датой или временем');
    	}
    }

    $scope.getCountOfArticles = function(elementId){

        if (dateValidation($scope.dateTypeSend)) {


            $http.defaults.headers.post['X-CSRFToken'] = $cookies['csrftoken'];
            $http.post('http://'+ window.location.host +'/'+ $rootScope.productSlug +'/monitoring_product/'+ $rootScope.productId +'/widget0-data/', {
                data: angular.toJson(getAllFilters(false)),
                date_start: createDateValue('start', $scope.dateTypeSend),
                date_end: createDateValue('end', $scope.dateTypeSend),
                get_count: true,
                date_type: $scope.dateTypeSend,
                page:1
            }).success(function(response) {

                $scope.windowBadge.countOfNews = response.articles_count;
                showBadeWindow(elementId, response.date_type);

            });
        }
    }

    $scope.showFilteredArticles = function(lessArticles, page) {
        if (!lessArticles) {
            if ($scope.windowBadge.countOfNews > dataService.lessArticles){
                $scope.windowBadgeMuch.show = true;
                $scope.windowBadge.show = false;
                return false;
            }
        }

        if (typeof(page)==='undefined') page = 1;
        console.log(page);
        $rootScope.loadingView = true;
        $http.defaults.headers.post['X-CSRFToken'] = $cookies['csrftoken'];
        $http.post('http://'+ window.location.host +'/'+ $rootScope.productSlug +'/monitoring_product/'+ $rootScope.productId +'/widget0-data/', {
            data: angular.toJson(getAllFilters(true)),
            date_start: createDateValue('start', $scope.dateTypeSend),
            date_end: createDateValue('end', $scope.dateTypeSend),
            date_type: $scope.dateTypeSend,
            page: page

        }).success(function(res) {
            hideBadgeWindow();
            $rootScope.loadingView = false;
            dataService.data = res;
            dataService.titleOffset.length = 0;
            var menu = dataService.data.monitoring_product_template;
            console.log(res)
            $rootScope.items_list = [];
            $rootScope.totalItems = res.articles_count;
            $rootScope.pagination = {
                current: res.article_pagination_list.number
            };
            $rootScope.PerPage = res.article_pagination_list.articles_on_page;

            $rootScope.visibleMenu.length = 0;
            for(var i = 0; i < menu.length; i++) {
                $rootScope.visibleMenu.push(menu[i]);
            }
            angular.element("body, html").animate({scrollTop: angular.element('html').offset().top});
            $rootScope.$emit('loadStart');
            $timeout(function() {
                $scope.widget0NewsMarginTop = widget0NewsMarginTopFn();
            });
        });
    }

    $scope.removeFilter = function(filter) {
        if (filter.filter_list) {
            for (var i = 0; i < filter.filter_list.length; i++) {
                filter.filter_list[i].selected = false;
                if (!angular.isUndefined(filter.isNull.emptyValue)) filter.isNull.emptyValue = false;
                filter.isNull.selectAll = false;
            }
        } else {
            filter.id.slider.min = filter.id.slider.floor;
            filter.id.slider.max = filter.id.slider.ceil;
        }
        $scope.showFilteredArticles(true);
    }

    function getAllFilters(flag) {
        if (flag) $scope.activeFilters.length = 0;
        var data = {}, log = [];

        for (var key in $scope.filters) {
            if (~key.indexOf('range')) {
                console.log('1');
                var filterList = $scope.filters[key];
                if (filterList.slider.floor != filterList.slider.min || filterList.slider.ceil != filterList.slider.max) {
                    data[key] = [filterList.slider.min, filterList.slider.max];
                    if (flag) {
                        $scope.activeFilters.push({
                            filter_list: null,
                            title: filterList.settings.a2 ? filterList.settings.a2 : filterList.settings.a1,
                            id: filterList
                        });
                    }
                }
                continue;
            }
            var filterList = $scope.filters[key].list,
                objectLength = filterList.length,
                objectTrueLength = 0;

            // hard code
            if (~key.indexOf('km_result')) {
                if ((filterList[0].selected && filterList[1].selected) ||
                    (!filterList[0].selected && !filterList[1].selected)) {
                    continue;
                } else {
                    data[key] = filterList[0].selected ? true: false;
                    if (flag) {
                        $scope.activeFilters.push({
                            filter_list: filterList,
                            title: $scope.filters[key].settings.a2 ? $scope.filters[key].settings.a2 : $scope.filters[key].settings.a1,
                            isNull: $scope.filters[key]
                        });
                    }
                }
                continue;
            }
            // endof hard code

            log.length = 0;
            angular.forEach(filterList, function(value, index) {
                if (value.selected) {
                    this.push(value.id)
                    objectTrueLength++;
                }
            }, log);

            var emptyValue = $scope.filters[key].emptyValue,
                emptyUndefined = angular.isUndefined(emptyValue),
                addInFilter = true,
            	isNullKey = key.slice(0, key.indexOf('__id__in')) + '__isnull';

            if (!emptyUndefined && emptyValue && objectLength != objectTrueLength) {
                data[isNullKey] = true;
            }

            if (log.length || data[isNullKey]) {
                if (emptyUndefined) {
                    if (objectLength == objectTrueLength || objectTrueLength == 0) addInFilter = false;
                } else {
                    if ((objectLength == objectTrueLength && emptyValue) ||
                        (!objectTrueLength && !emptyValue)) addInFilter = false;
                }
                if (addInFilter) {
                    data[key] = angular.copy(log);
                    if (flag) {
                        $scope.activeFilters.push({
                            filter_list: filterList,
                            title: $scope.filters[key].settings.a2 ? $scope.filters[key].settings.a2 : $scope.filters[key].settings.a1,
                            isNull: $scope.filters[key]
                        });
                    }
                }
            }

        }
        return data;
    }

    function showHidefederalDistrict() {
        $scope.hideFederalDistrict = false;
        var cityList = $scope.filters['city__id__in'],
        	federalDistrict = $scope.filters['federal_district__id__in'];
        if (!angular.isUndefined(cityList)) {
			for (var i = 0; i < cityList.list.length; i++) {
				if (cityList.list[i].title == 'Тула' && !cityList.list[i].selected) {
					$scope.hideFederalDistrict = true;
					$scope.$apply();
				}
			}
			if (!$scope.hideFederalDistrict && !angular.isUndefined(federalDistrict)) {
			    for (var i = 0; i < federalDistrict.list.length; i++) {
			    	federalDistrict.list[i].selected = true;
			    }
			    if (!angular.isUndefined(federalDistrict.emptyValue)) federalDistrict.emptyValue = true;
			}
        }
    }

    $scope.$root.$on('slideEnded', function(event, elem) {
        $http.defaults.headers.post['X-CSRFToken'] = $cookies['csrftoken'];
        $http.post('http://'+ window.location.host +'/'+ $rootScope.productSlug +'/monitoring_product/'+ $rootScope.productId +'/widget0-data/', {
            data: angular.toJson(getAllFilters()),
            date_start: createDateValue('start', $scope.dateTypeSend),
            date_end: createDateValue('end', $scope.dateTypeSend),
            get_count: true,
            date_type: $scope.dateTypeSend,
            page:1
        }).success(function(response) {
            $scope.windowBadge.countOfNews = response.articles_count;
            $scope.widget0NewsMarginTop = widget0NewsMarginTopFn();
            showBadeWindow(elem);
        });
    });

    function dateValidation(dateType) {
        var valid = false;
        if (dateType == 'date_upload') {
            if (moment($filter('date')($scope.dateStart, 'dd-MM-yyyy'), ["DD-MM-YYYY"], true).isValid() &&
                moment($filter('date')($scope.dateEnd, 'dd-MM-yyyy'), ["DD-MM-YYYY"], true).isValid() &&
                moment($scope.timeStart.replace(/\:+/g, '-'), ["hh-mm-ss"], true).isValid() &&
                moment($scope.timeEnd.replace(/\:+/g, '-'), ["hh-mm-ss"], true).isValid()) valid = true;
            return valid;
        }
        if (dateType == 'date') {
            if ($scope.haveReleaseTime()) {
                if (moment($filter('date')($scope.dateStartRelease, 'dd-MM-yyyy'), ["DD-MM-YYYY"], true).isValid() &&
                    moment($filter('date')($scope.dateEndRelease, 'dd-MM-yyyy'), ["DD-MM-YYYY"], true).isValid() &&
                    moment($scope.timeStartRelease.replace(/\:+/g, '-'), ["hh-mm-ss"], true).isValid() &&
                    moment($scope.timeEndRelease.replace(/\:+/g, '-'), ["hh-mm-ss"], true).isValid()) valid = true;
            } else {
                if (moment($filter('date')($scope.dateStartRelease, 'dd-MM-yyyy'), ["DD-MM-YYYY"], true).isValid() &&
                    moment($filter('date')($scope.dateEndRelease, 'dd-MM-yyyy'), ["DD-MM-YYYY"], true).isValid()) valid = true;
            }
            return valid;
        }}

    function widget0NewsMarginTopFn() {return document.getElementById('under-header').offsetHeight;}

    function showBadeWindow(elem, date) {

        var margin_top = 22;
        if (date && date ==='date'){
            margin_top = - 30
        }
        else{
            margin_top = - 100
        }
        $('#window-badge').css('top', $('#' + elem).parent().offset().top - margin_top );
        $scope.windowBadge.show = true;
        clearTimeout(windowBadgeTimeout);
        windowBadgeTimeout = setTimeout(function() {
            hideBadgeWindow();
            $scope.$apply();
        }, 3000);}

    function hideBadgeWindow() {$scope.windowBadge.show = false;}

    function createDateValue(position, type) {
        var str = '',
            date = null,
            time = null;
        console.log(position, type);
        if (position == 'start' && type == 'date_upload') {
            date = angular.copy($scope.dateStart);
            time = angular.copy($scope.timeStart);
        }
        if (position == 'start' && type == 'date') {
            date = angular.copy($scope.dateStartRelease);
            time = angular.copy($scope.timeStartRelease);
        }
        if (position == 'end' && type == 'date_upload') {
            date = angular.copy($scope.dateEnd);
            time = angular.copy($scope.timeEnd);
        }
        if (position == 'end' && type == 'date') {
            date = angular.copy($scope.dateEndRelease);
            time = angular.copy($scope.timeEndRelease);
        }
        time = !time ? '00:00:00' : time;
        return $filter('date')(date, 'dd-MM-yyyy') + '-' + time.replace(/\:+/g, '-');
    }

    $scope.openedStart = false;
    $scope.openedEnd = false;
    $scope.openedStartRelease = false;
    $scope.openedEndRelease = false;

    $scope.title = dataService.data.product[0].title;
    $scope.category = dataService.data.monitoring_product_template;
    $scope.dataService = dataService;

    $scope.$root.dataService = dataService;
    root = $scope.$root;
    $scope.$root.$on('loadStart', function (){
        res = [];
        angular.forEach(dataService.titleOffset, function(elem) {
            res.push({'top': elem.offset().top, 'elem': elem})
        });
        res = $filter('orderBy')(res, 'top');
        if (res[0]) {
            dataService.activeTitle = res[0].elem.html();
            res[0].elem.css({
                height: 1,
                overflow: 'hidden'
            });
        }
        if(!$scope.$root.$$phase) {$scope.$root.$apply();}
        dataService.header = 23;
        if(!$scope.$root.$$phase) {$scope.$root.$apply();}
    });
    // select current referrer object from filter
    $timeout(function () { // TODO: automatize that
        if (!angular.isUndefined($routeParams.referred_object_id)) {
            var referredObjects = $scope.filters.referred_object__id__in;
            if (!angular.isUndefined(referredObjects)) {
                for (var i = 0; i < referredObjects.list.length; i++) {
                    var element = referredObjects.list[i];
                    element.selected = false;
                    if (+$routeParams.referred_object_id == element.id) element.selected = true;
                }
                referredObjects.toggled = false;
                $scope.activeFilters.push({
                    filter_list: referredObjects.list,
                    title: referredObjects.settings.a2 ? referredObjects.settings.a2 : referredObjects.settings.a1,
                    isNull: referredObjects
                });
            }
        }
        if (!angular.isUndefined($routeParams.emotional_tone_object__id)) {
            var referredObjects = $scope.filters.emotional_tone_object__id__in;
            if (!angular.isUndefined(referredObjects)) {
                for (var i = 0; i < referredObjects.list.length; i++) {
                    var element = referredObjects.list[i];
                    element.selected = false;
                    if (+$routeParams.emotional_tone_object__id == element.id) element.selected = true;
                }
                referredObjects.toggled = false;
                $scope.activeFilters.push({
                    filter_list: referredObjects.list,
                    title: referredObjects.settings.a2 ? referredObjects.settings.a2 : referredObjects.settings.a1,
                    isNull: referredObjects
                });
            }
        }
        $scope.$apply();
        $scope.widget0NewsMarginTop = widget0NewsMarginTopFn();

    });
    $scope.$watch('windowBadge.countOfNews', function(newVal, oldVal) {
        $scope.windowBadge.isDisable = !$scope.windowBadge.countOfNews ? true : false;
    });
}).
    run(['$rootScope', function($root) {
    $root.$on('$routeChangeStart', function(e, curr, prev) {
        if (curr.$$route && curr.$$route.resolve) {
            $root.loadingView = true;
        }
    });
    $root.$on('$routeChangeSuccess', function(e, curr, prev) {
        $root.loadingView = false;
    });
}]).
    filter('highlight', function () {
        return function (text, search, caseSensitive) {
            if (text && (search || angular.isNumber(search))) {
                text = text.toString();
                search = search.toString();
                if (caseSensitive) {
                    return text.split(search).join('<span class="ui-match">' + search + '</span>');
                } else {
                    return text.replace(new RegExp(search, 'gi'), '<span class="ui-match">$&</span>');
                }
            } else {
                return text;
            }
        };
    });
    //directive('profileTmp', function(){
    //    return{
    //        restrict: 'E',
    //        scope: {
    //            profile: '='
    //        },
    //        templateUrl: '/template/profileModals.html',
    //        controller: 'mainsWidget'
    //    }
    //}).
    //controller('PersonalTabs', function ($scope, $http) {
    //    $scope.currentProfileTab = 'profile.html';
    //    $scope.currentSettingTab = 'amendment.html';
    //    $scope.$on("update_parent_tab", function(event, message){
    //        $scope.currentProfileTab = message;
    //    });
    //    $scope.onClickTab = function (tab) {
    //        $scope.currentProfileTab = tab;
    //    };
    //    $scope.isActiveTab = function(tabUrl) {
    //        return tabUrl == $scope.currentProfileTab;
    //
    //
    //    };
    //    $scope.main_list = [
    //        {name: 'source', text: 'Источник',id:1 },
    //        {name: 'date_up', text: 'Дата',id:2},
    //        {name: 'release_time', text: 'Время',id:3},
    //        {name: 'title', text: 'Заголовок',id:4},
    //        {name: 'author', text: 'Автор',id:5},
    //        {name: 'description', text: 'Полнотекст',id:6},
    //        {name: 'annotation', text: 'Аннотация',id:7},
    //        {name: '?????', text: 'Перепечатки',id:8}
    //
    //      ];
    //    $scope.media_list = [
    //        {name: 'media_level', text: 'Уровень',id:21 },
    //        {name: 'media_type', text: 'Тип',id:22},
    //        {name: 'media_category', text: 'Категория',id:25},
    //        {name: 'media_view', text: 'Вид',id:23}
    //      ];
    //    $scope.geography_list = [
    //        {name: 'country', text: 'Страна',id:31 },
    //        {name: 'federal_district', text: 'Федеральный округ',id:32},
    //        {name: 'city', text: 'Город',id:35}
    //      ];
    //    $scope.indicators_list = [
    //        {name: 'prv', text: 'PRV',id:41 },
    //        {name: 'prt', text: 'PRt',id:42},
    //        {name: 'mo', text: 'Media outreach',id:43},
    //        {name: 'emotional_tone_object', text: 'Тональность',id:44}
    //      ];
    //    $scope.mention_list = [
    //        {name: 'mention_object', text: 'Объекты',id:51 },
    //        {name: 'fio', text: 'ФИО спикера',id:52}
    //
    //      ];
    //
    //    $scope.date_checked = {
    //        main_list: [$scope.main_list[1]],
    //        media_list :[$scope.media_list[1]],
    //        geography_list :[$scope.geography_list[1]],
    //        indicators_list :[$scope.indicators_list[1]],
    //        mention_list :[$scope.mention_list[1]]
    //      };
    //
    //    $scope.saveSettings = function(){
    //        $http.defaults.headers.post['X-CSRFToken'] = $cookies['csrftoken'];
    //        var responsePromise = $http.post("/save-product-settings/", $scope.date_checked, {});
    //    };
    //    $scope.onClickSetTab = function (tab) {
    //        $scope.currentSettingTab = tab;
    //    };
    //    $scope.isActiveSetTab = function(tabUrl) {
    //        return tabUrl == $scope.currentSettingTab;
    //    };
    //});