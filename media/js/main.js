$(function() {
	var $datepicker = $('.datepicker'),
		$dateInput = $datepicker.find('input[type="text"]'),
		$dateElements = $datepicker.children('.element');
	// $dateInput.datepicker($.datepicker.regional["ru"]);
	$dateInput.datepicker({
		altFormat: 'dd-mm-yy',
		dateFormat: 'dd-mm-yy'
	});
	console.log('$dateElements.length', $dateElements.length)
	if ($dateElements.length == 1) {
		// console.log('однодневный календарь');
		$('.arrow.prev').click(function() {
			var date = $dateInput.datepicker('getDate');
			date.setDate(date.getDate() - 1);
			$dateInput.datepicker('setDate', date);
			return false;
		});
		$('.arrow.next').click(function() {
			var date = $dateInput.datepicker('getDate');
			date.setDate(date.getDate() + 1);
			$dateInput.datepicker('setDate', date);
			return false;
		});
	} else {
		var $firstCalendar = $dateInput.first(),
			$secondCalendar = $dateInput.last();
		if (true) { // недельный календарь
			$firstCalendar.datepicker('option', 'onSelect', function() {
				var date = $(this).datepicker('getDate');
				date.setDate(date.getDate() + 7);
				$secondCalendar.datepicker('setDate', date);
			});
			$secondCalendar.datepicker('option', 'onSelect', function() {
				var date = $(this).datepicker('getDate');
				date.setDate(date.getDate() - 7);
				$firstCalendar.datepicker('setDate', date);
			});
			$('.arrow.prev').click(function() {
				var date1 = $firstCalendar.datepicker('getDate'),
					date2 = $secondCalendar.datepicker('getDate');
				date1.setDate(date1.getDate() - 1);
				date2.setDate(date2.getDate() - 1);
				$firstCalendar.datepicker('setDate', date1);
				$secondCalendar.datepicker('setDate', date2);
				return false;
			});
			$('.arrow.next').click(function() {
				var date1 = $firstCalendar.datepicker('getDate'),
					date2 = $secondCalendar.datepicker('getDate');
				date1.setDate(date1.getDate() + 1);
				date2.setDate(date2.getDate() + 1);
				$firstCalendar.datepicker('setDate', date1);
				$secondCalendar.datepicker('setDate', date2);
				return false;
			});
		} else { // месячный календарь

		}
		// console.log('недельный календарь');
		// console.log('месячный календарь');
	}

});