var exlibris = angular.module('exlibris', ['ui.bootstrap', 'ngSanitize', 'ngCookies', 'rzModule', 'ngRoute', 'ngAnimate',  'ngClipboard', 'perfect_scrollbar']);
exlibris.config(function($routeProvider, $locationProvider, $interpolateProvider) {
	$interpolateProvider.startSymbol('{$');
	$interpolateProvider.endSymbol('$}');
	$routeProvider.when('/:user/monitoring_product/:id/', {
		templateUrl: "/template/news.html/",
		controller: "NewsListCtrl",
        resolve:{
            load: function ($rootScope, $route, dataService) {
            	$rootScope.productId = $route.current.params.id;
                $rootScope.$emit('loadStart');
                return dataService.load($route.current.params.id).then(function(res){
                    dataService.data = res;
                    dataService.lastId = res.last_id;
                    dataService.count_end = res.count_end;
                });	
            },
            filter: function($rootScope, $route, dataFilter){
                $rootScope.productId = $route.current.params.id;
                $rootScope.productSlug = $route.current.params.user;
                $rootScope.$emit('loadStart');
                return dataFilter.filter($rootScope.productId, $rootScope.productSlug).then(function(res){
                    //_.each(res.filters.referred_object__id__in.list, function(el){
                    //    el.mention_object = _.findWhere(res.mention_objects_list, {id: el.id});
                    //})
                    dataFilter.data = res;
                });
            }
        }
	})
	$locationProvider.html5Mode(true);
}).
  //  config(['ngClipProvider', function(ngClipProvider) {
  //  ngClipProvider.setPath("//cdnjs.cloudflare.com/ajax/libs/zeroclipboard/2.1.6/ZeroClipboard.swf");
  //}]).
    factory('product_settings', function(){
        return product_settings
    }).
    factory('dataFilter', function($q, $http){
        return{
            data:{},
            filter: function(id, slug){
                var defer = $q.defer();
                $http.get('http://'+ window.location.host +'/' + slug + '/get-monitoring-product-filters/' + id + '/').success(function(res){
                    defer.resolve(res);
                });
                return defer.promise;
            }
        }
    }).
    //factory('dataAccessUser', function($q, $http){
    //    return{
    //        data:{},
    //        access: function(id){
    //            var defer = $q.defer();
    //            $http.get('http://'+ window.location.host + '/monitoring_product/get_users_access/' + id + '/').success(function(res){});
    //        }
    //    }
    //}).
//    factory('productData', function($q, $http){
//        return{
//            data:{},
//            filter: function(id, slug){
//                var defer = $q.defer();
//                $http.get('http://'+ window.location.host +'/' + slug + '/get-monitoring-product-filters/' + id + '/').success(function(res){
//                    defer.resolve(res);
//                });
//                return defer.promise;
//            }
//        }
//    }).
    factory('dataService', function ($q, $http, $cookies, $rootScope, $timeout) {
    return {
        data: {},
        load: function(id) {
            var defer = $q.defer();
           	$http.get('http://'+ window.location.host +'/monitoring-product/' + id + '/').success(function(res){
                defer.resolve(res);
            });
            return defer.promise;
        },
	    titleOffset: [],
	    activeTitle: [],
        activeTitleChange: [],
	    headerObj: null,
	    header: 0,
        lastId: null,
        count_end: null,
        stopLoadArticles: false,
        waitLoadAjax: true,
        scrollToElement: function(id) {
            var currentTitle = angular.element('#' + id);
            if (currentTitle.length) {
                var currentTitleHeight = currentTitle.height(),
                    currentTitleTop = currentTitleHeight == 0 ? 0 : currentTitle.offset().top - angular.element('.wrap-header').height() + currentTitleHeight + 2;
                angular.element("body, html").animate({scrollTop: currentTitleTop});
            }
        },
        findFullCategory: function(children, id) {
        	if (children.length) {
	            for (var i=0; i<children.length; i++) {
	                if (children[i].articles && children[i].articles.length) {
	                    this.scrollToElement(children[i].id);
	                    return false;
	                }
	                if (children[i].children && children[i].children.length) {
	                    this.findFullCategory(children[i].children, children[i].id);
	                    return false;
	                }
	                this.getAllArticles(id);
	                return false;
	            }
        	} else {
        		this.scrollToElement(id);
        	}
        },
        getAllArticles: function(id) {
            if (!this.stopLoadArticles) {
                $rootScope.loadingView = false;
                $http.defaults.headers.post['X-CSRFToken'] = $cookies['csrftoken'];
                var data = {
                    date_start: $rootScope.dateStart.replace(/\.+/g, '-'),
                    date_end: $rootScope.dateEnd.replace(/\.+/g, '-'),
                    all: true
                }
				$http.post('http://'+ window.location.host +'/monitoring-product/' + $rootScope.productId + '/', data).success($.proxy(function(res){
				    this.data.monitoring_product_template = res.monitoring_product_template;
				    $rootScope.hideBreadTitleFlag = true;
				    this.stopLoadArticles = true;
				    $rootScope.loadingView = false;
					$timeout($.proxy(function(){
						for (var i = 0; i < res.monitoring_product_template.length; i++) {
							if (res.monitoring_product_template[i].id == id) {
								this.findFullCategory(res.monitoring_product_template[i].children, res.monitoring_product_template[i].id);
							} else {
								this.findCurrentLelev(res.monitoring_product_template[i].children, id)
							}
						}
					}, this));
                }, this));
            }
        },
		findCurrentLelev: function(data, id) {
			for (var i = 0; i < data.length; i++) {
				if (data[i].id == id) {
					this.findFullCategory(data[i].children, data[i].id);
				} else {
					this.findCurrentLelev(data[i].children, id);
				}
			}
		}
    };
}).
    directive('categoryList', function($compile, $rootScope) {
    return {
        restrict: 'E',
        scope: {
            category: '=',
            pretitle: '=',
            searchText: '='
        },
        templateUrl: 'categoryListTemplate',
        controller: function($scope) {
            $scope.createArray = function(pretitle, title, id, brothers){
                var arr = '';
                if (pretitle) arr = angular.copy(pretitle);
                arr += '{'+
                	'"title":"'+title+'",'+
                	'"id":'+id+','+
                	'"brothers":'+angular.toJson(brothers)+
            	'},';
                return arr;
            };
        },
        compile: function(tElement, tAttr, transclude) {
            var contents = tElement.contents().remove();
            var compiledContents;
            return function(scope, iElement, iAttr) {
                if(!compiledContents) {
                    compiledContents = $compile(contents, transclude);
                }
                compiledContents(scope, function(clone, scope) {
                    iElement.append(clone);
                });
            }
        }
    }
}).
    directive('singleArticle', function($compile) {
    return {
        restrict: 'E',
        scope: {
            item: '=',
            searchText: '='
        },
        require: "^ngController",
        replace: true,
        templateUrl: 'singleArticleTemplate',
        controller: function($scope, $filter, $sce, globalFlags) {
            $scope.currentIndex = 0;
            $scope.activeWord = 0;
            $scope.flagType = globalFlags.flagType;
            $scope.status = {
                isCollapsed: true
            }
            $scope.activeWordChange = function(index) {
                $scope.activeWord = index;
                $scope.currentIndex = index;
            }
            $scope.previewWordChange = function(index) {
                $scope.currentIndex = $scope.activeWord;
                $scope.activeWord = index;
            }
            $scope.returnLastActive = function() {
                $scope.activeWord = $scope.currentIndex;
            }
            $scope.paintTheWords = function(html, mo) {

                text = html;
                for(var i = 0; i < mo.length; i++) {
                    for(var j = 0; j < mo[i].keywords.length; j++) {
                        text = $filter('paintTheWordsFilter')(text, mo[i].keywords[j], '<span class="colored-text" style="background-color:#'+ mo[i].color +'">' + mo[i].keywords[j] + '</span>');
                    }
                }
                return $sce.trustAsHtml(text);
            }
            $scope.renderSpeakers = function(speakers) {
                var text = '(';
                for (var i = 0; i < speakers.length; i++)
                    text += speakers[i].fio + (i != speakers.length - 1 ? ', ': '');
                text += ')';
                return text;
            }
            $scope.sourcePopup = true;
            $scope.copyRightPopup = true;
            $scope.urlPopup = true;
            $scope.showSourcePopup = function() {
                $scope.sourcePopup = false;
            }
            $scope.hideSourcePopup = function() {
                $scope.sourcePopup = true;
            }
            $scope.showCopyRightPopup = function() {
                $scope.copyRightPopup = false;
            }
            $scope.hideCopyRightPopup = function() {
                $scope.copyRightPopup = true;
            }
            $scope.showUrlPopup = function() {
                $scope.urlPopup = false;
            }
            $scope.hideUrlPopup = function() {
                $scope.urlPopup = true;
            }
        },
        compile: function(tElement, tAttr, transclude) {
            var contents = tElement.contents().remove();
            var compiledContents;
            return function(scope, iElement, iAttr) {
                if(!compiledContents) {
                    compiledContents = $compile(contents, transclude);
                }
                compiledContents(scope, function(clone, scope) {
                    iElement.append(clone);
                });
            }
        }

    }
}).
    directive("scroll", function($rootScope, $window, $filter, $http, $cookies, dataService) {
    return function(scope, element, attrs) {
        var raw = element[0];
        angular.element($window).bind("scroll", function() {
            if (!dataService.stopLoadArticles && dataService.waitLoadAjax) {
                if ($(window).scrollTop() + raw.offsetHeight >= raw.scrollHeight) {
                    $rootScope.loadingLazyLoad = true;
                    dataService.waitLoadAjax = false;
                    $http.defaults.headers.post['X-CSRFToken'] = $cookies['csrftoken'];
                    var data = {
                        date_start: $rootScope.dateStart.replace(/\.+/g, '-'),
                        date_end: $rootScope.dateEnd.replace(/\.+/g, '-')
                    }
                    if (dataService.lastId){
                        angular.extend(data, {
                            last_id: dataService.lastId,
                            count_end: dataService.count_end
                        });
                    }
                    $http.post('http://'+ window.location.host +'/monitoring-product/' + $rootScope.productId + '/', data).success(function(res){
                    	dataService.waitLoadAjax = true;
                        dataService.lastId = res.last_id;
                        dataService.count_end = res.count_end;
                        if (!res.last_id) dataService.stopLoadArticles = true;
                        // ------------------------------------------
                        var currentData = dataService.data.monitoring_product_template,
                            nextData = res.monitoring_product_template,
                            mergeData = function(newArticles, path){
                                var currentlevel = currentData;
                                for (var index=0; index<path.length; index++){
                                    currentlevel = currentlevel[path[index]];
                                }
                                for(var counter=0; counter<newArticles.length; counter++){
                                    currentlevel.push(newArticles[counter]);
                                }
                            };
                        for (var i=0; i<nextData.length; i++){
                            var level_1 = nextData[i];
                            if (level_1.articles.length) mergeData(level_1.articles, [i, 'articles']);
                            if (level_1.children.length){
                                for(var j=0; j<level_1.children.length;j++){
                                    var level_2 = level_1.children[j];
                                    if (level_2.articles.length) mergeData(level_2.articles, [i, 'children', j, 'articles']);
                                    if (level_2.children.length){
                                        for(var k=0; k<level_2.children.length;k++){
                                            var level_3 = level_2.children[k];
                                            if (level_3.articles.length) mergeData(level_3.articles, [i, 'children', j, 'children', k, 'articles']);
                                            if (level_3.children.length){
                                                for(var e=0; e<level_3.children.length;e++){
                                                    var level_4 = level_3.children[e];
                                                    if (level_4.articles.length) mergeData(level_4.articles, [i, 'children', j, 'children', k, 'children', e, 'articles']);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        $rootScope.loadingLazyLoad = false;
                        // ------------------------------------------
                    });
                }
            }
            var res = [],
                headerHeight = 103;
            angular.forEach(dataService.titleOffset, function(item) {
                res.push({'top': item.elem.offset().top, 'list': item.list});
            });

            res = $filter('orderBy')(res, 'top');
            for (var i = 0; i < res.length; i++){
            	if (res[i].top >= headerHeight) {
	                if (i < res.length - 1) {
	                    if (res[i].top < pageYOffset + headerHeight - 30 && pageYOffset + headerHeight - 30 < res[i + 1].top) {
	                        dataService.activeTitle = res[i].list;
                            dataService.activeTitleChange = angular.copy(res[i].list);
	                        break;
	                    }
	                }
	                if (i == res.length - 1 && res[i].top < pageYOffset + headerHeight - 30) {
	                    dataService.activeTitle = res[i].list;
                        dataService.activeTitleChange = angular.copy(res[i].list);
	                    break;
	                }
            	}
            }
            scope.$apply();
        });
    };
}).
    directive('titleInfo', function($rootScope, dataService){
    return {
        scope: {
            title: '=',
            titleList: '=',
            catId: '=',
            brothers: '='
        },
        template: '<span ng-hide="hideBreadTitle" ng-repeat="elem in list">{$ elem.title $}</span>',
        link: function(scope, elem, attrs) {
//            console.log('titleInfo')
//        	var str = scope.titleList;
//
//        	scope.list = angular.fromJson('[' + str.substring(0, str.length - 1) + ']');
            scope.list = [];
        	scope.list.push({
        		id: scope.catId,
        		title: scope.title,
        		brothers: scope.brothers
        	});
            dataService.titleOffset.push({
            	'elem': elem,
            	'list': scope.list
            });
			scope.hideBreadTitle = false;
			if ($rootScope.hideBreadTitleFlag) {
				scope.hideBreadTitle = true;
				$rootScope.hideBreadTitleFlag = false;
			}
            scope.$root.$emit('loadStart');
        }
    }
}).
    controller('headerCtrl', function($rootScope, $scope, $http, $cookies, $filter, $location, dataService) {
    $rootScope.visibleMenu = [];
    $scope.dataService = dataService;
    $scope.showFiltersList = false;
    $scope.generalFilters = dataService.data.product[0].referred_objects;
    $scope.showHideFiltersList = function() {
        $scope.showFiltersList = !$scope.showFiltersList;
    };
    $rootScope.dateStart = angular.copy(dataService.data.product[0].date_start.replace(/\-+/g, '.'));
    $rootScope.dateEnd = angular.copy(dataService.data.product[0].date_end.replace(/\-+/g, '.'));

    $rootScope.timeStart = dataService.data.product[0].time_start;
    $rootScope.timeEnd = dataService.data.product[0].time_end;

    $scope.dateType = dataService.data.product[0].type;
    $scope.title = dataService.data.product[0].title;
    if ($scope.dateType == "day") {
    	$scope.daysCount = 1;
    } else if ($scope.dateType == "week") {
    	$scope.daysCount = 7;
    } else if ($scope.dateType == "month") {
    	$scope.daysCount = 30;
    } else if (dataService.data.product[0].dateType == "date_release") {
        dataService.data.product[0].date_release_start = $rootScope.dateStart;
        dataService.data.product[0].date_release_end = $rootScope.dateEnd;
    } else if (dataService.data.product[0].dateType == "date_upload") {
        dataService.data.product[0].date_upload_start = $rootScope.dateStart;
        dataService.data.product[0].date_upload_end = $rootScope.dateEnd;
    }
    var menu = dataService.data.monitoring_product_template;

    $scope.openedStart = false;
    $scope.openedEnd = false;
    $scope.filterWidgetNull = function(mentionObjectId, tonalityId){
        var getDate = '?referred_object_id='+mentionObjectId +
            '&date_start='+$scope.dateStart.replace(/\.+/g, '-') + '-' + $rootScope.timeStart +
            '&date_end='+$scope.dateEnd.replace(/\.+/g, '-') + '-' + $rootScope.timeEnd +
            '&type=date_upload';
        if (tonalityId) getDate += '&emotional_tone_object__id='+tonalityId;
        window.location.href = $location.path() + 'widget0/' + getDate;
    }
    $scope.getNewsList = function(datePicker, val) {
        if (datePicker == 'start') {
            $scope.dateEnd = moment(new Date(val.split('.')[2], val.split('.')[1] - 1, val.split('.')[0])).add($scope.daysCount, 'days').format('DD.MM.YYYY');
        } else if (datePicker == 'end') {
            $scope.dateStart = moment(new Date(val.split('.')[2], val.split('.')[1] - 1, val.split('.')[0])).subtract($scope.daysCount, 'days').format('DD.MM.YYYY');
        }
        dateFilter(datePicker);
    }
    $scope.nextPrev = function(type) {
        var operType = type == "prev" ? 'subtract' : 'add';
        $scope.dateStart = moment(new Date($scope.dateStart.split('.')[2], $scope.dateStart.split('.')[1] - 1, $scope.dateStart.split('.')[0]))[operType]($scope.daysCount, 'days').format('DD.MM.YYYY');
        $scope.dateEnd = moment(new Date($scope.dateEnd.split('.')[2], $scope.dateEnd.split('.')[1] - 1, $scope.dateEnd.split('.')[0]))[operType]($scope.daysCount, 'days').format('DD.MM.YYYY');
        dateFilter('end');
    }
    $scope.goToWidgetNull = function() {
        window.location.href = $location.path() + 'widget0'+
            '?date_start='+ $scope.dateStart.replace(/\.+/g, '-') + '-' + $rootScope.timeStart +
            '&date_end='+ $scope.dateEnd.replace(/\.+/g, '-') + '-' + $rootScope.timeEnd +
            '&type=date_upload';
    }
    $scope.redirectToIndex = function(url) {
        window.location.href = '/' + url;
    }
    function dateFilter(changesDate) {
        var ajaxData = {
            date_type: $scope.dateType
        }
        if (changesDate == 'start') {
            angular.extend(ajaxData, {date_start: $scope.dateStart.replace(/\.+/g, '-')});
        } else {
            angular.extend(ajaxData, {date_end: $scope.dateEnd.replace(/\.+/g, '-')});
        }
        $rootScope.loadingView = true;
        $http.defaults.headers.post['X-CSRFToken'] = $cookies['csrftoken'];
        $http.post('http://'+ window.location.host +'/monitoring-product/'+ $rootScope.productId +'/', ajaxData).success(function(res) {
            $scope.dateStart = res.product[0].date_start.replace(/\-+/g, '.');
            $scope.dateEnd = res.product[0].date_end.replace(/\-+/g, '.');
            $rootScope.dateStart = $scope.dateStart;
            $rootScope.dateEnd = $scope.dateEnd;
        	$rootScope.hideBreadTitleFlag = true;
            dataService.activeTitle.length = 0;
            $rootScope.loadingView = false;
            dataService.stopLoadArticles = false;
            dataService.data = res;
            dataService.titleOffset.length = 0;
            var menu = dataService.data.monitoring_product_template;
            $rootScope.visibleMenu.length = 0;
            for(var i = 0; i < menu.length; i++) {
                $rootScope.visibleMenu.push(menu[i]);
            }
            angular.element("body, html").animate({scrollTop: angular.element('html').offset().top});        
            // $rootScope.$emit('loadStart');
        });
    }
}).
    controller('NewsListCtrl', function($scope,$rootScope, $filter, dataService) {
    $scope.category = dataService.data.monitoring_product_template;
    $scope.dataService = dataService;
    $scope.$root.dataService = dataService;
    root = $scope.$root;
    $rootScope.articles_check = [];
    $rootScope.send_articles = function(){
        console.log($rootScope.articles_check, '1111111')
    };
    $scope.isWorkedLink = function(value) {
        return value ? true : false;
    }

    $scope.$root.$on('loadStart', function (){
        var res = [];
        angular.forEach(dataService.titleOffset, function(item) {
            res.push({'top': item.elem.offset().top, 'list': item.list});
        });
        res = $filter('orderBy')(res, 'top');
        if (res[0].list) dataService.activeTitle = res[0].list;
        if($scope.$root && !$scope.$root.$$phase) {$scope.$root.$apply();}
        dataService.header = 125;
        if($scope.$root && !$scope.$root.$$phase) {$scope.$root.$apply();}
    });
}).
    run(['$rootScope', function($root) {
    $root.$on('$routeChangeStart', function(e, curr, prev) {
        if (curr) {
            if (curr.$$route && curr.$$route.resolve) {
                $root.loadingView = true;
            }
        }
    });
    $root.$on('$routeChangeSuccess', function(e, curr, prev) {
        $root.loadingView = false;
    });
    $root.hideBreadTitleFlag = true;
}]).
    controller('checkboxCtrl', function($scope, $rootScope, $http, $cookies, dataService, dataFilter, profileView, modalConfirm, $location){
    $scope.sedModal = false;
    $scope.product = dataService.data.product[0];
    $scope.title = dataService.data.product[0].title;
    $scope.save_sub_product = false;
    $scope.openProfile = function (){
            profileView($scope);
            $scope.settings = false;
            console.log($scope.settings, '$scope.settings');
    };
    if ('profile' in $location.search()){
            $scope.openProfile();
    }

    $scope.shareMenu = false;
    $scope.searchText = '';
    $scope.profile = false;
    $scope.setings = false;
    $scope.openSetings = function (){
        $scope.settings = !$scope.settings;
    };
    if ('openProductWhithSettings' in $location.search()){

        $scope.openSetings();
    }
    $scope.closeSetting = function(){
        $scope.settings = false;
    };
    $scope.$on("update_parent_controller", function(event, message){
        $scope.profile = message;
    });
    $scope.$watch("searchText", function(newValue, oldValue) {
        return $scope.searchText;
    });
    $scope.sendArticle = function(){
        send_obj={
           file_name:$scope.file_name,
           from:$scope.from,
           to:$scope.to,
           message:$scope.message,
           topic:$scope.topic
        };
    $http.defaults.headers.post['X-CSRFToken'] = $cookies['csrftoken'];
    $http.post("/send/articles/", {send_obj:send_obj}).success(function(res){
        $scope.sedModal = false;
    });
    };
    $scope.generateDoc = function(topic){
        console.log(dataService,'-=-=-=-=-=-');

        $scope.topic =dataService.data.product[0].title;
        if(topic == 'translation'){
            $scope.topic ='На перевод';
        }

        var inputs = document.getElementsByName("checkbox");
        var checked = 0;
        var articles_id_list=[];
        for (var i = 0; i < inputs.length; i++) {
            if (inputs[i].checked){
                var id = inputs[i].id;
                articles_id_list.push(id.replace('select-', ''));
            }
        }

        $scope.shareMenu = false;
        console.log(articles_id_list);
    $http.defaults.headers.post['X-CSRFToken'] = $cookies['csrftoken'];
    $http.post("/generate-doc/", {articles_id_list:articles_id_list}).success(function(res){
        $scope.sedModal = true;
        $scope.file_name = res['file_name'];
        $scope.message ='';

        $scope.from= res['from'];
        $scope.to='';
    });
    };
    $scope.printArticles = function(){
        var inputs = document.getElementsByName("checkbox");
        var checked = 0;
        var articles_id_list=[];
        for (var i = 0; i < inputs.length; i++) {
            if (inputs[i].checked){
                var id = inputs[i].id;
                articles_id_list.push(id.replace('select-', ''));
            }
        }
    $http.defaults.headers.post['X-CSRFToken'] = $cookies['csrftoken'];
    $http.post("/print/articles/", {articles_id_list:articles_id_list}).success(function(res) {
        $scope.shareMenu = false;
        window.location.href = '/'+res['file_path']
    });

    };
    $scope.copyLink = function(scope){
        console.log('qwerty')
        var inputs = document.getElementsByName("checkbox");
        var checked = 0;
        var articles_link_list = [];
        for (var i = 0; i < inputs.length; i++) {
            if (inputs[i].checked){
                var title = inputs[i].title;
                articles_link_list.push(title);
            }
        }
        var result = '';
        for (var i = 0; i < articles_link_list.length; i++ ){
            console.log(articles_link_list.length);
            if(articles_link_list.length ==1){
                result = result + articles_link_list[i];
            }
            else{
                if(articles_link_list.length == i){
                    result = result + articles_link_list[i]
                }
                else{
                    result = result + articles_link_list[i] + ', ';
                }
            }
        }
        return result;
    };
    $scope.openMenu = function (){

        var inputs = document.getElementsByName("checkbox");

        var checked = 0;
        for (var i = 0; i < inputs.length; i++) {
            'checkbox' == inputs[i].type && inputs[i].checked ? checked++ : false;
        }

        checked >= 1 ? $scope.shareMenu = !$scope.shareMenu : $scope.shareMenu = false
    };
    // other filters
    $scope.newFilters = dataFilter.data.filters;
    // sort by order
    $scope.sortableNewFilters = [];
    for (var key in $scope.newFilters) {
        if (!$scope.newFilters[key].settings.a3) $scope.newFilters[key].settings.a3 = Infinity;
        $scope.sortableNewFilters.push([key, $scope.newFilters[key]]);
    }
    $scope.sortableNewFilters.sort(function(a, b) {
        return a[1].settings.a3 - b[1].settings.a3
    });
    // скрыть ФО
    $scope.hideFederalDistrict = false;
    $scope.is_right = function(ind) {
        return $scope.sortableNewFilters.length < 2*ind + 4;
    };
    $scope.alineFilters = function() {
        $('.filter-left').wrapAll('<div class="filter-block-left"></div>');
        $('.filter-right').wrapAll('<div class="filter-block-right"></div>');
        var heights = $('body').height();
        console.log(heights, 'heights');
        var block = $('.scroll-filter').css("height", heights - 270);
        console.log(block, 'block');
    };
    $scope.isRangeFilter = function(key) {return ~key.indexOf('range');}
    $scope.saveProduct = function(){
        //var filters = dataFilter.data.filters.slice();

        var product = dataService.data.product[0];


        if (product.type == "date_release"){
            product.date_from = product.date_release_start;
            product.date_to = product.date_release_end;
        }
        if (product.type == "date_upload"){
            var time_start = product.time_upload_start.split(":"), time_end = product.time_upload_end.split(":");

            product.date_from = product.date_upload_start;
            product.date_to = product.date_upload_end;

            product.date_from.setHours(parseInt(time_start[0])||0);
            product.date_from.setMinutes(parseInt(time_start[1])||0);
            product.date_from.setSeconds(parseInt(time_start[2])||0);

            product.date_to.setHours(parseInt(time_end[0])||0);
            product.date_to.setMinutes(parseInt(time_end[1])||0);
            product.date_to.setSeconds(parseInt(time_end[2])||0);
        }
        if(product.date_to && typeof(product.date_to)=='string'){
            var tmp = product.date_to.split(/[-\.]/);
            tmp[0]^=tmp[1];
            tmp[1]^=tmp[0];
            tmp[0]^=tmp[1];
            product.date_to = new Date(tmp.join('-'));
        }
        if(product.date_from && typeof(product.date_from)=='string') {
            var tmp = product.date_from.split(/[-\.]/);
            tmp[0] ^= tmp[1];
            tmp[1] ^= tmp[0];
            tmp[0] ^= tmp[1];
            product.date_from = new Date(tmp.join('-'));
        }
        var data = angular.toJson({product: $scope.product,
            category_list: dataFilter.data.category_list,
            mention_objects_list: dataFilter.data.mention_objects_list,
            filters: dataFilter.data.filters,
            product: product
        });

        var confirm = function(){
            $http({
                method: 'POST',
                url: "/create-or-update-product/" +$rootScope.productId+ "/",
                data: data,
                headers: {"X-CSRFToken": $cookies.csrftoken,
                    "Content-Type": undefined}
            }).success(function(data){
                window.location = '/'+$rootScope.productSlug + "/monitoring_product/" +  data.id + "/";
            });
        };
        console.log(product.parent_id)
        if (product.title==product.old_title && product.parent_id != null){

            var question = "";
            var no_more_param_name = "";

            question = "Вы уверены, что хотите сохранить измененный продукт, сохранив его последнюю версию? Для подтверждения действия нажмите \"Да\"." +
            "Для сохранения нового продукта без удаления его исходной версии кликните на кнопку \"НАЗАД\", затем измените название продукта и нажмите" +
            "на \"Сохранить\"";

            no_more_param_name = "dont_ask_if_change_without_rename_product_availible_only_for_me";

            var modal_data = {title: "Точно хотите сохранить?", question: question};
            modalConfirm($scope, confirm, no_more_param_name, modal_data);
        }
        else{
            if(product.title==product.old_title && product.parent_id == null){
                $rootScope.openModal("ChangeProductName.html")
            }
            else{confirm();}

        }
    };

    $scope.getMentionObjects = function(key) {return key=='referred_object__id__in'?$scope.mentionObjects:null;}
    // km_result
    // проверка на исключения фильтра release_time из общего списка
    $scope.isCustomFilter = function(key) {return ~key.indexOf('release_time');}
    $scope.haveReleaseTime = function() {return !angular.isUndefined($scope.newFilters.release_time);}

    if ($scope.haveReleaseTime()) {
        $scope.newFilters.km_result.list = [{
            id: 1, title: 'да'
        }, {
            id: 2, title: 'нет'
        }];
    }
    $scope.removeFilter = function(filter) {
        if (filter.filter_list) {
            for (var i = 0; i < filter.filter_list.length; i++) {
                filter.filter_list[i].selected = false;
                if (!angular.isUndefined(filter.isNull.emptyValue)) filter.isNull.emptyValue = false;
                filter.isNull.selectAll = false;
            }
        } else {
            filter.id.slider.min = filter.id.slider.floor;
            filter.id.slider.max = filter.id.slider.ceil;
        }
        $scope.showFilteredArticles(true);
    };

}).
    filter('highlight', function () {
        return function (text, search, caseSensitive) {
            if (text && (search || angular.isNumber(search))) {
                text = text.toString();
                search = search.toString();
                if (caseSensitive) {
                    return text.split(search).join('<span class="ui-match">' + search + '</span>');
                } else {
                    return text.replace(new RegExp(search, 'gi'), '<span class="ui-match">$&</span>');
                }
            } else {
                return text;
            }
        };

}).
    //controller('PersonalTabs', function ($scope, $rootScope, $http, $cookies, product_settings, modalConfirm, dataAccessUser) {
    //    $scope.currentProfileTab = 'profile.html';
    //    $scope.currentSettingTab = 'amendment.html';
    //    $scope.$on("update_parent_tab", function(event, message){
    //        $scope.currentProfileTab = message;
    //    });
    //    $scope.onClickTab = function (tab) {
    //        $scope.currentProfileTab = tab;
    //    };
    //    $scope.isActiveTab = function(tabUrl) {
    //        return tabUrl == $scope.currentProfileTab;
    //    };
    //    $scope.main_list = [
    //        {name: 'source', text: 'Источник',id:1 },
    //        {name: 'date', text: 'Дата',id:2},
    //        {name: 'release_time', text: 'Время',id:3},
    //        {name: 'title', text: 'Заголовок',id:4},
    //        {name: 'author', text: 'Автор',id:5},
    //        {name: 'description', text: 'Полнотекст',id:6},
    //        {name: 'annotation', text: 'Аннотация',id:7},
    //        {name: '?????', text: 'Перепечатки',id:8},
    //        {name: 'url', text: 'Url',id:9},
    //        {name: 'mo', text: 'Media outreach',id:10}
    //      ];
    //    $scope.media_list = [
    //        {name: 'media_level', text: 'Уровень',id:21 },
    //        {name: 'media_type', text: 'Тип',id:22},
    //        {name: 'media_category', text: 'Категория',id:25},
    //        {name: 'media_view', text: 'Вид',id:23}
    //      ];
    //    $scope.geography_list = [
    //        {name: 'country', text: 'Страна',id:31 },
    //        {name: 'federal_district', text: 'Федеральный округ',id:32},
    //        {name: 'city', text: 'Город',id:35}
    //      ];
    //    $scope.indicators_list = [
    //        {name: 'prv', text: 'PRV',id:41 },
    //        {name: 'prt', text: 'PRt',id:42},
    //        {name: 'emotional_tone_object', text: 'Тональность',id:44}
    //      ];
    //    $scope.mention_list = [
    //        {name: 'title', text: 'Объекты',id:51 },
    //        {name: 'speakers', text: 'ФИО спикера',id:52}
    //      ];
    //
    //    if(Object.getOwnPropertyNames(product_settings.settings).length === 0){
    //        $scope.date_checked = {
    //        main_list: $scope.main_list,
    //        media_list :$scope.media_list,
    //        geography_list :$scope.geography_list,
    //        indicators_list :$scope.indicators_list,
    //        mention_list :$scope.mention_list
    //    };
    //    }else {
    //        $scope.date_checked = {
    //        main_list: product_settings.settings.main_list,
    //        media_list :product_settings.settings.media_list,
    //        geography_list :product_settings.settings.geography_list,
    //        indicators_list :product_settings.settings.indicators_list,
    //        mention_list :product_settings.settings.mention_list
    //    };
    //    }
    //    $scope.saveSettings = function(){
    //        $http.defaults.headers.post['X-CSRFToken'] = $cookies['csrftoken'];
    //        var responsePromise = $http.post("/save-product-settings/" + $rootScope.productId + "/" , {settings: $scope.date_checked}).success(function(){
    //            window.location.reload()
    //        });
    //    };
    //    $scope.onClickSetTab = function (tab, id) {
    //        $scope.currentSettingTab = tab;
    //        $http.get('/monitoring_product/get_users_access/' + $rootScope.productId + '/').success(function(userData) {
    //            $scope.company = userData;
    //            $scope.users = $scope.company.users;
    //            $scope.available_only_for_me = $scope.company.available_only_for_me;
    //            $scope.usersAccess = _.filter($scope.company.users, function(e){return e.selected});
    //            $scope.selected_departments = _.each($scope.company.departments, function(el){
    //                el.selected = false;
    //                el.users = _.where($scope.usersAccess, {department:el.id});
    //            });
    //            $scope.selected_departments = _.filter($scope.selected_departments, function(el){return el.users.length});
    //        });
    //
    //    };
    //    $scope.isActiveSetTab = function(tabUrl) {
    //        return tabUrl == $scope.currentSettingTab;
    //    };
    //    $scope.changeAvailability = function() {
    //        var confirm = function(){
    //            $scope.available_only_for_me = !$scope.available_only_for_me;
    //        };
    //        if (!$scope.available_only_for_me){
    //            var modal_data = {title: "Точно хотите сделать доступным только для вас?", question: "Вы действительно хотите сделать продукт доступным только Вам? Ваши коллеги не будут иметь доступак даному продукту."};
    //            modalConfirm($scope, confirm, "dont_ask_if_make_product_availible_only_for_me", modal_data);
    //        }
    //        else {
    //            confirm();
    //        }
    //    };
    //    $scope.saveHavingAccessUsers = function(){
    //        var data = {users: _.map($scope.usersAccess, function(el){return el.id})};
    //        $http.defaults.headers.post['X-CSRFToken'] = $cookies['csrftoken'];
    //        $http.post('/monitoring_product/set_users_access/' + $rootScope.productId + '/', data).success(function(res) {
    //            console.log("Access to product was updated");
    //        });
    //    };
    //}).
    directive('checklistModel', ['$parse', '$compile', function($parse, $compile) {
      // contains
      function contains(arr, item) {
        if (angular.isArray(arr)) {
          for (var i = 0; i < arr.length; i++) {
            if (angular.equals(arr[i], item)) {
              return true;
            }
          }
        }
        return false;
      }

      // add
      function add(arr, item) {
        arr = angular.isArray(arr) ? arr : [];
        for (var i = 0; i < arr.length; i++) {
          if (angular.equals(arr[i], item)) {
            return arr;
          }
        }
        arr.push(item);
        return arr;
      }

      // remove
      function remove(arr, item) {
        if (angular.isArray(arr)) {
          for (var i = 0; i < arr.length; i++) {
            if (angular.equals(arr[i], item)) {
              arr.splice(i, 1);
              return;
            }
          }
        }
      }

      return {
        restrict: 'A',
        scope: true,
        link: function(scope, elem, attrs) {
          if (elem[0].tagName !== 'INPUT' || !elem.attr('type', 'checkbox')) {
            throw 'checklist-model should be applied to `input[type="checkbox"]`.';
          }

          if (!attrs.checklistValue) {
            throw 'You should provide `checklist-value`.';
          }

          // link to original model. Initially assigned in $watch
          var model;
          // need setter for case when original model not array
          var setter = $parse(attrs.checklistModel).assign;
          // value added to list
          var value = $parse(attrs.checklistValue)(scope.$parent);

          // local var storing individual checkbox model
          // scope.checked - will be set in $watch

          // exclude recursion
          elem.removeAttr('checklist-model');
          // compile with `ng-model` pointing to `checked`
          elem.attr('ng-model', 'checked');
          $compile(elem)(scope);

          // watch UI checked change
          scope.$watch('checked', function(newValue, oldValue) {
            if (newValue === oldValue) {
              return;
            } if (newValue === true) {
              // see https://github.com/vitalets/checklist-model/issues/11
              if (!angular.isArray(model)) {
                setter(scope.$parent, add([], value));
                // `model` will be updated in $watch
              } else {
                add(model, value);
              }
            } else if (newValue === false) {
              remove(model, value);
            }
          });

          // watch model change
          scope.$parent.$watch(attrs.checklistModel, function(newArr, oldArr) {
            // keep link with original model
            model = newArr;
            scope.checked = contains(newArr, value);
          }, true);
        }
      };
}]).
    controller('dropsDown', function($scope, dataService){
        $scope.product.old_title = $scope.product.title;
        $scope.source = false;
        $scope.periodicity = false;
        $scope.product = dataService.data.product[0];
        $scope.clearTime = function (){
            $scope.product.date_release_start = "";
            $scope.product.date_release_end = "";
            $scope.product.date_upload_start = "";
            $scope.product.date_upload_end = "";
            $scope.product.time_upload_start = "";
            $scope.product.time_upload_end = "";
        };
        $scope.initPeriodicity = function(){
            if($scope.product.type=='date_release'){
                $scope.product.date_release_start = $scope.product.date_start;
                $scope.product.date_release_end = $scope.product.date_end;
            }
            if($scope.product.type=='date_upload'){
                $scope.product.date_upload_start = $scope.product.date_start;
                $scope.product.date_upload_end = $scope.product.date_end;
                $scope.product.time_upload_start = "00:00:00";
                $scope.product.time_upload_end = "00:00:00";
            }
        }
        $scope.openSource = function (){
            $scope.source = !$scope.source;
            $scope.periodicity = false;
        };
        $scope.openPeriodicity = function (){
            $scope.periodicity = !$scope.periodicity;
            $scope.source = false;
        };
        $scope.setProductType = function (productType){
            $scope.periodicity = !$scope.periodicity;
            $scope.source = false;
            if (productType == 'period'){
                productType = 'date_release';
            }
            $scope.product.type = productType;
        };
    }).
    controller('filterUpdateNew', function($scope){
         $scope.toggleDatePicker = function($event, variable) {
            $event.preventDefault();
            $event.stopPropagation();
            this.openedStart = false;
            this.openedEnd = false;
            this.openedStartRelease = false;
            this.openedEndRelease = false;
            this[variable] = true;
        }
    });