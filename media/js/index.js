var exlibris = angular.module('exlibris', ['ui.bootstrap', 'ngSanitize', 'ngCookies', 'rzModule', 'ngRoute', 'ngAnimate',  'ngClipboard', 'perfect_scrollbar']);
angular.module('ng').filter('tel', function (){});
exlibris.config(function($routeProvider, $locationProvider, $interpolateProvider) {
	$interpolateProvider.startSymbol('{$');
	$interpolateProvider.endSymbol('$}');
	//$routeProvider.when('/:user/monitoring_product/:id/', {
	//	templateUrl: "/template/news.html/",
	//	controller: "NewsListCtrl",
     //   resolve:{
     //       load: function ($rootScope, $route, dataService) {
     //       	$rootScope.productId = $route.current.params.id;
     //           $rootScope.$emit('loadStart');
     //           return dataService.load($route.current.params.id).then(function(res){
     //               dataService.data = res;
     //               dataService.lastId = res.last_id;
     //               dataService.count_end = res.count_end;
     //           });
     //       },
     //       filter: function($rootScope, $route, dataFilter){
     //           $rootScope.productId = $route.current.params.id;
     //           $rootScope.productSlug = $route.current.params.user;
     //           $rootScope.$emit('loadStart');
     //           return dataFilter.filter($rootScope.productId, $rootScope.productSlug).then(function(res){
     //               //_.each(res.filters.referred_object__id__in.list, function(el){
     //               //    el.mention_object = _.findWhere(res.mention_objects_list, {id: el.id});
     //               //})
     //               dataFilter.data = res;
     //           });
     //       }
     //   }
	//})
	$locationProvider.html5Mode(true);
}).
  //  config(['ngClipProvider', function(ngClipProvider) {
  //  ngClipProvider.setPath("//cdnjs.cloudflare.com/ajax/libs/zeroclipboard/2.1.6/ZeroClipboard.swf");
  //}]).
    factory('product_settings', function(){
        return {
            settings: [],
            id: product_settings.id,
            load: function(id, slug){
                var defer = $q.defer();
                $http.get('/monitoring_product/get_product_settings/' + id + '/').success(function(res){
                    defer.resolve(res);
                });
                return defer.promise;
            }
        };
    }).

    factory('dataFilter', function($q, $http){
        var init = {
                category_list: [],
                filters: [],
                mention_objects_list: []
            };
        return{
            init: init,
            data: init,
            filter: function(id, slug){
                var defer = $q.defer();
                $http.get('/get-monitoring-product-filters/' + id + '/').success(function(res){
                    defer.resolve(res);
                });
                return defer.promise;
            }
        }
    }).
    factory('dataService', function ($q, $http, $cookies, $rootScope, $timeout) {

    var init = {
            articles_count: null,
            count_end: null,
            last_id: null,
            product:[{
                date_end: null,
                date_start: null,
                referred_objects: [],
                time_end: null,
                time_start: null,
                title: null,
                type: null
            }],
            template_settings: null
        };

    return {
        init: init,
        data: init,
        load: function(id) {
            var defer = $q.defer();
           	$http.get('http://'+ window.location.host +'/monitoring-product/' + id + '/').success(function(res){
                defer.resolve(res);
            });
            return defer.promise;
        },
	    titleOffset: [],
	    activeTitle: [],
        activeTitleChange: [],
	    headerObj: null,
	    header: 0,
        lastId: null,
        count_end: null,
        stopLoadArticles: false,
        waitLoadAjax: true,
        scrollToElement: function(id) {
            var currentTitle = angular.element('#' + id);
            if (currentTitle.length) {
                var currentTitleHeight = currentTitle.height(),
                    currentTitleTop = currentTitleHeight == 0 ? 0 : currentTitle.offset().top - angular.element('.wrap-header').height() + currentTitleHeight + 2;
                angular.element("body, html").animate({scrollTop: currentTitleTop});
            }
        },
        findFullCategory: function(children, id) {
        	if (children.length) {
	            for (var i=0; i<children.length; i++) {
	                if (children[i].articles && children[i].articles.length) {
	                    this.scrollToElement(children[i].id);
	                    return false;
	                }
	                if (children[i].children && children[i].children.length) {
	                    this.findFullCategory(children[i].children, children[i].id);
	                    return false;
	                }
	                this.getAllArticles(id);
	                return false;
	            }
        	} else {
        		this.scrollToElement(id);
        	}
        },

		findCurrentLelev: function(data, id) {
			for (var i = 0; i < data.length; i++) {
				if (data[i].id == id) {
					this.findFullCategory(data[i].children, data[i].id);
				} else {
					this.findCurrentLelev(data[i].children, id);
				}
			}
		}
    };
}).
    controller('headerCtrl', function($rootScope, $scope, $http, $cookies, $filter, $location, dataService) {
    //$rootScope.visibleMenu = [];
    //$scope.dataService = dataService;
    //$scope.showFiltersList = false;
    //$scope.generalFilters = dataService.data.product[0].referred_objects;
    //$rootScope.dateStart = angular.copy(dataService.data.product[0].date_start.replace(/\-+/g, '.'));
    //$rootScope.dateEnd = angular.copy(dataService.data.product[0].date_end.replace(/\-+/g, '.'));
    //
    //$rootScope.timeStart = dataService.data.product[0].time_start;
    //$rootScope.timeEnd = dataService.data.product[0].time_end;
    //
    //$scope.dateType = dataService.data.product[0].type;
    //$scope.title = dataService.data.product[0].title;
    //if ($scope.dateType == "day") {
    //	$scope.daysCount = 1;
    //} else if ($scope.dateType == "week") {
    //	$scope.daysCount = 7;
    //} else if ($scope.dateType == "month") {
    //	$scope.daysCount = 30;
    //}
    //var menu = dataService.data.monitoring_product_template;
    //
    //$scope.openedStart = false;
    //$scope.openedEnd = false;
    $scope.showHideFiltersList = function() {
        $scope.showFiltersList = !$scope.showFiltersList;
    };
    $scope.filterWidgetNull = function(mentionObjectId, tonalityId){
        var getDate = '?referred_object_id='+mentionObjectId +
            '&date_start='+$scope.dateStart.replace(/\.+/g, '-') + '-' + $rootScope.timeStart +
            '&date_end='+$scope.dateEnd.replace(/\.+/g, '-') + '-' + $rootScope.timeEnd +
            '&type=date_upload';
        if (tonalityId) getDate += '&emotional_tone_object__id='+tonalityId;
        window.location.href = $location.path() + 'widget0/' + getDate;
    }
    $scope.getNewsList = function(datePicker, val) {
        if (datePicker == 'start') {
            $scope.dateEnd = moment(new Date(val.split('.')[2], val.split('.')[1] - 1, val.split('.')[0])).add($scope.daysCount, 'days').format('DD.MM.YYYY');
        } else if (datePicker == 'end') {
            $scope.dateStart = moment(new Date(val.split('.')[2], val.split('.')[1] - 1, val.split('.')[0])).subtract($scope.daysCount, 'days').format('DD.MM.YYYY');
        }
        dateFilter(datePicker);
    }
    $scope.nextPrev = function(type) {
        var operType = type == "prev" ? 'subtract' : 'add';
        $scope.dateStart = moment(new Date($scope.dateStart.split('.')[2], $scope.dateStart.split('.')[1] - 1, $scope.dateStart.split('.')[0]))[operType]($scope.daysCount, 'days').format('DD.MM.YYYY');
        $scope.dateEnd = moment(new Date($scope.dateEnd.split('.')[2], $scope.dateEnd.split('.')[1] - 1, $scope.dateEnd.split('.')[0]))[operType]($scope.daysCount, 'days').format('DD.MM.YYYY');
        dateFilter('end');
    }
    $scope.goToWidgetNull = function() {
        window.location.href = $location.path() + 'widget0'+
            '?date_start='+ $scope.dateStart.replace(/\.+/g, '-') + '-' + $rootScope.timeStart +
            '&date_end='+ $scope.dateEnd.replace(/\.+/g, '-') + '-' + $rootScope.timeEnd +
            '&type=date_upload';
    }
    $scope.redirectToIndex = function(url) {
        window.location.href = '/' + url;
    }
    function dateFilter(changesDate) {
        var ajaxData = {
            date_type: $scope.dateType
        }
        if (changesDate == 'start') {
            angular.extend(ajaxData, {date_start: $scope.dateStart.replace(/\.+/g, '-')});
        } else {
            angular.extend(ajaxData, {date_end: $scope.dateEnd.replace(/\.+/g, '-')});
        }
        $rootScope.loadingView = true;
        $http.defaults.headers.post['X-CSRFToken'] = $cookies['csrftoken'];
        $http.post('http://'+ window.location.host +'/monitoring-product/'+ $rootScope.productId +'/', ajaxData).success(function(res) {
            $scope.dateStart = res.product[0].date_start.replace(/\-+/g, '.');
            $scope.dateEnd = res.product[0].date_end.replace(/\-+/g, '.');
            $rootScope.dateStart = $scope.dateStart;
            $rootScope.dateEnd = $scope.dateEnd;
        	$rootScope.hideBreadTitleFlag = true;
            dataService.activeTitle.length = 0;
            $rootScope.loadingView = false;
            dataService.stopLoadArticles = false;
            dataService.data = res;
            dataService.titleOffset.length = 0;
            var menu = dataService.data.monitoring_product_template;
            $rootScope.visibleMenu.length = 0;
            for(var i = 0; i < menu.length; i++) {
                $rootScope.visibleMenu.push(menu[i]);
            }
            angular.element("body, html").animate({scrollTop: angular.element('html').offset().top});
            // $rootScope.$emit('loadStart');
        });
    }
}).
    controller('checkboxCtrl', function($scope, $rootScope, $http, $cookies, dataService, dataFilter, profileView, modalConfirm, $location){
        $scope.openProfile = function (){
            profileView($scope);
            $scope.settings = false;
        };
        if ('profile' in $location.search()){
            $scope.openProfile();
        }

        $rootScope.showmodal = true;
        $rootScope.profile = true;
        $scope.sedModal = false;
        $scope.product = {};
        $scope.shareMenu = false;
        $scope.searchText = '';
        $scope.profile = false;
        $scope.setings = false;
        $scope.openSetings = function (){
            $scope.settings = !$scope.settings;
        };
        $scope.closeSetting = function(){
            $scope.settings = false;
        };
        $scope.$on("update_parent_controller", function(event, message){
            $scope.profile = message;
        });
        $scope.$watch("searchText", function(newValue, oldValue) {
            return $scope.searchText;
        });
        $scope.sendArticle = function(){
            send_obj={
                file_name:$scope.file_name,
                from:$scope.from,
                to:$scope.to,
                message:$scope.message,
                topic:$scope.topic
            };
            $http.defaults.headers.post['X-CSRFToken'] = $cookies['csrftoken'];
            $http.post("/send/articles/", {send_obj:send_obj}).success(function(res){
                $scope.sedModal = false;
            });
        };
        $scope.generateDoc = function(topic){
            console.log(dataService,'-=-=-=-=-=-');

            $scope.topic =dataService.data.product[0].title;
            if(topic == 'translation'){
                $scope.topic ='На перевод';
            }

            var inputs = document.getElementsByName("checkbox");
            var checked = 0;
            var articles_id_list=[];
            for (var i = 0; i < inputs.length; i++) {
                if (inputs[i].checked){
                    var id = inputs[i].id;
                    articles_id_list.push(id.replace('select-', ''));
                }
            }

            $scope.shareMenu = false;
            console.log(articles_id_list);
            $http.defaults.headers.post['X-CSRFToken'] = $cookies['csrftoken'];
            $http.post("/generate-doc/", {articles_id_list:articles_id_list}).success(function(res){
                $scope.sedModal = true;
                $scope.file_name = res['file_name'];
                $scope.message ='';

                $scope.from= res['from'];
                $scope.to='';
            });
        };
        $scope.printArticles = function(){
            var inputs = document.getElementsByName("checkbox");
            var checked = 0;
            var articles_id_list=[];
            for (var i = 0; i < inputs.length; i++) {
                if (inputs[i].checked){
                    var id = inputs[i].id;
                    articles_id_list.push(id.replace('select-', ''));
                }
            }
            $http.defaults.headers.post['X-CSRFToken'] = $cookies['csrftoken'];
            $http.post("/print/articles/", {articles_id_list:articles_id_list}).success(function(res) {
                $scope.shareMenu = false;
                window.location.href = '/'+res['file_path']
            });

        };
        $scope.copyLink = function(scope){
            var inputs = document.getElementsByName("checkbox");
            var checked = 0;
            var articles_link_list = [];
            for (var i = 0; i < inputs.length; i++) {
                if (inputs[i].checked){
                    var title = inputs[i].title;
                    articles_link_list.push(title);
                }
            }
            var result = '';
            for (var i = 0; i < articles_link_list.length; i++ ){
                console.log(articles_link_list.length);
                if(articles_link_list.length ==1){
                    result = result + articles_link_list[i];
                }
                else{
                    if(articles_link_list.length == i){
                        result = result + articles_link_list[i]
                    }
                    else{
                        result = result + articles_link_list[i] + ', ';
                    }
                }
            }
            return result;
        };
        $scope.openMenu = function (){
            var inputs = document.getElementsByName("checkbox");
            var checked = 0;
            for (var i = 0; i < inputs.length; i++) {
                'checkbox' == inputs[i].type && inputs[i].checked ? checked++ : false;
            }
            checked >= 1 ? $scope.shareMenu = !$scope.shareMenu : $scope.shareMenu = false
        };
        $scope.is_right = function(ind) {
            return $scope.sortableNewFilters.length < 2*ind + 4;
        };
        $scope.alineFilters = function() {
            $('.filter-left').wrapAll('<div class="filter-block-left"></div>');
            $('.filter-right').wrapAll('<div class="filter-block-right"></div>');
            var heights = $('body').height();
            console.log(heights, 'heights');
            var block = $('.scroll-filter').css("height", heights - 270);
            console.log(block, 'block');
        };

        $scope.isRangeFilter = function(key) {return ~key.indexOf('range');}
        $scope.saveProduct = function(){
            //var filters = dataFilter.data.filters.slice();
            var product = dataService.data.product[0];
            if (product.type == "date_release"){
                product.date_from = product.date_release_start;
                product.date_to = product.date_release_end;
            }
            if (product.type == "date_upload"){
                var time_start = product.time_upload_start.split(":"), time_end = product.time_upload_end.split(":");

                product.date_from = product.date_upload_start;
                product.date_to = product.date_upload_end;

                product.date_from.setHours(parseInt(time_start[0])||0);
                product.date_from.setMinutes(parseInt(time_start[1])||0);
                product.date_from.setSeconds(parseInt(time_start[2])||0);

                product.date_to.setHours(parseInt(time_end[0])||0);
                product.date_to.setMinutes(parseInt(time_end[1])||0);
                product.date_to.setSeconds(parseInt(time_end[2])||0);
            }
            if(product.date_to && typeof(product.date_to)=='string'){
                var tmp = product.date_to.split(/[-\.]/);
                tmp[0]^=tmp[1];
                tmp[1]^=tmp[0];
                tmp[0]^=tmp[1];
                product.date_to = new Date(tmp.join('-'));
            }
            if(product.date_from && typeof(product.date_from)=='string'){
                var tmp = product.date_from.split(/[-\.]/);
                tmp[0]^=tmp[1];
                tmp[1]^=tmp[0];
                tmp[0]^=tmp[1];
                product.date_from = new Date(tmp.join('-'));
            }
            var data = angular.toJson({product: $scope.product,
                category_list: dataFilter.data.category_list,
                mention_objects_list: dataFilter.data.mention_objects_list,
                filters: dataFilter.data.filters,
                product: product
            });
            var confirm = function(){
                $http({
                    method: 'POST',
                    url: "/create-or-update-product/" +$rootScope.productId+ "/",
                    data: data,
                    headers: {"X-CSRFToken": $cookies.csrftoken,
                        "Content-Type": undefined}
                }).success(function(data){
                    window.location.reload();
                });
            };
            if (product.title==product.old_title){
                var question = "";
                var no_more_param_name = "";

                question = "Вы уверены, что хотите сохранить измененный продукт, сохранив его последнюю версию? Для подтверждения действия нажмите \"Да\"." +
                "Для сохранения нового продукта без удаления его исходной версии кликните на кнопку \"НАЗАД\", затем измените название продукта и нажмите" +
                "на \"Сохранить\"";

                no_more_param_name = "dont_ask_if_change_without_rename_product_availible_only_for_me";

                var modal_data = {title: "Точно хотите сохранить?", question: question};
                modalConfirm($scope, confirm, no_more_param_name, modal_data);
            }
            else{
                confirm();
            }
        };

        $scope.getMentionObjects = function(key) {return key=='referred_object__id__in'?$scope.mentionObjects:null;}
        // km_result
        // проверка на исключения фильтра release_time из общего списка
        $scope.isCustomFilter = function(key) {return ~key.indexOf('release_time');}
        $scope.haveReleaseTime = function() {
            if (typeof($scope.newFilters)=="undefined") return false;
            return !angular.isUndefined($scope.newFilters.release_time);
        }

        if ($scope.haveReleaseTime()) {
            $scope.newFilters.km_result.list = [{
                id: 1, title: 'да'
            }, {
                id: 2, title: 'нет'
            }];
        }
        $scope.removeFilter = function(filter) {
            if (filter.filter_list) {
                for (var i = 0; i < filter.filter_list.length; i++) {
                    filter.filter_list[i].selected = false;
                    if (!angular.isUndefined(filter.isNull.emptyValue)) filter.isNull.emptyValue = false;
                    filter.isNull.selectAll = false;
                }
            } else {
                filter.id.slider.min = filter.id.slider.floor;
                filter.id.slider.max = filter.id.slider.ceil;
            }
            $scope.showFilteredArticles(true);
        };

    }).
    $rootScope.openModal = function(templateUrl) {
    var modalInstance;
    templateUrl = templateUrl || [];
    modalInstance = $modal.open({
      templateUrl: templateUrl,
      windowClass: templateUrl,
      controller: function($scope, $modalInstance) {
        $scope.close = function() {
          $modalInstance.dismiss('cancel');
        };
        $scope.goStep = function(templateUrl) {
          $modalInstance.dismiss('cancel');
          $scope.$root.openModal(templateUrl);
        };
         $scope.ok = function () {
            $modalInstance.close();
          };
      }
    });
  };
    controller('dropsDown', function($scope, dataService){
        $scope.product.old_title = $scope.product.title;
        $scope.source = false;
        $scope.periodicity = false;
        $scope.product = dataService.data.product[0];
        $scope.clearTime = function (){
            $scope.product.date_release_start = "";
            $scope.product.date_release_end = "";
            $scope.product.date_upload_start = "";
            $scope.product.date_upload_end = "";
            $scope.product.time_upload_start = "";
            $scope.product.time_upload_end = "";
        };
        $scope.initPeriodicity = function(){
            if($scope.product.type=='date_release'){
                $scope.product.date_release_start = $scope.product.date_start;
                $scope.product.date_release_end = $scope.product.date_end;
            }
            if($scope.product.type=='date_upload'){
                $scope.product.date_upload_start = $scope.product.date_start;
                $scope.product.date_upload_end = $scope.product.date_end;
                $scope.product.time_upload_start = "00:00:00";
                $scope.product.time_upload_end = "00:00:00";
            }
        }
        $scope.openSource = function (){
            $scope.source = !$scope.source;
            $scope.periodicity = false;
        };
        $scope.openPeriodicity = function (){
            $scope.periodicity = !$scope.periodicity;
            $scope.source = false;
        };
        $scope.setProductType = function (productType){
            $scope.periodicity = !$scope.periodicity;
            $scope.source = false;
            if (productType == 'period'){
                productType = 'date_release';
            }
            $scope.product.type = productType;
        };
    }).
    controller('filterUpdateNew', function($scope){
        $scope.toggleDatePicker = function($event, variable) {
            $event.preventDefault();
            $event.stopPropagation();
            this.openedStart = false;
            this.openedEnd = false;
            this.openedStartRelease = false;
            this.openedEndRelease = false;
            this[variable] = true;
        }
    }).
    controller('openSettingsCtrl', function($scope, $rootScope, dataService, dataFilter){
        var settings_flags = {
            filters: false,
            services: false
        };
        var enable_settings = function (){
            if (_.reduce(settings_flags, function(x, y){return x&&y})){
                $scope.$parent.$parent.settings = true;
                $rootScope.loadingView = false;
                settings_flags.filters = false;
                settings_flags.services = false;
            }
        };

        $scope.openSetings = function (id){
            $rootScope.loadingView = true;
            if (!id && typeof(id) == "undefined"){
                return;
            }
            ////////////////////////////////////
            // нужен рефакторинг, без говнокода ничего не работает

            $rootScope.productId = id;

            $rootScope.visibleMenu = [];
            $scope.showFiltersList = false;

            $scope.$parent.$parent.openedStart = false;
            $scope.$parent.$parent.openedEnd = false;

            dataService.data = dataService.init;
            dataFilter.data = dataFilter.init;
            angular.element($('.change_product.icon-moon')[0]).scope().currentSettingTab = 'amendment.html';

            ////////////////////////////////////
            dataService.load(id).then(function(res){
                dataService.data = res;
                dataService.lastId = res.last_id;
                dataService.count_end = res.count_end;
                $scope.$parent.$parent.product = dataService.data.product[0];
                $scope.$parent.$parent.dataService = dataService;

                $scope.$parent.$parent.generalFilters = dataService.data.product[0].referred_objects;
                $rootScope.dateStart = angular.copy(dataService.data.product[0].date_start.replace(/\-+/g, '.'));
                $rootScope.dateEnd = angular.copy(dataService.data.product[0].date_end.replace(/\-+/g, '.'));

                $rootScope.timeStart = dataService.data.product[0].time_start;
                $rootScope.timeEnd = dataService.data.product[0].time_end;

                $scope.$parent.$parent.dateType = dataService.data.product[0].type;
                $scope.$parent.$parent.title = dataService.data.product[0].title;
                if ($scope.$parent.$parent.dateType == "day") {
                    $scope.$parent.$parent.daysCount = 1;
                } else if ($scope.$parent.$parent.dateType == "week") {
                    $scope.$parent.$parent.daysCount = 7;
                } else if ($scope.$parent.$parent.dateType == "month") {
                    $scope.$parent.$parent.daysCount = 30;
                } else if ($scope.$parent.$parent.dateType == "date_release" || $scope.$parent.$parent.dateType == "date_upload") {
                    $scope.$parent.$parent.product.date_release_start = $rootScope.dateStart;
                    $scope.$parent.$parent.product.date_release_end = $rootScope.dateEnd;
                    $scope.$parent.$parent.product.date_upload_start = $rootScope.dateStart;
                    $scope.$parent.$parent.product.date_upload_end = $rootScope.dateEnd;
                    $scope.$parent.$parent.product.time_upload_start = "00:00:00";
                    $scope.$parent.$parent.product.time_upload_end = "00:00:00";
                }
                var menu = dataService.data.monitoring_product_template;
                settings_flags.filters = true;
                enable_settings();
                angular.element($('[ng-controller=dropsDown]')[0]).scope().loadSources();
                $scope.$parent.$parent.product.old_title = $scope.$parent.$parent.product.title;
            });
            dataFilter.filter($rootScope.productId, $rootScope.productSlug).then(function(res){
                //_.each(res.filters.referred_object__id__in.list, function(el){
                //    el.mention_object = _.findWhere(res.mention_objects_list, {id: el.id});
                //})
                dataFilter.data = res;

                // other filters
                $scope.$parent.$parent.newFilters = dataFilter.data.filters;
                // sort by order
                $scope.$parent.$parent.sortableNewFilters = [];
                for (var key in $scope.$parent.$parent.newFilters) {
                    if (!$scope.$parent.$parent.newFilters[key].settings.a3) $scope.$parent.$parent.newFilters[key].settings.a3 = Infinity;
                    $scope.$parent.$parent.sortableNewFilters.push([key, $scope.$parent.$parent.newFilters[key]]);
                }
                $scope.$parent.$parent.sortableNewFilters.sort(function(a, b) {
                    return a[1].settings.a3 - b[1].settings.a3
                });
                // скрыть ФО
                $scope.$parent.$parent.hideFederalDistrict = false;
                settings_flags.services = true;
                enable_settings();
            });
        }
    });