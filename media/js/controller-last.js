var exlibris = angular
    .module('exlibris', ['ui.bootstrap', 'ngSanitize', 'ngCookies'])
    .config(function($interpolateProvider) {
    	$interpolateProvider.startSymbol('{$');
    	$interpolateProvider.endSymbol('$}');
}).service('dataService', function(){
    this.data = {
        "status": "OK",
        "monitoring_product_template": [],
        "product": [
            {}
        ]
    }
    this.titleOffset = [];
    this.activeTitle = '';
    this.headerObj = null;
    this.header = 0;
}).factory('globalFlags', function () {
    var toneList = {
        'позитивное': 'green',
        'негативное': 'red',
        'нейтральное': 'grey'
    };
    var roleList = {
        'ведущая': 'full',
        'значимая': 'half',
        'контекстная': 'third'
    }
    return {
        flagType: function(tone, role) {
            return toneList[tone] + ' ' + roleList[role];
        }
    }
}).filter("paintTheWordsFilter", function() {
    return function(input, searchRegex, replaceRegex) {
        return input.replace(RegExp(searchRegex), replaceRegex);
    };
}).directive('categoryList', function($compile) {
    return {
        restrict: 'E',
        scope: {
            category: '=',
            title: '='
        },
        templateUrl: 'categoryListTemplate',
        
        controller: function($scope, $document) {
            $scope.createTitle = function(title, catTitle) {
                if (!title) return catTitle;
                return title + '/' + catTitle;
            }
        }, 
        compile: function(tElement, tAttr, transclude) {
            //We are removing the contents/innerHTML from the element we are going to be applying the 
            //directive to and saving it to adding it below to the $compile call as the template
            var contents = tElement.contents().remove();
            var compiledContents;
            return function(scope, iElement, iAttr) {
                if(!compiledContents) {
                    //Get the link function with the contents frome top level template with 
                    //the transclude
                    compiledContents = $compile(contents, transclude);
                }
                //Call the link function to link the given scope and
                //a Clone Attach Function, http://docs.angularjs.org/api/ng.$compile :
                // "Calling the linking function returns the element of the template. 
                //    It is either the original element passed in, 
                //    or the clone of the element if the cloneAttachFn is provided."
                compiledContents(scope, function(clone, scope) {
                        //Appending the cloned template to the instance element, "iElement", 
                        //on which the directive is to used.
                         iElement.append(clone); 
                });
            }}  
    }
}).directive('singleArticle', function() {
    return {
        restrict: 'E',
        scope: {
            item: '='
        },
        templateUrl: 'singleArticleTemplate',
        controller: function($scope, $filter, $sce, globalFlags, dataService) {
            $scope.currentIndex = 0;
            $scope.activeWord = 0;
            $scope.flagType = globalFlags.flagType;
            $scope.status = {
                isCollapsed: true
            }
            $scope.activeWordChange = function(index) {
                $scope.activeWord = index;
                $scope.currentIndex = index;
            }
            $scope.previewWordChange = function(index) {
                $scope.currentIndex = $scope.activeWord;
                $scope.activeWord = index;
            }
            $scope.returnLastActive = function() {
                $scope.activeWord = $scope.currentIndex;
            }
            $scope.paintTheWords = function(html, mo) {
                text = html;
                for(var i = 0; i < mo.length; i++) {
                    for(var j = 0; j < mo[i].keywords.length; j++) {
                        text = $filter('paintTheWordsFilter')(text.toLocaleLowerCase(), '('+ mo[i].keywords[j].toLocaleLowerCase() +')', '<span class="colored-text" style="background-color:#'+ mo[i].color +'">' + mo[i].keywords[j].toLocaleLowerCase() + '</span>');
                    }
                }
                return $sce.trustAsHtml(text);
            }
            $scope.renderSpeakers = function(speakers) {
                var text = '(';
                for (var i = 0; i < speakers.length; i++)
                    text += speakers[i].fio + (i != speakers.length - 1 ? ', ': '');
                text += ')';
                return text;
            }
        }
    }
}).directive('renderRightData', function() {
    return {
        scope: {
            value: '=',
            prt: '='
        },
        template: '<a href ng-class="{active: actived}">&nbsp;</a>' +
                  '<span ng-class="{positive: prtPositive}">{$ render(value, prt) $}</span>',
        link: function(scope) {
            scope.actived = true;
            scope.prtPositive = false;
            console.log(prt, '----');
            scope.render = function(value, prt) {
                var newValue = parseFloat(value) || '';
                if (newValue) {
                    if (prt) {
                        if (newValue >= 0.75) scope.prtPositive = true;
                        newValue = newValue.toFixed(2);
                    } else {
                        if (newValue <= 1) {
                            newValue = newValue.toFixed(2);
                        } else if (newValue > 1 && newValue <= 1000) {
                            newValue = Math.round(newValue);
                        } else if (newValue > 1000 && newValue <= 1000000) {
                            newValue = (newValue / 1000).toFixed(2) + ' K';
                        } else if (newValue > 1000000 && newValue <= 1000000000) {
                            newValue = (newValue / 1000000).toFixed(2) + ' M';
                        }
                    }
                } else {
                    scope.actived = false;
                }
                return newValue;
            }
        }
    }
}).directive('headerHeight', function(dataService) {
    return {
        link: function(scope, element, attrs) {
            dataService.headerObj = element;
        }
    }
}).directive('titleInfo', function(dataService){
    return {
        link: function(scope, elem, attrs){
            dataService.titleOffset.push(elem);
        }
    }
}).directive("scroll", function ($window, $filter, dataService) {
    return function(scope, element, attrs) {
        angular.element($window).bind("scroll", function() {
            var res = [],
                headerHeight = 149;
            angular.forEach(dataService.titleOffset, function(elem){
                res.push({'top': elem.offset().top, 'elem': elem})
            })
            res = $filter('orderBy')(res, 'top');
            for (var i = 0; i < res.length; i++){
                if (i < res.length - 1) {
                    if (res[i].top < pageYOffset + headerHeight - 30 && pageYOffset + headerHeight - 30 < res[i + 1].top) {
                        dataService.activeTitle = res[i].elem.html();
                        break;
                    }
                }
                if (i == res.length - 1 && res[i].top < pageYOffset + headerHeight - 30) {
                    dataService.activeTitle = res[res.length - 1].elem.html();
                    break;
                }
            }
            scope.$apply();
        });
        angular.element($window).bind("load", function() {
            res = [];
            angular.forEach(dataService.titleOffset, function(elem) {
                res.push({'top': elem.offset().top, 'elem': elem})
            });
            res = $filter('orderBy')(res, 'top');
            if (res[0]) {
                dataService.activeTitle = res[0].elem.html();
                res[0].elem.css({
                    height: 1,
                    overflow: 'hidden'
                });
            }
            if(!scope.$$phase) {scope.$apply();}
            dataService.header = dataService.headerObj.height() + 20;
            if(!scope.$$phase) {scope.$apply();}
        });
    };
}).controller('headerCtrl', function($rootScope, $scope, $http, $filter, dataService) {
    $scope.visibleMenu = [];
    $scope.dataService = dataService;
    $scope.$root.$watch('data', function (q, w){
        if(q!=w){
            if(dataService.titleOffset.length){
                console.table(dataService.titleOffset);
                console.log(dataService.titleOffset[0].html());
                res = [];
                // $scope.$$phase = null;
                if(!$scope.$$phase) {$scope.$apply();}
                console.log('$scope.$$phase', $scope.$$phase)
                angular.forEach(dataService.titleOffset, function(elem) {
                    res.push({'top': elem.offset().top, 'elem': elem})
                });
                res = $filter('orderBy')(res, 'top');
                if (res[0]) {
                    dataService.activeTitle = res[0].elem.html();
                }
            }
            var menu = q.monitoring_product_template;
            $scope.visibleMenu = [];
            for(var i = 0; i < menu.length; i++) {
                $scope.visibleMenu.push(menu[i]);
            }        

            $scope.date_start = q.product[0].date_start;
            $scope.date_end = q.product[0].date_end;
            $scope.date_type = q.product[0].type;
            $scope.title = q.product[0].title;
        }
    });
    $scope.getNewsList = function() {
        $http.post('http://78.47.195.126:200/monitoring-product/'+ angular.element('#product-id').val() +'/', {
            date_start: $scope.date_start,
            date_end: $scope.date_end,
            date_type: $scope.date_type
        }).then(function(result) {
            
            $rootScope.data = result.data;
            dataService.data = result.data;
        });
    }
    $scope.rerenderMenu = function(children, id) {
        console.log('rerenderMenu');
        if (children.length) {
            $scope.visibleMenu = !id ? [] : [{
                title: "",
                children: $scope.visibleMenu,
                id: null,
                back: true
            }];
            $scope.visibleMenu = $scope.visibleMenu.concat(children);
        } else {
            // scroll
        }
    }
}).controller('NewsListCtrl', function($scope, dataService) {
    $scope.$root.$watch('data', function (q, w){
        dataService.titleOffset = [];
        $scope.category = q.monitoring_product_template;

    })
    $scope.dataService = dataService;
}).run(function($rootScope, $http, $cookies, dataService) {
    $http.defaults.headers.post['X-CSRFToken'] = $cookies['csrftoken'];
    $rootScope.data = {
        "status": "OK",
        "monitoring_product_template": [],
        "product": [
            {}
        ]
    }
    $http({method:'GET', url:'http://78.47.195.126:200/monitoring-product/'+ angular.element('#product-id').val() +'/'}).then(function(result){
        $rootScope.data = result.data;
        dataService.data = result.data;
        $rootScope.dataService = dataService;
        root = $rootScope;
    });
});