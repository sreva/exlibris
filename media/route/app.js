/**
 * Created by vako on 30.05.14.
 */
var app = angular.module('app', ['ngRoute']);

app.config(function($routeProvider){
    $routeProvider
        .when('/', {
            templateUrl:'extends.html',
            controller: 'Ctrl1',
            resolve:{
                load: function ($route, dataService) {
                    console.log('resolve_load')
//                    dataService.load().then(function(res){
//                        dataService.data = res;
//                    });
                    return dataService.load().then(function(res){
                        dataService.data = res;
                    });
                }
            }
        })
})
    .controller('Ctrl1', function($scope, dataService){
        console.log('hello');
        console.log(dataService.data);
        $scope.data = dataService.data;

    })
    .factory('dataService', function ($q, $http, $timeout) {
//        var data_i = data_i || {};
        return {
            data: {},
            load: function() {
                var defer = $q.defer();
               $http.get('http://78.47.195.126:200/monitoring-product/58/').success(function(res){
                    defer.resolve(res);

//                    return data_i;
                });
//                $timeout(function () {
//                    data.id = 1;
//                    defer.resolve(data);
//                }, 1000);
                return defer.promise;
            }
        };
    })
    .run(['$rootScope', function($root) {
        $root.$on('$routeChangeStart', function(e, curr, prev) {
            if (curr.$$route && curr.$$route.resolve) {
                // Show a loading message until promises are not resolved
                console.log(curr.$$route.resolve);
                $root.loadingView = true;
            }
        });
        $root.$on('$routeChangeSuccess', function(e, curr, prev) {
            // Hide loading message
            $root.loadingView = false;
        });
    }]);