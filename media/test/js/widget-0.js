var exlibris = angular.module('exlibris', ['ui.bootstrap', 'ngSanitize', 'rzModule', 'ngCookies', 'ngRoute', 'ngAnimate', 'perfect_scrollbar']);
exlibris.config(function($routeProvider, $locationProvider, $interpolateProvider) {
	$interpolateProvider.startSymbol('{$');
	$interpolateProvider.endSymbol('$}');
	$routeProvider.when('/:product/monitoring_product/:id/:title/', {
		templateUrl: "/template/news2.html/",
		controller: "WidgetNullController",
        resolve:{
            load: function ($rootScope, $route, dataService) {
            	$rootScope.productId = $route.current.params.id;
                $rootScope.productSlug = $route.current.params.product;
                $rootScope.$emit('loadStart');
                return dataService.load($rootScope.productId, $rootScope.productSlug).then(function(res){
                    dataService.data = res;
                });
            }
        }
	})
	$locationProvider.html5Mode(true);
}).factory('dataService', function ($q, $http) {
    return {
        data: {},
        load: function(id, slug) {
            var defer = $q.defer();
           	$http.get('http://'+ window.location.host +'/' + slug + '/monitoring_product/' + id + '/widget0-data/').success(function(res){
                defer.resolve(res);
            });
            return defer.promise;
        },
	    titleOffset: [],
	    activeTitle: '',
	    headerObj: null,
	    header: 39
    };
}).factory('globalFlags', function () {
    var toneList = {
        'позитивное': 'green',
        'негативное': 'red',
        'нейтральное': 'grey'
    };
    var roleList = {
        'ведущая': 'full',
        'значимая': 'half',
        'контекстная': 'third'
    }
    return {
        flagType: function(tone, role) {
            return toneList[tone] + ' ' + roleList[role];
        }
    }
}).filter("paintTheWordsFilter", function() {
    return function(input, searchRegex, replaceRegex){
        return input.replace(RegExp(searchRegex), replaceRegex);
    };
}).filter('searchTextEntry', function() {
  return function(items, query) {
    var filtered = [];
    if (!query) query = '';
    angular.forEach(items, function(el) {
      if (el.title.toLowerCase().indexOf(query.toLowerCase()) === 0) filtered.push(el);
    });
    return filtered;
  }
}).directive('numbersOnly', function(){
   return {
     require: 'ngModel',
     link: function(scope, element, attrs, modelCtrl) {
       modelCtrl.$parsers.push(function (inputValue) {
           if (inputValue == undefined) return ''
           var transformedInput = inputValue.replace(/[^0-9.]/g, '');
           if (transformedInput!=inputValue) {
              modelCtrl.$setViewValue(transformedInput);
              modelCtrl.$render();
           } else {
                if (+inputValue < +attrs.min) {
                      modelCtrl.$setViewValue(attrs.min);
                      modelCtrl.$render();
                      return attrs.min;
                } else if (+inputValue > +attrs.max) {
                      modelCtrl.$setViewValue(attrs.max);
                      modelCtrl.$render();
                      return attrs.max;
                }
           }
           return transformedInput;
       });
     }
   };
}).directive('categoryList', function($compile) {
    return {
        restrict: 'E',
        scope: {
            category: '=',
            name: '='
        },
        templateUrl: 'categoryListTemplate',

        controller: function($scope, $document) {
            $scope.createTitle = function(title, catTitle) {
                if (!title) return catTitle;
                return title + '/' + catTitle;
            }
        },
        compile: function(tElement, tAttr, transclude) {
            var contents = tElement.contents().remove();
            var compiledContents;
            return function(scope, iElement, iAttr) {
                if(!compiledContents) {
                    compiledContents = $compile(contents, transclude);
                }
                compiledContents(scope, function(clone, scope) {
                         iElement.append(clone);
                });
            }}
    }
}).directive('singleArticle', function() {
    return {
        restrict: 'E',
        scope: {
            item: '='
        },
        templateUrl: 'singleArticleTemplate',
        controller: function($scope, $filter, $sce, globalFlags) {
            $scope.currentIndex = 0;
            $scope.activeWord = 0;
            $scope.flagType = globalFlags.flagType;
            $scope.status = {
                isCollapsed: true
            }
            $scope.activeWordChange = function(index) {
                $scope.activeWord = index;
                $scope.currentIndex = index;
            }
            $scope.previewWordChange = function(index) {
                $scope.currentIndex = $scope.activeWord;
                $scope.activeWord = index;
            }
            $scope.returnLastActive = function() {
                $scope.activeWord = $scope.currentIndex;
            }
            $scope.paintTheWords = function(html, mo) {
                text = html;
                for(var i = 0; i < mo.length; i++) {
                    for(var j = 0; j < mo[i].keywords.length; j++) {
                        text = $filter('paintTheWordsFilter')(text, '('+ mo[i].keywords[j] +')', '<span class="colored-text" style="background-color:#'+ mo[i].color +'">' + mo[i].keywords[j] + '</span>');
                    }
                }
                return $sce.trustAsHtml(text);
            }
            $scope.renderSpeakers = function(speakers) {
                var text = '(';
                for (var i = 0; i < speakers.length; i++)
                    text += speakers[i].fio + (i != speakers.length - 1 ? ', ': '');
                text += ')';
                return text;
            }
            $scope.sourcePopup = true;
            $scope.copyRightPopup = true;
            $scope.urlPopup = true;
            $scope.showSourcePopup = function() {
                $scope.sourcePopup = false;
            }
            $scope.hideSourcePopup = function() {
                $scope.sourcePopup = true;
            }
            $scope.showCopyRightPopup = function() {
                $scope.copyRightPopup = false;
            }
            $scope.hideCopyRightPopup = function() {
                $scope.copyRightPopup = true;
            }
            $scope.showUrlPopup = function() {
                $scope.urlPopup = false;
            }
            $scope.hideUrlPopup = function() {
                $scope.urlPopup = true;
            }
        }
    }
}).directive('renderRightData', function() {
    return {
        scope: {
            value: '=',
            prt: '='
        },
        template: '<a href ng-class="{active: actived}" class="icon-moon">&nbsp;</a>' +
                  '<span ng-class="{positive: prtPositive}">{$ render(value, prt) $}</span>',
        link: function(scope) {
            scope.actived = true;
            scope.prtPositive = false;
            scope.render = function(value, prt) {
                scope.prtPositive = false;
                var newValue = parseFloat(value) || '';
                if (newValue) {
                	scope.actived = true;
                    if (prt) {
                        if (newValue >= 0.75) scope.prtPositive = true; else scope.prtPositive = false;
                        newValue = newValue.toFixed(2);
                    } else {
                        if (newValue <= 1) {
                            newValue = newValue.toFixed(2);
                        } else if (newValue > 1 && newValue <= 1000) {
                            newValue = Math.round(newValue);
                        } else if (newValue > 1000 && newValue <= 1000000) {
                            newValue = (newValue / 1000).toFixed(2) + ' K';
                        } else if (newValue > 1000000 && newValue <= 1000000000) {
                            newValue = (newValue / 1000000).toFixed(2) + ' M';
                        }
                    }
                } else {
                    scope.actived = false;
                }
                return newValue;
            }
        }
    }
}).directive("scroll", function ($window, $filter, dataService) {
    return function(scope, element, attrs) {
        angular.element($window).bind("scroll", function() {
            var res = [],
                headerHeight = 39;
            angular.forEach(dataService.titleOffset, function(elem) {
                res.push({'top': elem.offset().top, 'elem': elem})
            })
            res = $filter('orderBy')(res, 'top');
            for (var i = 0; i < res.length; i++){
                if (i < res.length - 1) {
                    if (res[i].top < pageYOffset + headerHeight - 30 && pageYOffset + headerHeight - 30 < res[i + 1].top) {
                        dataService.activeTitle = res[i].elem.html();
                        break;
                    }
                }
                if (i == res.length - 1 && res[i].top < pageYOffset + headerHeight - 30) {
                    dataService.activeTitle = res[res.length - 1].elem.html();
                    break;
                }
            }
            scope.$apply();
        });
    };
}).directive('titleInfo', function(dataService){
    return {
        link: function(scope, elem, attrs) {
            dataService.titleOffset.push(elem);
            scope.$root.$emit('loadStart');
        }
    }
})

  .controller('headerCtrl', function($rootScope, $scope, $http, $cookies, $filter, dataService) {
    $rootScope.visibleMenu = [];
    $scope.dataService = dataService;
    $scope.dateType = dataService.data.product[0].type;
    var menu = dataService.data.monitoring_product_template;
    for(var i = 0; i < menu.length; i++) {
        $rootScope.visibleMenu.push(menu[i]);
    }
    $scope.rerenderMenu = function(children, id) {
        if (children) {
        	if (children.length) {
	            $rootScope.visibleMenu = !id ? [] : [{
	                title: "",
	                children: $rootScope.visibleMenu,
	                id: null,
	                back: true
	            }];
	            $rootScope.visibleMenu = $rootScope.visibleMenu.concat(children);
                getFirstCategoryId(children);
        	} else {
        		scrollToElement(id);
        	}
        } else {
        	scrollToElement(id);
        }
        function scrollToElement(id) {
            var currentTitle = angular.element('#' + id);
            if (currentTitle.length) {
                angular.element("body, html").animate({scrollTop: currentTitle.offset().top - angular.element('.wrap-header').height() + currentTitle.height() + 2});
            }
        }
        function getFirstCategoryId(children) {
            if (children[0].articles) {
                if (children[0].articles.length) {
                    scrollToElement(children[0].id);
                } else {
                    getFirstCategoryId(children[0].children);
                }
            } else {
                getFirstCategoryId(children[0].children);
            }
        }
    }
}).controller('WidgetNullController', function($rootScope, $scope, $filter, dataService, $cookies, $http, $window) {
    var windowBadgeTimeout = null;
    $scope.activeFilters = []; // active filters in the header
    $scope.values = {}; // dynamically ng-model's for input[type="checkbox"]
    $scope.values.empty = {}; // dynamically ng-model's for input[type="checkbox"] "Пусто"
    ttt = $scope.values;
    $scope.widget0NewsMarginTop = widget0NewsMarginTopFn();
    $scope.openStart = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.openedStart = true;
        $scope.openedEnd = false;
    }
    $scope.openEnd = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.openedEnd = true;
        $scope.openedStart = false;
    }
    $scope.windowBadge = {
        countOfNews: null,
        show: false
    }
    // all filters
/*    $scope.dateStart = angular.copy(dataService.data.product[0].date_start.replace(/\-+/g, '.'));
    $scope.dateEnd = angular.copy(dataService.data.product[0].date_end.replace(/\-+/g, '.'));*/
    $scope.dateStart = dataService.data.product[0].date_start;
    $scope.dateEnd = dataService.data.product[0].date_end;
    console.log($scope.dateStart);
    $scope.referred_objects = dataService.data.product[0].filters.referred_object_filter;
    $scope.mention_product_filter = dataService.data.product[0].filters.mention_product_filter;
    $scope.emotionalTone = dataService.data.product[0].filters.emotional_tone_object_filter;
    $scope.country_filter = dataService.data.product[0].filters.country_filter;
    $scope.federal_district_filter = dataService.data.product[0].filters.federal_district_filter;
    $scope.cityFilter = dataService.data.product[0].filters.city_filter;
    $scope.source_filter = dataService.data.product[0].filters.source_filter;
    $scope.media_level_filter = dataService.data.product[0].filters.media_level_filter;
    $scope.media_category_filter = dataService.data.product[0].filters.media_category_filter;
    $scope.media_view_filter = dataService.data.product[0].filters.media_view_filter;
    $scope.info_reason_filter = dataService.data.product[0].filters.info_reason_filter;
    $scope.author_filter = dataService.data.product[0].filters.author_filter;
    $scope.speaker_fio_filter = dataService.data.product[0].filters.speaker_fio_filter;
    $scope.speaker_category_filter = dataService.data.product[0].filters.speaker_category_filter;
    $scope.speaker_level_filter = dataService.data.product[0].filters.speaker_level_filter;
    // показать ФО
    $scope.haveFederalDistrict = false;
    for (var i = 0; i < $scope.country_filter.length; i++) {
        console.log($scope.country_filter[i]);
        if ($scope.country_filter[i].title == 'Россия') {
            $scope.haveFederalDistrict = true;
            break;
        }
    }
    // supporting expressions
    var data = [],
        log = [],
        prefix = '__id__in';
    $scope.selectAll = function(filterList, filterPrefixId, filterTitle, selectAll) {
        for (var i = 0; i < filterList.length; i++) {
            $scope.values[filterPrefixId + filterList[i].id] = selectAll;
        }
        $scope.values.empty[filterPrefixId + 'null'] = selectAll;
        if (filterPrefixId == 'country_filter_') {
            showHidefederalDistrict(filterList, filterPrefixId);
        }
        $http.defaults.headers.post['X-CSRFToken'] = $cookies['csrftoken'];
        $http.post('http://'+ window.location.host +'/'+ $rootScope.productSlug +'/monitoring_product/'+ $rootScope.productId +'/widget0-data/', {
            data: angular.toJson(getAllFilters()),
            date_start: $filter('date')($scope.dateStart, 'dd-MM-yyyy'),
            date_end: $filter('date')($scope.dateEnd, 'dd-MM-yyyy'),
            get_count: true,
            page:1
        }).success(function(response) {
            $scope.windowBadge.countOfNews = response.articles_count;
            $scope.widget0NewsMarginTop = widget0NewsMarginTopFn();
            showBadeWindow(filterPrefixId);
        });
    }
    $scope.changeFilters = function(filterList, filterPrefixId, filterTitle, filterId) {
        if (filterPrefixId == 'country_filter_') {
            showHidefederalDistrict(filterList, filterPrefixId);
        }
        $http.defaults.headers.post['X-CSRFToken'] = $cookies['csrftoken'];
        $http.post('http://'+ window.location.host +'/'+ $rootScope.productSlug +'/monitoring_product/'+ $rootScope.productId +'/widget0-data/', {
            data: angular.toJson(getAllFilters()),
            date_start: $filter('date')($scope.dateStart, 'dd-MM-yyyy'),
            date_end: $filter('date')($scope.dateEnd, 'dd-MM-yyyy'),
            get_count: true,
            page:1

        }).success(function(response) {
            $scope.windowBadge.countOfNews = response.articles_count;
            $scope.widget0NewsMarginTop = widget0NewsMarginTopFn();
            showBadeWindow(filterPrefixId + filterId);
        });
    }
    $scope.changeEmpty = function(id) {
        $http.defaults.headers.post['X-CSRFToken'] = $cookies['csrftoken'];
        $http.post('http://'+ window.location.host +'/'+ $rootScope.productSlug +'/monitoring_product/'+ $rootScope.productId +'/widget0-data/', {
            data: angular.toJson(getAllFilters()),
            date_start: $filter('date')($scope.dateStart, 'dd-MM-yyyy'),
            date_end: $filter('date')($scope.dateEnd, 'dd-MM-yyyy'),
            get_count: true,
            page:1
        }).success(function(response) {
            $scope.windowBadge.countOfNews = response.articles_count;
            $scope.widget0NewsMarginTop = widget0NewsMarginTopFn();
            showBadeWindow(id + 'null');
        });
    }
    $scope.showFilteredArticles = function(typeDate) {
        $rootScope.loadingView = true;
        $http.defaults.headers.post['X-CSRFToken'] = $cookies['csrftoken'];
        console.log($scope.dateStart, $scope.dateEnd);
        $http.post('http://'+ window.location.host +'/'+ $rootScope.productSlug +'/monitoring_product/'+ $rootScope.productId +'/widget0-data/', {
            data: angular.toJson(getAllFilters(true)),
            date_start: angular.copy($filter('date')($scope.dateStart, 'dd-MM-yyyy')),
            date_end: angular.copy($filter('date')($scope.dateEnd, 'dd-MM-yyyy')),
            page:1
        }).success(function(res) {
            hideBadgeWindow();
            // dataService.activeTitle = '';

            $rootScope.loadingView = false;
            dataService.data = res;
            dataService.titleOffset.length = 0;
            var menu = dataService.data.monitoring_product_template;
            $rootScope.visibleMenu.length = 0;
            for(var i = 0; i < menu.length; i++) {
                $rootScope.visibleMenu.push(menu[i]);
            }
            angular.element("body, html").animate({scrollTop: angular.element('html').offset().top});
            $rootScope.$emit('loadStart');
            setTimeout(function() {
                $scope.widget0NewsMarginTop = widget0NewsMarginTopFn();
            }, 1000);
        });
    }
    $scope.removeFilter = function(filterList, filterId) {
        if (filterList) {
            for (var i = 0; i < filterList.length; i++) {
                $scope.values[filterId + filterList[i].id] = false;
            }
        } else {
            filterId['min'] = filterId['floor'];
            filterId['max'] = filterId['ceil'];
        }
        $scope.showFilteredArticles(false);
    }
    function getAllFilters(flag) {
        if (flag) $scope.activeFilters.length = 0;
        data = {},
        log = [];
        // Упоминаемость объектов
        getFilterString($scope.referred_objects, 'referred_object_', 'referred_object', 'Упоминаемость объектов', flag);
        // Упоминаемость продуктов
        getFilterString($scope.mention_product_filter, 'mentioned_product_', 'mentioned_product', 'Упоминаемость продуктов', flag);
        // Тональность
        getFilterString($scope.emotionalTone, 'emotional_tone_object_', 'emotional_tone_object', 'Тональность', flag);
        // Страна
        getFilterString($scope.country_filter, 'country_filter_', 'country', 'Страна', flag);
        // Федеральный округ
        getFilterString($scope.federal_district_filter, 'federal_district_filter_', 'federal_district', 'Федеральный округ', flag);
        // Город
        getFilterString($scope.cityFilter, 'city_filter_', 'city', 'Город', flag);
        // Источник
        getFilterString($scope.source_filter, 'source_filter_', 'source', 'Источник', flag);
        // Уровень СМИ
        getFilterString($scope.media_level_filter, 'media_level_filter_', 'media_level', 'Уровень СМИ', flag);
        // Категория СМИ
        getFilterString($scope.media_category_filter, 'media_category_filter_', 'media_category', 'Категория СМИ', flag);
        // Вид СМИ
        getFilterString($scope.media_view_filter, 'media_view_filter_', 'media_view', 'Вид СМИ', flag);
        // Инфоповод
        getFilterString($scope.info_reason_filter, 'info_reason_filter_', 'info_reason', 'Инфоповод', flag);
        // PRV
        if ($scope.prvSlider.floor) {
            if ($scope.currentPrv.min != $scope.prvSlider.min || $scope.currentPrv.max != $scope.prvSlider.max) {
                data['prv__range'] = [$scope.prvSlider.min, $scope.prvSlider.max];
                if (flag) {
                    $scope.activeFilters.push({
                        filter_list: null,
                        title: 'PRV',
                        id: $scope.prvSlider
                    });
                }
            }
        }
        // PRT
        if ($scope.prtSlider.floor) {
            if ($scope.currentPrt.min != $scope.prtSlider.min || $scope.currentPrt.max != $scope.prtSlider.max) {
                data['prt__range'] = [$scope.prtSlider.min, $scope.prtSlider.max];
                if (flag) {
                    $scope.activeFilters.push({
                        filter_list: null,
                        title: 'PRT',
                        id: $scope.prtSlider
                    });
                }
            }
        }
        // MediaOutreach
        if ($scope.MediaOutreachSlider.floor) {
            if ($scope.currentMo.min != $scope.MediaOutreachSlider.min || $scope.currentMo.max != $scope.MediaOutreachSlider.max) {
                data['mo__range'] = [$scope.MediaOutreachSlider.min, $scope.MediaOutreachSlider.max];
                if (flag) {
                    $scope.activeFilters.push({
                        filter_list: null,
                        title: 'MediaOutreach',
                        id: $scope.MediaOutreachSlider
                    });
                }
            }
        }
        // Автор
        getFilterString($scope.author_filter, 'author_filter_', 'author', 'Автор', flag);
        // ФИО спикера
        getFilterString($scope.speaker_fio_filter, 'speaker_fio_filter_', 'speaker', 'ФИО спикера', flag);
        // Категория спикера
        getFilterString($scope.speaker_category_filter, 'speaker_category_filter_', 'speaker_category', 'Категория спикера', flag);
        // Уровень спикера
        getFilterString($scope.speaker_level_filter, 'speaker_level_filter_', 'speaker_level', 'Уровень спикера', flag);
        return data;
    }
    function getFilterString(object, keyPrefix, arrPrefix, title, flag) {
        var _key = 'id',
            objectLength = object.length,
            objectTrueLength = 0;
        angular.forEach(object, function(value, key) {
            if ($scope.values[keyPrefix + value.id]) {
                this.push(value[_key]);
                objectTrueLength++;
            }
        }, log);
        var emptyUndefined = angular.isUndefined($scope.values.empty[keyPrefix + 'null']),
            addInFilter = true;
        if (log.length) {
            if (emptyUndefined) {
                if (objectLength == objectTrueLength || objectTrueLength == 0) addInFilter = false;
            } else {
                if (objectLength == objectTrueLength && $scope.values.empty[keyPrefix + 'null']) addInFilter = false;
                if (objectTrueLength == 0 && !$scope.values.empty[keyPrefix + 'null']) addInFilter = false;
            }
            if (addInFilter) {
                if (keyPrefix == 'speaker_fio_filter_') {
                    data['speaker__id__in'] = angular.copy(log);
                } else {
                    // data[arrPrefix + (keyPrefix == 'info_reason_filter_' ? '__in' : prefix)] = angular.copy(log);
                    data[arrPrefix + prefix] = angular.copy(log);
                }
                if (flag) {
                    $scope.activeFilters.push({
                        filter_list: object,
                        title: title,
                        id: keyPrefix
                    });
                }
            }
            log.length = 0;
        }
        if (!emptyUndefined && $scope.values.empty[keyPrefix + 'null'] && objectLength != objectTrueLength) {
            data[arrPrefix + '__isnull'] = true;
        }
    }
    function showHidefederalDistrict(filterList, filterPrefixId) {
        console.log(filterList, filterPrefixId);
        $scope.haveFederalDistrict = false;
            for (var i = 0; i < filterList.length; i++) {
            if ($scope.values[filterPrefixId + filterList[i].id]) {
                if (filterList[i].title == 'Россия') $scope.haveFederalDistrict = true;
            }
        }
        if (!$scope.haveFederalDistrict) {
            for (var i = 0; i < $scope.federal_district_filter.length; i++) {
                $scope.values['federal_district_filter_' + $scope.federal_district_filter[i].id] = true;
            }
            $scope.values.empty['federal_district_filter_null'] = true;
        }
    }
    $scope.$root.$on('slideEnded', function(event, elem) {
        $http.defaults.headers.post['X-CSRFToken'] = $cookies['csrftoken'];
        $http.post('http://'+ window.location.host +'/'+ $rootScope.productSlug +'/monitoring_product/'+ $rootScope.productId +'/widget0-data/', {
            data: angular.toJson(getAllFilters()),
            date_start: $filter('date')($scope.dateStart, 'dd-MM-yyyy'),
            date_end: $filter('date')($scope.dateEnd, 'dd-MM-yyyy'),
            get_count: true,
            page:1
        }).success(function(response) {
            $scope.windowBadge.countOfNews = response.articles_count;
            $scope.widget0NewsMarginTop = widget0NewsMarginTopFn();
            showBadeWindow(elem);
        });
    });
    function widget0NewsMarginTopFn() {
        return document.getElementById('under-header').offsetHeight;
    }
    function showBadeWindow(elem) {
        $('#window-badge').css('top', $('#' + elem).parent().offset().top - 38)
        $scope.windowBadge.show = true;
        clearTimeout(windowBadgeTimeout);
        windowBadgeTimeout = setTimeout(function() {
            hideBadgeWindow();
            $scope.$apply();
        }, 3000);
    }
    function hideBadgeWindow() {
        $scope.windowBadge.show = false;
    }
	$scope.toggle = {
		referred_objects: true,
		mentionedProduct: true,
		tonality: true,
		country: true,
		federalDistrict: true,
		city: true,
		source: true,
		mediaLevel: true,
		mediaType: true,
		mediaView: true,
		infopovod: true,
		prv: false,
		prt: false,
		MediaOutreach: false,
		author: true,
		speakerName: true,
		speakerCat: true,
		speakerLvl: true
	}
	$scope.prvSlider = {
		min: dataService.data.product[0].filters.prv_min,
		max: dataService.data.product[0].filters.prv_max,
		floor: dataService.data.product[0].filters.prv_min,
		ceil: dataService.data.product[0].filters.prv_max
	}
	$scope.prtSlider = {
        min: dataService.data.product[0].filters.prt_min,
        max: dataService.data.product[0].filters.prt_max,
        floor: dataService.data.product[0].filters.prt_min,
        ceil: dataService.data.product[0].filters.prt_max
	}
	$scope.MediaOutreachSlider = {
        min: dataService.data.product[0].filters.mo_min,
        max: dataService.data.product[0].filters.mo_max,
        floor: dataService.data.product[0].filters.mo_min,
        ceil: dataService.data.product[0].filters.mo_max
	}
    $scope.currentPrv = {
        min: $scope.prvSlider.min,
        max: $scope.prvSlider.max
    }
    $scope.currentPrt = {
        min: $scope.prtSlider.min,
        max: $scope.prtSlider.max
    }
    $scope.currentMo = {
        min: $scope.MediaOutreachSlider.min,
        max: $scope.MediaOutreachSlider.max
    }

    // getAllFilters(false, true);
    $scope.openedStart = false;
    $scope.openedEnd = false;
	$scope.prvTranslate = function(value) {
		// return value + ' М';
        return value;
	}
    $scope.title = dataService.data.product[0].title;
    $scope.category = dataService.data.monitoring_product_template;
    $scope.dataService = dataService;
    $scope.$root.dataService = dataService;
    root = $scope.$root
    $scope.$root.$on('loadStart', function (){
        res = [];
        angular.forEach(dataService.titleOffset, function(elem) {
            res.push({'top': elem.offset().top, 'elem': elem})
        });
        res = $filter('orderBy')(res, 'top');
        if (res[0]) {
            dataService.activeTitle = res[0].elem.html();
            res[0].elem.css({
                height: 1,
                overflow: 'hidden'
            });
        }
        if(!$scope.$root.$$phase) {$scope.$root.$apply();}
        // dataService.header = dataService.headerObj.height() + 20;
        dataService.header = 39;
        if(!$scope.$root.$$phase) {$scope.$root.$apply();}
    });
}).run(['$rootScope', function($root) {
    $root.$on('$routeChangeStart', function(e, curr, prev) {
        if (curr.$$route && curr.$$route.resolve) {
            $root.loadingView = true;
        }
    });
    $root.$on('$routeChangeSuccess', function(e, curr, prev) {
        $root.loadingView = false;
    });
}]);