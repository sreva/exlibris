var exlibris = angular.module('exlibris', ['ui.bootstrap', 'ngSanitize', 'ngCookies', 'ngRoute', 'ngAnimate']);
exlibris.config(function($routeProvider, $locationProvider, $interpolateProvider) {
	$interpolateProvider.startSymbol('{$');
	$interpolateProvider.endSymbol('$}');
	$routeProvider.when('/:user/monitoring_product_test/:id/', {
		templateUrl: "/template/news_test.html/",
		controller: "NewsListCtrl",
        resolve:{
            load: function ($rootScope, $route, dataService) {
            	$rootScope.productId = $route.current.params.id
                $rootScope.$emit('loadStart');
                return dataService.load($route.current.params.id).then(function(res){
                    dataService.data = res;
                });	
            }
        }
	})
	$locationProvider.html5Mode(true);
    
}).factory('dataService', function ($q, $http) {
    return {
        data: {},
        load: function(id) {
            var defer = $q.defer();
           	$http.get('http://'+ window.location.host +'/monitoring-product/' + id + '/').success(function(res){
                defer.resolve(res);
            });
            return defer.promise;
        },
	    titleOffset: [],
	    activeTitle: '',
	    headerObj: null,
	    header: 0
    };
}).factory('globalFlags', function () {
    var toneList = {
        'позитивное': 'green',
        'негативное': 'red',
        'нейтральное': 'grey'
    };
    var roleList = {
        'ведущая': 'full',
        'значимая': 'half',
        'контекстная': 'third'
    }
    return {
        flagType: function(tone, role) {
            return toneList[tone] + ' ' + roleList[role];
        }
    }
}).filter("paintTheWordsFilter", function() {
    return function(input, searchRegex, replaceRegex) {
        return input.replace(RegExp(searchRegex), replaceRegex);
    };
}).directive('categoryList', function($compile) {
    return {
        restrict: 'E',
        scope: {
            category: '=',
            name: '='
        },
        templateUrl: 'categoryListTemplate',
        
        controller: function($scope, $document) {
            $scope.createTitle = function(title, catTitle) {
                if (!title) return catTitle;
                return title + '/' + catTitle;
            }
        }, 
        compile: function(tElement, tAttr, transclude) {
            var contents = tElement.contents().remove();
            var compiledContents;
            return function(scope, iElement, iAttr) {
                if(!compiledContents) {
                    compiledContents = $compile(contents, transclude);
                }
                compiledContents(scope, function(clone, scope) {
                         iElement.append(clone); 
                });
            }}  
    }
}).directive('singleArticle', function() {
    return {
        restrict: 'E',
        scope: {
            item: '='
        },
        templateUrl: 'singleArticleTemplate',
        controller: function($scope, $filter, $sce, globalFlags) {
            $scope.currentIndex = 0;
            $scope.activeWord = 0;
            $scope.flagType = globalFlags.flagType;
            $scope.status = {
                isCollapsed: true
            }
            $scope.activeWordChange = function(index) {
                $scope.activeWord = index;
                $scope.currentIndex = index;
            }
            $scope.previewWordChange = function(index) {
                $scope.currentIndex = $scope.activeWord;
                $scope.activeWord = index;
            }
            $scope.returnLastActive = function() {
                $scope.activeWord = $scope.currentIndex;
            }
            $scope.paintTheWords = function(html, mo) {
                text = html;
                for(var i = 0; i < mo.length; i++) {
                    for(var j = 0; j < mo[i].keywords.length; j++) {
                        text = $filter('paintTheWordsFilter')(text, '('+ mo[i].keywords[j] +')', '<span class="colored-text" style="background-color:#'+ mo[i].color +'">' + mo[i].keywords[j] + '</span>');
                    }
                }
                return $sce.trustAsHtml(text);
            }
            $scope.renderSpeakers = function(speakers) {
                var text = '(';
                for (var i = 0; i < speakers.length; i++)
                    text += speakers[i].fio + (i != speakers.length - 1 ? ', ': '');
                text += ')';
                return text;
            }
            $scope.sourcePopup = true;
            $scope.copyRightPopup = true;
            $scope.urlPopup = true;
            $scope.showSourcePopup = function() {
                $scope.sourcePopup = false;
            }
            $scope.hideSourcePopup = function() {
                $scope.sourcePopup = true;
            }
            $scope.showCopyRightPopup = function() {
                $scope.copyRightPopup = false;
            }
            $scope.hideCopyRightPopup = function() {
                $scope.copyRightPopup = true;
            }
            $scope.showUrlPopup = function() {
                $scope.urlPopup = false;
            }
            $scope.hideUrlPopup = function() {
                $scope.urlPopup = true;
            }
        }
    }
}).directive('renderRightData', function() {
    return {
        scope: {
            value: '=',
            prt: '=',
            popupt: '@'
        },
        template: '<a href ng-class="{active: actived}" popover-trigger="mouseenter" popover="{$ popupt $}" class="icon-moon">' +
                  '</a>' +
                  '<span ng-class="{positive: prtPositive}">{$ render(value, prt) $}</span>',
        link: function(scope) {
            scope.actived = true;
            scope.prtPositive = false;
            scope.render = function(value, prt) {
                var newValue = parseFloat(value) || '';
                if (newValue) {
                	scope.actived = true;
                    if (prt) {
                        if (newValue >= 0.75) scope.prtPositive = true; else scope.prtPositive = false;
                        newValue = newValue.toFixed(2);
                    } else {
                        if (newValue <= 1) {
                            newValue = newValue.toFixed(2);
                        } else if (newValue > 1 && newValue <= 1000) {
                            newValue = Math.round(newValue);
                        } else if (newValue > 1000 && newValue <= 1000000) {
                            newValue = (newValue / 1000).toFixed(2) + ' K';
                        } else if (newValue > 1000000 && newValue <= 1000000000) {
                            newValue = (newValue / 1000000).toFixed(2) + ' M';
                        }
                    }
                } else {
                    scope.actived = false;
                }
                return newValue;
            }
        }
    }
}).directive('headerHeight', function(dataService) {
    return {
        link: function(scope, element, attrs) {
            // dataService.headerObj = element;
        }
    }
}).directive("scroll", function ($window, $filter, dataService) {
    return function(scope, element, attrs) {
        angular.element($window).bind("scroll", function() {
            var res = [],
                headerHeight = 149;
            angular.forEach(dataService.titleOffset, function(elem) {
                res.push({'top': elem.offset().top, 'elem': elem})
            })
            res = $filter('orderBy')(res, 'top');
            for (var i = 0; i < res.length; i++){
                if (i < res.length - 1) {
                    if (res[i].top < pageYOffset + headerHeight - 30 && pageYOffset + headerHeight - 30 < res[i + 1].top) {
                        dataService.activeTitle = res[i].elem.html();
                        break;
                    }
                }
                if (i == res.length - 1 && res[i].top < pageYOffset + headerHeight - 30) {
                    dataService.activeTitle = res[res.length - 1].elem.html();
                    break;
                }
            }
            scope.$apply();
        });
    };
}).directive('titleInfo', function(dataService){
    return {
        link: function(scope, elem, attrs) {
            dataService.titleOffset.push(elem);
            scope.$root.$emit('loadStart');
        }
    }
}).controller('headerCtrl', function($rootScope, $scope, $http, $cookies, $filter, $location,dataService) {
    $rootScope.visibleMenu = [];
    $scope.dataService = dataService;

    $scope.dateStart = angular.copy(dataService.data.product[0].date_start.replace(/\-+/g, '.'));

    $scope.dateEnd = angular.copy(dataService.data.product[0].date_end.replace(/\-+/g, '.'));

    $scope.dateType = dataService.data.product[0].type;
    $scope.title = dataService.data.product[0].title;
    if ($scope.dateType == "day") {
    	$scope.daysCount = 1;
    } else if ($scope.dateType == "week") {
    	$scope.daysCount = 7;
    } else if ($scope.dateType == "month") {
    	$scope.daysCount = 30;
    }
    var menu = dataService.data.monitoring_product_template;
    for(var i = 0; i < menu.length; i++) {
        $rootScope.visibleMenu.push(menu[i]);
    }
    $scope.openedStart = false;
    $scope.openedEnd = false;
    $scope.rerenderMenu = function(children, id) {
        if (children) {
        	if (children.length) {
	            $rootScope.visibleMenu = !id ? [] : [{
	                title: "",
	                children: $rootScope.visibleMenu,
	                id: null,
	                back: true
	            }];
	            $rootScope.visibleMenu = $rootScope.visibleMenu.concat(children);
                getFirstCategoryId(children);
        	} else {
        		scrollToElement(id);
        	}
        } else {
        	scrollToElement(id);
        }
        function scrollToElement(id) {
            var currentTitle = angular.element('#' + id);
            if (currentTitle.length) {
                angular.element("body, html").animate({scrollTop: currentTitle.offset().top - angular.element('.wrap-header').height() + currentTitle.height() + 2});
            }
        }
        function getFirstCategoryId(children) {
            if (children[0].articles) {
                if (children[0].articles.length) {
                    scrollToElement(children[0].id);
                } else {
                    getFirstCategoryId(children[0].children);
                }
            } else {
                getFirstCategoryId(children[0].children);
            }
        }
    }
    $scope.getNewsList = function(datePicker) {
        if (datePicker == 'first') {
            $scope.dateEnd = angular.copy($scope.dateStart);
            $scope.dateEnd.setDate($scope.dateEnd.getDate() + $scope.daysCount);
        } else if (datePicker == 'second') {
            $scope.dateStart = angular.copy($scope.dateEnd);
            $scope.dateStart.setDate($scope.dateStart.getDate() - $scope.daysCount);
        }
        dateFilter();
    }
    $scope.nextPrev = function(type) {
        if (angular.isString($scope.dateStart)) {
            console.log('string');
            $scope.dateStart = angular.copy($scope.dateStart.replace(/\.+/g, '-'));    
        } else {
            $scope.dateStart = angular.copy($filter('date')($scope.dateStart, 'dd-MM-yyyy'));
            console.log('!string');
        }
        if (angular.isString($scope.dateEnd)) {
            console.log('string');
            $scope.dateEnd = angular.copy($scope.dateEnd.replace(/\.+/g, '-'));    
        } else {
            console.log('!string');
            $scope.dateEnd = angular.copy($filter('date')($scope.dateEnd, 'dd-MM-yyyy'));   
        }
        if (!angular.isDate($scope.dateStart)) {
            var arr = angular.copy($scope.dateStart);
            $scope.dateStart = new Date(arr.split('-')[2], arr.split('-')[1] - 1, arr.split('-')[0]);
        }
        if (!angular.isDate($scope.dateEnd)) {
            var arr = angular.copy($scope.dateEnd);
            $scope.dateEnd = new Date(arr.split('-')[2], arr.split('-')[1] - 1, arr.split('-')[0]);
        }
        var operType = type == "prev" ? -$scope.daysCount: $scope.daysCount;
        $scope.dateStart.setDate($scope.dateStart.getDate() + operType);
        $scope.dateEnd.setDate($scope.dateEnd.getDate() + operType);
        dateFilter();
    }
    $scope.goToWidgetNull = function() {
        window.location.href = $location.path() + 'widget0/';
    }
    function dateFilter() {
        $rootScope.loadingView = true;
        $http.defaults.headers.post['X-CSRFToken'] = $cookies['csrftoken'];
        $http.post('http://'+ window.location.host +'/monitoring-product/'+ $rootScope.productId +'/', {
            date_start: $filter('date')($scope.dateStart, 'dd-MM-yyyy'),
            date_end: $filter('date')($scope.dateEnd, 'dd-MM-yyyy'),
            date_type: $scope.dateType
        }).success(function(res) {
            dataService.activeTitle = '';
            $rootScope.loadingView = false;
            dataService.data = res;
            dataService.titleOffset.length = 0;
            var menu = dataService.data.monitoring_product_template;
            $rootScope.visibleMenu.length = 0;
            for(var i = 0; i < menu.length; i++) {
                $rootScope.visibleMenu.push(menu[i]);
            }
            angular.element("body, html").animate({scrollTop: angular.element('html').offset().top});        
            $rootScope.$emit('loadStart');
        });
    }
}).controller('NewsListCtrl', function($scope, $filter, dataService) {
    $scope.category = dataService.data.monitoring_product_template;
    $scope.dataService = dataService;
    $scope.$root.dataService = dataService;
    root = $scope.$root
    $scope.$root.$on('loadStart', function (){
        res = [];
            angular.forEach(dataService.titleOffset, function(elem) {
                res.push({'top': elem.offset().top, 'elem': elem})
            });
            res = $filter('orderBy')(res, 'top');
            if (res[0]) {
                dataService.activeTitle = res[0].elem.html();
                res[0].elem.css({
                    height: 1,
                    overflow: 'hidden'
                });
            }
            if(!$scope.$root.$$phase) {$scope.$root.$apply();}
            // dataService.header = dataService.headerObj.height() + 20;
            dataService.header = 125;
            if(!$scope.$root.$$phase) {$scope.$root.$apply();}
    })
}).run(['$rootScope', function($root) {
    $root.$on('$routeChangeStart', function(e, curr, prev) {
        if (curr) {
            if (curr.$$route && curr.$$route.resolve) {
                $root.loadingView = true;
            }
        }
    });
    $root.$on('$routeChangeSuccess', function(e, curr, prev) {
        $root.loadingView = false;
    });
}]);