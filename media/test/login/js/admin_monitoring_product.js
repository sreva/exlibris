var form_valid = false,
	ajax_sended = false;
$(function() {
	var $widget_priorities = $('.widget-priorities'),
		$widget_monitoring = $('.widget-monitoring'),
		$monitoringproduct_form = $('#monitoringproduct_form');
		$list_of_object = $('.list-of-object');
	$('#id_title, #id_type, #id_day_start').change(function() {
		validate_form($monitoringproduct_form);
	});
	$('#id_time_start').focusout(function() {
		validate_form($monitoringproduct_form);
	});
	if ($widget_priorities.length && $widget_monitoring.length) {
		var $arrow = $('.vertical-scroll').find('.arrow'),
			$clicable_label, $sections;
		$widget_monitoring.on('click', 'ul label', function() {
			if ($(this).parent().find('input[type="checkbox"]').prop('checked')) {
				var index = $(this).parents('li').index(),
					id = $(this).data().id;
				$sections.hide().filter(function() {
					return this.id == id
				}).show();
				$clicable_label.removeClass('active');
				$(this).addClass('active');
				$arrow.css('top', 5 * index + 1 + index * 22);
			}
		});
		// select project
		$('#id_project').change(function() {
			validate_form($monitoringproduct_form);
			var value = $(this).val();
			if (value) {
				$.ajax({
	                url: "/api/get-mentions-list/",
	                cache: false,
	                type: "POST",
	                data: {
	                    'csrfmiddlewaretoken': getCookie('csrftoken'),
	                    'id': value
	                },
	                dataType: "json",
	                success: function (data) {
	                    if (data.status == 'OK') {
	                    	var category_list = $.parseJSON(data.category_list),
	                    		mention_objects_list = $.parseJSON(data.mention_objects_list),
	                    		$ul = $widget_priorities.find('ul').empty();
                    		// Содержание
	                    	for (var i = 0; i < category_list.length; i++) {
	                    		console.log(category_list[i].fields.title);
	                    		$ul.append('<li data-send-id="'+ category_list[i].pk +'">' +
                    							'<label for="c-fild-'+ category_list[i].pk +'">'+ category_list[i].fields.title +'</label>' +
                								'<span class="page">' +
                    								'<input type="text" name="paragraph-'+ category_list[i].pk +'" id="c-fild-'+ category_list[i].pk +'" value="1"/>' +
                								'</span>' +
                							'</li>');
	                    	}
	                    	// Объекты мониторинга
	                    	var $monitoring_objects = $list_of_object.children('.column').first().children('ul').empty(),
	                    		$keywords_block = $('.keywords-block').empty(),
	                    		tab_elements = function(object, id) {
	                    			var lis = '';
	                    			for (var j = 0; j < object.length; j++) {
	                    				lis += '<li data-send-id="'+ object[i].pk +'"><input type="text" name="keyword-'+ id + object[i].pk +'" value=""/></li>'
	                    			}			
	                    			return lis;
	                    		};
	                    	for (var i = 0; i < mention_objects_list.length; i++) {
	                    		$monitoring_objects.append('<li data-send-id="'+ mention_objects_list[i].pk +'">' +
	                    										'<div class="left">' +
	                    											'<label data-id="c-tab-'+ mention_objects_list[i].pk +'" '+ (i == 0 ? 'class="active"' : '') +'>'+ mention_objects_list[i].fields.title +'</label>' +
	                    											'<span class="color-picker">&nbsp;</span>' +
	                    											'<input type="checkbox" name="check-'+ mention_objects_list[i].pk +'" '+ (i == 0 ? 'checked' : '') +'/>' +
                    											'</div>' +
	                    									'</li>');
	                    		$keywords_block.append('<section id="c-tab-'+ mention_objects_list[i].pk +'">'+
	                    									'<ul>' +
	                    										tab_elements(mention_objects_list, mention_objects_list[i].pk) +
	                    									'</ul>' +
	                    								'</section>');
	                    	}
	                    	$clicable_label = $widget_monitoring.find('ul').find('label');
	                    	$sections = $('.keywords-block').children('section');
	                    	$clicable_label.first().click();
							$('.color-picker').colpick({
								submit: 0,
								color: '#666666',
								onChange:function(hsb,hex,rgb,el,bySetColor) {
									var $element = $(el);
									$element.css('background-color', '#' + hex);
									$sections.filter(function() {
										return this.id == $element.prev().data().id
									}).find('input').css('color', '#' + hex);
								}
							});
							$widget_priorities.show();
							$widget_monitoring.show();
	                    }
	                }
	            });
			} else {
				$widget_priorities.hide();
				$widget_monitoring.hide();
			}
		});
	}
	$monitoringproduct_form.on('submit', function(){
		if (!form_valid) {
			validate_form($(this), true);
			return false;
		} else if (!ajax_sended) {
			var data = {};
			data.category_list = [];
			data.mention_objects_list = [];
			data.product = {};
			$widget_priorities.find('li').each(function() {
				data.category_list.push({
					id: $(this).data().sendId,
					index: $(this).find('input[type="text"]').val()
				});
			});
			var $keywords_block = $('.keywords-block');
			$list_of_object.children('.column').first().find('li').each(function() {
				if ($(this).find('input[type="checkbox"]').prop('checked')) {
					var added_keywords = [],
						obj_id = $(this).data().sendId;
					$keywords_block.find('li').filter(function() {
						return $(this).data().sendId == obj_id
					}).each(function() {
						var current_word = $(this).find('input[type="text"]').val();
						if (current_word) {
							added_keywords.push(current_word);
						}
					});
					data.mention_objects_list.push({
						id: $(this).data().sendId,
						color: $(this).find('.color-picker').css('background-color'),
						keywords: added_keywords
					});
				}
			});
			data.product.project_name = $(this).find('select[name="project"]').val();
			data.product.title = $(this).find('input[name="title"]').val();
			data.product.type = $(this).find('select[name="type"]').val();
			data.product.day_start = $(this).find('select[name="day_start"]').val();
			data.product.time_start = $(this).find('input[name="time_start"]').val();
			$.ajax({
                url: "/api/mp_mention_obj_category/",
                cache: false,
                type: "POST",
                data: {
                    'csrfmiddlewaretoken': getCookie('csrftoken'),
                    data: JSON.stringify(data)
                },
                dataType: "json",
                success: function (data) {
                	if (data.status == 'OK') {
                		/*ajax_sended = true;
                		$monitoringproduct_form.submit();*/
                	}
                }
            });
			console.log(data);
			return false;
		}
	})
});
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
function validate_form($form, submit) {
	var $project = $form.find('select[name="project"]'),
		$title = $form.find('input[name="title"]'),
		$type_of_monitoring = $form.find('select[name="type"]'),
		$day_start = $form.find('select[name="day_start"]'),
		$start_time = $form.find('input[name="time_start"]'),
		valid = true;
	if (!$project.val()) {
		valid = false;
		 if (submit) $project.parents('.form-row').addClass('errors');
	} else {
		$project.parents('.form-row').removeClass('errors');
	}
	if (!$title.val()) {
		valid = false;
		if (submit) $title.parents('.form-row').addClass('errors');
	} else {
		$title.parents('.form-row').removeClass('errors')
	}
	if (!$type_of_monitoring.val()) {
		valid = false;
		if (submit) $type_of_monitoring.parents('.form-row').addClass('errors');
	} else {
		$type_of_monitoring.parents('.form-row').removeClass('errors')
	}
	if (!$day_start.val()) {
		valid = false;
		if (submit) $day_start.parents('.form-row').addClass('errors');
	} else {
		$day_start.parents('.form-row').removeClass('errors')
	}
	var time = $start_time.val().split(':');
	if (!(time.length == 3 && time[0] != '' && time[1] != '' && time[2] != '' && +time[0] >= 0 && +time[0] <= 24 && +time[1] >= 0 && +time[1] <= 60 && +time[2] >= 0 && +time[2] <= 60)) {
		valid = false;
		if (submit) $start_time.parents('.form-row').addClass('errors');
	} else {
		$start_time.parents('.form-row').removeClass('errors')
	}
	if (valid) {
		form_valid = true;
	} else {
		form_valid = false;
	}
}