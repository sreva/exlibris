# --*-- coding: utf-8 --*--

from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.views.generic import TemplateView
from info import settings
from django.contrib.auth import views as auth_views
media = settings.MEDIA_ROOT
static = settings.STATIC_ROOT
report = settings.REPORT_ROOT
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'info.views.home', name='home'),
    # url(r'^info/', include('info.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    #url(r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root':static, 'show_indexes': True}),


    url(r'^admin/custom_user/(?P<pk>\d+)/download_votes_report/', 'custom_user.views.make_votes_report'),

    url(r'^ckeditor/', include('ckeditor.urls')),
    url(r'^convert/$', 'monitoring_product.views.convert_docx'),
    url(r'^no-convert/$', 'monitoring_product.views.no_convert'),
    url(r'^generate-doc/$', 'main.views.generate_doc'),
    url(r'^(?P<slug>.*)/get-monitoring-product-filters/(?P<id>\d+)/$', 'monitoring_product.views.get_monitoring_product_filters'),
    # url(r'^get-monitoring-product-filters/(?P<id>\d+)/$', 'monitoring_product.views.get_monitoring_product_filters'),
    url(r'^create-or-update-product/(?P<id>\d+)/$', 'monitoring_product.views.create_or_update_sub_product'),
    url(r'^send/articles/$', 'main.views.send_articles'),
    url(r'^print/articles/$', 'main.views.print_articles'),
    url(r'^login/$', 'custom_user.views.custom_login'),
    url(r'^profile/$', 'custom_user.views.profile'),
    url(r'^profile/(?P<id>\d+)/$', 'custom_user.views.update_profile'),
    url(r'^profile-api/$', 'custom_user.views.profile_api'),
    url(r'^change/password/(?P<id>\d+)/$', 'custom_user.views.password_change'),
    url(r'^save-product-settings/(?P<id>\d+)/$', 'monitoring_product.views.save_product_settings'),
    url(r'^delete/user/(?P<id>\d+)/$', 'custom_user.views.delete_user'),
    url(r'^delete/social/(?P<id>\d+)/$', 'custom_user.views.delete_social'),
    url(r'^add/social/$', 'custom_user.views.add_social'),
    url(r'^profile/change-photo/(?P<id>\d+)/$', 'custom_user.views.change_photo'),
    url(r'^profile/application-settings/$', 'custom_user.views.get_user_settings'),
    url(r'^profile/set-application-settings/$', 'custom_user.views.set_user_settings'),

    url(r'^monitoring_product/get_users_access/(?P<pk>\d+)/$', 'custom_user.views.get_users_access'),
    url(r'^monitoring_product/set_users_access/(?P<pk>\d+)/$', 'custom_user.views.set_users_access'),
    url(r'^monitoring_product/get_product_settings/(?P<id>\d+)/$', 'monitoring_product.views.get_product_settings'),

    url(r'^send/feedback/$', 'main.views.feedback'),
    # url(r'^login/$', auth_views.login, {'template_name': 'login.html'},name='auth_login'),
    # url(r'^logout/$', auth_views.logout, {'template_name': 'registration/logout.html'}, name='auth_logout'),
    url(r'^(?P<slug>.*)/monitoring_product/(?P<id>\d+)/widget0/constructor/$', 'monitoring_product.views.constructor_widget0'),
    url(r'^product/vote/(?P<pk>\d+)/$', 'custom_user.views.vote'),
    url(r'^product/rename/(?P<pk>\d+)/$', 'main.views.rename_product'),
    url(r'^product/delete/(?P<pk>\d+)/$', 'main.views.delete_product'),

    url(r'^angular/templates/(?P<slug>.*)$', 'main.views.get_angular_template'),

    url(r'^world-kaspersky/dashboard/$', TemplateView.as_view(template_name='analytic_product/analytic_product.html')),
    url(r'^vympelkom/dashboard/$', TemplateView.as_view(template_name='analytic_product/analytic_product_vympelkom.html')),
    url(r'^template/analitic_product_vympelkom_inner\.html/$', TemplateView.as_view(template_name='analytic_product/analytic_product_vympelkom_inner.html')),

    (r'^(?P<slug>.*)/analytic_product/(?P<referred_id>\d+)/json1$', 'monitoring_product.views.analytic_product_1'),
    (r'^(?P<slug>.*)/analytic_product/(?P<referred_id>\d+)/json2$', 'monitoring_product.views.analytic_product_2'),
    (r'^(?P<slug>.*)/analytic_product/(?P<referred_id>\d+)/json3$', 'monitoring_product.views.analytic_product_3'),
    (r'^(?P<slug>.*)/analytic_product/(?P<referred_id>\d+)/json4$', 'monitoring_product.views.analytic_product_4'),
    (r'^(?P<slug>.*)/analytic_product/(?P<referred_id>\d+)/json5$', 'monitoring_product.views.analytic_product_5'),
    (r'^(?P<slug>.*)/analytic_product/(?P<referred_id>\d+)/json6$', 'monitoring_product.views.analytic_product_6'),
    (r'^(?P<slug>.*)/analytic_product/(?P<referred_id>\d+)/json7$', 'monitoring_product.views.analytic_product_7'),
    # vimpelcom
    (r'^(?P<slug>.*)/analytic_product_vympelkom/(?P<referred_id>\d+)/json1$', 'monitoring_product.views.analytic_product_1_vympelkom'),
    (r'^(?P<slug>.*)/analytic_product_vympelkom/(?P<referred_id>\d+)/json2$', 'monitoring_product.views.analytic_product_2_vympelkom'),
    (r'^(?P<slug>.*)/analytic_product_vympelkom/(?P<referred_id>\d+)/json3$', 'monitoring_product.views.analytic_product_3_vympelkom'),
    (r'^(?P<slug>.*)/analytic_product_vympelkom/(?P<referred_id>\d+)/json4$', 'monitoring_product.views.analytic_product_4_vympelkom'),
    (r'^(?P<slug>.*)/analytic_product_vympelkom/(?P<referred_id>\d+)/json5$', 'monitoring_product.views.analytic_product_5_vympelkom'),
    (r'^(?P<slug>.*)/analytic_product_vympelkom/(?P<referred_id>\d+)/json6$', 'monitoring_product.views.analytic_product_6_vympelkom'),
    (r'^(?P<slug>.*)/analytic_product_vympelkom/(?P<referred_id>\d+)/json7$', 'monitoring_product.views.analytic_product_7_vympelkom'),
    (r'^get_filter_dictionary/$', 'monitoring_product.views.filter_parameters'),
    (r'^get_filter_dictionary_vympelcom/$', 'monitoring_product.views.filter_parameters_vympelcom'),

    url(r'^template/analitic_product\.html/$', TemplateView.as_view(template_name='analytic_product/analiric_product_jula.html')),

    (r'^(?P<slug>.*)/analytic_product/(?P<referred_id>\d+)/$', 'monitoring_product.views.analytic_product_1'),
    (r'^(?P<slug>.*)/analytic_product/(?P<referred_id>\d+)/$', 'monitoring_product.views.analytic_product_1'),
    url(r'^template/profile\.html/$', TemplateView.as_view(template_name='profile.html')),
    url(r'^template/news\.html/$', TemplateView.as_view(template_name='monitoring_product/news.html')),
    url(r'^template/profileModals\.html/$', TemplateView.as_view(template_name='profileModals.html')),
    url(r'^template/news2\.html/$', TemplateView.as_view(template_name='monitoring_product/news2.html')),
    url(r'^template/news_test\.html/$', TemplateView.as_view(template_name='monitoring_product/news_test.html')),

    (r'^admin/add_monitoring/(?P<id>\d+)/$', 'monitoring_product.views.add_monitoring_product'),
    (r'^(?P<slug>.*)/monitoring_product/(?P<id>\d+)/$', 'monitoring_product.views.monitoring_product' ),
    (r'^(?P<slug>.*)/monitoring_product_test/(?P<id>\d+)/$', 'monitoring_product.views.monitoring_product_test_test'),
    (r'^(?P<slug>.*)/monitoring_product/(?P<id>\d+)/widget0/', 'monitoring_product.views.widget0'),


    (r'^(?P<slug>.*)/monitoring_product/load_sources/', 'monitoring_product.views.get_product_sources'),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^static_file/report/(?P<path>.*)$','django.views.static.serve', {'document_root':report}),
    url(r'^admin_tools/', include('admin_tools.urls')),
    url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root':media}),
    # url(r'^(?P<slug>.*)/login/$', 'main.views.login'),
    url(r'^api/get-mentions-list/$', 'monitoring_product.views.get_mention_object'),

    url(r'^monitoring-product/(?P<id>\d+)/$', 'monitoring_product.views.monitoring_product_api'),
    url(r'^monitoring-product/get_product_data/(?P<pk>\d+)/$', 'monitoring_product.views.get_product_data'),

    # url(r'^(?P<slug>.*)/login/$', 'main.views.login'),
    url(r'^(?P<slug>.*)/monitoring_product/(?P<id>\d+)/widget0-data/$', 'monitoring_product.views.get_widget0_data'),
    url(r'^monitoring_product/check_valid_constructors_widget0/$', 'monitoring_product.views.check_valid_constructors_widget0'),
    url(r'^api/get-widget0-data/$', 'monitoring_product.views.get_widget0_data'),
    url(r'^api/mp_mention_obj_category/$', 'monitoring_product.views.mp_mention_obj_category'), #создание категорий и упоминаемих обектов для мониторингового продукта
    url(r'^api/mp_mention_obj_category_update/$', 'monitoring_product.views.mp_mention_obj_category_update'),# обновление категорий и упоминаемих обектов для мониторингового продукта
    url(r'^api/get_mp_mention_obj_category/$', 'monitoring_product.views.get_save_mp_mention_obj_category'), #,берем создание категорий и упоминаемих обектов для мониторингового продукта
    url(r'^api/filter_products_from_project/$', 'monitoring_product.views.filter_products_from_project'),
    (r'^(?P<slug>.*)/$', 'main.views.index'),

    url(r'^message', TemplateView.as_view(template_name='email/message.html')),
    url(r'^messagetwo', TemplateView.as_view(template_name='email/messageTwo.html')),


) + staticfiles_urlpatterns()
