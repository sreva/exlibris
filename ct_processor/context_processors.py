# --*-- coding: utf-8 --*--
import json
from django.http import HttpResponse
from custom_user.models import CustomUser, Company
from main.models import DirectoryCategory
from django.contrib.auth.models import Group, User


def menu(request):
    path = request.META.get('PATH_INFO')
    if path == '/world-kaspersky/':
        analytic_item = 'show'
    elif path == '/vympelkom/':
        vympelkom_item = 'show'
    return locals()


def directory(request):
    fist_category_list = []
    def get_first_level(category_list):
        first_category = category_list.first()
        brothers = category_list.values('title', 'id')
        width = int(len(first_category.title)) + 100
        fist_category_list.append({'title': first_category.title, 'brothers':list(brothers), 'collapsed': 'true', 'width': 'auto' })
        if first_category.get_children():
            return get_first_level(first_category.get_children())
        else:
            return fist_category_list

    parents = DirectoryCategory.objects.filter(parent = None)

    fist_category_list = get_first_level(parents)
    categoryes = DirectoryCategory.objects.all()
    directory_date = []
    for elem in parents:

        directory_date.append({

            'title' : elem.title,
            'id': elem.id,
            'text': elem.text,
            'children':[]
        })
        for el in elem.get_children():
            directory_date[-1]['children'].append({
            'title' : el.title,
            'text': el.text,
            'id': el.id
            })
    category_list = {}
    for elem in DirectoryCategory.objects.all():
        list_obj=[]


        for par in elem.get_ancestors():

            lists = DirectoryCategory.objects.filter(parent = par.parent).values('title', 'id')
            obj = {'title' : par.title, 'brothers': list(lists)}
            list_obj.append(obj)


        lists = DirectoryCategory.objects.filter(parent = elem.parent).values('title', 'id')
        obj = {'title' : elem.title, 'brothers': list(lists)}
        list_obj.append(obj)


        def get_children_list(obj):
            print obj
            lists = DirectoryCategory.objects.filter(parent = obj.parent).values('title', 'id')

            obj_list = {'title' : obj.title, 'brothers': list(lists)}
            list_obj.append(obj_list)
            category = DirectoryCategory.objects.filter(parent = obj.parent)
            if category[0].get_children():
                return get_children_list(category[0].get_children())

        if elem.get_children():
            get_children_list(elem.get_children()[0])

        # obj['id']=elem.id
        # obj['list']=list_obj
        category_list[elem.id] = list_obj


    # print category_list, '-!!_!_!_!_!_!'


    category_list = json.dumps(category_list)
    directory_date = json.dumps(directory_date)
    fist_category_list = json.dumps(fist_category_list)
    return locals()


def user_permissions(request):
    context = {}

    u = request.user

    context["can_create_product"] = u.has_perm('custom_user.can_create_product')
    context["can_change_product"] = u.has_perm('custom_user.can_change_product')
    context["can_delete_product"] = u.has_perm('custom_user.can_delete_product')
    context["can_recovery_product"] = u.has_perm('custom_user.can_recovery_product')
    context["can_read_logs"] = u.has_perm('custom_user.can_read_logs')
    context["can_analityc_constructor"] = u.has_perm('custom_user.can_analityc_constructor')
    context["transformer"] = u.has_perm('custom_user.transformer')
    context["can_import_file"] = u.has_perm('custom_user.can_import_file')
    context["can_constructor_w0"] = u.has_perm('custom_user.can_constructor_w0')
    context["can_see_product"] = u.has_perm('custom_user.can_see_product')
    context["can_rate_the_product"] = u.has_perm('custom_user.can_rate_the_product')
    context["can_change_self_profile"] = u.has_perm('custom_user.can_change_self_profile')
    context["can_change_all_profile"] = u.has_perm('custom_user.can_change_all_profile')
    context["can_add_permission"] = u.has_perm('custom_user.can_add_permission')

    return {'permissions': json.dumps(context)}

def profile(request):
    # user = CustomUser.objects.get(id = request.user.id)
    #
    # if request.method == "POST":
    #     data= json.loads(request.body)
    #
    #
    #     for key, val in data.items():
    #         if key == 'password1':
    #             user.set_password(data['password1'])
    #         if key == 'group':
    #             for group in user.groups.all():
    #                 user.groups.remove(group)
    #             user.groups.add(Group.objects.get(id=int(val)))
    #
    #         else:
    #             print key, val, '------'
    #             setattr(user, key, val)
    #         user.save()
    #         return HttpResponse(json.dumps({
    #             'status': 'OK',
    #         }))
    # social_list = user.social_set.all().values()
    # social_list = json.dumps(list(social_list))
    # company = Company.objects.get(id= user.company.id)
    # group = user.groups.all().first()
    # departament_list = company.department_set.all().values()
    # departament_list = json.dumps(list(departament_list))
    # groups_list = Group.objects.all().values('name', 'id')
    # groups_list = json.dumps(list(groups_list))

    return locals()