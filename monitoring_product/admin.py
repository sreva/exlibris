from django.contrib import admin
from models import MonitoringProduct, MonitoringProductCategory, MonitoringProductMentionWord, MonitoringProductMention, ConstructorWidget, \
    MonitoringProductSetting
from form import MonitoringProductForm
from guardian.admin import GuardedModelAdmin


class MonitoringProductAdmin(GuardedModelAdmin):
    list_display = ('title', 'project', 'constructor_widget0_link', 'product_source',)
    form = MonitoringProductForm
    add_form_template = 'admin/add_form_monitoring.html'
    change_form_template = 'admin/add_form_monitoring.html'

    def save_model(self, request, obj, form, change):
        obj.save()
        # request.user.add(obj)

    class Media:
        js = (
            '/media/js/lib/jquery.min.js',
            '/static/admin/js/colorpicker/colpick.js',
            '/media/js/admin/admin_monitoring_product.js',

        )
    def constructor_widget0_link(self,obj):
        return u'<a href="/%s/monitoring_product/%d/widget0/constructor/">constructor widget0</a>' % (obj.project.slug ,obj.id)
    constructor_widget0_link.allow_tags = True
    constructor_widget0_link.short_description = "link constructor widget0"

    # def __init__(self,*args,**kwargs):
    #     super(MonitoringProductAdmin, self).__init__(*args, **kwargs)
    #     self.list_display_links = (None, )
admin.site.register(MonitoringProduct, MonitoringProductAdmin)

class MonitoringProductCategoryAdmin(admin.ModelAdmin):
    list_display = ['category', 'index_sort']


class MonitoringProductSettingAdmin(admin.ModelAdmin):
    list_display = ['user', 'monitoring_product',]
admin.site.register(MonitoringProductSetting, MonitoringProductSettingAdmin)

admin.site.register(MonitoringProductCategory, MonitoringProductCategoryAdmin)

class MonitoringProductMentionWordAdmin(admin.ModelAdmin):
    list_display = ('word', )


admin.site.register(MonitoringProductMentionWord, MonitoringProductMentionWordAdmin)

class MonitoringProductMentionAdmin(admin.ModelAdmin):
    pass

admin.site.register(MonitoringProductMention, MonitoringProductMentionAdmin)

#



class ConstructorWidgetAdmin(admin.ModelAdmin):
    pass
admin.site.register(ConstructorWidget, ConstructorWidgetAdmin)


