# --*-- coding: utf-8 --*--
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger#
from collections import OrderedDict
from itertools import groupby
import json
import mimetypes
import os
from django.core.mail import send_mail

from operator import or_
from django.contrib.auth.models import Permission
from django.contrib.contenttypes.models import ContentType
from django.core.cache import cache
from django.contrib.auth.decorators import user_passes_test, permission_required
import urllib
from django.core.exceptions import ObjectDoesNotExist, PermissionDenied
from django.db.models import Max, Min, Q, Count
from django.core import serializers
from django.http import HttpResponse, Http404, HttpResponseRedirect
from django.shortcuts import redirect, get_object_or_404
from django.template.response import TemplateResponse
import re
from django.contrib.auth.decorators import login_required
from custom_user.models import CustomUser
from braces.views import JSONResponseMixin
from django.views.generic import DetailView, View
from info import settings
from info.settings import BASE_DIR
from main.models import Project, Category, Article, MentionsObject, ReferredObject, Region, EmotionalToneObject
from monitoring_product.form import MonitoringProductFormStep2
# import django.utils.simplejson as json
from monitoring_product.models import MonitoringProduct, MonitoringProductCategory, MonitoringProductMention, MonitoringProductMentionWord, \
    ConstructorWidget, MonitoringProductSetting
from datetime import datetime, timedelta, date
import time
#from monitoring_product.serializers import ProjectSerializer, MonitoringProductSerializer
decorator_with_arguments = lambda decorator: lambda *args, **kwargs: lambda func: decorator(func, *args, **kwargs)
@decorator_with_arguments
def custom_permission_required(function, perm):
    def _function(request, *args, **kwargs):
        if request.user.has_perm(perm):
            return function(request, *args, **kwargs)
        else:
            return redirect ('/')
            # Return a response or redirect to referrer or some page of your choice
    return _function

def filter_products_from_project(request):
    project_id = request.POST.get('project_id', False)
    project = Project.objects.get(id = project_id)
    monitoring_products = project.monitoring_products.all().values_list('id', flat= True)
    # monitoring_products = json.loads(monitoring_products)
    return HttpResponse(json.dumps({
            'status': 'OK',
            'monitoring_products' : list(monitoring_products),
        }))


def check_valid_constructors_widget0(request):
    list_id = request.POST.get('list_id', False)
    project_id = request.POST.get('project_id', False)
    project = Project.objects.get(id = project_id)
    list_id = json.loads(list_id)
    constructors = project.constructorwidget_set.filter(monitoring_product__id__in = list_id)
    try:
        first = constructors[0].data
    except IndexError:
        first = False
    if first:
        constructors_data_list = [it.data for it in constructors if it.data == first]
        if len(constructors_data_list) == constructors.count():
            return HttpResponse(json.dumps({
                'status': 'OK',
                }))
        else:
            return HttpResponse(json.dumps({
                'status': 'ERROR',
                }))
    return HttpResponse(json.dumps({
            'status': 'OK',
            'message': 'ProjectDoesNotExistProducts',
            }))



def add_monitoring_product(request, id):
    form = MonitoringProductFormStep2
    return TemplateResponse(request, 'add_monitoring.html', locals())


def get_mention_object(request):
    id = request.POST['id']
    project = Project.objects.get(id=id)
    mention_objects_list = project.referred_objects.all()
    category_list = Category.objects.filter(project_id=id, parent=None)
    #mention_objects_list = serializers.serialize('json', mention_objects_list)
    #category_list = serializers.serialize('json', category_list)
    return HttpResponse(json.dumps({
        'status': 'OK',
        'mention_objects_list': list(mention_objects_list.values()),
        'category_list': list(category_list.values()),

    }))


def mp_mention_obj_category(request):
    data = request.POST['data'],
    data = json.loads(data[0])
    type = data['product']['type']
    title = data['product']['title']
    # day_start = data['product']['day_start']
    project = data['product']['project_name']
    # time_start = data['product']['time_start']
    product = MonitoringProduct.objects.get_or_create(type=type, title=title,
                                                      project=Project.objects.get(id = int(project)), \
                                                      )
    MonitoringProductCategory.objects.filter(monitoring_product=product[0]).delete()
    for key in data['category_list']:
        monitoring_category = MonitoringProductCategory.objects.create(category_id=int(key['id']),
                                                                       index_sort=int(key['index']), \
                                                                        monitoring_product=product[0])
    ment_prod_list = MonitoringProductMention.objects.filter(monitoring_product=product[0])
    for el in ment_prod_list:
        MonitoringProductMentionWord.objects.filter(monitoring_product_mention=el).delete()
        el.delete()
    for key in data['mention_objects_list']:

        monitoring_product_mention = MonitoringProductMention.objects.create(color=key['color'],
                                                                             monitoring_product=product[0], \
                                                                             mention_object_id=int(key['id']))
        for elem in key['keywords']:
            word = MonitoringProductMentionWord.objects.create(word=elem,
                                                               monitoring_product_mention=monitoring_product_mention)
    return HttpResponse(json.dumps({

        'status': 'OK',

    }))


def mp_mention_obj_category_update(request):

    data = request.POST['data'],
    data = json.loads(data[0])
    type = data['product']['type']
    id = data['id']
    title = data['product']['title']
    # day_start = data['product']['day_start']
    project = data['product']['project_name']
    # time_start = data['product']['time_start']
    product = MonitoringProduct.objects.get(id = id)
    product.type = type
    product.title = title
    product.settings = json.loads(data['product']['settings'])
    product.article_count = data['product']['article_count']
    product.changed_by = request.user
    product.changed= datetime.now()
    # product.day_start = day_start
    product.project_id = int(project)
    # product.time_start = time_start
    product.save()
    # MonitoringProductCategory.objects.filter(monitoring_product=product).delete()

    for key in data['category_list']:
        monitoring_category = MonitoringProductCategory.objects.create(category_id=int(key['id']),
                                                                       index_sort=int(key['index']), \
                                                                       monitoring_product=product)
    ment_prod_list = MonitoringProductMention.objects.filter(monitoring_product=product)
    for el in ment_prod_list:
        MonitoringProductMentionWord.objects.filter(monitoring_product_mention=el).delete()
        el.delete()
    for key in data['mention_objects_list']:


        monitoring_product_mention = MonitoringProductMention.objects.create(color=key['color'],
                                                                             monitoring_product=product, \
                                                                             mention_object_id=int(key['id']))
        for elem in key['keywords']:
            word = MonitoringProductMentionWord.objects.create(word=elem,
                                                               monitoring_product_mention=monitoring_product_mention)
    return HttpResponse(json.dumps({

        'status': 'OK',

    }))


def get_save_mp_mention_obj_category(request):
    id = request.POST['id']
    monitoring_product = MonitoringProduct.objects.get(id=id)
    monitoring_product_mention = monitoring_product.monitoring_product_mentions.all()
    project = Project.objects.get(id=monitoring_product.project.id)
    mention_objects_list = project.referred_objects.all()
    mention_objects_list_to_template = []
    for elem in mention_objects_list:
        keywords = {}
        checked = False
        color = "666666"

        for el in monitoring_product_mention:
            if elem.id == el.mention_object.id:
                color = el.color
                checked = True
                keywords = el.monitoring_product_mention_words.all()
            #keywords = serializers.serialize('json', keywords)
        mention_objects_list_to_template.append(
            {'id': elem.id, 'title': elem.title, 'checked': checked, 'color': color,
             'keywords': list(keywords.values())})
    monitoring_product_category = monitoring_product.monitoring_product_category.all()

    category_list_to_template = []
    for el in monitoring_product_category:
        category_list_to_template.append({'id': el.category.id, 'index': el.index_sort, 'title': el.category.title})


    return HttpResponse(json.dumps({
        'status': 'OK',
        'mention_objects_list': mention_objects_list_to_template,
        'category_list': category_list_to_template,
    }))


def __get_mention_object_dict(object, color, keywords, *settings):
    full_row_list = {'id':object.id,
            'title': object.referred_object.title if object.referred_object else '',
            'prt':object.prt if object.prt else False,'prv':object.prv if object.prv else False ,
            'emotional_tone_object':object.emotional_tone_object.title if object.emotional_tone_object else False,
            'speakers':[],
                    'color':color,
        'keywords':keywords,
        'object_role':object.object_role.title if object.object_role else '',
    }
    custom_row_list = {
        'color':color,
        'keywords':keywords,
        'object_role':object.object_role.title if object.object_role else '',

    }
    for elem in settings[0]:
        if elem in full_row_list.keys():
            custom_row_list[elem] = full_row_list[elem]
    return full_row_list


def get_date_range(date_start, date_end):
    result = [date_start, date_end]
    return result

mapping_dict = {

}

def __get_article_dict(article, data_type = '1', *settings):
    if isinstance(article, Article):
        if data_type =='1':
            text = article.description
            paragraph = text.split('\n')
            description = ''
            for elem in paragraph:
                description += '<p>'+elem+'</p>'

            date = datetime.strftime(article.date + timedelta(hours=4), '%d-%m-%Y')
            if date == "None":
                date = None
            try:
                mo = float(article.mo)
            except:
                mo=0

            full_row_list = {'title': article.title.title if article.title else '',
                     'id': article.id,
                     'date': date,
                     'mo':mo,
                     'url': article.url.title if article.url else '',
                     'source': article.source.title if article.source else '',
                     'annotation': article.annotation,
                     'description': article.description,
                     'federal_district': article.federal_district.title if article.federal_district else '',
                     'region': article.region.title if article.region else '',
                     'country': article.country.title if article.country else '',
                     'city': article.city.title if article.city else '' ,
                     'author': article.author.title if article.author else '' ,
                     'media_level':article.media_level.title if article.media_level else '',
                     'media_category': article.media_category.title if article.media_category else '',
                     'media_type': article.media_type.title if article.media_type else '',
                     }
            custom_row_list = {'id': article.id,'mention_objects':[]}

            for elem in settings[0]:
                if elem in full_row_list.keys():
                    custom_row_list[elem] = full_row_list[elem]

            return custom_row_list



        elif data_type == '2':
            pass
            # hash_tags = article.hash_tags.all().values('title','id').distinct().order_by('title'),
            # text = article.description
            # paragraph = text.split('\n')
            # description = ''
            # for elem in paragraph:
            #     description += '<p>'+elem+'</p>'
            # date = datetime.strftime(article.date, '%d-%m-%Y')
            # release_time = datetime.strftime(article.release_time, '%H-%M')
            # mo = float(article.mo)
            # if date == "None":
            #     date = None
            # return {
            #     'title': article.title.title if article.title else '',
            #     'id': article.id,
            #     'user_image': article.material_reason if article.material_reason else '',
            #     'logo_image': article.bissines_direction if article.bissines_direction else '',
            #     'top_image': article.media_list.title if article.media_list else '',
            #     'author': article.author.title if article.author else '',
            #     'author_type' : article.material_genre.title if article.material_genre else '',
            #     'date' : date,
            #     'release_time': release_time,
            #     'url': article.url.title if article.url else '',
            #     'mo':mo,
            #     'type' : article.media_type.title if article.media_type else '',
            #     'mention_objects':[],
            #     'annotation': article.annotation,
            #     'description': description,
            #     'author_link' : article.edge_region.title if article.edge_region else '' ,
            #     'hash_tags' : hash_tags
            #
            # }


@login_required(login_url='/login/')
# @custom_permission_required('custom_user.can_see_product', )
def monitoring_product_api(request, id):

    monitoring_product_item = MonitoringProduct.objects.get(id=id)
    monitoring_product_source = monitoring_product_item.parent or monitoring_product_item
    monitoring_product = []
    monitoring_product_template = []
    monitoring_product_template = []
    monitoring_product_type = monitoring_product_item.type
    project_id = monitoring_product_item.project.id
    project= Project.objects.get(id = project_id)
    try:
        article_last = monitoring_product_item.get_article_set().latest('date_upload')
    except:
        article_last = False
    referred_objects_dict = []
    days =1

    if article_last:
        if monitoring_product_type == "day":
            days = 1
            date_start = article_last.date_upload  - timedelta(days = 1)
            date_end = article_last.date_upload
        elif monitoring_product_type == "week":
            days = 7
            date_start = article_last.date_upload  - timedelta(weeks = 1)
            date_end = article_last.date_upload
        elif monitoring_product_type == "month":
            days = 30
            date_start = article_last.date_upload  - timedelta(days = 30)
            date_end = article_last.date_upload
        elif monitoring_product_type == "date_release" or monitoring_product_type == "date_upload":
            date_start = monitoring_product_item.time_from
            date_end = monitoring_product_item.time_to
            filters = {'date_release': {'date__lte': date_end, 'date__gte': date_start},
                       'date_upload': {'date_upload__lte': date_end, 'date_upload__gte': date_start},
                      }
        next_id = False
        count = 0
        all = False
        menu_category = False
        if request.method == "POST":

            date= json.loads(request.body)
            date_start = date.get('date_start', False)
            all = date.get('all', False)
            if date_start:
                date_lte = datetime.strptime(date_start, '%d-%m-%Y')
                date_lte = date_lte + timedelta(days = days)
                date_start = date_lte
                date_end = date_start + timedelta(days = days)

            else:
                date_lte = date.get('date_end')
                date_lte = datetime.strptime(date_lte, '%d-%m-%Y')
                date_end = date_lte
                date_start = date_end - timedelta(days = days)

            # h = article_last.date_upload.hour + 4
            # m = article_last.date_upload.minute + 1
            next_id = date.get('last_id', False)
            count = date.get('count_end',False)
            try:
                date_lte += timedelta(hours=23, minutes=59 )
                article_last = project.articles.filter(date_upload__lte = date_lte).latest('date_upload')
                date_end = article_last.date_upload
                date_start = date_end - timedelta(days = days)
            except:
                pass

            # date_start = datetime.strptime(date_start, '%d-%m-%Y')
            # date_start = date_start + timedelta(hours=h) + timedelta(minutes=m)
            # all = date.get('all', False)
            # date_end = date['date_end']
            # date_end = datetime.strptime(date_end, '%d-%m-%Y')
            #
            # date_end = date_end + timedelta(hours=h) + timedelta(minutes=m)
        monitoring_product_mention_objects = monitoring_product_item.monitoring_product_mentions.all()


        monitoring_product_category = monitoring_product_item.monitoring_product_category.all()

        # date_end -= timedelta(days=1)
        # date_start -= timedelta(days=1)
        date_start_to_template = date_start.strftime("%d-%m-%Y")

        date_end_to_template = date_end.strftime("%d-%m-%Y")
        time_end_to_template = date_end.strftime("%H-%M-%S")
        time_start_to_template = date_start.strftime("%H-%M-%S")
        # datetime_end_to_template = date_end.strftime("%d-%m-%Y-%H-%M")
        if monitoring_product_type == "date_release" or monitoring_product_type == "date_upload":
            article_filter_list_id = monitoring_product_item.get_article_set().filter(**filters[monitoring_product_type])
        else:
            article_filter_list_id = monitoring_product_item.get_article_set().filter(date_upload = date_end)
        referred_list_id = monitoring_product_item.mentionsobject_set.filter(article__id__in = article_filter_list_id).values_list('referred_object__id', flat=True).distinct()
        mentions_list = monitoring_product_item.monitoring_product_mentions.values_list('mention_object__id', flat = True).distinct()
        try:
            positive_id = monitoring_product_item.emotionaltoneobject_set.get(title=u'позитивное').id
        except:
            positive_id = None
        try:
            neutral_id = monitoring_product_item.emotionaltoneobject_set.get(title=u'нейтральное').id
        except:
            neutral_id = None
        try:
            negative_id = monitoring_product_item.emotionaltoneobject_set.get(title=u'негативное').id
        except:
            negative_id = None
        all_cont = 0
        for elem in mentions_list:
            project_mentions_list = monitoring_product_item.mentionsobject_set.filter(referred_object_id = elem)
            referred_object = ReferredObject.objects.get(id = elem)

            positive_count = project_mentions_list.filter(article__id__in = article_filter_list_id, emotional_tone_object__title = u'позитивное').count()
            neutral_count = project_mentions_list.filter(article__id__in = article_filter_list_id, emotional_tone_object__title = u'нейтральное').count()
            negative_count = project_mentions_list.filter(article__id__in = article_filter_list_id, emotional_tone_object__title = u'негативное').count()
            all_cont = project_mentions_list.filter(article__id__in = article_filter_list_id).count()
            referred_objects_dict.append({'title':referred_object.title,  'reffered_object_id' : referred_object.id, 'object_count' :{
                'positive' :{'count': positive_count,'id': positive_id},
                'neutral' :{'count': neutral_count,'id': neutral_id},
                'negative' :{'count': negative_count,'id': negative_id},
            }, 'count': all_cont })
        monitoring_product.append(
            {'title': monitoring_product_item.title, 'parent_id': monitoring_product_item.parent_id,
             'users_count': monitoring_product_item.users_with_perm.count(), 'type': monitoring_product_item.type,
             'date_start': date_start_to_template, 'date_end': date_end_to_template,
             'referred_objects': referred_objects_dict, 'time_end': time_end_to_template,
             'time_start': time_start_to_template,
             'created_by': "{0} {1}".format(monitoring_product_item.created_by.first_name, monitoring_product_item.created_by.last_name),
             'created': monitoring_product_item.created.strftime("%d.%m.%Y %H:%M") if monitoring_product_item.created else '' ,
             'changed_by': "{0} {1}".format(monitoring_product_item.changed_by.first_name, monitoring_product_item.changed_by.last_name),
             'changed': monitoring_product_item.changed.strftime("%d.%m.%Y %H:%M") if monitoring_product_item.changed else '',
             })
        next = False
        article_id = 0
        if next_id:
            article_id = next_id
            next = True
        last_id = None
        category_id_list = article_filter_list_id.values_list('category__id', flat = True).distinct()
        category_id = []
        for elem in category_id_list:

            category = Category.objects.get(id = elem)
            ancestors = category.get_ancestors().values_list('id', flat = True)
            category_id += ancestors

            # category_id += elem


        category_id = category_id + list(category_id_list)

        category_id = list(set(category_id))
        count_end = count + monitoring_product_item.article_count
        # 'nezabyt6 izmenit6 -------------------------------------------------------------'
        count_end = count + count_end

        if all:
            count_end = 1000
        #level 1-------------------------------------------------------
        articles_count = 0
        flag = False
        category = monitoring_product_category.filter(category_id__in = category_id).order_by('index_sort')

        date_end = date_end

        # try:
        settings = MonitoringProductSetting.objects.get(user = request.user, monitoring_product = MonitoringProduct.objects.get(id=id))
        # except ObjectDoesNotExist:
            # settings = MonitoringProductSetting.objects.create(user = request.user, monitoring_product = MonitoringProduct.objects.get(id=id), setting = '')
        template_settings = settings.setting
        settings_from_article = []
        settings_from_mention_object = []
        settings = json.loads(settings.setting)
        for key ,val in settings.items():
            if key=='main_list' or key =='media_list' or key =='geography_list':
                for elem in val:
                    settings_from_article.append(elem['name'])
            elif key=='mention_list' or 'indicators_list':
                for elem in val:
                    settings_from_mention_object.append(elem['name'])

        # for elem in settings.get('geography_list'):
        #     settings_from_article.append(elem['name'])
        # for elem in settings.get('geography_list'):
        #     settings_from_article.append(el
        # em['name'])
        if monitoring_product_item.parent_id is None:
            monitoring_product_item.settings = {}
        print category, '-----'
        if count_end or all:
            for elem in category:
                #monitoring_product_category = MonitoringProductCategory.objects.get(category__id = elem.id)
                monitoring_product_template.append({'id': elem.category.id,'title' : elem.category.title,'index_sort': elem.index_sort , 'children':[], 'articles':[],})
                if monitoring_product_type == "date_release" or monitoring_product_type == "date_upload":
                    articles = elem.category.article_set(manager='filtered_objects').filtered_by(monitoring_product_item.settings).all().filter(**filters[monitoring_product_type]).filter(monitoring_product = monitoring_product_source)
                else:
                    articles = elem.category.article_set(manager='filtered_objects').filtered_by(monitoring_product_item.settings).all().filter(date_upload = date_end, monitoring_product = monitoring_product_source)
                articles_count += articles.count()
                #articles 1------------------------------------------------------------------
                for article in articles:
                    if next and article.id == article_id or all:
                        flag = True
                    if flag or not next or all:

                        if count < count_end or all:
                            if article.id == article_id:
                                pass
                            else:
                                last_id = article.id
                                count += 1
                                monitoring_product_template[-1]['articles'].append(__get_article_dict(article, monitoring_product_item.data_type, settings_from_article))
                                mention_object = article.mention_objects.all()
                                #mention_object 1----------------------------------------------------------
                                for object in mention_object:
                                    try:
                                        monitoring_product_mention = monitoring_product_mention_objects.get(mention_object_id =  object.referred_object.id)
                                        #monitoring_product_mention = MonitoringProductMention.objects.get(mention_object = object.referred_object)

                                        monit_obj = True
                                    except:
                                        monit_obj = False
                                    if monit_obj:
                                        color = monitoring_product_mention.color
                                        keywords = monitoring_product_mention.monitoring_product_mention_words.all()
                                        keywords_list = []
                                        for word in keywords:
                                            keywords_list.append(word.word)
                                        monitoring_product_template[-1]['articles'][-1]['mention_objects'].append(__get_mention_object_dict(object, color, keywords_list, settings_from_mention_object))
                                        speaker_list = object.statementspeakers.all()
                                        #speaker 1 --------------------------------------------------------------------------------------------
                                        try:
                                            for speaker in speaker_list:
                                                monitoring_product_template[-1]['articles'][-1]['mention_objects'][-1]['speakers'].append({'fio':speaker.speaker.fio})
                                        except KeyError:
                                            pass
                                        side_speaker_list = object.sidespeaker_set.all()

                                        #side_speaker 1 --------------------------------------------------------------------------------------------
                                        for side_speaker in side_speaker_list:
                                                monitoring_product_template[-1]['articles'][-1]['mention_objects'][-1]['side_speakers'].append({'fio':side_speaker.fio})
                        else :
                            flag = False
                #level 2-------------------------------------------------------
                for ele in elem.category.get_children().filter(id__in = category_id):
                    monitoring_product_template[-1]['children'].append({'id': ele.id,'title': ele.title, 'children':[], 'articles':[],})
                    if monitoring_product_type == "date_release" or monitoring_product_type == "date_upload":
                        articles = ele.article_set(manager='filtered_objects').filtered_by(monitoring_product_item.settings).all().filter(**filters[monitoring_product_type]).filter(monitoring_product = monitoring_product_source)
                    else:
                        articles = ele.article_set(manager='filtered_objects').filtered_by(monitoring_product_item.settings).all().filter(date_upload = date_end, monitoring_product = monitoring_product_source)
                    articles_count += articles.count()
                    #articles 2------------------------------------------------------------------
                    for article in articles:
                        if next and article.id == article_id or all:
                            flag = True
                        if flag or  not next or all:
                            if count < count_end or all:
                                if article.id == article_id:
                                    pass
                                else:
                                    last_id = article.id
                                    count += 1
                                    monitoring_product_template[-1]['children'][-1]['articles'].append(__get_article_dict(article, monitoring_product_item.data_type, settings_from_article))
                                    mention_object = article.mention_objects.all()
                                    #mention_object 2----------------------------------------------------------
                                    for object in mention_object:
                                        try:
                                            monitoring_product_mention = monitoring_product_mention_objects.get(mention_object =  object.referred_object)
                                            #monitoring_product_mention = MonitoringProductMention.objects.get(mention_object = object.referred_object)

                                            monit_obj = True
                                        except:
                                            monit_obj = False
                                        if monit_obj:
                                            color = monitoring_product_mention.color
                                            keywords = monitoring_product_mention.monitoring_product_mention_words.all()
                                            keywords_list = []
                                            for word in keywords:
                                                keywords_list.append(word.word)
                                            monitoring_product_template[-1]['children'][-1]['articles'][-1]['mention_objects'].append(__get_mention_object_dict(object, color, keywords_list, settings_from_mention_object))
                                            speaker_list = object.statementspeakers.all()
                                            #speaker 2 --------------------------------------------------------------------------------------------
                                            try:
                                                for speaker in speaker_list:
                                                    monitoring_product_template[-1]['children'][-1]['articles'][-1]['mention_objects'][-1]['speakers'].append({'fio':speaker.speaker.fio})
                                            except KeyError:
                                                pass
                                            side_speaker_list = object.sidespeaker_set.all()
                                            #side_speaker 2 --------------------------------------------------------------------------------------------
                                            for side_speaker in side_speaker_list:
                                                monitoring_product_template[-1]['children'][-1]['articles'][-1]['mention_objects'][-1]['side_speakers'].append({'fio':side_speaker.fio})
                            elif count == count_end:
                                flag = False
                    #level 3-------------------------------------------------------
                    for el in ele.get_children().filter(id__in = category_id):
                        monitoring_product_template[-1]['children'][-1]['children'].append({'id': el.id,'title': el.title, 'children':[], 'articles':[],})
                        if monitoring_product_type == "date_release" or monitoring_product_type == "date_upload":
                            articles = el.article_set(manager='filtered_objects').filtered_by(monitoring_product_item.settings).all().filter(**filters[monitoring_product_type]).filter(monitoring_product = monitoring_product_source)
                        else:
                            articles = el.article_set(manager='filtered_objects').filtered_by(monitoring_product_item.settings).all().filter(date_upload = date_end, monitoring_product = monitoring_product_source)
                        articles_count += articles.count()
                        #articles 3------------------------------------------------------------------
                        for article in articles:
                            if next and article.id == article_id or all:
                                flag = True
                            if flag or  not next or all:
                                if count < count_end or all:
                                    if article.id == article_id:
                                        pass
                                    else:
                                        last_id = article.id
                                        count += 1
                                        monitoring_product_template[-1]['children'][-1]['children'][-1]['articles'].append(__get_article_dict(article, monitoring_product_item.data_type, settings_from_article))
                                        mention_object = article.mention_objects.all()
                                        #mention_object 3----------------------------------------------------------
                                        for object in mention_object:
                                            try:
                                                monitoring_product_mention = monitoring_product_mention_objects.get(mention_object =  object.referred_object)
                                                #monitoring_product_mention = MonitoringProductMention.objects.get(mention_object = object.referred_object)
                                                monit_obj = True
                                            except:
                                                monit_obj = False
                                            if monit_obj:
                                                color = monitoring_product_mention.color
                                                keywords = monitoring_product_mention.monitoring_product_mention_words.all()
                                                keywords_list = []
                                                for word in keywords:
                                                    keywords_list.append(word.word)
                                                monitoring_product_template[-1]['children'][-1]['children'][-1]['articles'][-1]['mention_objects'].append(__get_mention_object_dict(object, color, keywords_list, settings_from_mention_object))
                                                speaker_list = object.statementspeakers.all()
                                                #speaker 3 --------------------------------------------------------------------------------------------
                                                try:
                                                    for speaker in speaker_list:
                                                        monitoring_product_template[-1]['children'][-1]['children'][-1]['articles'][-1]['mention_objects'][-1]['speakers'].append({'fio':speaker.speaker.fio})
                                                except KeyError:
                                                    pass
                                                side_speaker_list = object.sidespeaker_set.all()
                                                #side_speaker 3 --------------------------------------------------------------------------------------------
                                                for side_speaker in side_speaker_list:
                                                    monitoring_product_template[-1]['children'][-1]['children'][-1]['articles'][-1]['mention_objects'][-1]['side_speakers'].append({'fio':side_speaker.fio})
                                elif count == count_end:
                                    flag = False
                        #level 4-------------------------------------------------------
                        for e in el.get_children().filter(id__in = category_id):
                            monitoring_product_template[-1]['children'][-1]['children'][-1]['children'].append({'id': e.id,'title': e.title, 'articles':[],})
                            if monitoring_product_type == "date_release" or monitoring_product_type == "date_upload":
                                articles = e.article_set(manager='filtered_objects').filtered_by(monitoring_product_item.settings).all().filter(**filters[monitoring_product_type]).filter(monitoring_product=monitoring_product_source)
                            else:
                                articles = e.article_set(manager='filtered_objects').filtered_by(monitoring_product_item.settings).all().filter(date_upload=date_end, monitoring_product=monitoring_product_source)
                            articles_count += articles.count()
                            #articles 4------------------------------------------------------------------
                            for article in articles:
                                if next and article.id == article_id or all:
                                    flag = True
                                if flag or  not next or all:
                                    if count < count_end or all:
                                        if article.id == article_id:
                                            pass
                                        else:
                                            last_id = article.id
                                            count += 1
                                            monitoring_product_template[-1]['children'][-1]['children'][-1]['children'][-1]['articles'].append(__get_article_dict(article, monitoring_product_item.data_type, settings_from_article))
                                            mention_object = article.mention_objects.all()
                                            #mention_object 4----------------------------------------------------------
                                            for object in mention_object:
                                                try:
                                                    monitoring_product_mention = monitoring_product_mention_objects.get(mention_object =  object.referred_object)
                                                    #monitoring_product_mention = MonitoringProductMention.objects.get(mention_object = object.referred_object)
                                                    monit_obj = True
                                                except:
                                                    monit_obj = False
                                                if monit_obj:
                                                    color = monitoring_product_mention.color
                                                    keywords = monitoring_product_mention.monitoring_product_mention_words.all()
                                                    keywords_list = []
                                                    for word in keywords:
                                                        keywords_list.append(word.word)
                                                    monitoring_product_template[-1]['children'][-1]['children'][-1]['children'][-1]['articles'][-1]['mention_objects'].append(__get_mention_object_dict(object, color, keywords_list, settings_from_mention_object))
                                                    speaker_list = object.statementspeakers.all()
                                                    #speaker 4 --------------------------------------------------------------------------------------------
                                                    try:
                                                        for speaker in speaker_list:
                                                            monitoring_product_template[-1]['children'][-1]['children'][-1]['children'][-1]['articles'][-1]['mention_objects'][-1]['speakers'].append({'fio':speaker.speaker.fio})
                                                    except KeyError:
                                                        pass
                                                    side_speaker_list = object.sidespeaker_set.all()
                                                    #side_speaker 4 --------------------------------------------------------------------------------------------
                                                    for side_speaker in side_speaker_list:
                                                        monitoring_product_template[-1]['children'][-1]['children'][-1]['children'][-1]['articles'][-1]['mention_objects'][-1]['side_speakers'].append({'fio':side_speaker.fio})
                                    elif count == count_end:
                                        flag = False
        return HttpResponse(json.dumps({
            'status': 'OK',
            'monitoring_product_template':monitoring_product_template,
            'product': monitoring_product,
            'last_id' : last_id,
            'count_end' : count_end,
            'articles_count':article_filter_list_id.count(),
            'template_settings': template_settings
        }))
    return HttpResponse(json.dumps({
            'status': 'OK',
            'monitoring_product_template':monitoring_product_template,
            'product': monitoring_product,
            'last_id' : 0,
            'count_end' : 0,
            'articles_count':0
        }))


def get_float_value(element):
    try:
        element = float(element)
    except:
        element = None
    return element



def monitoring_product(request, id, slug):
    project = Project.objects.get(slug = slug)

    try:
        settings = MonitoringProductSetting.objects.get(user = request.user, monitoring_product = MonitoringProduct.objects.get(id=id))
    except ObjectDoesNotExist:

        settings = MonitoringProductSetting.objects.create(user = request.user, monitoring_product = MonitoringProduct.objects.get(id=id))

    template_settings = settings.setting
    monitoring_products = project.monitoring_products.all()
    try:
        monitoring_product = monitoring_products.get(id = id)
    except MonitoringProduct.DoesNotExist:
        raise Http404

    # if not monitoring_product.users_with_perm.filter(pk=request.user.pk).exists():
    #     raise PermissionDenied

    monitoring_products = monitoring_products.exclude(id = id)


    if monitoring_product.data_type == '1':
        return TemplateResponse(request, 'monitoring_product/monitoring_product.html', locals())
    else :
        return TemplateResponse (request, 'monitoring_product/monitoring_product_social_media.html', locals())




def monitoring_product_test_test(request, id, slug):
    project = Project.objects.get(slug = slug)
    monitoring_products = project.monitoring_products.all()
    try:
        monitoring_product = monitoring_products.get(id = id)
    except MonitoringProduct.DoesNotExist:
        raise Http404
    monitoring_products = monitoring_products.exclude(id = id)


    # return HttpResponseRedirect ('/'+slug+'/login/')



def __get_mention_object_dict_0(object):
    return {'id':object.id, 'title': object.referred_object.title if object.referred_object.title else '','prt':object.prt,'prv':object.prv ,'emotional_tone_object':object.emotional_tone_object.title,\
    'object_role':object.object_role.title if object.object_role.title else '', 'speakers':[], 'side_speakers':[]}


def get_date_range(date_start, date_end):
    result = [date_start, date_end]
    return result


def get_filter (obj):
    return obj


def __get_article_dict_0(article):
    if isinstance(article, Article):
        text = article.description
        paragraph = text.split('\n')
        description = ''
        for elem in paragraph:
            description += '<p>'+elem+'</p>'
        date = '%s'%article.date
        if date == "None":
            date = None
        mo = float(article.mo)
        return  {'title': article.title, 'id': article.id, 'date': date,'release_time': article.release_time ,'url': article.url.title, 'source': article.source.title, \
        # 'annotation': article.annotation,'description': description  ,'mo':mo,'federal_district': article.federal_district.title if article.federal_district else '', \
        # 'region': article.region.title if article.region else '','country': article.country.title if article.country else '','city': article.city,'author': article.author,'media_level':article.media_level.title if article.media_level else '',\
        'media_category': article.media_category.title if article.media_category else '', 'media_type': article.media_type.title if article.media_type else '', 'mention_objects':[]}


def normalize_query(query_string,
                    findterms=re.compile(r'"([^"]+)"|(\S+)').findall,
                    normspace=re.compile(r'\s{2,}').sub):
    ''' Splits the query string in invidual keywords, getting rid of unecessary spaces
        and grouping quoted words together.
        Example:

        >>> normalize_query('  some random  words "with   quotes  " and   spaces')
        ['some', 'random', 'words', 'with quotes', 'and', 'spaces']

    '''
    return [normspace(' ', (t[0] or t[1]).strip()) for t in findterms(query_string)]


def get_query(query_string):

    f = query_string
    g = {i: [dicts[1] for dicts in lst] for (i, lst) in groupby(sorted([(key.replace('__id__in', '').replace('__isnull', ''), (key, f[key]), ) for key in f.keys()], key=lambda x: x[0]), key=lambda x: x[0])}
    query = Q(*[reduce(or_, [Q(i) for i in g[key]]) for key in g.keys()])

    return query

keys_filter_from_article = ['cover__range', 'km_result', 'release_time', 'mo__range','infographics__range','duration__id__in', 'day__id__in', 'week__id__in', 'month__id__in', 'media_level__id__in', 'federal_district__id__in', 'country__id__in', 'media_view__id__in','media_list__id__i','media_category__id__in',
                            'material_genre__id__in','image__id__in','media_type__id__in','interaction_kind__id__in','penetration__id__in','visibility__id__in','macroregion__id__in',
                            'bissines_direction__id__in','city__id__in','source__id__in','author__id__in','branch__id__in','media_list_special__id__in','media_type_special__id__in',
                            'media_category_special__id__in','visibility_special__id__in','km_special__id__in','brand_or_product__id__in','speaker_quote__id__in','third_patry_speakers__id__in',
                            'headder_tonality__id__in','main_tonality__id__in','global_local_event__id__in','emotional_tone_material__id__in','emotional_tone_title__id__in',
                            'url__id__in','thematic_category__id__in','title__id__in' ,'material_reason__id__in','id_press_release__id__in','tag__id__in','edge_region__id__in',]


# filters_from__mentions__object__________________________________

keys_filter_from_mention_object = ['referred_object__id__in','referred_object_id','emotional_tone_object__id' ,'emotional_tone_object__id__in','emotional_tone_title__id__in',
                                       'object_role__id__in','technologies__id__in','advertisement_promo__id__in','initiation__id__in', 'info_reason__id__in', 'prt__range', 'prv__range']


def get_widget0_data(request, slug, id):
    def get_round(value, decimal):
        if value:
            value = round(value, decimal)
        return value
    article_id_list = []
    project = Project.objects.get(slug = slug)
    articles_on_page = 10
    monitoring_product_item = MonitoringProduct.objects.get(id=id)
    monitoring_product_source = monitoring_product_item.parent or monitoring_product_item
    custom_filters = {}
    for key in monitoring_product_item.settings.keys():

        if monitoring_product_item.settings[key]['settings']['a9'] == u'шкала':
            param = monitoring_product_item.settings[key]['list']
            if None in param:
                param = False
        else:
            param = [x['id']
                     for x in monitoring_product_item.settings[key]['list'] if x.get('selected', True)]
            if not param or len(param) == len(monitoring_product_item.settings[key]['list']):
                continue
        if key in keys_filter_from_mention_object:
            key = "%s" % key
        elif key.startswith('speaker'):
            continue
        if param:
            custom_filters[key] = param
    # print custom_filters, 'custom_filters'
    articles = monitoring_product_item.get_article_set()(manager='filtered_objects').filtered_by(monitoring_product_item.settings).all()
    mention_objects_filter =  project.mentionsobject_set.all()
    refered_object_id = monitoring_product_item.monitoring_product_mentions.values_list('mention_object__id', flat = True).distinct().order_by('id')

    prv_max = get_round(mention_objects_filter.aggregate(Max('prv')).values()[0],2)

    prv_min = get_round(mention_objects_filter.aggregate(Min('prv')).values()[0],2)
    mo_max = articles.aggregate(Max('mo')).values()[0]
    mo_min = articles.aggregate(Min('mo')).values()[0]
    prt_max = get_round(mention_objects_filter.aggregate(Max('prt')).values()[0],2)
    prt_min = get_round(mention_objects_filter.aggregate(Min('prt')).values()[0],2)
    # mq
    mq_max = get_round(mention_objects_filter.aggregate(Max('mq')).values()[0],2)
    mq_min = get_round(mention_objects_filter.aggregate(Min('mq')).values()[0],2)
    # filters_from__speaker___________________________________________
    keys_filters_from_speaker = ['speaker_category__id__in', 'speaker_level__id__in', 'speaker_quote_uniqueness__id__in', 'speaker__id__in',]
    keys_filters_from_side_speaker = ['side__speaker__id__in', 'side_speaker_tonality__id__in', 'side_speaker_status__id__in',]
    keys_filters_from_km = ['km__id__in',]
    keys_filters_from_statement_speaker = ['speaker_speech_hate__id__in',]

    monitoring_product = []
    monitoring_product_type = monitoring_product_item.type
    now = datetime.now()
    date_start = now - timedelta(minutes=now.minute, hours= now.hour, seconds= now.second)
    date_end = datetime.now()
    article_id_list_filter = False
    get_filters = request.GET.get('type', False)
    # try:
    # if not get_filters:
    #     date_type  = "date"

    if request.method == "POST" or request.method == "GET":
        if request.method == "GET":
            page = request.GET.get('page')
            if get_filters:
                date_type = get_filters
            else :
                date_type  = "date"
            data= request.GET
            filter = data
            date_start = data['date_start']
            date_start = datetime.strptime(date_start, '%d-%m-%Y-%H-%M-%S')
            date_end = data['date_end']
            date_end = datetime.strptime(date_end, '%d-%m-%Y-%H-%M-%S') + timedelta(hours=5)
        elif request.method == "POST":
            data= json.loads(request.body)
            filter = json.loads(data['data'])
            date_start = data['date_start']
            date_type = data['date_type']
            page = data['page']
            date_start = datetime.strptime(date_start, '%d-%m-%Y-%H-%M-%S')
            date_end = data['date_end']
            date_end = datetime.strptime(date_end, '%d-%m-%Y-%H-%M-%S')  + timedelta(hours=5)
        arguments = {}
        arguments_object = {}
        arguments_speaker = {}
        arguments_statemen = {}
        arguments_side_speaker = {}
        arguments_km = {}
        for k, v in filter.items():
            if k in keys_filter_from_article:
                arguments[k] = v
            elif v and k in keys_filter_from_mention_object:
                arguments_object[k] = v
            elif v and k in keys_filters_from_statement_speaker:
                arguments_statemen[k]=v
            elif v and k in keys_filters_from_speaker:
                arguments_speaker[k] = v
            elif v and k in keys_filters_from_side_speaker:
                arguments_side_speaker[k] = v
            elif v and k in keys_filters_from_km:
                arguments_km[k] = v
            elif k =='date_start' or k =='date_end' or k =='type':
                pass
        # print arguments_object, 'arguments_object'
        # print arguments_statemen, 'arguments_statemen'
        # print arguments, 'arguments'
        # print arguments_speaker, 'arguments_speaker'
        # print arguments_side_speaker, 'arguments_side_speaker'
        arguments =  get_query(arguments)
        arguments_object = get_query(arguments_object)
        arguments_statemen = get_query(arguments_statemen)
        arguments_speaker = get_query(arguments_speaker)
        arguments_side_speaker = get_query(arguments_side_speaker)
        article_id_list_filter = True
        speakers_list_id = monitoring_product_item.speaker_set.filter(arguments_speaker).values_list('id', flat= True).order_by('id')
        side_speakers_list_id = monitoring_product_item.sidespeaker_set.filter(arguments_side_speaker).values_list('id', flat= True).order_by('id')
        if arguments_speaker:
            mention_object_list_id = monitoring_product_item.statementspeaker_set.filter(Q(speaker__id__in = speakers_list_id) , Q(arguments_statemen)).values_list('mentions_object__id', flat= True)
        else:
            mention_object_list_id = monitoring_product_item.statementspeaker_set.filter(arguments_statemen).values_list('mentions_object__id', flat= True)
        if arguments_statemen or arguments_speaker:
            mention_object_id = monitoring_product_item.mentionsobject_set.filter(id__in = mention_object_list_id).filter( arguments_object).values_list('article__id', flat= True)
        else:
            mention_object_id = monitoring_product_item.mentionsobject_set.filter(arguments_object).values_list('article__id', flat= True).distinct().order_by('article__id')
        if arguments_object or arguments_speaker or arguments_statemen:
            if date_type=='date':
                articles_list = monitoring_product_item.get_article_set()(manager='filtered_objects').filtered_by(monitoring_product_item.settings).all().filter( id__in = mention_object_id, ).filter(Q(date__range = get_date_range(date_start, date_end)),arguments)
            else:
                articles_list = monitoring_product_item.get_article_set()(manager='filtered_objects').filtered_by(monitoring_product_item.settings).all().filter(arguments, id__in = mention_object_id).filter(date_upload__range = get_date_range(date_start, date_end))

        else:
            if date_type=='date':
                articles_list = monitoring_product_item.get_article_set()(manager='filtered_objects').filtered_by(monitoring_product_item.settings).all().filter(Q(date__range = get_date_range(date_start, date_end)), arguments )
            else:
                articles_list = monitoring_product_item.get_article_set()(manager='filtered_objects').filtered_by(monitoring_product_item.settings).all().filter(date_upload__range = get_date_range(date_start, date_end)).filter(arguments)
        if arguments_km:
            articles_list = articles_list.filter(id__in = monitoring_product_item.kmmention_set.values_list('article__id', flat = True))
        articles_count = articles_list.count()

        paginator = Paginator(articles_list, articles_on_page)
        article_id_list = articles_list.values_list('id', flat=True)

        articles_count_filter = articles_count
        print articles_count, page, 'pages'
           # Make sure page request is an int. If not, deliver first page.

        try:
            article_pagination_list = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            article_pagination_list = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            article_pagination_list = paginator.page(paginator.num_pages)
        article_pagination_list_id = article_pagination_list.object_list.values_list('id', flat=True)
        if data.has_key('get_count'):
            return HttpResponse(json.dumps({
                'status': 'OK',
                'articles_count':articles_count,
                'date_type': date_type
            }))
    date_start_to_template = date_start.strftime("%d-%m-%Y")
    date_end_to_template = date_end.strftime("%d-%m-%Y")
    time_end_to_template = date_end.strftime("%H-%M-%S")
    time_start_to_template = date_start.strftime("%H-%M-%S")
    if monitoring_product_item.parent is not None:
        monitoring_product_item = monitoring_product_item.parent

    filters_from_article = {
    'day__id__in' : monitoring_product_item.day_set.values('title','id').distinct().order_by('title'),
    'month__id__in' : monitoring_product_item.month_set.values('title','id').distinct().order_by('title'),
    'week__id__in' : monitoring_product_item.week_set.values('title','id').distinct().order_by('title'),
    'media_level__id__in' : monitoring_product_item.medialevel_set.filter(~Q(title=""), title__isnull = False).values('title','id').distinct().order_by('title'),
    'title__id__in' : monitoring_product_item.articletitle_set.values('title','id').distinct().order_by('title'),
    'federal_district__id__in' : monitoring_product_item.federaldistrict_set.values('title','id').distinct().order_by('title'),
    'country__id__in' : monitoring_product_item.country_set.values('title','id').distinct().order_by('title'),
    'media_view__id__in' : monitoring_product_item.mediaview_set.values('title','id').distinct().order_by('title'),
    'media_list__id__in' : monitoring_product_item.medialist_set.values('title','id').distinct().order_by('title'),
    'media_category__id__in' : monitoring_product_item.mediacategory_set.values('title','id').distinct().order_by('title'),
    'material_genre__id__in' : monitoring_product_item.materialgenre_set.values('title','id').distinct().order_by('title'),
    'image__id__in' : monitoring_product_item.image_set.values('title','id').distinct().order_by('title'),
    'media_type__id__in' : monitoring_product_item.mediatype_set.values('title','id').distinct().order_by('title'),
    'interaction_kind__id__in' : monitoring_product_item.interactionkind_set.values('title','id').distinct().order_by('title'),
    'penetration__id__in' : monitoring_product_item.penetration_set.values('title','id').distinct().order_by('title'),
    'visibility__id__in' : monitoring_product_item.visibility_set.values('title','id').distinct().order_by('title'),
    'visibility_special__id__in' : monitoring_product_item.visibilityspecial_set.values('title','id').distinct().order_by('title'),
    'macroregion__id__in' : monitoring_product_item.macroregion_set.values('title','id').distinct().order_by('title'),
    'region__id__in' : monitoring_product_item.region_set.values('title','id').distinct().order_by('title'),
    'bissines_direction__id__in' : monitoring_product_item.bissinesdirection_set.values('title','id').distinct().order_by('title'),
    'city__id__in' : monitoring_product_item.city_set.values('title','id').distinct().order_by('title'),
    'source__id__in' : monitoring_product_item.source_set.values('title','id').distinct().order_by('title'),
    'author__id__in' : monitoring_product_item.author_set.values('title','id').distinct().order_by('title'),
    'branch__id__in': monitoring_product_item.branch_set.values('title','id').distinct().order_by('title'),
    'info_reason__id__in' : monitoring_product_item.inforeason_set.values('title','id').distinct().order_by('title'),
    'media_list_special__id__in' : monitoring_product_item.medialistspecial_set.values('title','id').distinct().order_by('title'),
    'media_type_special__id__in' : monitoring_product_item.mediatypespecial_set.values('title','id').distinct().order_by('title'),
    'media_category_special__id__in' : monitoring_product_item.mediacategory_set.values('title','id').distinct().order_by('title'),
    'km_special__id__in' : monitoring_product_item.kmspecial_set.values('title','id').distinct().order_by('title'),
    'brand_or_product__id__in' : monitoring_product_item.brandorproduct_set.values('title','id').distinct().order_by('title'),
    'speaker_quote__id__in' : monitoring_product_item.speakerquote_set.values('title','id').distinct().order_by('title'),
    'third_patry_speakers__id__in' : monitoring_product_item.thirdpartyspeakers_set.values('title','id').distinct().order_by('title'),
    'headder_tonality__id__in' : monitoring_product_item.headertonality_set.values('title','id').distinct().order_by('title'),
    'main_tonality__id__in' : monitoring_product_item.maintonality_set.values('title','id').distinct().order_by('title'),
    'global_local_event__id__in' : monitoring_product_item.globallocalevent_set.values('title','id').distinct().order_by('title'),
    'emotional_tone_material__id__in' : monitoring_product_item.emotionaltonematerial_set.values('title','id').distinct().order_by('title'),
    'emotional_tone_title__id__in' : monitoring_product_item.emotionaltonetitle_set.values('title','id').distinct().order_by('title'),
    'url__id__in' : monitoring_product_item.url_set.values('title','id').distinct().order_by('title'),
    'thematic_category__id__in' : monitoring_product_item.thematiccategory_set.values('title','id').distinct().order_by('title'),
    'material_reason__id__in' : monitoring_product_item.materialreason_set.values('title','id').distinct().order_by('title'),
    'id_press_release__id__in' : monitoring_product_item.idpressrelease_set.values('title','id').distinct().order_by('title'),
    'tag__id__in' : monitoring_product_item.tag_set.values('title','id').distinct().order_by('title'),
    'edge_region__id__in' : monitoring_product_item.edgeregion_set.values('title','id').distinct().order_by('title'),
    'referred_object__id__in' : project.referred_objects.values('title','id').distinct().order_by('title'),
    'duration__id__in' : monitoring_product_item.duration_set.values('title','id').distinct().order_by('title'),
    'cover__range' : [monitoring_product_item.get_article_set().aggregate(Min('cover')).values()[0], monitoring_product_item.get_article_set().aggregate(Max('cover')).values()[0]],
    'infographics__range' : [monitoring_product_item.get_article_set().aggregate(Min('infographics')).values()[0], monitoring_product_item.get_article_set().aggregate(Max('infographics')).values()[0]],
    'km_result': 'true',

    'emotional_tone_object__id__in' : monitoring_product_item.emotionaltoneobject_set.values('title','id').distinct().order_by('title'),
    'object_role__id__in' : monitoring_product_item.objectrole_set.values('title','id').distinct().order_by('title'),
    'technologies__id__in' : monitoring_product_item.technologies_set.values('title','id').distinct().order_by('title'),
    'advertisement_promo__id__in' : monitoring_product_item.advertisementpromo_set.values('title','id').distinct().order_by('title'),
    'initiation__id__in' : monitoring_product_item.initiation_set.values('title','id').distinct().order_by('title'),
    'speaker__id__in' : monitoring_product_item.speaker_set.extra(select={'title': 'fio'}).values('title', 'id').distinct().order_by('title'),
    'speaker_category__id__in' : monitoring_product_item.speakercategory_set.values('title','id').distinct().order_by('title'),
    'speaker_speech_hate__id__in' : monitoring_product_item.speakerspeechhate_set.values('title','id').distinct().order_by('title'),
    'speaker_level__id__in' : monitoring_product_item.speakerlevel_set.values('title','id').distinct().order_by('title'),
    'side__speaker__id__in' : monitoring_product_item.sidespeaker_set.extra(select={'title': 'fio'}).values('title', 'id').distinct().order_by('title'),
    'side_speaker_tonality__id__in' : monitoring_product_item.sidespeakertonality_set.values('title','id').distinct().order_by('title'),
    'side_speaker_status__id__in' : monitoring_product_item.sidespeakerstatus_set.values('title','id').distinct().order_by('title'),
    'speaker_quote_uniqueness__id__in' : monitoring_product_item.speakerquoteuniqueness_set.values('title', 'id').distinct().order_by('title'),
    'release_time' : 'true',
    'mq__range': [mq_min, mq_max],
    'prt__range': [prt_min, prt_max],
    'prv__range':[prv_min, prv_max],
    'mo__range': [mo_min, mo_max],
    'km__id__in' : monitoring_product_item.km_set.values('title', 'id').distinct().order_by('title'),}


    mention_objects_has_filters = monitoring_product_item.mentionsobject_set.filter(article__id__in = article_id_list)
    speaker_statement_has_filters = monitoring_product_item.statementspeaker_set.filter(mentions_object__id__in = mention_objects_has_filters.values_list('id', flat= True))
    speaker_id_has_filters = speaker_statement_has_filters.values_list('speaker__id', flat= True)
    speaker_has_filters = monitoring_product_item.speaker_set.filter(id__in = speaker_id_has_filters)
    side_speaker_has_filters = monitoring_product_item.sidespeaker_set.filter(article__id__in = article_id_list)
    km_mentions_has_filters = monitoring_product_item.kmmention_set.filter(article__id__in = article_id_list)
    re_filters_from_article = {
    'day__id__in' : monitoring_product_item.day_set.filter(id__in = articles_list.values_list('day__id', flat=True)).values('title','id').distinct().order_by('title'),
    'month__id__in' : monitoring_product_item.month_set.filter(id__in = articles_list.values_list('month__id', flat=True)).values('title','id').distinct().order_by('title'),
    'week__id__in' : monitoring_product_item.week_set.filter(id__in = articles_list.values_list('week__id', flat=True)).values('title','id').distinct().order_by('title'),
    'media_level__id__in' : monitoring_product_item.medialevel_set.filter(id__in = articles_list.values_list('media_level__id', flat=True)).values('title','id').distinct().order_by('title'),
    'title__id__in' : monitoring_product_item.articletitle_set.filter(id__in = articles_list.values_list('title__id', flat=True)).values('title','id').distinct().order_by('title'),
    'federal_district__id__in' : monitoring_product_item.federaldistrict_set.filter(id__in = articles_list.values_list('federal_district__id', flat=True)).values('title','id').distinct().order_by('title'),
    'country__id__in' : monitoring_product_item.country_set.filter(id__in = articles_list.values_list('country__id', flat=True)).values('title','id').distinct().order_by('title'),
    'media_view__id__in' : monitoring_product_item.mediaview_set.filter(id__in = articles_list.values_list('media_level__id', flat=True)).values('title','id').distinct().order_by('title'),
    'media_list__id__in' : monitoring_product_item.medialist_set.filter(id__in = articles_list.values_list('media_list__id', flat=True)).values('title','id').distinct().order_by('title'),
    'media_category__id__in' : monitoring_product_item.mediacategory_set.filter(id__in = articles_list.values_list('media_category__id', flat=True)).values('title','id').distinct().order_by('title'),
    'material_genre__id__in' : monitoring_product_item.materialgenre_set.filter(id__in = articles_list.values_list('material_genre__id', flat=True)).values('title','id').distinct().order_by('title'),
    'image__id__in' : monitoring_product_item.image_set.filter(id__in = articles_list.values_list('image__id', flat=True)).values('title','id').distinct().order_by('title'),
    'media_type__id__in' : monitoring_product_item.mediatype_set.filter(id__in = articles_list.values_list('media_type__id', flat=True)).values('title','id').distinct().order_by('title'),
    'interaction_kind__id__in' : monitoring_product_item.interactionkind_set.filter(id__in = articles_list.values_list('interaction_kind__id', flat=True)).values('title','id').distinct().order_by('title'),
    'penetration__id__in' : monitoring_product_item.penetration_set.filter(id__in = articles_list.values_list('penetration__id', flat=True)).values('title','id').distinct().order_by('title'),
    'visibility__id__in' : monitoring_product_item.visibility_set.filter(id__in = articles_list.values_list('visibility__id', flat=True)).values('title','id').distinct().order_by('title'),
    'visibility_special__id__in' : monitoring_product_item.visibilityspecial_set.filter(id__in = articles_list.values_list('visibility_special__id', flat=True)).values('title','id').distinct().order_by('title'),
    'macroregion__id__in' : monitoring_product_item.macroregion_set.filter(id__in = articles_list.values_list('macroregion__id', flat=True)).values('title','id').distinct().order_by('title'),
    'region__id__in' : monitoring_product_item.region_set.filter(id__in = articles_list.values_list('region__id', flat=True)).values('title','id').distinct().order_by('title'),
    'bissines_direction__id__in' : monitoring_product_item.bissinesdirection_set.filter(id__in = articles_list.values_list('bissines_direction__id', flat=True)).values('title','id').distinct().order_by('title'),
    'city__id__in' : monitoring_product_item.city_set.filter(id__in = articles_list.values_list('city__id', flat=True)).values('title','id').distinct().order_by('title'),
    'source__id__in' : monitoring_product_item.source_set.filter(id__in = articles_list.values_list('source__id', flat=True)).values('title','id').distinct().order_by('title'),
    'author__id__in' : monitoring_product_item.author_set.filter(id__in = articles_list.values_list('author__id', flat=True)).values('title','id').distinct().order_by('title'),
    'branch__id__in': monitoring_product_item.branch_set.filter(id__in = articles_list.values_list('branch__id', flat=True)).values('title','id').distinct().order_by('title'),
    'media_list_special__id__in' : monitoring_product_item.medialistspecial_set.filter(id__in = articles_list.values_list('media_list_special__id', flat=True)).values('title','id').distinct().order_by('title'),
    'media_type_special__id__in' : monitoring_product_item.mediatypespecial_set.filter(id__in = articles_list.values_list('media_type_special__id', flat=True)).values('title','id').distinct().order_by('title'),
    'media_category_special__id__in' : monitoring_product_item.mediacategory_set.filter(id__in = articles_list.values_list('media_type_special__id', flat=True)).values('title','id').distinct().order_by('title'),
    'km_special__id__in' : monitoring_product_item.kmspecial_set.filter(id__in = articles_list.values_list('km_special__id', flat=True)).values('title','id').distinct().order_by('title'),
    'brand_or_product__id__in' : monitoring_product_item.brandorproduct_set.filter(id__in = articles_list.values_list('brand_or_product__id', flat=True)).values('title','id').distinct().order_by('title'),
    'speaker_quote__id__in' : monitoring_product_item.speakerquote_set.filter(id__in = articles_list.values_list('speaker_quote__id', flat=True)).values('title','id').distinct().order_by('title'),
    'third_patry_speakers__id__in' : monitoring_product_item.thirdpartyspeakers_set.filter(id__in = articles_list.values_list('third_patry_speakers__id', flat=True)).values('title','id').distinct().order_by('title'),
    'headder_tonality__id__in' : monitoring_product_item.headertonality_set.filter(id__in = articles_list.values_list('headder_tonality__id', flat=True)).values('title','id').distinct().order_by('title'),
    'main_tonality__id__in' : monitoring_product_item.maintonality_set.filter(id__in = articles_list.values_list('main_tonality__id', flat=True)).values('title','id').distinct().order_by('title'),
    'global_local_event__id__in' : monitoring_product_item.globallocalevent_set.filter(id__in = articles_list.values_list('global_local_event__id', flat=True)).values('title','id').distinct().order_by('title'),
    'emotional_tone_material__id__in' : monitoring_product_item.emotionaltonematerial_set.filter(id__in = articles_list.values_list('emotional_tone_material__id', flat=True)).values('title','id').distinct().order_by('title'),
    'url__id__in' : monitoring_product_item.url_set.filter(id__in = articles_list.values_list('url__id', flat=True)).values('title','id').distinct().order_by('title'),
    'thematic_category__id__in' : monitoring_product_item.thematiccategory_set.filter(id__in = articles_list.values_list('thematic_category__id', flat=True)).values('title','id').distinct().order_by('title'),
    'material_reason__id__in' : monitoring_product_item.materialreason_set.filter(id__in = articles_list.values_list('material_reason__id', flat=True)).values('title','id').distinct().order_by('title'),
    'id_press_release__id__in' : monitoring_product_item.idpressrelease_set.filter(id__in = articles_list.values_list('id_press_release__id', flat=True)).values('title','id').distinct().order_by('title'),
    'tag__id__in' : monitoring_product_item.tag_set.filter(id__in = articles_list.values_list('tag__id', flat=True)).values('title','id').distinct().order_by('title'),
    'edge_region__id__in' : monitoring_product_item.edgeregion_set.filter(id__in = articles_list.values_list('edge_region__id', flat=True)).values('title','id').distinct().order_by('title'),
    'duration__id__in' : monitoring_product_item.duration_set.filter(id__in = articles_list.values_list('duration__id', flat=True)).values('title','id').distinct().order_by('title'),
    'release_time' : 'true',
    'mo__range': [articles_list.aggregate(Min('mo')).values()[0], articles_list.aggregate(Max('mo')).values()[0]],
    'cover__range' : [articles_list.aggregate(Min('cover')).values()[0], articles_list.aggregate(Max('cover')).values()[0]],
    'infographics__range' : [articles_list.aggregate(Min('infographics')).values()[0], articles_list.aggregate(Max('infographics')).values()[0]],
    'km_result': 'true',

    'referred_object__id__in' : project.referred_objects.filter(id__in = mention_objects_filter.values_list('referred_object__id', flat= True)).values('title', 'id').distinct().order_by('title'),
    'info_reason__id__in' : monitoring_product_item.inforeason_set.filter(id__in = mention_objects_filter.values_list('info_reason__id', flat= True)).values('title','id').distinct().order_by('title'),
    'emotional_tone_object__id__in' : monitoring_product_item.emotionaltoneobject_set.filter(id__in = mention_objects_filter.values_list('emotional_tone_object__id', flat= True)).values('title','id').distinct().order_by('title'),
    'object_role__id__in'  : monitoring_product_item.objectrole_set.filter(id__in = mention_objects_filter.values_list('object_role__id', flat= True)).values('title','id').distinct().order_by('title'),
    'emotional_tone_title__id__in' : monitoring_product_item.emotionaltonetitle_set.filter(id__in = mention_objects_filter.values_list('emotional_tone_title__id', flat= True)).values('title','id').distinct().order_by('title'),
    'technologies__id__in' : monitoring_product_item.technologies_set.filter(id__in = mention_objects_filter.values_list('technologies__id', flat= True)).values('title','id').distinct().order_by('title'),
    'advertisement_promo__id__in' : monitoring_product_item.advertisementpromo_set.filter(id__in = mention_objects_filter.values_list('advertisement_promo__id', flat= True)).values('title','id').distinct().order_by('title'),
    'initiation__id__in' : monitoring_product_item.initiation_set.filter(id__in = mention_objects_filter.values_list('initiation__id', flat= True)).values('title','id').distinct().order_by('title'),
    'mq__range': [mention_objects_has_filters.aggregate(Min('mq')).values()[0], mention_objects_has_filters.aggregate(Max('mq')).values()[0]],
    'prt__range': [mention_objects_has_filters.aggregate(Min('prt')).values()[0], mention_objects_has_filters.aggregate(Max('prt')).values()[0]],
    'prv__range':[mention_objects_has_filters.aggregate(Min('prv')).values()[0], mention_objects_has_filters.aggregate(Max('prv')).values()[0]],


    'speaker__id__in' : speaker_has_filters.extra(select={'title': 'fio'}).values('title', 'id').distinct().order_by('title'),
    'speaker_category__id__in' : monitoring_product_item.speakercategory_set.filter(id__in = speaker_has_filters.values_list('speaker_category__id', flat= True)).values('title','id').distinct().order_by('title'),
    'speaker_speech_hate__id__in' : monitoring_product_item.speakerspeechhate_set.filter(id__in = speaker_statement_has_filters.values_list('speaker_speech_hate__id', flat= True)).values('title','id').distinct().order_by('title'),
    'speaker_level__id__in' : monitoring_product_item.speakerlevel_set.filter(id__in = speaker_has_filters.values_list('speaker_level__id', flat= True)).values('title','id').distinct().order_by('title'),
    'speaker_quote_uniqueness__id__in' : monitoring_product_item.speakerquoteuniqueness_set.filter(id__in = speaker_has_filters.values_list('speaker_quote_uniqueness__id', flat= True)).values('title', 'id').distinct().order_by('title'),

    'side__speaker__id__in' : side_speaker_has_filters.extra(select={'title': 'fio'}).values('title', 'id').distinct().order_by('title'),
    'side_speaker_tonality__id__in' : monitoring_product_item.sidespeakertonality_set.filter(id__in = side_speaker_has_filters.values_list('side_speaker_tonality__id', flat= True)).values('title','id').distinct().order_by('title'),
    'side_speaker_status__id__in' : monitoring_product_item.sidespeakerstatus_set.values('title','id').filter(id__in = side_speaker_has_filters.values_list('side_speaker_status__id', flat= True)).distinct().order_by('title'),

    'km__id__in' : monitoring_product_item.km_set.filter(id__in = km_mentions_has_filters.values_list('km__id', flat= True)).values('title', 'id').distinct().order_by('title'),
    }

    data = ConstructorWidget.objects.get(monitoring_product_id = id).data
    data = json.loads(data)
    filters = {}
    filter=[]
    item_lists = []

    for elem in data:

        if elem['a3'] !='':
            if elem['a7'] == u'да':
                filter = re_filters_from_article.get(elem['a10'], False)
            else:
                filter = filters_from_article.get(elem['a10'], False)
            if filter:
                item_list = filter

                if elem['a10'] in custom_filters.keys():
                    if elem['a9'] == u'шкала':
                        item_lists =  item_list
                    else:
                        item_lists = [x for x in item_list if x['id'] in custom_filters[elem['a10']]]
                else:
                    print type(item_list)
                    item_lists =  list(item_list)
                if None not in item_lists:
                    filters.update({elem['a10']: {
                        'list': item_lists,
                        'settings' : elem,
                    }})



    # for key, value in custom_filters.items():
    #     if key in filters and filters[key]['satting']:
    #         filters[key]['list']
    #

    monitoring_product.append({'title':monitoring_product_item.title, 'parent_id':monitoring_product_item.parent_id, 'users_count':monitoring_product_item.users_with_perm.count(), 'type':monitoring_product_item.type, 'date_type': date_type, 'date_start': date_start_to_template, 'date_end':date_end_to_template, 'time_end':time_end_to_template, 'time_start': time_start_to_template,
                       'filters':filters,

    })


    monitoring_product_mention_objects = monitoring_product_item.monitoring_product_mentions.all()
    project_id = monitoring_product_item.project.id
    project= Project.objects.get(id = project_id)
    monitoring_product_category = monitoring_product_item.monitoring_product_category.all()
    category = monitoring_product_category.order_by('index_sort')
    monitoring_product_template = []

    settings = MonitoringProductSetting.objects.get(user = request.user, monitoring_product = MonitoringProduct.objects.get(id=id))
    # except ObjectDoesNotExist:
        # settings = MonitoringProductSetting.objects.create(user = request.user, monitoring_product = MonitoringProduct.objects.get(id=id), setting = '')
    template_settings = settings.setting
    settings_from_article = []
    settings_from_mention_object = []
    settings = json.loads(settings.setting)
    for key ,val in settings.items():
        if key=='main_list' or key =='media_list' or key =='geography_list':
            for elem in val:
                settings_from_article.append(elem['name'])
        elif key=='mention_list' or 'indicators_list':
            for elem in val:
                settings_from_mention_object.append(elem['name'])

    project_template = []
    #level 1-------------------------------------------------------
    articles_count = 0
    article_id_list = article_pagination_list_id
    if monitoring_product_item.parent_id is None:
        monitoring_product_item.settings = {}
    for elem in category:
        #monitoring_product_category = MonitoringProductCategory.objects.get(category__id = elem.id)
        monitoring_product_template.append({'id': elem.category.id,'title' : elem.category.title,'index_sort': elem.index_sort , 'children':[], 'articles':[],})
        if article_id_list_filter:
            articles = elem.category.article_set(manager='filtered_objects').filtered_by(monitoring_product_item.settings).all().filter(id__in = article_id_list, monitoring_product = monitoring_product_source)
        else:
            articles = elem.category.article_set(manager='filtered_objects').filtered_by(monitoring_product_item.settings).all().filter(date__range = get_date_range(date_start, date_end), monitoring_product = monitoring_product_source)
        articles_count += articles.count()
        #articles 1------------------------------------------------------------------
        for article in articles:
            monitoring_product_template[-1]['articles'].append(__get_article_dict(article,monitoring_product_item.data_type, settings_from_article))
            mention_object = article.mention_objects.all()
            #mention_object 1----------------------------------------------------------
            for object in mention_object:
                try:
                    monitoring_product_mention = monitoring_product_mention_objects.get(mention_object_id =  object.referred_object.id)
                    #monitoring_product_mention = MonitoringProductMention.objects.get(mention_object = object.referred_object)

                    monit_obj = True
                except:
                    monit_obj = False
                if monit_obj:
                    color = monitoring_product_mention.color
                    keywords = monitoring_product_mention.monitoring_product_mention_words.all()
                    keywords_list = []
                    for word in keywords:
                        keywords_list.append(word.word)
                    monitoring_product_template[-1]['articles'][-1]['mention_objects'].append(__get_mention_object_dict(object, color, keywords_list, settings_from_mention_object))
                    speaker_list = object.statementspeakers.all()
                    #speaker 1 --------------------------------------------------------------------------------------------
                    for speaker in speaker_list:
                        monitoring_product_template[-1]['articles'][-1]['mention_objects'][-1]['speakers'].append({'fio':speaker.speaker.fio})
                    side_speaker_list = object.sidespeaker_set.all()
                    #side_speaker 1 --------------------------------------------------------------------------------------------
                    for side_speaker in side_speaker_list:
                            monitoring_product_template[-1]['articles'][-1]['mention_objects'][-1]['side_speakers'].append({'fio':side_speaker.fio})
        #level 2-------------------------------------------------------
        for ele in elem.category.get_children():
            monitoring_product_template[-1]['children'].append({'id': ele.id,'title': ele.title, 'children':[], 'articles':[],})
            if article_id_list_filter:
                articles = ele.article_set(manager='filtered_objects').filtered_by(monitoring_product_item.settings).all().filter(id__in = article_id_list, monitoring_product = monitoring_product_source)
            else:
                articles = ele.article_set(manager='filtered_objects').filtered_by(monitoring_product_item.settings).all().filter(date__range = get_date_range(date_start, date_end), monitoring_product = monitoring_product_source)
            articles_count += articles.count()
            #articles 2------------------------------------------------------------------
            for article in articles:
                monitoring_product_template[-1]['children'][-1]['articles'].append(__get_article_dict(article,monitoring_product_item.data_type, settings_from_article))
                mention_object = article.mention_objects.all()
                #mention_object 2----------------------------------------------------------
                for object in mention_object:
                    try:
                        monitoring_product_mention = monitoring_product_mention_objects.get(mention_object =  object.referred_object)
                        #monitoring_product_mention = MonitoringProductMention.objects.get(mention_object = object.referred_object)

                        monit_obj = True
                    except:
                        monit_obj = False
                    if monit_obj:
                        color = monitoring_product_mention.color
                        keywords = monitoring_product_mention.monitoring_product_mention_words.all()
                        keywords_list = []
                        for word in keywords:
                            keywords_list.append(word.word)
                        monitoring_product_template[-1]['children'][-1]['articles'][-1]['mention_objects'].append(__get_mention_object_dict(object, color, keywords_list, settings_from_mention_object))
                        speaker_list = object.statementspeakers.all()
                        #speaker 2 --------------------------------------------------------------------------------------------
                        for speaker in speaker_list:
                            monitoring_product_template[-1]['children'][-1]['articles'][-1]['mention_objects'][-1]['speakers'].append({'fio':speaker.speaker.fio})
                        side_speaker_list = object.sidespeaker_set.all()
                        #side_speaker 2 --------------------------------------------------------------------------------------------
                        for side_speaker in side_speaker_list:
                            monitoring_product_template[-1]['children'][-1]['articles'][-1]['mention_objects'][-1]['side_speakers'].append({'fio':side_speaker.fio})
            #level 3-------------------------------------------------------
            for el in ele.get_children():
                monitoring_product_template[-1]['children'][-1]['children'].append({'id': el.id,'title': el.title, 'children':[], 'articles':[],})
                if article_id_list_filter:
                    articles = el.article_set(manager='filtered_objects').filtered_by(monitoring_product_item.settings).all().filter(id__in = article_id_list, monitoring_product = monitoring_product_source)
                else:
                    articles = el.article_set(manager='filtered_objects').filtered_by(monitoring_product_item.settings).all().filter(date__range = get_date_range(date_start, date_end), monitoring_product = monitoring_product_source)
                articles_count += articles.count()
                #articles 3------------------------------------------------------------------
                for article in articles:
                    monitoring_product_template[-1]['children'][-1]['children'][-1]['articles'].append(__get_article_dict(article,monitoring_product_item.data_type, settings_from_article))
                    mention_object = article.mention_objects.all()
                    #mention_object 3----------------------------------------------------------
                    for object in mention_object:
                        try:
                            monitoring_product_mention = monitoring_product_mention_objects.get(mention_object =  object.referred_object)
                            #monitoring_product_mention = MonitoringProductMention.objects.get(mention_object = object.referred_object)

                            monit_obj = True
                        except:
                            monit_obj = False
                        if monit_obj:
                            color = monitoring_product_mention.color
                            keywords = monitoring_product_mention.monitoring_product_mention_words.all()
                            keywords_list = []
                            for word in keywords:
                                keywords_list.append(word.word)
                            monitoring_product_template[-1]['children'][-1]['children'][-1]['articles'][-1]['mention_objects'].append(__get_mention_object_dict(object, color, keywords_list, settings_from_mention_object))
                            speaker_list = object.statementspeakers.all()
                            #speaker 3 --------------------------------------------------------------------------------------------
                            for speaker in speaker_list:
                                monitoring_product_template[-1]['children'][-1]['children'][-1]['articles'][-1]['mention_objects'][-1]['speakers'].append({'fio':speaker.speaker.fio})
                            side_speaker_list = object.sidespeaker_set.all()
                            #side_speaker 3 --------------------------------------------------------------------------------------------
                            for side_speaker in side_speaker_list:
                                monitoring_product_template[-1]['children'][-1]['children'][-1]['articles'][-1]['mention_objects'][-1]['side_speakers'].append({'fio':side_speaker.fio})
                #level 4-------------------------------------------------------
                for e in el.get_children():
                    monitoring_product_template[-1]['children'][-1]['children'][-1]['children'].append({'id': e.id,'title': e.title, 'articles':[],})
                    if article_id_list_filter:
                        articles = e.article_set(manager='filtered_objects').filtered_by(monitoring_product_item.settings).all().filter(id__in = article_id_list, monitoring_product = monitoring_product_source)
                    else:
                        articles = e.article_set(manager='filtered_objects').filtered_by(monitoring_product_item.settings).all().filter(date__range = get_date_range(date_start, date_end), monitoring_product = monitoring_product_source)

                    articles_count += articles.count()
                    #articles 4------------------------------------------------------------------
                    for article in articles:
                        monitoring_product_template[-1]['children'][-1]['children'][-1]['children'][-1]['articles'].append(__get_article_dict(article,monitoring_product_item.data_type, settings_from_article))
                        mention_object = article.mention_objects.all()
                        #mention_object 4----------------------------------------------------------
                        for object in mention_object:
                            try:
                                monitoring_product_mention = monitoring_product_mention_objects.get(mention_object =  object.referred_object)
                                #monitoring_product_mention = MonitoringProductMention.objects.get(mention_object = object.referred_object)

                                monit_obj = True
                            except:
                                monit_obj = False
                            if monit_obj:
                                color = monitoring_product_mention.color
                                keywords = monitoring_product_mention.monitoring_product_mention_words.all()
                                keywords_list = []
                                for word in keywords:
                                    keywords_list.append(word.word)
                                monitoring_product_template[-1]['children'][-1]['children'][-1]['children'][-1]['articles'][-1]['mention_objects'].append(__get_mention_object_dict(object, color, keywords_list, settings_from_mention_object))
                                speaker_list = object.statementspeakers.all()
                                #speaker 4 --------------------------------------------------------------------------------------------
                                for speaker in speaker_list:
                                    monitoring_product_template[-1]['children'][-1]['children'][-1]['children'][-1]['articles'][-1]['mention_objects'][-1]['speakers'].append({'fio':speaker.speaker.fio})
                                side_speaker_list = object.sidespeaker_set.all()
                                #side_speaker 4 --------------------------------------------------------------------------------------------
                                for side_speaker in side_speaker_list:
                                    monitoring_product_template[-1]['children'][-1]['children'][-1]['children'][-1]['articles'][-1]['mention_objects'][-1]['side_speakers'].append({'fio':side_speaker.fio})



    article_pagination_object = {
        "has_previous": True if article_pagination_list.has_previous() else False,
        "articles_on_page": articles_on_page,
        "num_pages": article_pagination_list.paginator.num_pages,
        "number":article_pagination_list.number,
        "has_next":True if article_pagination_list.has_next() else False,
        "next_page_number":article_pagination_list.next_page_number() if article_pagination_list.has_next() else False
    }
    return HttpResponse(json.dumps({
        'status': 'OK',
        'monitoring_product_template':monitoring_product_template,
        'product': monitoring_product,
        'articles_count':articles_count_filter,
        'article_pagination_list': article_pagination_object
    }))
    # except Exception, e:
    #     print "Exception, %s" % e


@login_required(login_url='/login/')
# @custom_permission_required('custom_user.can_constructor_w0', )
def widget0(request, id, slug):
    project = Project.objects.get(slug = slug)
    monitoring_products = project.monitoring_products.all()
    try:
        settings = MonitoringProductSetting.objects.get(user = request.user, monitoring_product = MonitoringProduct.objects.get(id=id))
    except ObjectDoesNotExist:

        settings = MonitoringProductSetting.objects.create(user = request.user, monitoring_product = MonitoringProduct.objects.get(id=id))

    template_settings = settings.setting
    try:
        monitoring_product = monitoring_products.get(id = id)
    except MonitoringProduct.DoesNotExist:
        raise Http404

    # if not monitoring_product.users_with_perm.filter(pk=request.user.pk).exists():
    #     raise PermissionDenied
    return TemplateResponse(request, 'monitoring_product/widget0.html', locals())



def convector(data):
    info = []
    for key, val in data.items():
        dictt = {}
        dictt.update({'a1' : key})
        d = 2
        for elem in val:
            dictt.update({'a%d'%d : elem})
            d += 1
        info.append(dictt)
    return info



def convector_to_dict(data):
    info = {}
    for elem in data:
        info_list = []
        for key, val in elem.items():
            info_list.append(val)

        new_key = elem['a1']
        info.update({'%s'%new_key : info_list})

    # sorted(numbers, key=numbers.__getitem__)
    return info


def constructor_widget0(request, id , slug):
    data = ConstructorWidget.objects.get(monitoring_product_id = id).data

    project = Project.objects.get(slug = slug)
    monitoring_products= project.monitoring_products.all().exclude(id = id)
    if request.method == "POST":

        data_post= json.loads(request.body)
        try:
            data = data_post['data']
        except:
            data =False
        # data = request.POST.get('data', False)
        try:
            coppy_id =  data_post['coppy_id']
        except:
            coppy_id = False

        # coppy_id = request.POST.get('coppy_id', False)
        if data:
            constructor_widget = ConstructorWidget.objects.get(monitoring_product_id = id)
            data = json.loads(data)
            # data = convector_to_dict(data)
            data = json.dumps(data)
            constructor_widget.data = data
            constructor_widget.save()


            return HttpResponse(json.dumps({
                    'status': 'OK',
                    }))
        elif coppy_id:
            data = project.constructorwidget_set.get(monitoring_product_id = coppy_id).data
            data = json.loads(data)
            return HttpResponse(json.dumps({
                    'data': data,
                    }))
        return HttpResponse(json.dumps({
            'status': 'ERROR',
            'message' : 'does not exist data or id',
                }))

    return TemplateResponse(request, 'monitoring_product/constructor_widget0.html', locals())


DATES = {
    '02-2014': u"Февраль",
    '03-2014': u"Март",
    '04-2014': u"Апрель",
    '05-2014': u"Май"

}

DAYS = {
    '01-01-2014': u"1 Января",
    '02-01-2014': u"2 Января",
    '03-01-2014': u"3 Января",
    '04-01-2014': u"4 Января",
    '05-01-2014': u"5 Января",
    '06-01-2014': u"6 Января",
    '07-01-2014': u"7 Января",
    '08-01-2014': u"8 Января",
    '09-01-2014': u"9 Января",

}

TONE_LIST = {
    '1': u'нейтральное',
    '2': u'позитивное',
    '3': u'негативное'
}


COLORS = {
    7: '#ffc000',
    8: '#ff5050',
    9: '#008080'
}


def get_date(month):
    return datetime.strptime('01-%s-2014' % str(100 + int(month))[1:], '%d-%m-%Y')


def _get_month(param_name='', params=[]):
    return reduce(or_, [Q(*[("%s__gte" % param_name,  get_date(m)), ( "%s__lt" % param_name, get_date(m+1))]) for m in params])


def _get_days(param_name='', params=[]):
    return reduce(or_, [Q(*[("%s" % param_name,  get_date(m)), ( "%s__lt" % param_name, get_date(m+1))]) for m in params])


def _get_in(param_name='', params=[]):
    query = Q(("%s__in" % param_name,  params))
    return query


def analytic_product_1(request, slug, referred_id=0):
    month = map(lambda x: int(x), request.GET.getlist('month', [2,3,4,5]))
    region = map(lambda x: str(x), request.GET.getlist('region', []))
    tone = map(lambda x: TONE_LIST[str(x)], request.GET.getlist('tone', []))

    project = Project.objects.get(slug = slug)
    mentions_list = project.mentionsobject_set.filter(_get_month('article__date', month))
    if region:
        mentions_list = mentions_list.filter(_get_in('article__region__title', region))
    if tone:
        mentions_list = mentions_list.filter(_get_in('emotional_tone_object__title', tone))
    resul = [(k.strftime("%m-%Y"), len([i for i in v]), [i.prt for i in v], ) for k, v in groupby(sorted(mentions_list, key=lambda x: x.article.date), key=lambda x: x.article.date)]

    resul2 = [(k.strftime("%d-%m-%Y"), [i.prt for i in v], ) for k, v in groupby(sorted(mentions_list, key=lambda x: x.article.date), key=lambda x: x.article.date)]

    table2 = [(k, sum(v)/len(v))for k, v in resul2]
    groupby2 = groupby(table2, key=lambda x: '-'.join(x[0].split('-')[1:]))
    resul3 = [(k, [i[1] for i in v]) for k, v in groupby2]
    table3 = [(k, sum(v)/len(v))for k, v in resul3]


    resul = [(k, sum([i[1] for i in v])) for k, v in groupby(resul, key=lambda x: x[0])]

    decimal = max([i[1] for i in resul]) if resul else 0 + 1
    if decimal != 1 :
        decimal =''
    else:
        decimal = 0
    return HttpResponse(json.dumps({
        'status': 'OK',
        'chart': {
            "chart": {
                "caption": "",
                "numberprefix": "",
                "plotgradientcolor": "",
                "bgcolor": "FFFFFF",
                "showalternatehgridcolor": "0",
                "divlinecolor": "CCCCCC",
                "showvalues": "0",
                "showcanvasborder": "0",
                "canvasborderalpha": "0",
                "canvasbordercolor": "CCCCCC",
                "canvasborderthickness": "1",
                "yaxismaxvalue":  max([i[1] for i in resul]) if resul else 0 + 1,
                'forceYAxisValueDecimals': '0',
                'yAxisValueDecimals': '0',
                "captionpadding": "30",
                "linethickness": "3",
                "yaxisvaluespadding": "15",
                "legendshadow": "0",
                "legendborderalpha": "0",
                "palettecolors": "#006666,#00k8ee4,#33bdda,#e44a00,#6baa01,#583e78",
                "showborder": "0",
                "showlegend": "0",
                'numDivLines': '%s'%decimal,
            },
            # "data":[
            #     {
            #         "label": DATES[i[0]],
            #         "value": i[1],
            #         "tooltext": DATES[i[0]]
            #     } for i in resul
            # ]
            "categories": [
                {
                    "category": [
                        {
                            "label": DATES[i[0]]
                        } for i in resul],
                }
            ],
            "dataset": [
                {
                    # "seriesname": project.title,
                    "data": [
                        {
                            "value": i[1]
                        } for i in resul]
                },
            ]
        }
    }), content_type='application/json')


def analytic_product_2(request, slug, referred_id=0):
    month = map(lambda x: int(x), request.GET.getlist('month', [2,3,4,5]))
    region = map(lambda x: str(x), request.GET.getlist('region', []))

    project = Project.objects.get(slug = slug)
    # mentions_list = project.mentionsobject_set.filter(article__date__range = [date_end_product , date_start_product])
    mentions_list = project.mentionsobject_set.filter(_get_month('article__date', month))

    if region:
        mentions_list = mentions_list.filter(_get_in('article__region__title', region))
    # articles = Article.objects.filter(date__range = [date_start_product, date_end_product],)
    resul = [(k.strftime("%m-%Y"), len([i for i in v]), [i.prt for i in v], ) for k, v in groupby(sorted(mentions_list, key=lambda x: x.article.date), key=lambda x: x.article.date)]
    resul2 = [(k.strftime("%d-%m-%Y"), [i.prt for i in v], ) for k, v in groupby(sorted(mentions_list, key=lambda x: x.article.date), key=lambda x: x.article.date)]

    table2 = [(k, sum(v)/len(v))for k, v in resul2]
    groupby2 = groupby(table2, key=lambda x: '-'.join(x[0].split('-')[1:]))
    resul3 = [(k, [i[1] for i in v]) for k, v in groupby2]
    table3 = [(k, sum(v)/len(v))for k, v in resul3]


    resul = [(k, sum([i[1] for i in v])) for k, v in groupby(resul, key=lambda x: x[0])]


    return HttpResponse(json.dumps({
        'status': 'OK',
        'chart': {
            "chart": {
                "caption": "",
                "numberprefix": "",
                "plotgradientcolor": "",
                "bgcolor": "FFFFFF",
                "showalternatehgridcolor": "0",
                "divlinecolor": "CCCCCC",
                "showvalues": "0",
                "showcanvasborder": "0",
                "canvasborderalpha": "0",
                "canvasbordercolor": "CCCCCC",
                "canvasborderthickness": "1",
                "yAxisValuesStep": 0.2,
                "setAdaptiveYMin": "1",
                "captionpadding": "30",
                "linethickness": "3",
                "yaxisvaluespadding": "15",
                "legendshadow": "0",
                "legendborderalpha": "0",
                "palettecolors": "#006666,#008ee4,#33bdda,#e44a00,#6baa01,#583e78",
                "showborder": "0",
                "showlegend": "0"
            },
            "categories": [
                {
                    "category": [
                        {
                            "label": DATES[i[0]]
                        } for i in table3],
                }
            ],
            "dataset": [
                {
                    # "seriesname": project.title,
                    "data": [
                        {
                            "value": "%.4f" % i[1]
                        } for i in table3]
                },
            ]
        }}), content_type='application/json')


def analytic_product_3(request, slug, referred_id=0):
    month = map(lambda x: int(x), request.GET.getlist('month', [2,3,4,5]))
    query_region = map(lambda x: str(x), request.GET.getlist('region', []))
    tone = map(lambda x: TONE_LIST[str(x)], request.GET.getlist('tone', []))

    # project = Project.objects.get(slug = slug)
    # mentions_list = project.mentionsobject_set.filter(_get_month('article__date', month))
    # if region:
    #     mentions_list.filter(_get_in('article__region', region))
    # if tone:
    #     mentions_list.filter(_get_in('emotional_tone_object__title', tone))
    date_start_product = datetime.now()
    project = Project.objects.get(slug = slug)
    mount1 = datetime.strptime('01-04-2014', '%d-%m-%Y')
    mount2 = datetime.strptime('01-05-2014', '%d-%m-%Y')
    mount3 = datetime.strptime('01-06-2014', '%d-%m-%Y')
    colors = [{'name': u'Апрель', 'color': '31859c', 'key': 'mount1'}, {'name': u'Май', 'color': '215966', 'key': 'mount2'}, ]
    if query_region and tone:
        articles = project.mentionsobject_set.filter(_get_in('emotional_tone_object__title', tone)).values_list('article_id', flat=True)
        result = [(region.title, {
            'mount1': sum([int(float(ar.mo)) if ar.mo else 0 for ar in region.article_set.filter(date__gte=mount1, date__lt=mount2).filter(id__in=articles)]),
            'mount2': sum([int(float(ar.mo)) if ar.mo else 0 for ar in region.article_set.filter(date__gte=mount2, date__lt=mount3).filter(id__in=articles)]),
            }, ) for region in project.region_set.filter(_get_in('title', query_region))]
    elif query_region:
        result = [(region.title, {
            'mount1': sum([int(float(ar.mo)) if ar.mo else 0 for ar in region.article_set.filter(date__gte=mount1, date__lt=mount2)]),
            'mount2': sum([int(float(ar.mo)) if ar.mo else 0 for ar in region.article_set.filter(date__gte=mount2, date__lt=mount3)]),
            }, ) for region in project.region_set.filter(_get_in('title', query_region))]
    elif tone:
        articles = project.mentionsobject_set.filter(_get_in('emotional_tone_object__title', tone)).values_list('article_id', flat=True)
        result = [(region.title, {
            'mount1': sum([int(float(ar.mo)) if ar.mo else 0 for ar in region.article_set.filter(date__gte=mount1, date__lt=mount2).filter(id__in=articles)]),
            'mount2': sum([int(float(ar.mo)) if ar.mo else 0 for ar in region.article_set.filter(date__gte=mount2, date__lt=mount3).filter(id__in=articles)]),
            }, ) for region in project.region_set.all()]
    else:
        result = [(region.title, {
            'mount1': sum([int(float(ar.mo)) if ar.mo else 0 for ar in region.article_set.filter(date__gte=mount1, date__lt=mount2)]),
            'mount2': sum([int(float(ar.mo)) if ar.mo else 0 for ar in region.article_set.filter(date__gte=mount2, date__lt=mount3)]),
            }, ) for region in project.region_set.all()]

    return HttpResponse(json.dumps({
        'status': 'OK',
        'chart': {
            "chart": {
                "caption": "",
                "numberprefix": "",
                "plotgradientcolor": "",
                "bgcolor": "FFFFFF",
                "showalternatehgridcolor": "0",
                "divlinecolor": "CCCCCC",
                "showvalues": "0",
                "showcanvasborder": "0",
                "canvasborderalpha": "0",
                "canvasbordercolor": "CCCCCC",
                "canvasborderthickness": "1",
                "yaxismaxvalue": "30000",
                "captionpadding": "30",
                "yaxisvaluespadding": "15",
                'forceYAxisValueDecimals': '0',
                'yAxisValueDecimals': '0',
                "legendshadow": "0",
                "legendborderalpha": "0",
                "palettecolors": "#f8bd19,#008ee4,#33bdda,#e44a00,#6baa01,#583e78",
                "showplotborder": "0",
                "showborder": "0",
                "showlegend": "0"
            },
            "categories": [
                {
                    "category": [
                        {
                            "label": res[0]
                        } for res in result]
                }
            ],
            "dataset": [
                {
                    "seriesname": val["name"],
                    "color": val["color"],
                    "data": [{
                        "value": vaal[1][val["key"]]
                    } for vaal in result]
                } for val in colors]
        }}), content_type='application/json')


def analytic_product_4(request, slug, referred_id=0):
    date_start_product = datetime.now()
    project = Project.objects.get(slug = slug)
    mount1 = datetime.strptime('01-04-2014', '%d-%m-%Y')
    mount2 = datetime.strptime('01-05-2014', '%d-%m-%Y')
    mount3 = datetime.strptime('01-06-2014', '%d-%m-%Y')
    region = map(lambda x: str(x), request.GET.getlist('region', []))
    title_list = [u'Позитивное', u'Нейтральное', u'Негатавное']

    # positive_count = project_mentions_list.filter(emotional_tone_object__title = u'позитивное').count()
    # neutral_count = project_mentions_list.filter(emotional_tone_object__title = u'нейтральное').count()
    # negative_count = project_mentions_list.filter(emotional_tone_object__title = u'негатавное').count()
    colors = [{'name': u'Апрель', 'color': '31859c', 'key': 'mount1'}, {'name': u'Май', 'color': '215966', 'key': 'mount2'}, ]
    if region:
        result = [(types, {
            'mount1': project.mentionsobject_set.filter(article__date__gte=mount1, article__date__lt=mount2, emotional_tone_object__title=types).filter(_get_in('article__region__title', region)).count(),
            'mount2': project.mentionsobject_set.filter(article__date__gte=mount2, article__date__lt=mount3, emotional_tone_object__title=types).filter(_get_in('article__region__title', region)).count(),
            }, ) for types in title_list]
    else:
        result = [(types, {
            'mount1': project.mentionsobject_set.filter(article__date__gte=mount1, article__date__lt=mount2, emotional_tone_object__title=types).count(),
            'mount2': project.mentionsobject_set.filter(article__date__gte=mount2, article__date__lt=mount3, emotional_tone_object__title=types).count(),
            }, ) for types in title_list]


    return HttpResponse(json.dumps({
        'status': 'OK',
        'chart': {
            "chart": {
                "caption": "",
                "numberprefix": "",
                "plotgradientcolor": "",
                "bgcolor": "FFFFFF",
                "showalternatehgridcolor": "0",
                "divlinecolor": "CCCCCC",
                "showvalues": "0",
                "showcanvasborder": "0",
                "canvasborderalpha": "0",
                "canvasbordercolor": "CCCCCC",
                "canvasborderthickness": "1",
                "yaxismaxvalue": "",
                "captionpadding": "30",
                "yaxisvaluespadding": "15",
                'forceYAxisValueDecimals': '0',
                'yAxisValueDecimals': '0',
                "legendshadow": "0",
                "legendborderalpha": "0",
                "palettecolors": "#f8bd19,#008ee4,#33bdda,#e44a00,#6baa01,#583e78",
                "showplotborder": "0",
                "showborder": "0",
                "showlegend": "0"
            },
            "categories": [
                {
                    "category": [
                        {
                            "label": res[0]
                        } for res in result]
                }
            ],
            "dataset": [
                {
                    "seriesname": val["name"],
                    "color": val["color"],
                    "data": [{
                        "value": vaal[1][val["key"]]
                    } for vaal in result]
                } for val in colors]
        }}), content_type='application/json')


def analytic_product_5(request, slug, referred_id=0):
    project = Project.objects.get(slug = slug)
    month = map(lambda x: int(x), request.GET.getlist('month', [2,3,4,5]))
    region = map(lambda x: str(x), request.GET.getlist('region', []))

    # f = [{}project.medialist_set.all()[0].article_set.filter(date__gte=mount1, date__lt=mount2)]
    if region:
        f = [{"label": media.title, "value": media.article_set.filter(_get_month('date', month)).filter(_get_in('region__title', region)).count()} for media in project.medialist_set.all()]
    else:
        f = [{"label": media.title, "value": media.article_set.filter(_get_month('date', month)).count()} for media in project.medialist_set.all()]

    return HttpResponse(json.dumps({
        'status': 'OK',
        'chart': {
            "chart": {
                "caption": "",
                "subcaption": "",
                "paletteColors": "#dfe1d7,#f9f5d7,#7eb2b8,#f45b00,#8e0000",
                 "bgColor": "#ffffff",
                 "showBorder": "0",
                 "use3DLighting": "0",
                 "showShadow": "0",
                 "enableSmartLabels": "0",
                 "startingAngle": "310",
                 "showLabels": "0",
                 "showPercentValues": "1",
                 "showLegend": "1",
                 "legendShadow": "0",
                 "legendBorderAlpha": "0",
                "defaultcenterlabel": "",
                "centerlabel": "",
                "theme": "fint"
            },
            "data": f
        }}), content_type='application/json')


def analytic_product_6(request, slug, referred_id=0):
    from django.db.models import Count
    project = Project.objects.get(slug = slug)
    month = map(lambda x: int(x), request.GET.getlist('month', [2,3,4,5]))
    region = map(lambda x: str(x), request.GET.getlist('region', []))
    if region:
        f = sorted([{"label": product.title, "value": product.mentionproduct_set.filter(_get_month('article__date', month)).filter(_get_in('article__region__title', region)).count()}for product in project.product_set.all()], key=lambda x: x['value'], reverse=True)[:5]
    else:
        f = sorted([{"label": product.title, "value": product.mentionproduct_set.filter(_get_month('article__date', month)).count()}for product in project.product_set.all()], key=lambda x: x['value'], reverse=True)[:5]
    # state =  project.statementspeaker_set.exclude(speaker__fio="Absent").filter(article__date__gte=mount2, article__date__lt=mount3).annotate(col=Count('speaker')).values('speaker__fio', 'col')
    # f = sorted([{"label": k, "value": sum([i['col'] for i in v])} for k, v in groupby(sorted(state, key=lambda x: x['speaker__fio']), key=lambda x: x['speaker__fio'])], key=lambda x: x["value"], reverse=True)[:5]

    # f = [{}project.medialist_set.all()[0].article_set.filter(date__gte=mount1, date__lt=mount2)]

    # f = [{"label": media.title, "value": media.article_set.filter(date__gte=mount1, date__lt=mount2).count()} for media in project.medialist_set.all()]


    return HttpResponse(json.dumps({
        'status': 'OK',
        'chart': {
            "chart": {
                "caption": "",
                "subcaption": "",
                "yaxisname": "",
                "xaxisname": "",
                "bgcolor": "FFFFFF",
                "showalternatevgridcolor": "0",
                "showplotborder": "0",
                "showvalues": "0",
                "divlinecolor": "CCCCCC",
                "showcanvasborder": "0",
                "plotgradientcolor": "",
                "tooltipbordercolor": "FFFFFF",
                "palettecolors": "006666",
                'forceYAxisValueDecimals': '0',
                'yAxisValueDecimals': '0',
                "canvasborderalpha": "0",
                'numDivLines': '0',
                "showBorder": "0",
                "showLegend": "0"
            },
            "data": f
    }}), content_type='application/json')


def analytic_product_7(request, slug, referred_id=0):
    from django.db.models import Count
    month = map(lambda x: int(x), request.GET.getlist('month', [2,3,4,5]))
    region = map(lambda x: str(x), request.GET.getlist('region', []))
    project = Project.objects.get(slug = slug)
    if region:
        state =  project.statementspeaker_set.exclude(speaker__fio="Absent").filter(_get_month('article__date', month)).filter(_get_in('article__region__title', region)).annotate(col=Count('speaker')).values('speaker__fio', 'col')
    else:
        state =  project.statementspeaker_set.exclude(speaker__fio="Absent").filter(_get_month('article__date', month)).annotate(col=Count('speaker')).values('speaker__fio', 'col')
    f = sorted([{"label": k, "value": sum([i['col'] for i in v])} for k, v in groupby(sorted(state, key=lambda x: x['speaker__fio']), key=lambda x: x['speaker__fio'])], key=lambda x: x["value"], reverse=True)[:5]

    # f = [{}project.medialist_set.all()[0].article_set.filter(date__gte=mount1, date__lt=mount2)]

    # f = [{"label": media.title, "value": media.article_set.filter(date__gte=mount1, date__lt=mount2).count()} for media in project.medialist_set.all()]

    return HttpResponse(json.dumps({
        'status': 'OK',
        'chart': {
            "chart": {
                "caption": "",
                "subcaption": "",
                "yaxisname": "",
                "xaxisname": "",
                "bgcolor": "FFFFFF",
                "showalternatevgridcolor": "0",
                "showplotborder": "0",
                "showvalues": "0",
                "divlinecolor": "CCCCCC",
                "showcanvasborder": "0",
                "plotgradientcolor": "",
                "tooltipbordercolor": "FFFFFF",
                "palettecolors": "006666",
                'forceYAxisValueDecimals': '0',
                'yAxisValueDecimals': '0',
                'numDivLines': '0',
                "canvasborderalpha": "0",
                "showBorder": "0",
                "showLegend": "0"
            },
            "data": f
    }}), content_type='application/json')


def filter_parameters(request):
    return HttpResponse(json.dumps({
        'month': [{'title': 'Май', 'id': 5}, {'title': 'Апрель', 'id': 4}, {'title': 'Март', 'id': 3}, {'title': 'Февраль', 'id': 2}],
        'region': [{'title': 'EU'}, {'title': 'EM'}, {'title': 'NA'}, {'title': 'APAC'}, {'title': 'RUS+KZ'}],
        'tone': [{'title': 'нейтральное', "id": 1}, {'title': 'позитивное', "id": 2}, {'title': 'негативное', "id": 3}]
    }), content_type='application/json')


def _get_date_by_day_num(day):
    return date(2014, 1, day)


def get_days_filter(days):
    return reduce(or_, [Q(article__date=_get_date_by_day_num(m)) for m in days])


def get_days_filter_for_mo(days):
    return reduce(or_, [Q(date=_get_date_by_day_num(m)) for m in days])


def analytic_product_1_vympelkom(request, slug, referred_id=0):

    days = map(lambda x: int(x), request.GET.getlist('days', [1, 2, 3, 4, 5, 6, 7, 8, 9]))
    regions = map(lambda x: int(x), request.GET.getlist('region', [1, 2, 3]))
    tones = map(lambda x: TONE_LIST[str(x)], request.GET.getlist('tone', []))
    objects = map(lambda x: int(x), request.GET.getlist('objects', [7, 8, 9]))

    project = Project.objects.get(slug=slug)
    referred_objects = project.referred_objects.filter(id__in=objects).order_by('index_sort')

    mentions_list_by_objects = []
    for it in referred_objects:
        mentions_list_by_objects.append(
            {
                'object': it,
                'items': project.mentionsobject_set.filter(get_days_filter(days)).filter(referred_object=it)
            }
        )

    if regions:
        for it in mentions_list_by_objects:
            it['items'] = it['items'].filter(article__region__id__in=regions)

    if tones:
        for it in mentions_list_by_objects:
            it['items'] = it['items'].filter(_get_in('emotional_tone_object__title', tones))

    for it in mentions_list_by_objects:
        it['data'] = []
        for i in days:
            it['data'].append(len(filter(lambda x: x.article.date == date(2014, 1, i), it['items'])))
    # decimal = max([i[1] for i in mentions_list_by_objects]) if mentions_list_by_objects else 0 + 1
    # if decimal != 1 :
    #     decimal =''
    # else:
    #     decimal = 0
    return HttpResponse(json.dumps({
        'status': 'OK',
        'chart': {
            "chart": {
                "caption": "",
                "numberprefix": "",
                "plotgradientcolor": "",
                "bgcolor": "FFFFFF",
                "showalternatehgridcolor": "0",
                "divlinecolor": "CCCCCC",
                "showvalues": "0",
                "showcanvasborder": "0",
                "canvasborderalpha": "0",
                "canvasbordercolor": "CCCCCC",
                "canvasborderthickness": "1",
                "yaxismaxvalue":  max([max(it['data']) for it in mentions_list_by_objects]),
                "captionpadding": "30",
                "linethickness": "3",
                "yaxisvaluespadding": "15",
                "legendshadow": "0",
                "legendborderalpha": "0",
                "palettecolors": ",".join([COLORS[it['object'].id] for it in mentions_list_by_objects]),
                "showborder": "0",

            },

            "categories": [
                {
                    "category": [
                        {
                            "label": DAYS[i],
                        } for i in ['0%d-01-2014' % it for it in days]]
                }
            ],
            "dataset": [
                {
                    "seriesname": it['object'].title,
                    "data": [
                        {
                            "value": i
                        } for i in it['data']]
                } for it in mentions_list_by_objects
            ]
        }
    }), content_type='application/json')


def analytic_product_2_vympelkom(request, slug, referred_id=0):

    days = map(lambda x: int(x), request.GET.getlist('days', [1, 2, 3, 4, 5, 6, 7, 8, 9]))
    regions = map(lambda x: int(x), request.GET.getlist('region', [1, 2, 3]))
    tones = map(lambda x: TONE_LIST[str(x)], request.GET.getlist('tone', []))
    objects = map(lambda x: int(x), request.GET.getlist('objects', [7, 8, 9]))

    project = Project.objects.get(slug=slug)
    referred_objects = project.referred_objects.filter(id__in=objects).order_by('index_sort')

    mentions_list_by_objects = []
    for it in referred_objects:
        mentions_list_by_objects.append(
            {
                'object': it,
                'items': project.mentionsobject_set.filter(get_days_filter(days)).filter(referred_object=it)
            }
        )

    if regions:
        for it in mentions_list_by_objects:
            it['items'] = it['items'].filter(article__region__id__in=regions)

    if tones:
        for it in mentions_list_by_objects:
            it['items'] = it['items'].filter(_get_in('emotional_tone_object__title', tones))

    for it in mentions_list_by_objects:
        it['data'] = []
        for i in days:
            lst = filter(lambda x: x.article.date == date(2014, 1, i), it['items'])
            prts = [mo.prt for mo in lst]
            prt = reduce(lambda x, y: x + y, prts) / len(prts) if prts else 0
            it['data'].append(prt)

    return HttpResponse(json.dumps({
        'status': 'OK',
        'chart': {
            "chart": {
                "caption": "",
                "numberprefix": "",
                "plotgradientcolor": "",
                "bgcolor": "FFFFFF",
                "showalternatehgridcolor": "0",
                "divlinecolor": "CCCCCC",
                "showvalues": "0",
                "showcanvasborder": "0",
                "canvasborderalpha": "0",
                "canvasbordercolor": "CCCCCC",
                "canvasborderthickness": "1",
                "yaxismaxvalue":  max([max(it['data']) for it in mentions_list_by_objects]),
                "captionpadding": "30",
                "linethickness": "3",
                "yaxisvaluespadding": "15",
                "legendshadow": "0",
                "legendborderalpha": "0",
                "palettecolors": ",".join([COLORS[it['object'].id] for it in mentions_list_by_objects]),
                "showborder": "0",
                "showlegend": "0",
            },

            "categories": [
                {
                    "category": [
                        {
                            "label": DAYS[i],
                        } for i in ['0%d-01-2014' % it for it in days]]
                }
            ],
            "dataset": [
                {
                    "seriesname": it['object'].title,
                    "data": [
                        {
                            "value": '%.4f' % i
                        } for i in it['data']]
                } for it in mentions_list_by_objects
            ]
        }
    }), content_type='application/json')


def analytic_product_3_vympelkom(request, slug, referred_id=0):

    regions = map(lambda x: int(x), request.GET.getlist('region', [1, 2, 3]))
    regions_obj = Region.objects.filter(id__in=regions)
    tones = map(lambda x: TONE_LIST[str(x)], request.GET.getlist('tone', []))
    objects = map(lambda x: int(x), request.GET.getlist('objects', [7, 8, 9]))
    objects_obj = ReferredObject.objects.filter(id__in=objects).order_by('index_sort')

    result = [
        (
            region.title, dict([
                    (
                        obj, region.article_set.filter(id__in=obj.mentionsobject_set.all().values_list('id', flat=True))
                    ) for obj in objects_obj
                ]),
        ) for region in regions_obj
    ]
    if tones:
        for reg in result:
            for k, v in reg[1].items():
                reg[1][k] = v.filter(id__in=k.mentionsobject_set.filter(_get_in('emotional_tone_object__title', tones)).values_list('article_id', flat=True))

    for obj in result:
        for k, v in obj[1].items():
            obj[1][k] = sum([int(float(ar.mo)) if ar.mo else 0 for ar in v])


    return HttpResponse(json.dumps({
        'status': 'OK',
        'chart': {
            "chart": {
                "caption": "",
                "numberprefix": "",
                "plotgradientcolor": "",
                "bgcolor": "FFFFFF",
                "showalternatehgridcolor": "0",
                "divlinecolor": "CCCCCC",
                "showvalues": "0",
                "showcanvasborder": "0",
                "canvasborderalpha": "0",
                "canvasbordercolor": "CCCCCC",
                "canvasborderthickness": "1",
                "yaxismaxvalue": "30000",
                "captionpadding": "30",
                "yaxisvaluespadding": "15",
                "legendshadow": "0",
                "legendborderalpha": "0",
                "palettecolors": "#f8bd19,#008ee4,#33bdda,#e44a00,#6baa01,#583e78",
                "showplotborder": "0",
                "showborder": "0",
                "showlegend": "0"
            },
            "categories": [
                {
                    "category": [
                        {
                            "label": res[0]
                        } for res in result]
                }
            ],
            "dataset": [
                {
                    "seriesname": obj.title,
                    "color": COLORS[obj.id],
                    "data": [{
                        "value": row[1][obj]
                    } for row in result]
                } for obj in objects_obj]
        }}), content_type='application/json')


def analytic_product_4_vympelkom(request, slug, referred_id=0):
    project = Project.objects.get(slug=slug)
    regions = map(lambda x: int(x), request.GET.getlist('region', [1, 2, 3]))
    regions_obj = Region.objects.filter(id__in=regions)
    objects = map(lambda x: int(x), request.GET.getlist('objects', [7]))
    objects_obj = ReferredObject.objects.filter(id__in=objects).order_by('index_sort')


    title_list = [u'Позитивное', u'Нейтральное', u'Негатавное']

    result = [
        (
            t, dict([
                    (
                        obj, obj.mentionsobject_set.filter(emotional_tone_object__title=t.lower()).filter(article__region__id__in=regions)
                    ) for obj in objects_obj
                ]),
        ) for t in title_list
    ]
    # if tones:
    #     for reg in result:
    #         for k, v in reg[1].items():
    #             reg[1][k] = v.filter(id__in=k.mentionsobject_set.filter(_get_in('emotional_tone_object__title', tones)).values_list('article_id', flat=True))

    for obj in result:
        for k, v in obj[1].items():
            obj[1][k] = len(v)


    return HttpResponse(json.dumps({
        'status': 'OK',
        'chart': {
            "chart": {
                "caption": "",
                "numberprefix": "",
                "plotgradientcolor": "",
                "bgcolor": "FFFFFF",
                "showalternatehgridcolor": "0",
                "divlinecolor": "CCCCCC",
                "showvalues": "0",
                "showcanvasborder": "0",
                "canvasborderalpha": "0",
                "canvasbordercolor": "CCCCCC",
                "canvasborderthickness": "1",
                # "yaxismaxvalue": "30000",
                "captionpadding": "30",
                "yaxisvaluespadding": "15",
                "legendshadow": "0",
                "legendborderalpha": "0",
                "palettecolors": "#f8bd19,#008ee4,#33bdda,#e44a00,#6baa01,#583e78",
                "showplotborder": "0",
                "showborder": "0",
                "showlegend": "0"
            },
            "categories": [
                {
                    "category": [
                        {
                            "label": it
                        } for it in title_list]
                }
            ],
            "dataset": [
                {
                    "seriesname": obj.title,
                    "color": COLORS[obj.id],
                    "data": [{
                        "value": row[1][obj]
                    } for row in result]
                } for obj in objects_obj]
        }}), content_type='application/json')


def analytic_product_5_vympelkom(request, slug, referred_id=0):
    days = map(lambda x: int(x), request.GET.getlist('days', [1, 2, 3, 4, 5, 6, 7, 8, 9]))
    project = Project.objects.get(slug = slug)
    regions = map(lambda x: str(x), request.GET.getlist('region', [1, 2, 3]))
    objects = map(lambda x: int(x), request.GET.getlist('objects', [7]))
    objects_obj = ReferredObject.objects.filter(id__in=objects).order_by('index_sort')

    # f = [{}project.medialist_set.all()[0].article_set.filter(date__gte=mount1, date__lt=mount2)]
    if regions:
        f = [{"label": media.title, "value": media.article_set.filter(id__in=objects_obj[0].mentionsobject_set.values_list('article_id', flat=True)).filter(get_days_filter_for_mo(days)).filter(_get_in('region__id', regions)).count()} for media in project.mediatype_set.all()]
    else:
        f = [{"label": media.title, "value": media.article_set.filter(id__in=objects_obj[0].mentionsobject_set.values_list('article_id', flat=True)).filter(get_days_filter_for_mo(days)).count()} for media in project.mediatype_set.all()]

    return HttpResponse(json.dumps({
        'status': 'OK',
        'chart': {
            "chart": {
                "caption": "",
                "subcaption": "",
                "paletteColors": "#dfe1d7,#f9f5d7,#7eb2b8,#948a54,#8e0000",
                 "bgColor": "#ffffff",
                 "showBorder": "0",
                 "use3DLighting": "0",
                 "showShadow": "0",
                 "enableSmartLabels": "0",
                 "startingAngle": "310",
                 "showLabels": "0",
                 "showPercentValues": "1",
                 "showLegend": "1",
                 "legendShadow": "0",
                 "legendBorderAlpha": "0",
                "defaultcenterlabel": "",
                "centerlabel": "",
                "theme": "fint"
            },
            "data": f
        }}), content_type='application/json')


def analytic_product_6_vympelkom(request, slug, referred_id=0):
    days = map(lambda x: int(x), request.GET.getlist('days', [1, 2, 3, 4, 5, 6, 7, 8, 9]))
    project = Project.objects.get(slug=slug)
    regions = map(lambda x: str(x), request.GET.getlist('region', [1, 2, 3]))
    objects = map(lambda x: int(x), request.GET.getlist('objects', [7]))
    objects_obj = ReferredObject.objects.filter(id__in=objects).order_by('index_sort')
    # if regions:
    articles = project.articles.filter(id__in=objects_obj[0].mentionsobject_set.values_list('article_id', flat=True)).filter(get_days_filter_for_mo(days)).filter(_get_in('region__id', regions))
    res = {}
    for it in articles:
        if it.branch not in res.keys():
            res[it.branch] = 1
        else:
            res[it.branch] += 1
    f = sorted(
        [
            {
                "label": branch,
                "value": count
            } for branch, count in res.items()], key=lambda x: x['value'], reverse=True)[:5]
    return HttpResponse(json.dumps({
        'status': 'OK',
        'chart': {
            "chart": {
                "caption": "",
                "subcaption": "",
                "yaxisname": "",
                "xaxisname": "",
                "bgcolor": "FFFFFF",
                "showalternatevgridcolor": "0",
                "showplotborder": "0",
                "showvalues": "0",
                "divlinecolor": "CCCCCC",
                "showcanvasborder": "0",
                "plotgradientcolor": "",
                "tooltipbordercolor": "FFFFFF",
                "palettecolors": "006666",
                "canvasborderalpha": "0",
                "showBorder": "0",
                "showLegend": "0"
            },
            "data": f
    }}), content_type='application/json')


def analytic_product_7_vympelkom(request, slug, referred_id=0):
    from django.db.models import Count
    days = map(lambda x: int(x), request.GET.getlist('days', [1, 2, 3, 4, 5, 6, 7, 8, 9]))
    project = Project.objects.get(slug=slug)
    regions = map(lambda x: str(x), request.GET.getlist('region', [1, 2, 3]))
    objects = map(lambda x: int(x), request.GET.getlist('objects', [7]))
    objects_obj = ReferredObject.objects.filter(id__in=objects).order_by('index_sort')

    sources =  project.source_set.all()
    articles = project.articles.filter(id__in=objects_obj[0].mentionsobject_set.values_list('article_id', flat=True)).filter(get_days_filter_for_mo(days)).filter(_get_in('region__id', regions))

    f = sorted(
        [
            {
                "label": s.title,
                "value": len(filter(lambda x: x.source.id == s.id, articles))
            } for s in sources
        ], key=lambda x: x["value"], reverse=True)[:5]

    # f = [{}project.medialist_set.all()[0].article_set.filter(date__gte=mount1, date__lt=mount2)]

    # f = [{"label": media.title, "value": media.article_set.filter(date__gte=mount1, date__lt=mount2).count()} for media in project.medialist_set.all()]


    return HttpResponse(json.dumps({
        'status': 'OK',
        'chart': {
            "chart": {
                "caption": "",
                "subcaption": "",
                "yaxisname": "",
                "xaxisname": "",
                "bgcolor": "FFFFFF",
                "showalternatevgridcolor": "0",
                "showplotborder": "0",
                "showvalues": "0",
                "divlinecolor": "CCCCCC",
                "showcanvasborder": "0",
                "plotgradientcolor": "",
                "tooltipbordercolor": "FFFFFF",
                "palettecolors": "006666",
                "canvasborderalpha": "0",
                "showBorder": "0",
                "showLegend": "0"
            },
            "data": f
    }}), content_type='application/json')


def filter_parameters_vympelcom(request):
    project = project = Project.objects.get(slug='vympelkom')
    return HttpResponse(json.dumps({
        'days': [
            {'title': '1 Января', 'id': 1},
            {'title': '2 Января', 'id': 2},
            {'title': '3 Января', 'id': 3},
            {'title': '4 Января', 'id': 4},
            {'title': '5 Января', 'id': 5},
            {'title': '6 Января', 'id': 6},
            {'title': '7 Января', 'id': 7},
            {'title': '8 Января', 'id': 8},
            {'title': '9 Января', 'id': 9}
        ],
        'region': [
            {'title': it.title, 'id': it.id} for it in project.region_set.all()
        ],
        'tone': [
            {'title': 'нейтральное', "id": 1},
            {'title': 'позитивное', "id": 2},
            {'title': 'негативное', "id": 3}
        ],
        'objects': [
            {'title': it.title, 'id': it.id} for it in project.referred_objects.order_by('index_sort')
        ],
    }), content_type='application/json')
from django.template.loader import get_template, render_to_string
import cStringIO as StringIO
import ho.pisa as pisa
links = lambda uri, rel: os.path.join(settings.MEDIA_ROOT, uri.replace(settings.MEDIA_URL, ''))
from django.template import Context, RequestContext
def export_pdf(request):
    html  = render_to_string('email/messageTwo.html', { 'pagesize' : 'A4', }, context_instance=RequestContext(request, {
                'pagesize':'A4',
            }))
    result = StringIO.StringIO()
    pdf = pisa.pisaDocument(StringIO.StringIO(html.encode("UTF-8")), dest=result, link_callback=links, encoding='UTF-8' )
    if not pdf.err:
        return result
    else:
        return None


def render_to_pdf(template_src, context_dict):
    template = get_template(template_src)
    context = Context(context_dict)
    html  = template.render(context)
    result = StringIO.StringIO()
    pdf = pisa.pisaDocument(StringIO.StringIO(html.encode("UTF-8")), result, link_callback=links, encoding='UTF-8')
    if not pdf.err:
        return HttpResponse(result.getvalue(), content_type='application/pdf')
    return HttpResponse('We had some errors<pre>%s</pre>' % re.escape(html))
from docx import Document
path = "template.docx"



def generate_report(document, title, annotation, text):
    document.add_paragraph(title, style='Header')
    document.add_paragraph(annotation, style='Annotation')
    document.add_paragraph(text, style='TextContent')


    document = Document(path)
    title = u"python-docx is a Python library for creating and updating Microsoft Word (.docx) files."
    annotation = u"Here’s an example of what python-docx can do"
    text = u"Angular is not a single piece in the overall puzzle of building the client-side of a web application. It handles all of the DOM and AJAX glue code you once wrote by hand and puts it in a well-defined structure. This makes Angular opinionated about how a CRUD application should be built. But while it is opinionated, it also tries to make sure that its opinion is just a starting point you can easily change. Angular comes with the following out-of-the-box:"

    generate_report(document, title, annotation, text)
    document.save('demo.docx')

def convert_docx(request):
    file_to_be_sent= render_to_pdf('email/messageTwo.html',{})
    result = export_pdf(request)
    document = Document()
    document.add_heading('Document Title', 0)
    p = document.add_paragraph('A plain paragraph having some ')
    p.add_run('bold').bold = True
    p.add_run(' and some ')
    p.add_run('italic.').italic = True
    document.add_heading('Heading, level 1', level=1)
    document.add_paragraph('Intense quote', style='IntenseQuote')
    document.add_paragraph(
        'first item in unordered list', style='ListBullet'
    )
    document.add_paragraph(
        'first item in ordered list', style='ListNumber'
    )
    document.add_page_break()
    document.save('demo.docx')
     # response['X-Sendfile'] = os.path.join(BASE_DIR, 'media/demo.doc')
    # response = HttpResponse(os.path.join(BASE_DIR, 'media/demo.doc'), content_type='application/vnd.msword')
    file_path = os.path.join(BASE_DIR, 'media/demo.doc')
    # fp = open(file_path, 'rb')
    # # response = HttpResponse(fp.read())
    # fp.close()
    #
    # type = 'application/vnd.msword'
    # response['Content-Type'] = type
    # response['Content-Length'] = str(os.stat(file_path).st_size)

    # return response
    file = open(os.path.join(BASE_DIR, 'media/demo.doc'), 'r')
    response = HttpResponse(file, content_type ='application/vnd.msword')
    response['Content-Disposition'] = 'attachment; filename=yoyoyoyoyo.doc'

    # It's usually a good idea to set the 'Content-Length' header too.
    # You can also set any other required headers: Cache-Control, etc.
    return response

def no_convert(request):
    return TemplateResponse(request,'email/messageTwo.html', locals())


def save_product_settings(request, id):
    if request.method == "POST":
        data = json.loads(request.body)
        monitoring_settings = MonitoringProductSetting.objects.get_or_create(user=request.user, monitoring_product=MonitoringProduct.objects.get(id=id))[0]
        monitoring_settings.setting = json.dumps(data['settings'])
        monitoring_settings.save()
    return HttpResponse(json.dumps({
        'status': 'OK'
    })
    )


def get_monitoring_product_filters(request, id, slug=None):
    monitoring_product_item = MonitoringProduct.objects.get(id=id)

    project = Project.objects.get(slug = slug)
    # project = Project.objects.get(slug = slug)
    articles = monitoring_product_item.get_article_set().all()
    mention_objects_filter =  project.mentionsobject_set.all()
    prv_max = mention_objects_filter.aggregate(Max('prv')).values()[0]
    prv_min = mention_objects_filter.aggregate(Min('prv')).values()[0]
    mo_max = articles.aggregate(Max('mo')).values()[0]
    mo_min = articles.aggregate(Min('mo')).values()[0]
    prt_max = mention_objects_filter.aggregate(Max('prt')).values()[0]
    prt_min = mention_objects_filter.aggregate(Min('prt')).values()[0]


    # mq
    mq_max = mention_objects_filter.aggregate(Max('mq')).values()[0]
    mq_min = mention_objects_filter.aggregate(Min('mq')).values()[0]
    filters_from_article = {
    'day__id__in' : monitoring_product_item.day_set.values('title','id').distinct().order_by('title'),
    'month__id__in' : monitoring_product_item.month_set.values('title','id').distinct().order_by('title'),
    'week__id__in' : monitoring_product_item.week_set.values('title','id').distinct().order_by('title'),
    'media_level__id__in' : monitoring_product_item.medialevel_set.filter(~Q(title=""), title__isnull = False).values('title','id').distinct().order_by('title'),
    'title__id__in' : monitoring_product_item.articletitle_set.values('title','id').distinct().order_by('title'),
    'federal_district__id__in' : monitoring_product_item.federaldistrict_set.values('title','id').distinct().order_by('title'),
    'country__id__in' : monitoring_product_item.country_set.values('title','id').distinct().order_by('title'),
    'media_view__id__in' : monitoring_product_item.mediaview_set.values('title','id').distinct().order_by('title'),
    'media_list__id__in' : monitoring_product_item.medialist_set.values('title','id').distinct().order_by('title'),
    'media_category__id__in' : monitoring_product_item.mediacategory_set.values('title','id').distinct().order_by('title'),
    'material_genre__id__in' : monitoring_product_item.materialgenre_set.values('title','id').distinct().order_by('title'),
    'image__id__in' : monitoring_product_item.image_set.values('title','id').distinct().order_by('title'),
    'media_type__id__in' : monitoring_product_item.mediatype_set.values('title','id').distinct().order_by('title'),
    'interaction_kind__id__in' : monitoring_product_item.interactionkind_set.values('title','id').distinct().order_by('title'),
    'penetration__id__in' : monitoring_product_item.penetration_set.values('title','id').distinct().order_by('title'),
    'visibility__id__in' : monitoring_product_item.visibility_set.values('title','id').distinct().order_by('title'),
    'visibility_special__id__in' : monitoring_product_item.visibilityspecial_set.values('title','id').distinct().order_by('title'),
    'macroregion__id__in' : monitoring_product_item.macroregion_set.values('title','id').distinct().order_by('title'),
    'region__id__in' : monitoring_product_item.region_set.values('title','id').distinct().order_by('title'),
    'bissines_direction__id__in' : monitoring_product_item.bissinesdirection_set.values('title','id').distinct().order_by('title'),
    'city__id__in' : monitoring_product_item.city_set.values('title','id').distinct().order_by('title'),
    'source__id__in' : monitoring_product_item.source_set.values('title','id').distinct().order_by('title'),
    'author__id__in' : monitoring_product_item.author_set.values('title','id').distinct().order_by('title'),
    'branch__id__in': monitoring_product_item.branch_set.values('title','id').distinct().order_by('title'),
    'info_reason__id__in' : monitoring_product_item.inforeason_set.values('title','id').distinct().order_by('title'),
    'media_list_special__id__in' : monitoring_product_item.medialistspecial_set.values('title','id').distinct().order_by('title'),
    'media_type_special__id__in' : monitoring_product_item.mediatypespecial_set.values('title','id').distinct().order_by('title'),
    'media_category_special__id__in' : monitoring_product_item.mediacategory_set.values('title','id').distinct().order_by('title'),
    'km_special__id__in' : monitoring_product_item.kmspecial_set.values('title','id').distinct().order_by('title'),
    'brand_or_product__id__in' : monitoring_product_item.brandorproduct_set.values('title','id').distinct().order_by('title'),
    'speaker_quote__id__in' : monitoring_product_item.speakerquote_set.values('title','id').distinct().order_by('title'),
    'third_patry_speakers__id__in' : monitoring_product_item.thirdpartyspeakers_set.values('title','id').distinct().order_by('title'),
    'headder_tonality__id__in' : monitoring_product_item.headertonality_set.values('title','id').distinct().order_by('title'),
    'main_tonality__id__in' : monitoring_product_item.maintonality_set.values('title','id').distinct().order_by('title'),
    'global_local_event__id__in' : monitoring_product_item.globallocalevent_set.values('title','id').distinct().order_by('title'),
    'emotional_tone_material__id__in' : monitoring_product_item.emotionaltonematerial_set.values('title','id').distinct().order_by('title'),
    'emotional_tone_title__id__in' : monitoring_product_item.emotionaltonetitle_set.values('title','id').distinct().order_by('title'),
    'url__id__in' : monitoring_product_item.url_set.values('title','id').distinct().order_by('title'),
    'thematic_category__id__in' : monitoring_product_item.thematiccategory_set.values('title','id').distinct().order_by('title'),
    'material_reason__id__in' : monitoring_product_item.materialreason_set.values('title','id').distinct().order_by('title'),
    'id_press_release__id__in' : monitoring_product_item.idpressrelease_set.values('title','id').distinct().order_by('title'),
    'tag__id__in' : monitoring_product_item.tag_set.values('title','id').distinct().order_by('title'),
    'edge_region__id__in' : monitoring_product_item.edgeregion_set.values('title','id').distinct().order_by('title'),
    'referred_object__id__in' : project.referred_objects.values('title','id').distinct().order_by('title'),
    'duration__id__in' : monitoring_product_item.duration_set.values('title','id').distinct().order_by('title'),
    'cover__range' : [monitoring_product_item.get_article_set().aggregate(Min('cover')).values()[0], monitoring_product_item.get_article_set().aggregate(Max('cover')).values()[0]],
    'infographics__range' : [monitoring_product_item.get_article_set().aggregate(Min('infographics')).values()[0], monitoring_product_item.get_article_set().aggregate(Max('infographics')).values()[0]],
    'km_result': 'true',

    'emotional_tone_object__id__in' : monitoring_product_item.emotionaltoneobject_set.values('title','id').distinct().order_by('title'),
    'object_role__id__in' : monitoring_product_item.objectrole_set.values('title','id').distinct().order_by('title'),
    'technologies__id__in' : monitoring_product_item.technologies_set.values('title','id').distinct().order_by('title'),
    'advertisement_promo__id__in' : monitoring_product_item.advertisementpromo_set.values('title','id').distinct().order_by('title'),
    'initiation__id__in' : monitoring_product_item.initiation_set.values('title','id').distinct().order_by('title'),
    'speaker__id__in' : monitoring_product_item.speaker_set.extra(select={'title': 'fio'}).values('title', 'id').distinct().order_by('title'),
    'speaker_category__id__in' : monitoring_product_item.speakercategory_set.values('title','id').distinct().order_by('title'),
    'speaker_speech_hate__id__in' : monitoring_product_item.speakerspeechhate_set.values('title','id').distinct().order_by('title'),
    'speaker_level__id__in' : monitoring_product_item.speakerlevel_set.values('title','id').distinct().order_by('title'),
    'side__speaker__id__in' : monitoring_product_item.sidespeaker_set.extra(select={'title': 'fio'}).values('title', 'id').distinct().order_by('title'),
    'side_speaker_tonality__id__in' : monitoring_product_item.sidespeakertonality_set.values('title','id').distinct().order_by('title'),
    'side_speaker_status__id__in' : monitoring_product_item.sidespeakerstatus_set.values('title','id').distinct().order_by('title'),
    'speaker_quote_uniqueness__id__in' : monitoring_product_item.speakerquoteuniqueness_set.values('title', 'id').distinct().order_by('title'),
    'release_time' : 'true',
    'mq__range': [mq_min, mq_max],
    'prt__range': [prt_min, prt_max],
    'prv__range':[prv_min, prv_max],
    'mo__range': [mo_min, mo_max],
    'km__id__in' : monitoring_product_item.km_set.values('title', 'id').distinct().order_by('title'),}
    constructor_widget = json.loads(ConstructorWidget.objects.get(monitoring_product_id = id).data)
    filters = {}
    for elem in constructor_widget:
        if elem['a3'] !='':
            filters.update({elem['a10']: {
                'list': list(filters_from_article[elem['a10']]),
                'settings' : elem,
            }})
    # filters = sorted(filters.items(), key = lambda (k,v): (v['settings']['a3'],k))
    if request.method == 'POST':
        constructor_widget = sorted(constructor_widget, key=lambda k: k['a3'])
        index = 0
        for value in constructor_widget:
            if value['a3'] !='' and index < len(filters) -1 :
                constructor_widget[index] = filters[index][1]['settings']
                index +=1
    project = Project.objects.get(id=monitoring_product_item.project.id)
    mention_objects_list = project.referred_objects.all()
    monitoring_product_mention = monitoring_product_item.monitoring_product_mentions.all()
    mention_objects_list_to_template = []
    for elem in mention_objects_list:
        keywords = {}
        checked = False
        color = "666666"
        for el in monitoring_product_mention:
            if elem.id == el.mention_object.id:
                color = el.color
                checked = True
                keywords = el.monitoring_product_mention_words.all()
            #keywords = serializers.serialize('json', keywords)
        mention_objects_list_to_template.append(
            {'id': elem.id, 'title': elem.title, 'checked': checked, 'color': color,
             'keywords': list(keywords.values())})
    monitoring_product_category = monitoring_product_item.monitoring_product_category.all()

    category_list_to_template = []
    for el in monitoring_product_category:
        category_list_to_template.append({'id': el.category.id, 'index': el.index_sort, 'title': el.category.title})
    if monitoring_product_item.parent_id is not None:
        for key in monitoring_product_item.settings.keys():
            filters[key] = monitoring_product_item.settings[key]

    return HttpResponse(json.dumps({
        'status': 'OK',
        'filters': filters,
        'mention_objects_list': mention_objects_list_to_template,
        'category_list': category_list_to_template,
    })
    )


def create_or_update_sub_product(request, id):
    #
    # if not request.user.has_perm('cusrom_user.can_create_product'):
    #     raise PermissionDenied

    data = request.body
    data = json.loads(data)
    type = data['product']['type']
    title = data['product']['title']
    # project = data['product']['project_name']
    try:
        monitoring_product = MonitoringProduct.objects.get(title = title, id = id)
        product = MonitoringProduct.objects.get(id = id)
        product.type = type
        product.title = title
        # product.project_id = int(project)
        product.save()
        MonitoringProductCategory.objects.filter(monitoring_product=product).delete()
        for key in data['category_list']:
            monitoring_category = MonitoringProductCategory.objects.create(category_id=int(key['id']),
                                                                           index_sort=int(key['index']), \
                                                                           monitoring_product=product)
        ment_prod_list = MonitoringProductMention.objects.filter(monitoring_product=product)
        for el in ment_prod_list:
            MonitoringProductMentionWord.objects.filter(monitoring_product_mention=el).delete()
            el.delete()
        for key in data['mention_objects_list']:
            monitoring_product_mention = MonitoringProductMention.objects.create(color=key['color'],
                                                                                 monitoring_product=product, \
                                                                                 mention_object_id=int(key['id']))
            for elem in key['keywords']:
                word = MonitoringProductMentionWord.objects.create(word=elem['word'],
                                                                   monitoring_product_mention=monitoring_product_mention)
    except MonitoringProduct.DoesNotExist:
        monitoring_product = MonitoringProduct.objects.get(id = id)
        category_list = monitoring_product.get_category_list()
        monitoring_product_mentions = monitoring_product.monitoring_product_mentions.all()
        monitoring_product.title = title
        monitoring_product.id = None
        monitoring_product.parent_id = id
        monitoring_product.save()
        monitoring_product.users_with_perm.add(request.user)
        for elem in monitoring_product_mentions:
            words = elem.get_keywords_list()
            elem.monitoring_product = monitoring_product
            elem.id = None
            elem.save()
            for el in words:
                word = el
                word.monitoring_product_mention = elem
                word.id = None
                word.save()
        for category in category_list:
            category.monitoring_product = monitoring_product
            category.id = None
            category.save()
    monitoring_product.settings = data.get('filters')
    monitoring_product.time_from = data['product'].get('date_from', datetime(1980, 1, 1))
    monitoring_product.time_to = data['product'].get('date_to', datetime(2050, 1, 1))
    monitoring_product.save()

    return HttpResponse(json.dumps({
        'status': 'OK',
        'id': monitoring_product.pk,
        })
    )


class GetMonitoringProductData(JSONResponseMixin, View):

    def get(self, request, *args, **kwargs):
        product = get_object_or_404(MonitoringProduct, pk=self.kwargs.get('pk'))
        context = {
            "id": product.pk,
            "project_id": product.project_id,
            "title": product.title,
            "type": product.type,
            "data_type": product.data_type,
            "article_count": product.article_count
        }
        return self.render_json_response(context)

get_product_data = GetMonitoringProductData.as_view()


class GetMonitoringProductSettings(JSONResponseMixin, View):

    def get(self, request, id, *args, **kwargs):
        settings, created = MonitoringProductSetting.objects.get_or_create(user = request.user, monitoring_product = MonitoringProduct.objects.get(id=id))
        context = {
            "id": id,
            "settings": json.loads(settings.setting),
        }
        return self.render_json_response(context)

get_product_settings = GetMonitoringProductSettings.as_view()


class GetMonitoringProductSources(JSONResponseMixin, View):

    def get(self, request, slug, *args, **kwargs):
        products = MonitoringProduct.objects.filter(project__slug=slug, parent_id=None).\
            annotate(num_articles=Count('article')).filter(num_articles__gt=0).values('id', 'title')
        return self.render_json_response(list(products))

get_product_sources = GetMonitoringProductSources.as_view()


    # class MonitoringProduct(models.Model):
    # project = models.ForeignKey(Project, verbose_name=u'Проект', related_name='monitoring_products')
    # title = models.CharField(max_length=200, verbose_name=u'Название')
    # type = models.CharField(max_length=20, verbose_name=u'Тип мониторинга', choices=TYPE)
    # data_type = models.CharField(max_length=20, verbose_name=u'Мониторинг', choices=DATA_TYPE, default='1')
    # # day_start = models.CharField(max_length=20, verbose_name=u'День выпуска мониторинга', choices=DAYS, blank=True,
    # #                              null=True)
    # # time_start = models.TimeField(verbose_name=u'Время начала')
    # settings = models.TextField(blank= True, null= True)
    # article_count = models.IntegerField(default=1, verbose_name=u'Число статей на странице')
    # # def constructor_widget0_link(self, obj):
    # #     return '/' + self.project.slug + '/monitoring_product/' + self.pk +  '/widget0/constructor/'
    # class Meta:

