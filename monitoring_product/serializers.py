##from sqlalchemy import null
#from main.models import Project, Article, ReferredObject, MentionsObject, Speaker, Category
#from .models import (MonitoringProduct, MonitoringProductCategory,
#                     MonitoringProductMentionWord, MonitoringProductMention)
#from rest_framework import serializers
#
#
#class CustomerGlobal(object):
#    def get_title(self, obj):
#        return obj.title
#
#    def obj_get_children(self, obj):
#        children =  obj.get_children()
#        if children:
#            resul = True
#        else:
#            resul = False
#        return resul
#
#    def get_color(self, obj):
#        return obj.get_colors()
#
#    def get_date(self, obj):
#
#        date  = '%s'%(obj.date)
#        if date == "None":
#            date = None
#        return date
#
#
#class SerializateFK(serializers.SerializerMethodField):
#    def field_to_native(self, obj, field_name):
#        #method, objecta = self.method_name.split('__')
#        object_new = getattr(obj, field_name, False)
#        if object_new:
#            value = getattr(self.parent, self.method_name)(getattr(obj, field_name))
#            return self.to_native(value)
#
#
#class MonitoringProductMentionWordSerializer(serializers.ModelSerializer):
#    class Meta:
#        model = MonitoringProductMentionWord
#        fields = ('word',)
#
#class MonitoringProductMentionSerializer(CustomerGlobal, serializers.ModelSerializer):
#    monitoring_product_mention_words = MonitoringProductMentionWordSerializer(many=True)
#    mention_object = SerializateFK('get_title')
#    class Meta:
#        model = MonitoringProductMention
#        fields = ('monitoring_product_mention_words', 'mention_object', 'color', )
#
#class MonitoringProductCategorySerializer(CustomerGlobal ,serializers.ModelSerializer):
#
#    category = SerializateFK('get_title')
#    parent = SerializateFK('get_title')
#    children = SerializateFK('obj_get_children')
#    class Meta:
#        model = MonitoringProductCategory
#        fields = ('index_sort', 'category', 'parent', 'id', 'children',)
#
#
#class MonitoringProductSerializer(serializers.ModelSerializer):
#    monitoring_product_category = MonitoringProductCategorySerializer(many=True)
#    monitoring_product_mentions = MonitoringProductMentionSerializer(many=True)
#
#    class Meta:
#        model = MonitoringProduct
#        #exclude = ('time_start',)
#        fields = ('title', 'monitoring_product_category', 'monitoring_product_mentions',)
#
#class ReferredObjectSerializer(serializers.ModelSerializer):
#
#    class Meta:
#        model = ReferredObject
#        fields = ('title', 'index_sort',)
#
#
#class SpeakerSerializer(serializers.ModelSerializer):
#    class Meta:
#        model = Speaker
#        fields = ('fio', 'mentions_object',)
#
#class MentionObjectSerializer(CustomerGlobal, serializers.ModelSerializer):
#    def get_color(self, obj):
#        return obj.get_colors
#    emotional_tone_object = SerializateFK('get_title')
#    object_role = SerializateFK('get_title')
#    speakers = SpeakerSerializer(many=True)
#    color = serializers.BaseSerializer('get_color')
#    class Meta:
#        model = MentionsObject
#
#        fields = ('prt', 'prv', 'id', 'speakers', 'mo', 'referred_object', 'emotional_tone_object', 'object_role', 'color', )
#
#class CategorySerializer(serializers.ModelSerializer):
#    class Meta:
#        model = Category
#
#
#
#class ArticleSerializer(CustomerGlobal, serializers.ModelSerializer):
#    #def check(value):
#    #    if value == null:
#    #        result = None
#    #    else:
#    #        result = value
#    #    return result
#
#    #def __getattr__(self, item):
#    #    if 'get_item' in item:
#    #        print 'get_item', item
#    #        getattr(self, item)
#        #super(ArticleSerializer, self).__getattr__(item)
#
#    country = SerializateFK('get_title')
#    federal_district = SerializateFK('get_title')
#    region = SerializateFK('get_title')
#    #city = SerializateFK('get_title')
#    media_level = SerializateFK('get_title')
#    media_category = SerializateFK('get_title')
#    media_type = SerializateFK('get_title')
#    mention_objects = MentionObjectSerializer(many=True)
#
#    date_to = serializers.SerializerMethodField('get_date',)
#
#
#    #federal_district = check(federal_district)
#    class Meta:
#        model = Article
#
#        fields = ('id','date_to','title', 'url', 'source', 'annotation', 'description', 'mo', 'federal_district', 'region', 'country',\
#                  'city', 'author', 'media_level', 'media_category', 'media_type', 'mention_objects', 'category', )
#
#
#class ProjectSerializer(serializers.ModelSerializer):
#    articles = ArticleSerializer(many=True)
#    monitoring_products = MonitoringProductSerializer(many=True)
#    referred_objects = ReferredObjectSerializer(many=True)
#    #category = CategorySerializer(many=True)
#
#    class Meta:
#        model = Project
#        fields = ('title', 'articles', 'referred_objects',)