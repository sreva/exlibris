# --*-- coding: utf-8 --*--
import os
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist

from django.db import models

# Create your models here.
from django.db.models.signals import post_save
from custom_user.models import CustomUser
from info.settings import BASE_DIR
import xlrd
import json
from main.models import Project, Category, MentionsObject, ReferredObject
from jsonfield import JSONField

TYPE = (
    ('day', u'ежедневный'),
    ('week', u'еженедельный'),
    ('month', u'ежемесячный'),
    ('date_release', u'Дата выхода'),
    ('date_upload', u'Дата загрузки'),

)
DAYS = (
    ('0', u'понедельник'),
    ('1', u'вторник'),
    ('2', u'середа'),
    ('3', u'четверг'),
    ('4', u'пятница'),
    ('5', u'суббота'),
    ('6', u'воскресение'),
)

DATA_TYPE = (
    ('1', u'традиционных сми'),
    ('2', u'социальных сми'),


)


class MonitoringProduct(models.Model):
    project = models.ForeignKey(Project, verbose_name=u'Проект', related_name='monitoring_products')
    parent = models.ForeignKey('self', verbose_name=u'Родитель', related_name='subproducts', null=True, blank=True)
    title = models.CharField(max_length=200, verbose_name=u'Название')
    type = models.CharField(max_length=20, verbose_name=u'Тип мониторинга', choices=TYPE)
    data_type = models.CharField(max_length=20, verbose_name=u'Мониторинг', choices=DATA_TYPE, default='1')
    time_from = models.DateTimeField(verbose_name=u'Время от', blank=True, null=True)
    time_to = models.DateTimeField(verbose_name=u'Время до', blank=True, null=True)
    # day_start = models.CharField(max_length=20, verbose_name=u'День выпуска мониторинга', choices=DAYS, blank=True,
    #                              null=True)
    # time_start = models.TimeField(verbose_name=u'Время начала')
    settings = JSONField(verbose_name=u"Настройки", blank=True, null=True)
    article_count = models.IntegerField(default=1, verbose_name=u'Число статей на странице')
    created_by = models.ForeignKey(CustomUser, verbose_name=u"Создан пользователем",
                                   related_name="created_products", blank=True, null=True)
    created = models.DateTimeField(verbose_name=u"Создан", auto_now_add=True, blank=True, null=True)
    changed_by = models.ForeignKey(CustomUser, verbose_name=u"Изменен пользователем",
                                   related_name="changed_products", blank=True, null=True)
    changed = models.DateTimeField(verbose_name=u"Изменен", auto_now=True, blank=True, null=True)
    product_source = models.BooleanField(default=False)
    # def constructor_widget0_link(self, obj):
    #     return '/' + self.project.slug + '/monitoring_product/' + self.pk +  '/widget0/constructor/'
    class Meta:
        verbose_name = u'Мониторинговий продукт'
        verbose_name_plural = u'Мониторинговие продукты'

    def __unicode__(self):
        return self.title

    def get_category_list(self):
        return self.monitoring_product_category.all()

    def get_article_set(self):
        if self.parent:
            return self.parent.article_set
        return self.article_set

    def get_monitoring_product_mention_list(self):
        return self.monitoring_product_mentions.all()





class MonitoringProductSetting(models.Model):
    user = models.ForeignKey(User)
    monitoring_product = models.ForeignKey(MonitoringProduct)
    setting = models.TextField(blank=True, null=True,
                               default=u'{"mention_list":[{"text":"Объекты","name":"title","id":51},'
                                       u'{"text":"ФИО спикера","name":"speakers","id":52}],'
                                       u'"geography_list":[{"text":"Страна","name":"country",'
                                       u'"id":31},{"text":"Федеральный округ","name":"federal_district","id":32},'
                                       u'{"text":"Город","name":"city","id":35}],'
                                       u'"media_list":[{"text":"Уровень","name":"media_level","id":21},'
                                       u'{"text":"Тип","name":"media_type","id":22},'
                                       u'{"text":"Категория","name":"media_category","id":25},'
                                       u'{"text":"Вид","name":"media_view","id":23}],'
                                       u'"indicators_list":[{"text":"PRV","name":"prv","id":41},'
                                       u'{"text":"PRt","name":"prt","id":42},'
                                       u'{"text":"Тональность","name":"emotional_tone_object","id":44}],'
                                       u'"main_list":[{"text":"Источник","name":"source","id":1},'
                                       u'{"text":"Дата","name":"date","id":2},'
                                       u'{"text":"Время","name":"release_time","id":3},'
                                       u'{"text":"Полнотекст","name":"description","id":6},'
                                       u'{"text":"Аннотация","name":"annotation","id":7},'
                                       u'{"text":"Перепечатки","name":"?????","id":8},'
                                       u'{"text":"Url","name":"url","id":9},'
                                       u'{"text":"Media outreach","name":"mo","id":10},'
                                       u'{"text":"Заголовок","name":"title","id":4},'
                                       u'{"text":"Автор","name":"author","id":5}]}')

    class Meta:
        verbose_name = u'Настройки отображения продукта'
        verbose_name_plural = u'Настройки отображения продуктов'




    # def save(self, *args, **kwargs):
    #     workbook = xlrd.open_workbook('/var/www/info/widjet0_constructor1.xlsx')
    #     worksheet = workbook.sheet_by_index(0)
    #     num_rows = worksheet.nrows - 1
    #     # num_rows = 2
    #     curr_row = 0
    #
    #     list = []
    #     while curr_row < num_rows:
    #         option_dict = {}
    #
    #         list_values = []
    #         curr_row += 1
    #         row = worksheet.row(curr_row)
    #         # print row[0].value
    #         for el in range(0,10):
    #             # print row[el].value
    #             count = el + 1
    #             option_dict.update({'a%d'%count : row[el].value, })
    #         # print row[0].value , list_values
    #         list.append(option_dict)
    #         # print list
    #     # option_dict = sorted(option_dict, key=keys_list.__getitem__)
    #
    #     option_dict = json.dumps(list)
    #     print '!!!!!!!!!!!!!!!!!!!!!!!'
    #
    #     ConstructorWidget.objects.get_or_create(project = self.project, monitoring_product_id = 7, data = option_dict)
    #     super(MonitoringProduct, self).save(*args, **kwargs)

def create_constructor_widget0(sender, instance, created, **kwargs):
    # if 'created' in kwargs:
    #     print 'created'
    #     if kwargs['created']:
    try:
        constructor_widjet0 = ConstructorWidget.objects.get(id = instance.id)
    except ObjectDoesNotExist:
        constructor_widjet0 = False
    if not constructor_widjet0:

        workbook = xlrd.open_workbook(os.path.join(BASE_DIR, 'widjet0_constructor1.xlsx'))
        worksheet = workbook.sheet_by_index(0)
        # num_rows = worksheet.nrows - 1
        num_rows = worksheet.nrows - 1
        curr_row = 0

        list = []
        while curr_row < num_rows:
            option_dict = {}

            list_values = []
            curr_row += 1
            row = worksheet.row(curr_row)
            # print row[0].value
            for el in range(0,10):
                # print row[el].value
                count = el + 1
                option_dict.update({'a%d'%count : row[el].value, })
            option_dict.update({'a11' : {}, })
            # print row[0].value , list_values
            list.append(option_dict)
            # print list
        # option_dict = sorted(option_dict, key=keys_list.__getitem__)

        option_dict = json.dumps(list)
        print '!!!!!!!!!!!!!!!!!!!!!!!'

        ConstructorWidget.objects.get_or_create(project = instance.project, monitoring_product_id = instance.id, data = option_dict)
post_save.connect(create_constructor_widget0, sender=MonitoringProduct)



    # def constructor_widget0_link(self):
    #     return '/' + self.project.slug + '/monitoring_product/' + self.pk +  '/widget0/constructor/'

# class MonitoringProductSettings(models.Model):
#     monitoring_product = models.ForeignKey(MonitoringProduct, verbose_name=u'Мониторинг продукт',
#                                            related_name="monitoring_product_mentions")
#     configuration = models.TextField(verbose_name=u'Конфигурация')
#     user = models.ForeignKey(CustomUser, verbose_name=u'Пользователь')ыв


class MonitoringProductMention(models.Model):
    monitoring_product = models.ForeignKey(MonitoringProduct, verbose_name=u'Мониторинг продукт',
                                           related_name="monitoring_product_mentions")
    color = models.CharField(max_length=200, verbose_name=u'Цвет заливки')
    mention_object = models.ForeignKey(ReferredObject, verbose_name=u'Упоминаемый обьект', )

    class Meta:
        verbose_name = u'Упоминаемый обьект'
        verbose_name_plural = u'Упоминаемые обьекты'

    def get_keywords_list(self):
        return self.monitoring_product_mention_words.all()

    def __unicode__(self):
       return self.monitoring_product.title




class MonitoringProductMentionWord(models.Model):
    word = models.CharField(max_length=100, verbose_name=u'Слово')
    monitoring_product_mention = models.ForeignKey(MonitoringProductMention,
                                                   verbose_name=u'Упоминаемый обьект ',
                                                   related_name="monitoring_product_mention_words")

    class Meta:
        verbose_name = u'Слово'
        verbose_name_plural = u'Слова упоминаемые обьекты'

    def __unicode__(self):
        return self.word


class MonitoringProductCategory(models.Model):
    category = models.ForeignKey(Category, verbose_name=u'Категория')
    monitoring_product = models.ForeignKey(MonitoringProduct, verbose_name=u'Мониторинговий продукт',
                                           related_name="monitoring_product_category")
    index_sort = models.IntegerField(verbose_name=u'Индекс сортировки', default=1)


    class Meta:
        verbose_name = u'Категорию'
        verbose_name_plural = u'Категории (Мониторинговий продукт)'







TYPE_DATA = (
    ('1', u'числовой'),
    ('2', u'текстовый'),
    ('3', u'время'),
)
YES_NO = (
    ('1', u'Да'),
    ('2', u'НЕТ'),
)

INT_TYPE = (
    ('1',u'list'),
    ('2',u'шкала'),

)
class ConstructorWidget(models.Model):
    project = models.ForeignKey(Project, verbose_name=u'проект')
    monitoring_product = models.ForeignKey(MonitoringProduct, verbose_name=u'Мониторинговый продукт')
    data = models.TextField(verbose_name=u'dict')
    class Meta:
        verbose_name = u'Конструктор'
        verbose_name_plural = u'Конструктор Widget0'


