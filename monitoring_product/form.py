__author__ = 'stas'
from django import forms
from models import MonitoringProduct

class MonitoringProductForm(forms.ModelForm):
    class Meta:
        model = MonitoringProduct


class MonitoringProductFormStep2(forms.ModelForm):
    class Meta:
        model = MonitoringProduct
        exclude = ('project',)