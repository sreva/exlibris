# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('monitoring_product', '0005_auto_20150310_1823'),
    ]

    operations = [
        migrations.AlterField(
            model_name='monitoringproductsetting',
            name='setting',
            field=models.TextField(default='{"mention_list":[{"text":"\u041e\u0431\u044a\u0435\u043a\u0442\u044b","name":"title","id":51},{"text":"\u0424\u0418\u041e \u0441\u043f\u0438\u043a\u0435\u0440\u0430","name":"speakers","id":52}],"geography_list":[{"text":"\u0421\u0442\u0440\u0430\u043d\u0430","name":"country","id":31},{"text":"\u0424\u0435\u0434\u0435\u0440\u0430\u043b\u044c\u043d\u044b\u0439 \u043e\u043a\u0440\u0443\u0433","name":"federal_district","id":32},{"text":"\u0413\u043e\u0440\u043e\u0434","name":"city","id":35}],"media_list":[{"text":"\u0423\u0440\u043e\u0432\u0435\u043d\u044c","name":"media_level","id":21},{"text":"\u0422\u0438\u043f","name":"media_type","id":22},{"text":"\u041a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u044f","name":"media_category","id":25},{"text":"\u0412\u0438\u0434","name":"media_view","id":23}],"indicators_list":[{"text":"PRV","name":"prv","id":41},{"text":"PRt","name":"prt","id":42},{"text":"\u0422\u043e\u043d\u0430\u043b\u044c\u043d\u043e\u0441\u0442\u044c","name":"emotional_tone_object","id":44}],"main_list":[{"text":"\u0418\u0441\u0442\u043e\u0447\u043d\u0438\u043a","name":"source","id":1},{"text":"\u0414\u0430\u0442\u0430","name":"date","id":2},{"text":"\u0412\u0440\u0435\u043c\u044f","name":"release_time","id":3},{"text":"\u041f\u043e\u043b\u043d\u043e\u0442\u0435\u043a\u0441\u0442","name":"description","id":6},{"text":"\u0410\u043d\u043d\u043e\u0442\u0430\u0446\u0438\u044f","name":"annotation","id":7},{"text":"\u041f\u0435\u0440\u0435\u043f\u0435\u0447\u0430\u0442\u043a\u0438","name":"?????","id":8},{"text":"Url","name":"url","id":9},{"text":"Media outreach","name":"mo","id":10},{"text":"\u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a","name":"title","id":4},{"text":"\u0410\u0432\u0442\u043e\u0440","name":"author","id":5}]}', null=True, blank=True),
        ),
    ]
