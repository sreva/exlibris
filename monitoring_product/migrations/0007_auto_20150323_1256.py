# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('monitoring_product', '0006_auto_20150316_1216'),
    ]

    operations = [
        migrations.AlterField(
            model_name='monitoringproduct',
            name='type',
            field=models.CharField(max_length=20, verbose_name='\u0422\u0438\u043f \u043c\u043e\u043d\u0438\u0442\u043e\u0440\u0438\u043d\u0433\u0430', choices=[(b'day', '\u0435\u0436\u0435\u0434\u043d\u0435\u0432\u043d\u044b\u0439'), (b'week', '\u0435\u0436\u0435\u043d\u0435\u0434\u0435\u043b\u044c\u043d\u044b\u0439'), (b'month', '\u0435\u0436\u0435\u043c\u0435\u0441\u044f\u0447\u043d\u044b\u0439'), (b'date_release', '\u0414\u0430\u0442\u0430 \u0432\u044b\u0445\u043e\u0434\u0430'), (b'date_upload', '\u0414\u0430\u0442\u0430 \u0437\u0430\u0433\u0440\u0443\u0437\u043a\u0438')]),
        ),
    ]
