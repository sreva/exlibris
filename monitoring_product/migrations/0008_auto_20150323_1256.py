# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('monitoring_product', '0007_auto_20150323_1256'),
    ]

    operations = [
        migrations.AddField(
            model_name='monitoringproduct',
            name='time_from',
            field=models.DateTimeField(null=True, verbose_name='\u0412\u0440\u0435\u043c\u044f \u043e\u0442', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='monitoringproduct',
            name='time_to',
            field=models.DateTimeField(null=True, verbose_name='\u0412\u0440\u0435\u043c\u044f \u0434\u043e', blank=True),
            preserve_default=True,
        ),
    ]
