# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('monitoring_product', '0002_monitoringproductsetting'),
    ]

    operations = [
        migrations.AlterField(
            model_name='monitoringproductsetting',
            name='setting',
            field=models.TextField(null=True, blank=True),
        ),
    ]
