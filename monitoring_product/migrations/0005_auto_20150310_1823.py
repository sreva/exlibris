# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('monitoring_product', '0004_auto_20150310_1658'),
    ]

    operations = [
        migrations.AlterField(
            model_name='monitoringproduct',
            name='settings',
            field=models.TextField(null=True, blank=True),
        ),
    ]
