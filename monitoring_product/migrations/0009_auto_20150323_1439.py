# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import __builtin__
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('monitoring_product', '0008_auto_20150323_1256'),
    ]

    operations = [
        migrations.AlterField(
            model_name='monitoringproduct',
            name='settings',
            field=jsonfield.fields.JSONField(default=__builtin__.dict, verbose_name='\u041d\u0430\u0441\u0442\u0440\u043e\u0439\u043a\u0438', blank=True),
        ),
    ]
