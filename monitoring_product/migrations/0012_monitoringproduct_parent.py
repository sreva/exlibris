# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('monitoring_product', '0011_auto_20150402_0900'),
    ]

    operations = [
        migrations.AddField(
            model_name='monitoringproduct',
            name='parent',
            field=models.ForeignKey(related_name='subproducts', verbose_name='\u041f\u0440\u043e\u0435\u043a\u0442', blank=True, to='monitoring_product.MonitoringProduct', null=True),
            preserve_default=True,
        ),
    ]
