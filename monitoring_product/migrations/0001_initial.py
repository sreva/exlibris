# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='ConstructorWidget',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('data', models.TextField(verbose_name='dict')),
            ],
            options={
                'verbose_name': '\u041a\u043e\u043d\u0441\u0442\u0440\u0443\u043a\u0442\u043e\u0440',
                'verbose_name_plural': '\u041a\u043e\u043d\u0441\u0442\u0440\u0443\u043a\u0442\u043e\u0440 Widget0',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='MonitoringProduct',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=200, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
                ('type', models.CharField(max_length=20, verbose_name='\u0422\u0438\u043f \u043c\u043e\u043d\u0438\u0442\u043e\u0440\u0438\u043d\u0433\u0430', choices=[(b'day', '\u0435\u0436\u0435\u0434\u043d\u0435\u0432\u043d\u044b\u0439'), (b'week', '\u0435\u0436\u0435\u043d\u0435\u0434\u0435\u043b\u044c\u043d\u044b\u0439'), (b'month', '\u0435\u0436\u0435\u043c\u0435\u0441\u044f\u0447\u043d\u044b\u0439')])),
                ('data_type', models.CharField(default=b'1', max_length=20, verbose_name='\u041c\u043e\u043d\u0438\u0442\u043e\u0440\u0438\u043d\u0433', choices=[(b'1', '\u0442\u0440\u0430\u0434\u0438\u0446\u0438\u043e\u043d\u043d\u044b\u0445 \u0441\u043c\u0438'), (b'2', '\u0441\u043e\u0446\u0438\u0430\u043b\u044c\u043d\u044b\u0445 \u0441\u043c\u0438')])),
                ('article_count', models.IntegerField(default=1, verbose_name='\u0427\u0438\u0441\u043b\u043e \u0441\u0442\u0430\u0442\u0435\u0439 \u043d\u0430 \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u0435')),
                ('project', models.ForeignKey(related_name=b'monitoring_products', verbose_name='\u041f\u0440\u043e\u0435\u043a\u0442', to='main.Project')),
            ],
            options={
                'verbose_name': '\u041c\u043e\u043d\u0438\u0442\u043e\u0440\u0438\u043d\u0433\u043e\u0432\u0438\u0439 \u043f\u0440\u043e\u0434\u0443\u043a\u0442',
                'verbose_name_plural': '\u041c\u043e\u043d\u0438\u0442\u043e\u0440\u0438\u043d\u0433\u043e\u0432\u0438\u0435 \u043f\u0440\u043e\u0434\u0443\u043a\u0442\u044b',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='MonitoringProductCategory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('index_sort', models.IntegerField(default=1, verbose_name='\u0418\u043d\u0434\u0435\u043a\u0441 \u0441\u043e\u0440\u0442\u0438\u0440\u043e\u0432\u043a\u0438')),
                ('category', models.ForeignKey(verbose_name='\u041a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u044f', to='main.Category')),
                ('monitoring_product', models.ForeignKey(related_name=b'monitoring_product_category', verbose_name='\u041c\u043e\u043d\u0438\u0442\u043e\u0440\u0438\u043d\u0433\u043e\u0432\u0438\u0439 \u043f\u0440\u043e\u0434\u0443\u043a\u0442', to='monitoring_product.MonitoringProduct')),
            ],
            options={
                'verbose_name': '\u041a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u044e',
                'verbose_name_plural': '\u041a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u0438 (\u041c\u043e\u043d\u0438\u0442\u043e\u0440\u0438\u043d\u0433\u043e\u0432\u0438\u0439 \u043f\u0440\u043e\u0434\u0443\u043a\u0442)',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='MonitoringProductMention',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('color', models.CharField(max_length=200, verbose_name='\u0426\u0432\u0435\u0442 \u0437\u0430\u043b\u0438\u0432\u043a\u0438')),
                ('mention_object', models.ForeignKey(verbose_name='\u0423\u043f\u043e\u043c\u0438\u043d\u0430\u0435\u043c\u044b\u0439 \u043e\u0431\u044c\u0435\u043a\u0442', to='main.ReferredObject')),
                ('monitoring_product', models.ForeignKey(related_name=b'monitoring_product_mentions', verbose_name='\u041c\u043e\u043d\u0438\u0442\u043e\u0440\u0438\u043d\u0433 \u043f\u0440\u043e\u0434\u0443\u043a\u0442', to='monitoring_product.MonitoringProduct')),
            ],
            options={
                'verbose_name': '\u0423\u043f\u043e\u043c\u0438\u043d\u0430\u0435\u043c\u044b\u0439 \u043e\u0431\u044c\u0435\u043a\u0442',
                'verbose_name_plural': '\u0423\u043f\u043e\u043c\u0438\u043d\u0430\u0435\u043c\u044b\u0435 \u043e\u0431\u044c\u0435\u043a\u0442\u044b',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='MonitoringProductMentionWord',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('word', models.CharField(max_length=100, verbose_name='\u0421\u043b\u043e\u0432\u043e')),
                ('monitoring_product_mention', models.ForeignKey(related_name=b'monitoring_product_mention_words', verbose_name='\u0423\u043f\u043e\u043c\u0438\u043d\u0430\u0435\u043c\u044b\u0439 \u043e\u0431\u044c\u0435\u043a\u0442 ', to='monitoring_product.MonitoringProductMention')),
            ],
            options={
                'verbose_name': '\u0421\u043b\u043e\u0432\u043e',
                'verbose_name_plural': '\u0421\u043b\u043e\u0432\u0430 \u0443\u043f\u043e\u043c\u0438\u043d\u0430\u0435\u043c\u044b\u0435 \u043e\u0431\u044c\u0435\u043a\u0442\u044b',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='constructorwidget',
            name='monitoring_product',
            field=models.ForeignKey(verbose_name='\u041c\u043e\u043d\u0438\u0442\u043e\u0440\u0438\u043d\u0433\u043e\u0432\u044b\u0439 \u043f\u0440\u043e\u0434\u0443\u043a\u0442', to='monitoring_product.MonitoringProduct'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='constructorwidget',
            name='project',
            field=models.ForeignKey(verbose_name='\u043f\u0440\u043e\u0435\u043a\u0442', to='main.Project'),
            preserve_default=True,
        ),
    ]
