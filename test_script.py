# --*-- coding: utf-8 --*--

strings = ['30 brown bears', '30 brown foxes', '20 green turtles', '10 brown dogs']

# keep = ('brown', )
#
# ignore = ('x', )
#
# for text in strings:
#     if all([k in text for k in keep] + [i not in text for i in ignore]):
#         print text
#
#
#
#
#
# def search(self):
#     if not self.is_valid():
#         return self.no_query_found()
#
#     if not self.cleaned_data.get('q'):
#         return self.no_query_found()
#
#     sqs = self._parse_query(self.cleaned_data['q'])
#
#     if self.load_all:
#         sqs = sqs.load_all()
#
#     return sqs
#
# def _parse_query(self, query_content):
#     words = iter(query_content.split())
#     result_queryset = self.searchqueryset
#     for word in words:
#         try:
#             if word == 'AND':
#                 result_queryset = result_queryset.filter_and(
#                     content=words.next())
#             elif word == 'OR':
#                 result_queryset = result_queryset.filter_or(
#                     content=words.next())
#             elif word == 'NOT':
#                 result_queryset = result_queryset.exclude(
#                     content=words.next())
#             else:
#                 result_queryset = result_queryset.filter(
#                     content=word)
#         except StopIteration:
#             return result_queryset
#     return result_queryset
#
# def search(request):

import shlex, re
def foo(query):
    pieces = shlex.split(query)
    include, exclude = [], []
    for piece in pieces:
        if piece.startswith('-'):
            exclude.append(re.compile(piece[1:]))
        else:
            include.append(re.compile(piece))
    def validator(s):
        return (all(r.search(s) for r in include) and
                not any(r.search(s) for r in exclude))
    return validator

test = foo('без|пруда')
ss = test("труда не выловишь и рибку из пруда")
print ss