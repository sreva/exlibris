# --*-- coding: utf-8 --*--
from django.shortcuts import redirect, get_object_or_404, render_to_response, render
from openpyxl.writer.excel import save_virtual_workbook
import datetime
from info.settings import STATIC_URL, STATIC_ROOT, REPORT_ROOT
from main.form import LoginForm
from main.models import Project, FederalDistrict, MediaView, Category, Article, ReferredObject, MentionsObject, ImportFile
from xml.dom.minidom import parseString
from xlutils.copy import copy as xlcopy
from django.http import HttpResponse, HttpResponseRedirect, Http404
from main.models import Article
import xlrd
import xlsxwriter
import xlwt
from django.core.files.move import file_move_safe


def make_published(modeladmin, request, queryset):
    make_published.short_description = u"Импортировать выделиные статьи"
    print queryset
    #response = HttpResponse(mimetype="application/ms-excel")
    #response['Content-Disposition'] = 'attachment; filename=file.xls'
    #source_filename = STATIC_ROOT + "export_template.xls"
    #print source_filename
    date = datetime.datetime.now().strftime('%d_%m_%Y_%H_%M')

    workbook = xlsxwriter.Workbook('report' + date + '.xlsx', )
    ws = workbook.add_worksheet('info_sheet')
    #read_book = xlrd.open_workbook(source_filename, on_demand=True, formatting_info=True)
    #read_sheet = read_book.get_sheet(0)
    #
    #write_book = xlcopy(read_book)
    #
    #ws = write_book.get_sheet(0)
    fo = open(STATIC_ROOT + "all_title_list.txt", "r")
    line = fo.readlines()
    line_count = 0
    for elem in line:
        ws.write(0, line_count, elem.decode('utf-8'))
        line_count += 1
    fo.close()
    article_list = queryset

    def get_fk_value(item):
        try:
            result = item.title
        except:
            result = ''
        return result

    row = 1
    project = article_list[0].project
    km_list = project.km_set.all()
    km_count = 0
    for elem in km_list:
        ws.write(0, 265 + km_count, elem.title)
        km_count += 1
    for elem in article_list:

        ws.write(row, 0, row)
        date = elem.date.strftime('%m/%d/%Y')
        ws.write(row, 1, date)
        ws.write(row, 2, elem.day)
        ws.write(row, 3, elem.month)
        ws.write(row, 4, elem.week)
        ws.write(row, 5, get_fk_value(elem.media_level))
        ws.write(row, 6, elem.city)
        ws.write(row, 7, get_fk_value(elem.federal_district))
        ws.write(row, 8, get_fk_value(elem.region))
        ws.write(row, 9, elem.branch)
        ws.write(row, 10, get_fk_value(elem.country))
        ws.write(row, 11, elem.author)
        ws.write(row, 12, get_fk_value(elem.media_type))
        ws.write(row, 13, get_fk_value(elem.media_view))
        ws.write(row, 14, get_fk_value(elem.media_list))
        ws.write(row, 15, get_fk_value(elem.media_category))
        ws.write(row, 16, elem.source)
        ws.write(row, 17, elem.title)
        ws.write(row, 18, elem.annotation)
        ws.write(row, 19, elem.description)
        ws.write(row, 20, elem.url)
        ws.write(row, 21, elem.bissines_direction)
        ws.write(row, 22, elem.thematic_category)
        ws.write(row, 23, get_fk_value(elem.material_genre))
        ws.write(row, 24, elem.material_reason)
        ws.write(row, 25, get_fk_value(elem.emotional_tone_title))
        ws.write(row, 26, get_fk_value(elem.emotional_tone_material))
        ws.write(row, 196, elem.mo)
        ws.write(row, 209, get_fk_value(elem.interaction_kind))
        ws.write(row, 222, get_fk_value(elem.penetration))
        ws.write(row, 223, elem.id_press_release)
        if elem.km_result:
            km_result = u'Да'
        else:
            km_result = ''
        ws.write(row, 285, km_result)
        ws.write(row, 286, elem.tag)
        ws.write(row, 287, elem.duration)
        ws.write(row, 288, elem.release_time)
        ws.write(row, 289, elem.cover)
        ws.write(row, 290, get_fk_value(elem.visibility))
        ws.write(row, 291, elem.infographics)
        ws.write(row, 292, get_fk_value(elem.macroregion))
        ws.write(row, 293, get_fk_value(elem.media_list_special))
        ws.write(row, 294, get_fk_value(elem.media_type_special))
        ws.write(row, 295, get_fk_value(elem.media_category_special))
        ws.write(row, 296, get_fk_value(elem.visibility_special))
        ws.write(row, 297, get_fk_value(elem.image))
        ws.write(row, 298, get_fk_value(elem.km_special))
        ws.write(row, 299, get_fk_value(elem.brand_or_product))
        ws.write(row, 300, get_fk_value(elem.speaker_quote))
        ws.write(row, 301, get_fk_value(elem.third_patry_speakers))
        ws.write(row, 302, get_fk_value(elem.headder_tonality))
        ws.write(row, 303, get_fk_value(elem.main_tonality))
        ws.write(row, 304, get_fk_value(elem.global_local_event))
        ws.write(row, 309, get_fk_value(elem.edge_region))
        ws.write(row, 310, elem.id)
        #KM
        km_mention_list = elem.kmmention_set.all()
        for el in km_mention_list:
            try:
                key = el.key.kay

            except:
                key = ''
            ws.write(row, 264 + el.km.index_sort, key)
            #Product
        product_mention_list = elem.mentionproduct_set.all()
        for elem in product_mention_list:
            ws.write(row, 224 + elem.product.index_sort, get_fk_value(elem.mention_product_value))
            #Cattegory MPPT
        if elem.category != None:
            category_count = 0
            categorys_list = elem.category.get_ancestors()
            for it in categorys_list:
                ws.write(row, 305 + category_count, it.title)
                category_count += 1
        mention_objects = elem.mentionsobject_set.all()
        mention_objects_title = ";".join(get_fk_value(x.referred_object) for x in mention_objects)
        ws.write(row, 27, mention_objects_title)
        mention_object_count = 0
        speaker_count = 0
        side_speaker_count = 0
        #MentionObject
        for el in mention_objects:
            ws.write(row, 28 + mention_object_count, get_fk_value(el.emotional_tone_object))
            ws.write(row, 40 + mention_object_count, get_fk_value(el.emotional_tone_title))
            ws.write(row, 52 + mention_object_count, get_fk_value(el.object_role))
            ws.write(row, 64 + mention_object_count, el.info_reason)
            ws.write(row, 172 + mention_object_count, el.prt)
            ws.write(row, 184 + mention_object_count, el.prv)
            ws.write(row, 197 + mention_object_count, el.mq)
            ws.write(row, 210 + mention_object_count, get_fk_value(el.initiation))
            ws.write(row, 241 + mention_object_count, get_fk_value(el.advertisement_promo))
            technologies_list = el.technologies.all()
            technologies = ";".join(x.title for x in technologies_list)
            ws.write(row, 253 + mention_object_count, technologies)

            #Speaker
            speaker_list = el.speaker_set.all()
            speaker_fio = ";".join(x.fio for x in speaker_list)
            ws.write(row, 76 + speaker_count, speaker_fio)
            speaker_category = ";".join(get_fk_value(x.speaker_category) for x in speaker_list)
            ws.write(row, 77 + speaker_count, speaker_category)
            speaker_level = ";".join(get_fk_value(x.speaker_level) for x in speaker_list)
            ws.write(row, 78 + speaker_count, speaker_level)
            speaker_speech_hate = ";".join(get_fk_value(x.speaker_speech_hate) for x in speaker_list)
            ws.write(row, 79 + speaker_count, speaker_speech_hate)
            speaker_quote_uniqueness = ";".join(get_fk_value(x.speaker_quote_uniqueness) for x in speaker_list)
            ws.write(row, 80 + speaker_count, speaker_quote_uniqueness)
            speaker_count += 4
            #SideSpeaker
            side_speaker_list = elem.sidespeaker_set.all()
            side_speaker_fio_position = ";".join('%s, %s' % (x.fio, x.position) for x in side_speaker_list)
            ws.write(row, 136 + side_speaker_count, side_speaker_fio_position)
            side_speaker_tonality = ";".join(get_fk_value(x.side_speaker_tonality) for x in side_speaker_list)
            ws.write(row, 137 + side_speaker_count, side_speaker_tonality)
            side_speaker_status = ";".join(get_fk_value(x.side_speaker_status) for x in side_speaker_list)
            ws.write(row, 138 + side_speaker_count, side_speaker_status)
            side_speaker_count += 3

            mention_object_count += 1
        row += 1
        #file_name = "export%s%s"%(date, '.xlsx')



    workbook.close()
    file = workbook.filename
    file_move_safe(file, REPORT_ROOT + file)

    return redirect('/static_file/report/' + file)

    #response = HttpResponse(mimetype='application/vnd.ms-excel')
    ##
    #response['Content-Disposition'] = 'attachment; filename=%s'%workbook.filename
    ##workbook.save(response)
    #
    #
    #return response


def index(request, slug):
    try:

        project = get_object_or_404(Project, slug=slug)
    except Project.DoesNotExist:
        raise Http404
    auth = False
    aut = request.session.get(slug, False)
    if project.access == '2' or aut:
        auth = True

    if auth:
        return render(request, 'index.html', locals())
    else:
        return render('/' + slug + '/login/')


def login(request, slug):
    client = Project.objects.get(slug=slug)

    auth = request.session.get(slug, False)
    if auth or client.access == '2':
        return HttpResponseRedirect('/' + slug + '/')
    else:
        form = LoginForm()
        if request.POST:
            #client = Client.objects.get(slug=slug)
            form = LoginForm(request.POST)
            if form.is_valid():

                request.session[slug] = 'yes'
                return HttpResponseRedirect('/%s/' % slug)
            else:
                return render(request, 'login.html', locals())

    return render(request, 'login.html', locals())